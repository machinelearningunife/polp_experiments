# How to
The following is a tutorial on how to install the software and run the experiments for the paper ``Optimizing Probabilities in Probabilistic Logic Programs'' presented at [ICLP 2021](https://iclp2021.dcc.fc.up.pt/)
## Installation
In the future, all these steps will be automatized.
- Make sure you have installed `NLopt`: https://nlopt.readthedocs.io/en/latest/NLopt_Installation/
- Make sure you have installed `Python3` and `sympy`: https://docs.sympy.org/latest/install.html
- Make sure you have installed `SWI-Prolog` and updated: see https://www.swi-prolog.org/build/unix.html
- Make sure you have installed the `SWI-Prolog` packages `cplint` and `bddem` (that comes with `cplint`):
```
swipl
?- pack_install(cplint).
```
(github download is suggested)
- Now, go to the directories where `cplint` and `bddem` are stored (they should be in `/home/<user>/.local/share/swi-prolog/pack`). 
    - Replace `cplint/prolog/pita.pl` with the file `src/pita.pl` you find in this repository
    - Replace `bddem/prolog/bddem.pl` with the file `src/bddem.pl` you find in this repository
    - Replace `bddem/bddem.c` with the file `src/bddem.c` you find in this repository.
    - Copy the files `run.py` and `differentiate.pl`, that are in this repository under the folder `src`, in the `bddem` folder (the same of `bddem.c` you have replaced before)  
    - Go to line 31 of the file `bddem.c` and set variables `py_path` and `differentiate_path` respectively to `$pwd/run.py` and `$pwd/differentiate.pl` where `$pwd` is the directory where bddem is stored (should be something like `/home/<user>/.local/share/swi-prolog/pack/bddem`)
- Rebuild `bddem` with
``` 
swipl
?- pack_rebuild(bddem).
```
- Everything should be ready now

Test if everything works fine with:
```
cd src/
swipl -s minimize_test -g "test_constraint,halt"
```
you should obtain 
```
[p-0.60004560456,edge_b_d_-0.7352639408915171,edge_b_c_-0.6352639408915248]
```
If something goes wrong and the previous example prints some errors (such as `Syntax error: Operator expected` and `procedure prob_optimize(A,B,C,D) does not exist`), the files `pita.pl`, `bddem.pl` or `bddem.c` are not in the correct folder.
Maybe, there is some additional debugging output.

Note that in this example and in all the datasets, the probability (or the ranges of probabilities), came before the name of the probabilistic (or optimizable) fact. So, `0.1::a` is equivalent to `a:0.1` and `[0.2,0.3]::a` is equivalent to `a:[0.2,0.3]`.

## Dataset
All the dataset are into the `dataset/` folder. Each subfolder has the name of a dataset and contains three files:
- `<dataset>.pl`: file with the probabilistic optimizable logic program 
- `<dataset>_det.pl`: file with a deterministic version (where probabilistic and optimizable facts have been replaced with normal prolog facts)
- `run.sh`: bash file that call slurm manager to run the experiments (to run it in this way, you need to be on a cluster, or at least on a system that has slurm installed)

Complete graphs experiments are in the folder `dataset/complete_graph`.

## Paper and How to Cite
Cambridge University Press [link](https://www.cambridge.org/core/journals/theory-and-practice-of-logic-programming/article/abs/optimizing-probabilities-in-probabilistic-logic-programs/0998D7225CD947CC1EE1191421D5F20F)

ArXiv [link](https://arxiv.org/abs/2108.03095)

BibTeX entry:
```
@article{azzolini_riguzzi_2021, 
  title     = {Optimizing Probabilities in Probabilistic Logic Programs},
  author    = {Azzolini, Damiano and Riguzzi, Fabrizio},
  journal   = {Theory and Practice of Logic Programming}, 
  publisher = {Cambridge University Press},
  volume    = {21},
  year      = {2021}, 
  number    = {5}, 
  pages     = {543--556},
  doi       = {10.1017/S1471068421000260}
}
```

## Issues
The following program throws an error:
```
:- use_module(library(pita)).
:- pita.
:- begin_lpad.

compute_prob(Size,Cap,P):- Size =< Cap,P is (Cap - Size) / Cap.

path(X,X,_,_,_).
path(X,Y,Size,NMaxSteps,NSteps):-
	NSteps < NMaxSteps,
	connected(X,Z,Size),
	N1 is NSteps + 1,
	path(Z,Y,Size,NMaxSteps,N1).

connected(A,B,Size):P:- edge(A,B,C1), compute_prob(Size,C1,P).

edge(0,1,100000).
optimizable edge(1,2,1000000).
edge(0,2,10000).
optimizable edge(1,3,10000000).
edge(1,4,750000).

:- end_lpad.


query:- prob_optimize(path(0,1,11250,5,0),[edge(1,2,1000000) +edge(1,3,10000000) ],[path(0,1,11250,5,0) > 0.9],Assignments).
```