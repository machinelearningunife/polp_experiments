:- use_module(library(pita)).
:- pita.
:- begin_lpad.

0.9::edge(3, 216).
optimizable edge(203, 215).
0.9::edge(153, 215).
optimizable edge(6, 215).
0.9::edge(98, 168).
optimizable edge(62, 168).
0.9::edge(156, 214).
optimizable edge(111, 214).
0.9::edge(107, 214).
optimizable edge(90, 214).
0.9::edge(58, 214).
optimizable edge(50, 214).
0.9::edge(6, 214).
optimizable edge(212, 213).
0.9::edge(191, 212).
optimizable edge(114, 212).
0.9::edge(99, 212).
optimizable edge(60, 212).
0.9::edge(35, 212).
optimizable edge(6, 212).
0.9::edge(91, 211).
optimizable edge(42, 91).
0.9::edge(41, 42).
optimizable edge(18, 42).
0.9::edge(1, 42).
optimizable edge(209, 210).
0.9::edge(80, 210).
optimizable edge(6, 210).
0.9::edge(62, 209).
optimizable edge(59, 209).
0.9::edge(126, 208).
optimizable edge(120, 208).
0.9::edge(206, 207).
optimizable edge(89, 207).
0.9::edge(92, 197).
optimizable edge(74, 197).
0.9::edge(92, 206).
optimizable edge(92, 205).
0.9::edge(92, 204).
optimizable edge(85, 204).
0.9::edge(49, 204).
optimizable edge(5, 204).
0.9::edge(92, 203).
optimizable edge(126, 202).
0.9::edge(109, 201).
optimizable edge(109, 200).
0.9::edge(32, 199).
optimizable edge(16, 198).
0.9::edge(119, 197).
optimizable edge(75, 197).
0.9::edge(72, 197).
optimizable edge(119, 196).
0.9::edge(80, 195).
optimizable edge(80, 194).
0.9::edge(80, 193).
optimizable edge(191, 192).
0.9::edge(92, 192).
optimizable edge(89, 192).
0.9::edge(58, 192).
optimizable edge(130, 191).
0.9::edge(114, 191).
optimizable edge(60, 191).
0.9::edge(35, 191).
optimizable edge(34, 191).
0.9::edge(32, 191).
optimizable edge(3, 191).
0.9::edge(67, 190).
optimizable edge(67, 189).
0.9::edge(67, 188).
optimizable edge(67, 187).
0.9::edge(67, 186).
optimizable edge(61, 110).
0.9::edge(171, 185).
optimizable edge(170, 185).
0.9::edge(110, 185).
optimizable edge(109, 185).
0.9::edge(106, 185).
optimizable edge(49, 185).
0.9::edge(36, 185).
optimizable edge(171, 184).
0.9::edge(169, 184).
optimizable edge(155, 184).
0.9::edge(109, 184).
optimizable edge(89, 184).
0.9::edge(63, 184).
optimizable edge(46, 184).
0.9::edge(35, 184).
optimizable edge(34, 184).
0.9::edge(5, 184).
optimizable edge(1, 184).
0.9::edge(28, 183).
optimizable edge(148, 182).
0.9::edge(125, 182).
optimizable edge(113, 182).
0.9::edge(113, 181).
optimizable edge(135, 180).
0.9::edge(113, 180).
optimizable edge(176, 179).
0.9::edge(175, 176).
optimizable edge(107, 179).
0.9::edge(90, 179).
optimizable edge(6, 179).
0.9::edge(5, 179).
optimizable edge(128, 178).
0.9::edge(117, 178).
optimizable edge(114, 178).
0.9::edge(99, 178).
optimizable edge(88, 178).
0.9::edge(86, 178).
optimizable edge(60, 178).
0.9::edge(36, 178).
optimizable edge(6, 178).
0.9::edge(5, 178).
optimizable edge(3, 178).
0.9::edge(166, 177).
optimizable edge(140, 175).
0.9::edge(107, 175).
optimizable edge(47, 175).
0.9::edge(34, 175).
optimizable edge(15, 175).
0.9::edge(88, 174).
optimizable edge(88, 173).
0.9::edge(84, 172).
optimizable edge(170, 171).
0.9::edge(126, 170).
optimizable edge(110, 170).
0.9::edge(118, 169).
optimizable edge(110, 169).
0.9::edge(89, 169).
optimizable edge(62, 169).
0.9::edge(130, 167).
optimizable edge(117, 167).
0.9::edge(114, 167).
optimizable edge(111, 167).
0.9::edge(108, 167).
optimizable edge(107, 167).
0.9::edge(88, 167).
optimizable edge(84, 167).
0.9::edge(83, 167).
optimizable edge(60, 167).
0.9::edge(35, 167).
optimizable edge(32, 167).
0.9::edge(5, 167).
optimizable edge(156, 166).
0.9::edge(111, 166).
optimizable edge(98, 166).
0.9::edge(60, 166).
optimizable edge(50, 166).
0.9::edge(49, 166).
optimizable edge(36, 166).
0.9::edge(35, 166).
optimizable edge(34, 166).
0.9::edge(6, 166).
optimizable edge(5, 166).
0.9::edge(3, 166).
optimizable edge(155, 165).
0.9::edge(85, 165).
optimizable edge(63, 165).
0.9::edge(46, 165).
optimizable edge(18, 165).
0.9::edge(3, 165).
optimizable edge(163, 164).
0.9::edge(35, 164).
optimizable edge(140, 163).
0.9::edge(121, 163).
optimizable edge(88, 163).
0.9::edge(62, 163).
optimizable edge(4, 163).
0.9::edge(131, 162).
optimizable edge(160, 161).
0.9::edge(24, 160).
optimizable edge(158, 159).
0.9::edge(114, 159).
optimizable edge(99, 159).
0.9::edge(60, 159).
optimizable edge(36, 159).
0.9::edge(34, 159).
optimizable edge(6, 159).
0.9::edge(5, 159).
optimizable edge(3, 159).
0.9::edge(24, 158).
optimizable edge(99, 158).
0.9::edge(68, 158).
optimizable edge(62, 158).
0.9::edge(32, 158).
optimizable edge(3, 158).
0.9::edge(3, 157).
optimizable edge(24, 148).
0.9::edge(24, 91).
optimizable edge(24, 75).
0.9::edge(24, 66).
optimizable edge(24, 61).
0.9::edge(24, 54).
optimizable edge(24, 47).
0.9::edge(24, 31).
optimizable edge(6, 24).
0.9::edge(2, 24).
optimizable edge(1, 24).
0.9::edge(155, 156).
optimizable edge(150, 156).
0.9::edge(138, 155).
optimizable edge(106, 155).
0.9::edge(58, 155).
optimizable edge(48, 155).
0.9::edge(35, 155).
optimizable edge(18, 155).
0.9::edge(48, 154).
optimizable edge(109, 153).
0.9::edge(48, 153).
optimizable edge(48, 152).
0.9::edge(134, 151).
optimizable edge(63, 151).
0.9::edge(49, 151).
optimizable edge(48, 151).
0.9::edge(107, 150).
optimizable edge(98, 150).
0.9::edge(84, 150).
optimizable edge(64, 150).
0.9::edge(63, 150).
optimizable edge(32, 150).
0.9::edge(6, 150).
optimizable edge(5, 150).
0.9::edge(3, 150).
optimizable edge(63, 149).
0.9::edge(63, 148).
optimizable edge(18, 148).
0.9::edge(63, 147).
optimizable edge(61, 146).
0.9::edge(48, 145).
optimizable edge(3, 145).
0.9::edge(34, 144).
optimizable edge(63, 143).
0.9::edge(34, 143).
optimizable edge(21, 143).
0.9::edge(18, 143).
optimizable edge(81, 142).
0.9::edge(50, 142).
optimizable edge(3, 142).
0.9::edge(3, 141).
optimizable edge(62, 82).
0.9::edge(132, 140).
optimizable edge(104, 140).
0.9::edge(89, 140).
optimizable edge(6, 140).
0.9::edge(6, 139).
optimizable edge(3, 139).
0.9::edge(137, 138).
optimizable edge(110, 138).
0.9::edge(92, 138).
optimizable edge(3, 138).
0.9::edge(98, 137).
optimizable edge(92, 137).
0.9::edge(3, 137).
optimizable edge(3, 136).
0.9::edge(1, 135).
optimizable edge(3, 134).
0.9::edge(2, 134).
optimizable edge(1, 134).
0.9::edge(30, 133).
optimizable edge(107, 132).
0.9::edge(64, 132).
optimizable edge(6, 132).
0.9::edge(5, 132).
optimizable edge(3, 132).
0.9::edge(64, 131).
optimizable edge(63, 131).
0.9::edge(49, 131).
optimizable edge(48, 131).
0.9::edge(35, 131).
optimizable edge(34, 131).
0.9::edge(6, 131).
optimizable edge(5, 131).
0.9::edge(3, 131).
optimizable edge(6, 130).
0.9::edge(6, 129).
optimizable edge(6, 128).
0.9::edge(117, 127).
optimizable edge(6, 127).
0.9::edge(122, 126).
optimizable edge(6, 126).
0.9::edge(3, 126).
optimizable edge(72, 125).
0.9::edge(32, 125).
optimizable edge(6, 125).
0.9::edge(6, 124).
optimizable edge(6, 123).
0.9::edge(5, 123).
optimizable edge(62, 122).
0.9::edge(49, 122).
optimizable edge(5, 122).
0.9::edge(62, 121).
optimizable edge(4, 121).
0.9::edge(109, 120).
optimizable edge(30, 120).
0.9::edge(4, 120).
optimizable edge(2, 119).
0.9::edge(1, 119).
optimizable edge(85, 118).
0.9::edge(89, 117).
optimizable edge(59, 117).
0.9::edge(34, 117).
optimizable edge(6, 117).
0.9::edge(5, 117).
optimizable edge(2, 117).
0.9::edge(34, 116).
optimizable edge(3, 116).
0.9::edge(101, 115).
optimizable edge(5, 115).
0.9::edge(90, 114).
optimizable edge(88, 114).
0.9::edge(87, 114).
optimizable edge(35, 114).
0.9::edge(6, 114).
optimizable edge(23, 113).
0.9::edge(3, 113).
optimizable edge(1, 113).
0.9::edge(84, 112).
optimizable edge(62, 112).
0.9::edge(107, 111).
optimizable edge(101, 111).
0.9::edge(7, 111).
optimizable edge(98, 110).
0.9::edge(74, 110).
optimizable edge(61, 110).
0.9::edge(35, 110).
optimizable edge(103, 109).
0.9::edge(60, 109).
optimizable edge(36, 109).
0.9::edge(35, 109).
optimizable edge(6, 109).
0.9::edge(5, 109).
optimizable edge(107, 108).
0.9::edge(90, 108).
optimizable edge(60, 108).
0.9::edge(35, 108).
optimizable edge(5, 108).
0.9::edge(3, 108).
optimizable edge(1, 108).
0.9::edge(50, 107).
optimizable edge(63, 106).
0.9::edge(60, 106).
optimizable edge(49, 106).
0.9::edge(34, 106).
optimizable edge(5, 106).
0.9::edge(3, 106).
optimizable edge(1, 106).
0.9::edge(101, 105).
optimizable edge(18, 105).
0.9::edge(6, 105).
optimizable edge(1, 105).
0.9::edge(100, 104).
optimizable edge(35, 104).
0.9::edge(27, 104).
optimizable edge(18, 104).
0.9::edge(100, 103).
optimizable edge(100, 102).
0.9::edge(100, 101).
optimizable edge(6, 101).
0.9::edge(92, 100).
optimizable edge(86, 100).
0.9::edge(61, 100).
optimizable edge(4, 100).
0.9::edge(3, 100).
optimizable edge(88, 99).
0.9::edge(49, 99).
optimizable edge(6, 99).
0.9::edge(3, 99).
optimizable edge(88, 98).
0.9::edge(46, 98).
optimizable edge(5, 98).
0.9::edge(25, 97).
optimizable edge(25, 96).
0.9::edge(64, 95).
optimizable edge(60, 95).
0.9::edge(32, 95).
optimizable edge(18, 95).
0.9::edge(6, 95).
optimizable edge(3, 95).
0.9::edge(1, 94).
optimizable edge(63, 93).
0.9::edge(3, 93).
optimizable edge(85, 92).
0.9::edge(74, 92).
optimizable edge(49, 92).
0.9::edge(6, 92).
optimizable edge(5, 92).
0.9::edge(4, 92).
optimizable edge(3, 92).
0.9::edge(1, 92).
optimizable edge(18, 91).
0.9::edge(1, 91).
optimizable edge(46, 90).
0.9::edge(34, 90).
optimizable edge(3, 90).
0.9::edge(86, 89).
optimizable edge(34, 89).
0.9::edge(18, 89).
optimizable edge(6, 89).
0.9::edge(64, 88).
optimizable edge(60, 88).
0.9::edge(5, 88).
optimizable edge(85, 87).
0.9::edge(85, 86).
optimizable edge(80, 86).
0.9::edge(72, 86).
optimizable edge(59, 86).
0.9::edge(6, 86).
optimizable edge(49, 85).
0.9::edge(6, 85).
optimizable edge(3, 85).
0.9::edge(83, 84).
optimizable edge(62, 84).
0.9::edge(58, 84).
optimizable edge(49, 84).
0.9::edge(35, 84).
optimizable edge(32, 84).
0.9::edge(6, 84).
optimizable edge(3, 84).
0.9::edge(6, 83).
optimizable edge(69, 81).
0.9::edge(63, 81).
optimizable edge(49, 81).
0.9::edge(3, 80).
optimizable edge(12, 79).
0.9::edge(11, 79).
optimizable edge(3, 79).
0.9::edge(1, 78).
optimizable edge(68, 77).
0.9::edge(30, 77).
optimizable edge(74, 76).
0.9::edge(61, 76).
optimizable edge(5, 76).
0.9::edge(3, 76).
optimizable edge(6, 75).
0.9::edge(35, 74).
optimizable edge(34, 74).
0.9::edge(5, 74).
optimizable edge(1, 74).
0.9::edge(1, 73).
optimizable edge(59, 72).
0.9::edge(21, 72).
optimizable edge(4, 72).
0.9::edge(2, 72).
optimizable edge(1, 72).
0.9::edge(4, 71).
optimizable edge(4, 70).
0.9::edge(68, 69).
optimizable edge(55, 68).
0.9::edge(53, 68).
optimizable edge(35, 68).
0.9::edge(19, 68).
optimizable edge(16, 68).
0.9::edge(4, 68).
optimizable edge(59, 67).
0.9::edge(29, 67).
optimizable edge(16, 67).
0.9::edge(4, 67).
optimizable edge(2, 67).
0.9::edge(34, 65).
optimizable edge(3, 65).
0.9::edge(63, 64).
optimizable edge(61, 64).
0.9::edge(49, 64).
optimizable edge(18, 64).
0.9::edge(6, 64).
optimizable edge(50, 63).
0.9::edge(35, 63).
optimizable edge(18, 63).
0.9::edge(12, 63).
optimizable edge(5, 63).
0.9::edge(3, 63).
optimizable edge(2, 63).
0.9::edge(1, 63).
optimizable edge(61, 62).
0.9::edge(49, 62).
optimizable edge(46, 62).
0.9::edge(5, 62).
optimizable edge(59, 61).
0.9::edge(29, 61).
optimizable edge(11, 61).
0.9::edge(3, 61).
optimizable edge(1, 61).
0.9::edge(49, 60).
optimizable edge(46, 60).
0.9::edge(36, 60).
optimizable edge(35, 60).
0.9::edge(34, 60).
optimizable edge(6, 60).
0.9::edge(43, 59).
optimizable edge(30, 59).
0.9::edge(28, 59).
optimizable edge(5, 59).
0.9::edge(4, 59).
optimizable edge(49, 58).
0.9::edge(36, 58).
optimizable edge(35, 58).
0.9::edge(3, 58).
optimizable edge(33, 57).
0.9::edge(2, 57).
optimizable edge(1, 57).
0.9::edge(34, 56).
optimizable edge(30, 56).
0.9::edge(18, 56).
optimizable edge(3, 56).
0.9::edge(23, 55).
optimizable edge(4, 55).
0.9::edge(1, 55).
optimizable edge(47, 54).
0.9::edge(1, 54).
optimizable edge(1, 53).
0.9::edge(1, 52).
optimizable edge(3, 51).
0.9::edge(49, 50).
optimizable edge(35, 50).
0.9::edge(3, 50).
optimizable edge(48, 49).
0.9::edge(45, 49).
optimizable edge(35, 49).
0.9::edge(27, 49).
optimizable edge(6, 49).
0.9::edge(5, 49).
optimizable edge(3, 49).
0.9::edge(2, 49).
optimizable edge(47, 48).
0.9::edge(46, 48).
optimizable edge(33, 48).
0.9::edge(27, 48).
optimizable edge(12, 48).
0.9::edge(6, 48).
optimizable edge(4, 48).
0.9::edge(3, 48).
optimizable edge(1, 48).
0.9::edge(34, 47).
optimizable edge(10, 47).
0.9::edge(3, 47).
optimizable edge(32, 46).
0.9::edge(6, 46).
optimizable edge(1, 46).
0.9::edge(1, 45).
optimizable edge(35, 44).
0.9::edge(2, 44).
optimizable edge(1, 44).
0.9::edge(4, 43).
optimizable edge(1, 43).
0.9::edge(3, 41).
optimizable edge(30, 40).
0.9::edge(18, 39).
optimizable edge(1, 39).
0.9::edge(4, 38).
optimizable edge(1, 37).
0.9::edge(2, 36).
optimizable edge(30, 35).
0.9::edge(4, 35).
optimizable edge(3, 35).
0.9::edge(2, 35).
optimizable edge(1, 35).
0.9::edge(33, 34).
optimizable edge(4, 34).
0.9::edge(3, 34).
optimizable edge(1, 34).
0.9::edge(3, 33).
optimizable edge(2, 33).
0.9::edge(1, 33).
optimizable edge(21, 32).
0.9::edge(3, 32).
optimizable edge(2, 32).
0.9::edge(1, 32).
optimizable edge(3, 31).
0.9::edge(28, 30).
optimizable edge(9, 30).
0.9::edge(4, 30).
optimizable edge(3, 30).
0.9::edge(1, 30).
optimizable edge(25, 29).
0.9::edge(3, 29).
optimizable edge(2, 29).
0.9::edge(27, 28).
optimizable edge(1, 28).
0.9::edge(5, 27).
optimizable edge(3, 27).
0.9::edge(1, 27).
optimizable edge(25, 26).
0.9::edge(1, 26).
optimizable edge(2, 23).
0.9::edge(21, 22).
optimizable edge(2, 22).
0.9::edge(20, 21).
optimizable edge(16, 21).
0.9::edge(12, 21).
optimizable edge(2, 21).
0.9::edge(3, 20).
optimizable edge(2, 20).
0.9::edge(4, 19).
optimizable edge(2, 19).
0.9::edge(4, 18).
optimizable edge(1, 18).
0.9::edge(4, 17).
optimizable edge(4, 16).
0.9::edge(3, 16).
optimizable edge(13, 15).
0.9::edge(3, 15).
optimizable edge(3, 14).
0.9::edge(3, 13).
optimizable edge(3, 12).
0.9::edge(1, 11).
optimizable edge(4, 10).
0.9::edge(1, 10).
optimizable edge(1, 9).
0.9::edge(1, 8).
optimizable edge(1, 7).
0.9::edge(2, 6).
optimizable edge(1, 6).
0.9::edge(1, 5).
optimizable edge(2, 4).
0.9::edge(2, 3).
optimizable edge(1, 2).
0.9::edge(217, 218).
optimizable edge(123, 217).
0.9::edge(135, 217).
optimizable edge(114, 217).
0.9::edge(117, 217).
optimizable edge(60, 217).
0.9::edge(167, 217).
optimizable edge(1, 146).
0.9::edge(2, 146).
optimizable edge(16, 146).
0.9::edge(2, 28).
optimizable edge(6, 219).
0.9::edge(1, 219).
optimizable edge(30, 219).
0.9::edge(69, 219).

path(X, X).
path(X, Y):- path(X, Z), edge(Z, Y).



:- end_lpad.

run:- run_mma, run_ccsaq, run_slsqp.

run_mma(S,D):-
	statistics(runtime, [Start | _]), 
	prob_optimize(path(S,D),[edge(203,215) +edge(6,215) +edge(62,168) +edge(111,214) +edge(90,214) +edge(50,214) +edge(212,213) +edge(114,212) +edge(60,212) +edge(6,212) +edge(42,91) +edge(18,42) +edge(209,210) +edge(6,210) +edge(59,209) +edge(120,208) +edge(89,207) +edge(74,197) +edge(92,205) +edge(85,204) +edge(5,204) +edge(126,202) +edge(109,200) +edge(16,198) +edge(75,197) +edge(119,196) +edge(80,194) +edge(191,192) +edge(89,192) +edge(130,191) +edge(60,191) +edge(34,191) +edge(3,191) +edge(67,189) +edge(67,187) +edge(61,110) +edge(170,185) +edge(109,185) +edge(49,185) +edge(171,184) +edge(155,184) +edge(89,184) +edge(46,184) +edge(34,184) +edge(1,184) +edge(148,182) +edge(113,182) +edge(135,180) +edge(176,179) +edge(107,179) +edge(6,179) +edge(128,178) +edge(114,178) +edge(88,178) +edge(60,178) +edge(6,178) +edge(3,178) +edge(140,175) +edge(47,175) +edge(15,175) +edge(88,173) +edge(170,171) +edge(110,170) +edge(110,169) +edge(62,169) +edge(117,167) +edge(111,167) +edge(107,167) +edge(84,167) +edge(60,167) +edge(32,167) +edge(156,166) +edge(98,166) +edge(50,166) +edge(36,166) +edge(34,166) +edge(5,166) +edge(155,165) +edge(63,165) +edge(18,165) +edge(163,164) +edge(140,163) +edge(88,163) +edge(4,163) +edge(160,161) +edge(158,159) +edge(99,159) +edge(36,159) +edge(6,159) +edge(3,159) +edge(99,158) +edge(62,158) +edge(3,158) +edge(24,148) +edge(24,75) +edge(24,61) +edge(24,47) +edge(6,24) +edge(1,24) +edge(150,156) +edge(106,155) +edge(48,155) +edge(18,155) +edge(109,153) +edge(48,152) +edge(63,151) +edge(48,151) +edge(98,150) +edge(64,150) +edge(32,150) +edge(5,150) +edge(63,149) +edge(18,148) +edge(61,146) +edge(3,145) +edge(63,143) +edge(21,143) +edge(81,142) +edge(3,142) +edge(62,82) +edge(104,140) +edge(6,140) +edge(3,139) +edge(110,138) +edge(3,138) +edge(92,137) +edge(3,136) +edge(3,134) +edge(1,134) +edge(107,132) +edge(6,132) +edge(3,132) +edge(63,131) +edge(48,131) +edge(34,131) +edge(5,131) +edge(6,130) +edge(6,128) +edge(6,127) +edge(6,126) +edge(72,125) +edge(6,125) +edge(6,123) +edge(62,122) +edge(5,122) +edge(4,121) +edge(30,120) +edge(2,119) +edge(85,118) +edge(59,117) +edge(6,117) +edge(2,117) +edge(3,116) +edge(5,115) +edge(88,114) +edge(35,114) +edge(23,113) +edge(1,113) +edge(62,112) +edge(101,111) +edge(98,110) +edge(61,110) +edge(103,109) +edge(36,109) +edge(6,109) +edge(107,108) +edge(60,108) +edge(5,108) +edge(1,108) +edge(63,106) +edge(49,106) +edge(5,106) +edge(1,106) +edge(18,105) +edge(1,105) +edge(35,104) +edge(18,104) +edge(100,102) +edge(6,101) +edge(86,100) +edge(4,100) +edge(88,99) +edge(6,99) +edge(88,98) +edge(5,98) +edge(25,96) +edge(60,95) +edge(18,95) +edge(3,95) +edge(63,93) +edge(85,92) +edge(49,92) +edge(5,92) +edge(3,92) +edge(18,91) +edge(46,90) +edge(3,90) +edge(34,89) +edge(6,89) +edge(60,88) +edge(85,87) +edge(80,86) +edge(59,86) +edge(49,85) +edge(3,85) +edge(62,84) +edge(49,84) +edge(32,84) +edge(3,84) +edge(69,81) +edge(49,81) +edge(12,79) +edge(3,79) +edge(68,77) +edge(74,76) +edge(5,76) +edge(6,75) +edge(34,74) +edge(1,74) +edge(59,72) +edge(4,72) +edge(1,72) +edge(4,70) +edge(55,68) +edge(35,68) +edge(16,68) +edge(59,67) +edge(16,67) +edge(2,67) +edge(3,65) +edge(61,64) +edge(18,64) +edge(50,63) +edge(18,63) +edge(5,63) +edge(2,63) +edge(61,62) +edge(46,62) +edge(59,61) +edge(11,61) +edge(1,61) +edge(46,60) +edge(35,60) +edge(6,60) +edge(30,59) +edge(5,59) +edge(49,58) +edge(35,58) +edge(33,57) +edge(1,57) +edge(30,56) +edge(3,56) +edge(4,55) +edge(47,54) +edge(1,53) +edge(3,51) +edge(35,50) +edge(48,49) +edge(35,49) +edge(6,49) +edge(3,49) +edge(47,48) +edge(33,48) +edge(12,48) +edge(4,48) +edge(1,48) +edge(10,47) +edge(32,46) +edge(1,46) +edge(35,44) +edge(1,44) +edge(1,43) +edge(30,40) +edge(1,39) +edge(1,37) +edge(30,35) +edge(3,35) +edge(1,35) +edge(4,34) +edge(1,34) +edge(2,33) +edge(21,32) +edge(2,32) +edge(3,31) +edge(9,30) +edge(3,30) +edge(25,29) +edge(2,29) +edge(1,28) +edge(3,27) +edge(25,26) +edge(2,23) +edge(2,22) +edge(16,21) +edge(2,21) +edge(2,20) +edge(2,19) +edge(1,18) +edge(4,16) +edge(13,15) +edge(3,14) +edge(3,12) +edge(4,10) +edge(1,9) +edge(1,7) +edge(1,6) +edge(2,4) +edge(1,2) +edge(123,217) +edge(114,217) +edge(60,217) +edge(1,146) +edge(16,146) +edge(6,219) +edge(30,219) ],[path(S,D) > 0.8],mma,-1,A),
	statistics(runtime, [Stop | _]),
	Runtime is Stop - Start,
	writeln('mma'),
	writeln(Runtime),
	writeln(A).

run_ccsaq(S,D):-
	statistics(runtime, [Start | _]), 
	prob_optimize(path(S,D),[edge(203,215) +edge(6,215) +edge(62,168) +edge(111,214) +edge(90,214) +edge(50,214) +edge(212,213) +edge(114,212) +edge(60,212) +edge(6,212) +edge(42,91) +edge(18,42) +edge(209,210) +edge(6,210) +edge(59,209) +edge(120,208) +edge(89,207) +edge(74,197) +edge(92,205) +edge(85,204) +edge(5,204) +edge(126,202) +edge(109,200) +edge(16,198) +edge(75,197) +edge(119,196) +edge(80,194) +edge(191,192) +edge(89,192) +edge(130,191) +edge(60,191) +edge(34,191) +edge(3,191) +edge(67,189) +edge(67,187) +edge(61,110) +edge(170,185) +edge(109,185) +edge(49,185) +edge(171,184) +edge(155,184) +edge(89,184) +edge(46,184) +edge(34,184) +edge(1,184) +edge(148,182) +edge(113,182) +edge(135,180) +edge(176,179) +edge(107,179) +edge(6,179) +edge(128,178) +edge(114,178) +edge(88,178) +edge(60,178) +edge(6,178) +edge(3,178) +edge(140,175) +edge(47,175) +edge(15,175) +edge(88,173) +edge(170,171) +edge(110,170) +edge(110,169) +edge(62,169) +edge(117,167) +edge(111,167) +edge(107,167) +edge(84,167) +edge(60,167) +edge(32,167) +edge(156,166) +edge(98,166) +edge(50,166) +edge(36,166) +edge(34,166) +edge(5,166) +edge(155,165) +edge(63,165) +edge(18,165) +edge(163,164) +edge(140,163) +edge(88,163) +edge(4,163) +edge(160,161) +edge(158,159) +edge(99,159) +edge(36,159) +edge(6,159) +edge(3,159) +edge(99,158) +edge(62,158) +edge(3,158) +edge(24,148) +edge(24,75) +edge(24,61) +edge(24,47) +edge(6,24) +edge(1,24) +edge(150,156) +edge(106,155) +edge(48,155) +edge(18,155) +edge(109,153) +edge(48,152) +edge(63,151) +edge(48,151) +edge(98,150) +edge(64,150) +edge(32,150) +edge(5,150) +edge(63,149) +edge(18,148) +edge(61,146) +edge(3,145) +edge(63,143) +edge(21,143) +edge(81,142) +edge(3,142) +edge(62,82) +edge(104,140) +edge(6,140) +edge(3,139) +edge(110,138) +edge(3,138) +edge(92,137) +edge(3,136) +edge(3,134) +edge(1,134) +edge(107,132) +edge(6,132) +edge(3,132) +edge(63,131) +edge(48,131) +edge(34,131) +edge(5,131) +edge(6,130) +edge(6,128) +edge(6,127) +edge(6,126) +edge(72,125) +edge(6,125) +edge(6,123) +edge(62,122) +edge(5,122) +edge(4,121) +edge(30,120) +edge(2,119) +edge(85,118) +edge(59,117) +edge(6,117) +edge(2,117) +edge(3,116) +edge(5,115) +edge(88,114) +edge(35,114) +edge(23,113) +edge(1,113) +edge(62,112) +edge(101,111) +edge(98,110) +edge(61,110) +edge(103,109) +edge(36,109) +edge(6,109) +edge(107,108) +edge(60,108) +edge(5,108) +edge(1,108) +edge(63,106) +edge(49,106) +edge(5,106) +edge(1,106) +edge(18,105) +edge(1,105) +edge(35,104) +edge(18,104) +edge(100,102) +edge(6,101) +edge(86,100) +edge(4,100) +edge(88,99) +edge(6,99) +edge(88,98) +edge(5,98) +edge(25,96) +edge(60,95) +edge(18,95) +edge(3,95) +edge(63,93) +edge(85,92) +edge(49,92) +edge(5,92) +edge(3,92) +edge(18,91) +edge(46,90) +edge(3,90) +edge(34,89) +edge(6,89) +edge(60,88) +edge(85,87) +edge(80,86) +edge(59,86) +edge(49,85) +edge(3,85) +edge(62,84) +edge(49,84) +edge(32,84) +edge(3,84) +edge(69,81) +edge(49,81) +edge(12,79) +edge(3,79) +edge(68,77) +edge(74,76) +edge(5,76) +edge(6,75) +edge(34,74) +edge(1,74) +edge(59,72) +edge(4,72) +edge(1,72) +edge(4,70) +edge(55,68) +edge(35,68) +edge(16,68) +edge(59,67) +edge(16,67) +edge(2,67) +edge(3,65) +edge(61,64) +edge(18,64) +edge(50,63) +edge(18,63) +edge(5,63) +edge(2,63) +edge(61,62) +edge(46,62) +edge(59,61) +edge(11,61) +edge(1,61) +edge(46,60) +edge(35,60) +edge(6,60) +edge(30,59) +edge(5,59) +edge(49,58) +edge(35,58) +edge(33,57) +edge(1,57) +edge(30,56) +edge(3,56) +edge(4,55) +edge(47,54) +edge(1,53) +edge(3,51) +edge(35,50) +edge(48,49) +edge(35,49) +edge(6,49) +edge(3,49) +edge(47,48) +edge(33,48) +edge(12,48) +edge(4,48) +edge(1,48) +edge(10,47) +edge(32,46) +edge(1,46) +edge(35,44) +edge(1,44) +edge(1,43) +edge(30,40) +edge(1,39) +edge(1,37) +edge(30,35) +edge(3,35) +edge(1,35) +edge(4,34) +edge(1,34) +edge(2,33) +edge(21,32) +edge(2,32) +edge(3,31) +edge(9,30) +edge(3,30) +edge(25,29) +edge(2,29) +edge(1,28) +edge(3,27) +edge(25,26) +edge(2,23) +edge(2,22) +edge(16,21) +edge(2,21) +edge(2,20) +edge(2,19) +edge(1,18) +edge(4,16) +edge(13,15) +edge(3,14) +edge(3,12) +edge(4,10) +edge(1,9) +edge(1,7) +edge(1,6) +edge(2,4) +edge(1,2) +edge(123,217) +edge(114,217) +edge(60,217) +edge(1,146) +edge(16,146) +edge(6,219) +edge(30,219) ],[path(S,D) > 0.8],ccsaq,-1,A),
	statistics(runtime, [Stop | _]),
	Runtime is Stop - Start,
	writeln('ccsaq'),
	writeln(Runtime),
	writeln(A).

run_slsqp(S,D):-
	statistics(runtime, [Start | _]), 
	prob_optimize(path(S,D),[edge(203,215) +edge(6,215) +edge(62,168) +edge(111,214) +edge(90,214) +edge(50,214) +edge(212,213) +edge(114,212) +edge(60,212) +edge(6,212) +edge(42,91) +edge(18,42) +edge(209,210) +edge(6,210) +edge(59,209) +edge(120,208) +edge(89,207) +edge(74,197) +edge(92,205) +edge(85,204) +edge(5,204) +edge(126,202) +edge(109,200) +edge(16,198) +edge(75,197) +edge(119,196) +edge(80,194) +edge(191,192) +edge(89,192) +edge(130,191) +edge(60,191) +edge(34,191) +edge(3,191) +edge(67,189) +edge(67,187) +edge(61,110) +edge(170,185) +edge(109,185) +edge(49,185) +edge(171,184) +edge(155,184) +edge(89,184) +edge(46,184) +edge(34,184) +edge(1,184) +edge(148,182) +edge(113,182) +edge(135,180) +edge(176,179) +edge(107,179) +edge(6,179) +edge(128,178) +edge(114,178) +edge(88,178) +edge(60,178) +edge(6,178) +edge(3,178) +edge(140,175) +edge(47,175) +edge(15,175) +edge(88,173) +edge(170,171) +edge(110,170) +edge(110,169) +edge(62,169) +edge(117,167) +edge(111,167) +edge(107,167) +edge(84,167) +edge(60,167) +edge(32,167) +edge(156,166) +edge(98,166) +edge(50,166) +edge(36,166) +edge(34,166) +edge(5,166) +edge(155,165) +edge(63,165) +edge(18,165) +edge(163,164) +edge(140,163) +edge(88,163) +edge(4,163) +edge(160,161) +edge(158,159) +edge(99,159) +edge(36,159) +edge(6,159) +edge(3,159) +edge(99,158) +edge(62,158) +edge(3,158) +edge(24,148) +edge(24,75) +edge(24,61) +edge(24,47) +edge(6,24) +edge(1,24) +edge(150,156) +edge(106,155) +edge(48,155) +edge(18,155) +edge(109,153) +edge(48,152) +edge(63,151) +edge(48,151) +edge(98,150) +edge(64,150) +edge(32,150) +edge(5,150) +edge(63,149) +edge(18,148) +edge(61,146) +edge(3,145) +edge(63,143) +edge(21,143) +edge(81,142) +edge(3,142) +edge(62,82) +edge(104,140) +edge(6,140) +edge(3,139) +edge(110,138) +edge(3,138) +edge(92,137) +edge(3,136) +edge(3,134) +edge(1,134) +edge(107,132) +edge(6,132) +edge(3,132) +edge(63,131) +edge(48,131) +edge(34,131) +edge(5,131) +edge(6,130) +edge(6,128) +edge(6,127) +edge(6,126) +edge(72,125) +edge(6,125) +edge(6,123) +edge(62,122) +edge(5,122) +edge(4,121) +edge(30,120) +edge(2,119) +edge(85,118) +edge(59,117) +edge(6,117) +edge(2,117) +edge(3,116) +edge(5,115) +edge(88,114) +edge(35,114) +edge(23,113) +edge(1,113) +edge(62,112) +edge(101,111) +edge(98,110) +edge(61,110) +edge(103,109) +edge(36,109) +edge(6,109) +edge(107,108) +edge(60,108) +edge(5,108) +edge(1,108) +edge(63,106) +edge(49,106) +edge(5,106) +edge(1,106) +edge(18,105) +edge(1,105) +edge(35,104) +edge(18,104) +edge(100,102) +edge(6,101) +edge(86,100) +edge(4,100) +edge(88,99) +edge(6,99) +edge(88,98) +edge(5,98) +edge(25,96) +edge(60,95) +edge(18,95) +edge(3,95) +edge(63,93) +edge(85,92) +edge(49,92) +edge(5,92) +edge(3,92) +edge(18,91) +edge(46,90) +edge(3,90) +edge(34,89) +edge(6,89) +edge(60,88) +edge(85,87) +edge(80,86) +edge(59,86) +edge(49,85) +edge(3,85) +edge(62,84) +edge(49,84) +edge(32,84) +edge(3,84) +edge(69,81) +edge(49,81) +edge(12,79) +edge(3,79) +edge(68,77) +edge(74,76) +edge(5,76) +edge(6,75) +edge(34,74) +edge(1,74) +edge(59,72) +edge(4,72) +edge(1,72) +edge(4,70) +edge(55,68) +edge(35,68) +edge(16,68) +edge(59,67) +edge(16,67) +edge(2,67) +edge(3,65) +edge(61,64) +edge(18,64) +edge(50,63) +edge(18,63) +edge(5,63) +edge(2,63) +edge(61,62) +edge(46,62) +edge(59,61) +edge(11,61) +edge(1,61) +edge(46,60) +edge(35,60) +edge(6,60) +edge(30,59) +edge(5,59) +edge(49,58) +edge(35,58) +edge(33,57) +edge(1,57) +edge(30,56) +edge(3,56) +edge(4,55) +edge(47,54) +edge(1,53) +edge(3,51) +edge(35,50) +edge(48,49) +edge(35,49) +edge(6,49) +edge(3,49) +edge(47,48) +edge(33,48) +edge(12,48) +edge(4,48) +edge(1,48) +edge(10,47) +edge(32,46) +edge(1,46) +edge(35,44) +edge(1,44) +edge(1,43) +edge(30,40) +edge(1,39) +edge(1,37) +edge(30,35) +edge(3,35) +edge(1,35) +edge(4,34) +edge(1,34) +edge(2,33) +edge(21,32) +edge(2,32) +edge(3,31) +edge(9,30) +edge(3,30) +edge(25,29) +edge(2,29) +edge(1,28) +edge(3,27) +edge(25,26) +edge(2,23) +edge(2,22) +edge(16,21) +edge(2,21) +edge(2,20) +edge(2,19) +edge(1,18) +edge(4,16) +edge(13,15) +edge(3,14) +edge(3,12) +edge(4,10) +edge(1,9) +edge(1,7) +edge(1,6) +edge(2,4) +edge(1,2) +edge(123,217) +edge(114,217) +edge(60,217) +edge(1,146) +edge(16,146) +edge(6,219) +edge(30,219) ],[path(S,D) > 0.8],slsqp,-1,A),
	statistics(runtime, [Stop | _]),
	Runtime is Stop - Start,
	writeln('slsqp'),
	writeln(Runtime),
	writeln(A).
