sbatch --job-name=internet-industry-partnerships_53_159 --partition=longrun --mem=8G --output=internet-industry-partnerships_53_159_ccsaq.log --wrap='srun swipl -s internet-industry-partnerships.pl -g "run_ccsaq(53, 159), halt." '
sbatch --job-name=internet-industry-partnerships_53_159 --partition=longrun --mem=8G --output=internet-industry-partnerships_53_159_mma.log --wrap='srun swipl -s internet-industry-partnerships.pl -g "run_mma(53, 159), halt." '
sbatch --job-name=internet-industry-partnerships_53_159 --partition=longrun --mem=8G --output=internet-industry-partnerships_53_159_slsqp.log --wrap='srun swipl -s internet-industry-partnerships.pl -g "run_slsqp(53, 159), halt." '

sbatch --job-name=internet-industry-partnerships_64_156 --partition=longrun --mem=8G --output=internet-industry-partnerships_64_156_ccsaq.log --wrap='srun swipl -s internet-industry-partnerships.pl -g "run_ccsaq(64, 156), halt." '
sbatch --job-name=internet-industry-partnerships_64_156 --partition=longrun --mem=8G --output=internet-industry-partnerships_64_156_mma.log --wrap='srun swipl -s internet-industry-partnerships.pl -g "run_mma(64, 156), halt." '
sbatch --job-name=internet-industry-partnerships_64_156 --partition=longrun --mem=8G --output=internet-industry-partnerships_64_156_slsqp.log --wrap='srun swipl -s internet-industry-partnerships.pl -g "run_slsqp(64, 156), halt." '

sbatch --job-name=internet-industry-partnerships_25_173 --partition=longrun --mem=8G --output=internet-industry-partnerships_25_173_ccsaq.log --wrap='srun swipl -s internet-industry-partnerships.pl -g "run_ccsaq(25, 173), halt." '
sbatch --job-name=internet-industry-partnerships_25_173 --partition=longrun --mem=8G --output=internet-industry-partnerships_25_173_mma.log --wrap='srun swipl -s internet-industry-partnerships.pl -g "run_mma(25, 173), halt." '
sbatch --job-name=internet-industry-partnerships_25_173 --partition=longrun --mem=8G --output=internet-industry-partnerships_25_173_slsqp.log --wrap='srun swipl -s internet-industry-partnerships.pl -g "run_slsqp(25, 173), halt." '

sbatch --job-name=internet-industry-partnerships_50_213 --partition=longrun --mem=8G --output=internet-industry-partnerships_50_213_ccsaq.log --wrap='srun swipl -s internet-industry-partnerships.pl -g "run_ccsaq(50, 213), halt." '
sbatch --job-name=internet-industry-partnerships_50_213 --partition=longrun --mem=8G --output=internet-industry-partnerships_50_213_mma.log --wrap='srun swipl -s internet-industry-partnerships.pl -g "run_mma(50, 213), halt." '
sbatch --job-name=internet-industry-partnerships_50_213 --partition=longrun --mem=8G --output=internet-industry-partnerships_50_213_slsqp.log --wrap='srun swipl -s internet-industry-partnerships.pl -g "run_slsqp(50, 213), halt." '

sbatch --job-name=internet-industry-partnerships_45_197 --partition=longrun --mem=8G --output=internet-industry-partnerships_45_197_ccsaq.log --wrap='srun swipl -s internet-industry-partnerships.pl -g "run_ccsaq(45, 197), halt." '
sbatch --job-name=internet-industry-partnerships_45_197 --partition=longrun --mem=8G --output=internet-industry-partnerships_45_197_mma.log --wrap='srun swipl -s internet-industry-partnerships.pl -g "run_mma(45, 197), halt." '
sbatch --job-name=internet-industry-partnerships_45_197 --partition=longrun --mem=8G --output=internet-industry-partnerships_45_197_slsqp.log --wrap='srun swipl -s internet-industry-partnerships.pl -g "run_slsqp(45, 197), halt." '

sbatch --job-name=internet-industry-partnerships_80_218 --partition=longrun --mem=8G --output=internet-industry-partnerships_80_218_ccsaq.log --wrap='srun swipl -s internet-industry-partnerships.pl -g "run_ccsaq(80, 218), halt." '
sbatch --job-name=internet-industry-partnerships_80_218 --partition=longrun --mem=8G --output=internet-industry-partnerships_80_218_mma.log --wrap='srun swipl -s internet-industry-partnerships.pl -g "run_mma(80, 218), halt." '
sbatch --job-name=internet-industry-partnerships_80_218 --partition=longrun --mem=8G --output=internet-industry-partnerships_80_218_slsqp.log --wrap='srun swipl -s internet-industry-partnerships.pl -g "run_slsqp(80, 218), halt." '

sbatch --job-name=internet-industry-partnerships_92_167 --partition=longrun --mem=8G --output=internet-industry-partnerships_92_167_ccsaq.log --wrap='srun swipl -s internet-industry-partnerships.pl -g "run_ccsaq(92, 167), halt." '
sbatch --job-name=internet-industry-partnerships_92_167 --partition=longrun --mem=8G --output=internet-industry-partnerships_92_167_mma.log --wrap='srun swipl -s internet-industry-partnerships.pl -g "run_mma(92, 167), halt." '
sbatch --job-name=internet-industry-partnerships_92_167 --partition=longrun --mem=8G --output=internet-industry-partnerships_92_167_slsqp.log --wrap='srun swipl -s internet-industry-partnerships.pl -g "run_slsqp(92, 167), halt." '

sbatch --job-name=internet-industry-partnerships_80_184 --partition=longrun --mem=8G --output=internet-industry-partnerships_80_184_ccsaq.log --wrap='srun swipl -s internet-industry-partnerships.pl -g "run_ccsaq(80, 184), halt." '
sbatch --job-name=internet-industry-partnerships_80_184 --partition=longrun --mem=8G --output=internet-industry-partnerships_80_184_mma.log --wrap='srun swipl -s internet-industry-partnerships.pl -g "run_mma(80, 184), halt." '
sbatch --job-name=internet-industry-partnerships_80_184 --partition=longrun --mem=8G --output=internet-industry-partnerships_80_184_slsqp.log --wrap='srun swipl -s internet-industry-partnerships.pl -g "run_slsqp(80, 184), halt." '

sbatch --job-name=internet-industry-partnerships_155_166 --partition=longrun --mem=8G --output=internet-industry-partnerships_155_166_ccsaq.log --wrap='srun swipl -s internet-industry-partnerships.pl -g "run_ccsaq(155, 166), halt." '
sbatch --job-name=internet-industry-partnerships_155_166 --partition=longrun --mem=8G --output=internet-industry-partnerships_155_166_mma.log --wrap='srun swipl -s internet-industry-partnerships.pl -g "run_mma(155, 166), halt." '
sbatch --job-name=internet-industry-partnerships_155_166 --partition=longrun --mem=8G --output=internet-industry-partnerships_155_166_slsqp.log --wrap='srun swipl -s internet-industry-partnerships.pl -g "run_slsqp(155, 166), halt." '

sbatch --job-name=internet-industry-partnerships_61_84 --partition=longrun --mem=8G --output=internet-industry-partnerships_61_84_ccsaq.log --wrap='srun swipl -s internet-industry-partnerships.pl -g "run_ccsaq(61, 84), halt." '
sbatch --job-name=internet-industry-partnerships_61_84 --partition=longrun --mem=8G --output=internet-industry-partnerships_61_84_mma.log --wrap='srun swipl -s internet-industry-partnerships.pl -g "run_mma(61, 84), halt." '
sbatch --job-name=internet-industry-partnerships_61_84 --partition=longrun --mem=8G --output=internet-industry-partnerships_61_84_slsqp.log --wrap='srun swipl -s internet-industry-partnerships.pl -g "run_slsqp(61, 84), halt." '