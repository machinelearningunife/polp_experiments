sbatch --job-name=reptilia-tortoise-network-fi-2008_389_396 --partition=longrun --mem=8G --output=reptilia-tortoise-network-fi-2008_389_396_ccsaq.log --wrap='srun swipl -s reptilia-tortoise-network-fi-2008.pl -g "run_ccsaq(389, 396), halt." '
sbatch --job-name=reptilia-tortoise-network-fi-2008_389_396 --partition=longrun --mem=8G --output=reptilia-tortoise-network-fi-2008_389_396_mma.log --wrap='srun swipl -s reptilia-tortoise-network-fi-2008.pl -g "run_mma(389, 396), halt." '
sbatch --job-name=reptilia-tortoise-network-fi-2008_389_396 --partition=longrun --mem=8G --output=reptilia-tortoise-network-fi-2008_389_396_slsqp.log --wrap='srun swipl -s reptilia-tortoise-network-fi-2008.pl -g "run_slsqp(389, 396), halt." '

sbatch --job-name=reptilia-tortoise-network-fi-2008_290_289 --partition=longrun --mem=8G --output=reptilia-tortoise-network-fi-2008_290_289_ccsaq.log --wrap='srun swipl -s reptilia-tortoise-network-fi-2008.pl -g "run_ccsaq(290, 289), halt." '
sbatch --job-name=reptilia-tortoise-network-fi-2008_290_289 --partition=longrun --mem=8G --output=reptilia-tortoise-network-fi-2008_290_289_mma.log --wrap='srun swipl -s reptilia-tortoise-network-fi-2008.pl -g "run_mma(290, 289), halt." '
sbatch --job-name=reptilia-tortoise-network-fi-2008_290_289 --partition=longrun --mem=8G --output=reptilia-tortoise-network-fi-2008_290_289_slsqp.log --wrap='srun swipl -s reptilia-tortoise-network-fi-2008.pl -g "run_slsqp(290, 289), halt." '

sbatch --job-name=reptilia-tortoise-network-fi-2008_267_287 --partition=longrun --mem=8G --output=reptilia-tortoise-network-fi-2008_267_287_ccsaq.log --wrap='srun swipl -s reptilia-tortoise-network-fi-2008.pl -g "run_ccsaq(267, 287), halt." '
sbatch --job-name=reptilia-tortoise-network-fi-2008_267_287 --partition=longrun --mem=8G --output=reptilia-tortoise-network-fi-2008_267_287_mma.log --wrap='srun swipl -s reptilia-tortoise-network-fi-2008.pl -g "run_mma(267, 287), halt." '
sbatch --job-name=reptilia-tortoise-network-fi-2008_267_287 --partition=longrun --mem=8G --output=reptilia-tortoise-network-fi-2008_267_287_slsqp.log --wrap='srun swipl -s reptilia-tortoise-network-fi-2008.pl -g "run_slsqp(267, 287), halt." '

sbatch --job-name=reptilia-tortoise-network-fi-2008_226_325 --partition=longrun --mem=8G --output=reptilia-tortoise-network-fi-2008_226_325_ccsaq.log --wrap='srun swipl -s reptilia-tortoise-network-fi-2008.pl -g "run_ccsaq(226, 325), halt." '
sbatch --job-name=reptilia-tortoise-network-fi-2008_226_325 --partition=longrun --mem=8G --output=reptilia-tortoise-network-fi-2008_226_325_mma.log --wrap='srun swipl -s reptilia-tortoise-network-fi-2008.pl -g "run_mma(226, 325), halt." '
sbatch --job-name=reptilia-tortoise-network-fi-2008_226_325 --partition=longrun --mem=8G --output=reptilia-tortoise-network-fi-2008_226_325_slsqp.log --wrap='srun swipl -s reptilia-tortoise-network-fi-2008.pl -g "run_slsqp(226, 325), halt." '

sbatch --job-name=reptilia-tortoise-network-fi-2008_384_230 --partition=longrun --mem=8G --output=reptilia-tortoise-network-fi-2008_384_230_ccsaq.log --wrap='srun swipl -s reptilia-tortoise-network-fi-2008.pl -g "run_ccsaq(384, 230), halt." '
sbatch --job-name=reptilia-tortoise-network-fi-2008_384_230 --partition=longrun --mem=8G --output=reptilia-tortoise-network-fi-2008_384_230_mma.log --wrap='srun swipl -s reptilia-tortoise-network-fi-2008.pl -g "run_mma(384, 230), halt." '
sbatch --job-name=reptilia-tortoise-network-fi-2008_384_230 --partition=longrun --mem=8G --output=reptilia-tortoise-network-fi-2008_384_230_slsqp.log --wrap='srun swipl -s reptilia-tortoise-network-fi-2008.pl -g "run_slsqp(384, 230), halt." '

sbatch --job-name=reptilia-tortoise-network-fi-2008_356_230 --partition=longrun --mem=8G --output=reptilia-tortoise-network-fi-2008_356_230_ccsaq.log --wrap='srun swipl -s reptilia-tortoise-network-fi-2008.pl -g "run_ccsaq(356, 230), halt." '
sbatch --job-name=reptilia-tortoise-network-fi-2008_356_230 --partition=longrun --mem=8G --output=reptilia-tortoise-network-fi-2008_356_230_mma.log --wrap='srun swipl -s reptilia-tortoise-network-fi-2008.pl -g "run_mma(356, 230), halt." '
sbatch --job-name=reptilia-tortoise-network-fi-2008_356_230 --partition=longrun --mem=8G --output=reptilia-tortoise-network-fi-2008_356_230_slsqp.log --wrap='srun swipl -s reptilia-tortoise-network-fi-2008.pl -g "run_slsqp(356, 230), halt." '

sbatch --job-name=reptilia-tortoise-network-fi-2008_379_245 --partition=longrun --mem=8G --output=reptilia-tortoise-network-fi-2008_379_245_ccsaq.log --wrap='srun swipl -s reptilia-tortoise-network-fi-2008.pl -g "run_ccsaq(379, 245), halt." '
sbatch --job-name=reptilia-tortoise-network-fi-2008_379_245 --partition=longrun --mem=8G --output=reptilia-tortoise-network-fi-2008_379_245_mma.log --wrap='srun swipl -s reptilia-tortoise-network-fi-2008.pl -g "run_mma(379, 245), halt." '
sbatch --job-name=reptilia-tortoise-network-fi-2008_379_245 --partition=longrun --mem=8G --output=reptilia-tortoise-network-fi-2008_379_245_slsqp.log --wrap='srun swipl -s reptilia-tortoise-network-fi-2008.pl -g "run_slsqp(379, 245), halt." '

sbatch --job-name=reptilia-tortoise-network-fi-2008_343_393 --partition=longrun --mem=8G --output=reptilia-tortoise-network-fi-2008_343_393_ccsaq.log --wrap='srun swipl -s reptilia-tortoise-network-fi-2008.pl -g "run_ccsaq(343, 393), halt." '
sbatch --job-name=reptilia-tortoise-network-fi-2008_343_393 --partition=longrun --mem=8G --output=reptilia-tortoise-network-fi-2008_343_393_mma.log --wrap='srun swipl -s reptilia-tortoise-network-fi-2008.pl -g "run_mma(343, 393), halt." '
sbatch --job-name=reptilia-tortoise-network-fi-2008_343_393 --partition=longrun --mem=8G --output=reptilia-tortoise-network-fi-2008_343_393_slsqp.log --wrap='srun swipl -s reptilia-tortoise-network-fi-2008.pl -g "run_slsqp(343, 393), halt." '

sbatch --job-name=reptilia-tortoise-network-fi-2008_379_325 --partition=longrun --mem=8G --output=reptilia-tortoise-network-fi-2008_379_325_ccsaq.log --wrap='srun swipl -s reptilia-tortoise-network-fi-2008.pl -g "run_ccsaq(379, 325), halt." '
sbatch --job-name=reptilia-tortoise-network-fi-2008_379_325 --partition=longrun --mem=8G --output=reptilia-tortoise-network-fi-2008_379_325_mma.log --wrap='srun swipl -s reptilia-tortoise-network-fi-2008.pl -g "run_mma(379, 325), halt." '
sbatch --job-name=reptilia-tortoise-network-fi-2008_379_325 --partition=longrun --mem=8G --output=reptilia-tortoise-network-fi-2008_379_325_slsqp.log --wrap='srun swipl -s reptilia-tortoise-network-fi-2008.pl -g "run_slsqp(379, 325), halt." '

sbatch --job-name=reptilia-tortoise-network-fi-2008_244_325 --partition=longrun --mem=8G --output=reptilia-tortoise-network-fi-2008_244_325_ccsaq.log --wrap='srun swipl -s reptilia-tortoise-network-fi-2008.pl -g "run_ccsaq(244, 325), halt." '
sbatch --job-name=reptilia-tortoise-network-fi-2008_244_325 --partition=longrun --mem=8G --output=reptilia-tortoise-network-fi-2008_244_325_mma.log --wrap='srun swipl -s reptilia-tortoise-network-fi-2008.pl -g "run_mma(244, 325), halt." '
sbatch --job-name=reptilia-tortoise-network-fi-2008_244_325 --partition=longrun --mem=8G --output=reptilia-tortoise-network-fi-2008_244_325_slsqp.log --wrap='srun swipl -s reptilia-tortoise-network-fi-2008.pl -g "run_slsqp(244, 325), halt." '