sbatch --job-name=webkb-wisc_105_249 --partition=longrun --mem=8G --output=webkb-wisc_105_249_ccsaq.log --wrap='srun swipl -s webkb-wisc.pl -g "run_ccsaq(105, 249), halt." '
sbatch --job-name=webkb-wisc_105_249 --partition=longrun --mem=8G --output=webkb-wisc_105_249_mma.log --wrap='srun swipl -s webkb-wisc.pl -g "run_mma(105, 249), halt." '
sbatch --job-name=webkb-wisc_105_249 --partition=longrun --mem=8G --output=webkb-wisc_105_249_slsqp.log --wrap='srun swipl -s webkb-wisc.pl -g "run_slsqp(105, 249), halt." '

sbatch --job-name=webkb-wisc_203_144 --partition=longrun --mem=8G --output=webkb-wisc_203_144_ccsaq.log --wrap='srun swipl -s webkb-wisc.pl -g "run_ccsaq(203, 144), halt." '
sbatch --job-name=webkb-wisc_203_144 --partition=longrun --mem=8G --output=webkb-wisc_203_144_mma.log --wrap='srun swipl -s webkb-wisc.pl -g "run_mma(203, 144), halt." '
sbatch --job-name=webkb-wisc_203_144 --partition=longrun --mem=8G --output=webkb-wisc_203_144_slsqp.log --wrap='srun swipl -s webkb-wisc.pl -g "run_slsqp(203, 144), halt." '

sbatch --job-name=webkb-wisc_28_50 --partition=longrun --mem=8G --output=webkb-wisc_28_50_ccsaq.log --wrap='srun swipl -s webkb-wisc.pl -g "run_ccsaq(28, 50), halt." '
sbatch --job-name=webkb-wisc_28_50 --partition=longrun --mem=8G --output=webkb-wisc_28_50_mma.log --wrap='srun swipl -s webkb-wisc.pl -g "run_mma(28, 50), halt." '
sbatch --job-name=webkb-wisc_28_50 --partition=longrun --mem=8G --output=webkb-wisc_28_50_slsqp.log --wrap='srun swipl -s webkb-wisc.pl -g "run_slsqp(28, 50), halt." '

sbatch --job-name=webkb-wisc_84_211 --partition=longrun --mem=8G --output=webkb-wisc_84_211_ccsaq.log --wrap='srun swipl -s webkb-wisc.pl -g "run_ccsaq(84, 211), halt." '
sbatch --job-name=webkb-wisc_84_211 --partition=longrun --mem=8G --output=webkb-wisc_84_211_mma.log --wrap='srun swipl -s webkb-wisc.pl -g "run_mma(84, 211), halt." '
sbatch --job-name=webkb-wisc_84_211 --partition=longrun --mem=8G --output=webkb-wisc_84_211_slsqp.log --wrap='srun swipl -s webkb-wisc.pl -g "run_slsqp(84, 211), halt." '

sbatch --job-name=webkb-wisc_230_73 --partition=longrun --mem=8G --output=webkb-wisc_230_73_ccsaq.log --wrap='srun swipl -s webkb-wisc.pl -g "run_ccsaq(230, 73), halt." '
sbatch --job-name=webkb-wisc_230_73 --partition=longrun --mem=8G --output=webkb-wisc_230_73_mma.log --wrap='srun swipl -s webkb-wisc.pl -g "run_mma(230, 73), halt." '
sbatch --job-name=webkb-wisc_230_73 --partition=longrun --mem=8G --output=webkb-wisc_230_73_slsqp.log --wrap='srun swipl -s webkb-wisc.pl -g "run_slsqp(230, 73), halt." '

sbatch --job-name=webkb-wisc_4_147 --partition=longrun --mem=8G --output=webkb-wisc_4_147_ccsaq.log --wrap='srun swipl -s webkb-wisc.pl -g "run_ccsaq(4, 147), halt." '
sbatch --job-name=webkb-wisc_4_147 --partition=longrun --mem=8G --output=webkb-wisc_4_147_mma.log --wrap='srun swipl -s webkb-wisc.pl -g "run_mma(4, 147), halt." '
sbatch --job-name=webkb-wisc_4_147 --partition=longrun --mem=8G --output=webkb-wisc_4_147_slsqp.log --wrap='srun swipl -s webkb-wisc.pl -g "run_slsqp(4, 147), halt." '

sbatch --job-name=webkb-wisc_6_130 --partition=longrun --mem=8G --output=webkb-wisc_6_130_ccsaq.log --wrap='srun swipl -s webkb-wisc.pl -g "run_ccsaq(6, 130), halt." '
sbatch --job-name=webkb-wisc_6_130 --partition=longrun --mem=8G --output=webkb-wisc_6_130_mma.log --wrap='srun swipl -s webkb-wisc.pl -g "run_mma(6, 130), halt." '
sbatch --job-name=webkb-wisc_6_130 --partition=longrun --mem=8G --output=webkb-wisc_6_130_slsqp.log --wrap='srun swipl -s webkb-wisc.pl -g "run_slsqp(6, 130), halt." '

sbatch --job-name=webkb-wisc_6_64 --partition=longrun --mem=8G --output=webkb-wisc_6_64_ccsaq.log --wrap='srun swipl -s webkb-wisc.pl -g "run_ccsaq(6, 64), halt." '
sbatch --job-name=webkb-wisc_6_64 --partition=longrun --mem=8G --output=webkb-wisc_6_64_mma.log --wrap='srun swipl -s webkb-wisc.pl -g "run_mma(6, 64), halt." '
sbatch --job-name=webkb-wisc_6_64 --partition=longrun --mem=8G --output=webkb-wisc_6_64_slsqp.log --wrap='srun swipl -s webkb-wisc.pl -g "run_slsqp(6, 64), halt." '

sbatch --job-name=webkb-wisc_14_243 --partition=longrun --mem=8G --output=webkb-wisc_14_243_ccsaq.log --wrap='srun swipl -s webkb-wisc.pl -g "run_ccsaq(14, 243), halt." '
sbatch --job-name=webkb-wisc_14_243 --partition=longrun --mem=8G --output=webkb-wisc_14_243_mma.log --wrap='srun swipl -s webkb-wisc.pl -g "run_mma(14, 243), halt." '
sbatch --job-name=webkb-wisc_14_243 --partition=longrun --mem=8G --output=webkb-wisc_14_243_slsqp.log --wrap='srun swipl -s webkb-wisc.pl -g "run_slsqp(14, 243), halt." '

sbatch --job-name=webkb-wisc_7_144 --partition=longrun --mem=8G --output=webkb-wisc_7_144_ccsaq.log --wrap='srun swipl -s webkb-wisc.pl -g "run_ccsaq(7, 144), halt." '
sbatch --job-name=webkb-wisc_7_144 --partition=longrun --mem=8G --output=webkb-wisc_7_144_mma.log --wrap='srun swipl -s webkb-wisc.pl -g "run_mma(7, 144), halt." '
sbatch --job-name=webkb-wisc_7_144 --partition=longrun --mem=8G --output=webkb-wisc_7_144_slsqp.log --wrap='srun swipl -s webkb-wisc.pl -g "run_slsqp(7, 144), halt." '