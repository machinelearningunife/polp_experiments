:- use_module(library(pita)).
:- pita.
:- begin_lpad.

0.9::edge(2, 1).
optimizable edge(4, 1).
0.9::edge(264, 1).
optimizable edge(4, 2).
0.9::edge(9, 7).
optimizable edge(12, 9).
0.9::edge(44, 9).
optimizable edge(4, 11).
0.9::edge(4, 12).
optimizable edge(9, 12).
0.9::edge(10, 12).
optimizable edge(4, 13).
0.9::edge(4, 15).
optimizable edge(219, 15).
0.9::edge(4, 16).
optimizable edge(71, 16).
0.9::edge(165, 16).
optimizable edge(233, 16).
0.9::edge(4, 17).
optimizable edge(19, 18).
0.9::edge(18, 19).
optimizable edge(19, 20).
0.9::edge(146, 20).
optimizable edge(20, 21).
0.9::edge(4, 22).
optimizable edge(4, 23).
0.9::edge(4, 24).
optimizable edge(48, 24).
0.9::edge(4, 26).
optimizable edge(4, 28).
0.9::edge(4, 29).
optimizable edge(30, 29).
0.9::edge(31, 29).
optimizable edge(47, 29).
0.9::edge(54, 29).
optimizable edge(87, 29).
0.9::edge(219, 29).
optimizable edge(4, 30).
0.9::edge(29, 30).
optimizable edge(47, 30).
0.9::edge(4, 31).
optimizable edge(29, 31).
0.9::edge(54, 31).
optimizable edge(7, 32).
0.9::edge(27, 33).
optimizable edge(4, 34).
0.9::edge(5, 34).
optimizable edge(48, 34).
0.9::edge(70, 34).
optimizable edge(4, 35).
0.9::edge(10, 35).
optimizable edge(12, 35).
0.9::edge(83, 35).
optimizable edge(4, 36).
0.9::edge(27, 36).
optimizable edge(50, 36).
0.9::edge(67, 36).
optimizable edge(70, 36).
0.9::edge(4, 37).
optimizable edge(14, 37).
0.9::edge(77, 37).
optimizable edge(93, 37).
0.9::edge(165, 37).
optimizable edge(225, 37).
0.9::edge(249, 37).
optimizable edge(4, 38).
0.9::edge(66, 38).
optimizable edge(262, 38).
0.9::edge(40, 39).
optimizable edge(54, 40).
0.9::edge(4, 41).
optimizable edge(41, 41).
0.9::edge(42, 41).
optimizable edge(4, 43).
0.9::edge(54, 43).
optimizable edge(4, 45).
0.9::edge(57, 45).
optimizable edge(140, 46).
0.9::edge(52, 47).
optimizable edge(84, 47).
0.9::edge(118, 47).
optimizable edge(200, 47).
0.9::edge(239, 47).
optimizable edge(4, 48).
0.9::edge(13, 48).
optimizable edge(24, 48).
0.9::edge(28, 48).
optimizable edge(34, 48).
0.9::edge(103, 48).
optimizable edge(124, 48).
0.9::edge(133, 48).
optimizable edge(197, 48).
0.9::edge(235, 48).
optimizable edge(244, 48).
0.9::edge(4, 49).
optimizable edge(13, 49).
0.9::edge(24, 49).
optimizable edge(28, 49).
0.9::edge(34, 49).
optimizable edge(103, 49).
0.9::edge(124, 49).
optimizable edge(133, 49).
0.9::edge(197, 49).
optimizable edge(235, 49).
0.9::edge(244, 49).
optimizable edge(36, 50).
0.9::edge(48, 50).
optimizable edge(121, 50).
0.9::edge(127, 50).
optimizable edge(130, 50).
0.9::edge(141, 50).
optimizable edge(144, 50).
0.9::edge(168, 50).
optimizable edge(203, 50).
0.9::edge(237, 50).
optimizable edge(36, 51).
0.9::edge(48, 51).
optimizable edge(121, 51).
0.9::edge(127, 51).
optimizable edge(130, 51).
0.9::edge(141, 51).
optimizable edge(144, 51).
0.9::edge(168, 51).
optimizable edge(203, 51).
0.9::edge(237, 51).
optimizable edge(53, 52).
0.9::edge(54, 53).
optimizable edge(53, 54).
0.9::edge(4, 55).
optimizable edge(4, 56).
0.9::edge(45, 57).
optimizable edge(57, 57).
0.9::edge(254, 57).
optimizable edge(257, 57).
0.9::edge(45, 58).
optimizable edge(58, 58).
0.9::edge(140, 58).
optimizable edge(17, 59).
0.9::edge(4, 60).
optimizable edge(175, 61).
0.9::edge(154, 62).
optimizable edge(227, 62).
0.9::edge(190, 63).
optimizable edge(191, 64).
0.9::edge(64, 65).
optimizable edge(170, 65).
0.9::edge(25, 66).
optimizable edge(143, 67).
0.9::edge(68, 68).
optimizable edge(68, 69).
0.9::edge(82, 70).
optimizable edge(140, 71).
0.9::edge(148, 73).
optimizable edge(4, 74).
0.9::edge(10, 74).
optimizable edge(4, 75).
0.9::edge(18, 76).
optimizable edge(19, 76).
0.9::edge(20, 76).
optimizable edge(21, 76).
0.9::edge(93, 76).
optimizable edge(230, 76).
0.9::edge(243, 76).
optimizable edge(256, 76).
0.9::edge(102, 77).
optimizable edge(256, 77).
0.9::edge(4, 78).
optimizable edge(80, 78).
0.9::edge(4, 79).
optimizable edge(80, 79).
0.9::edge(4, 80).
optimizable edge(78, 80).
0.9::edge(79, 80).
optimizable edge(81, 80).
0.9::edge(108, 82).
optimizable edge(132, 82).
0.9::edge(185, 82).
optimizable edge(191, 82).
0.9::edge(255, 82).
optimizable edge(10, 83).
0.9::edge(12, 83).
optimizable edge(216, 85).
0.9::edge(222, 85).
optimizable edge(222, 86).
0.9::edge(4, 87).
optimizable edge(29, 87).
0.9::edge(85, 87).
optimizable edge(86, 87).
0.9::edge(161, 87).
optimizable edge(213, 87).
0.9::edge(215, 87).
optimizable edge(54, 88).
0.9::edge(89, 88).
optimizable edge(54, 89).
0.9::edge(88, 89).
optimizable edge(4, 90).
0.9::edge(11, 90).
optimizable edge(4, 92).
0.9::edge(165, 92).
optimizable edge(4, 93).
0.9::edge(14, 93).
optimizable edge(37, 93).
0.9::edge(76, 93).
optimizable edge(77, 93).
0.9::edge(140, 93).
optimizable edge(210, 93).
0.9::edge(230, 93).
optimizable edge(249, 93).
0.9::edge(256, 93).
optimizable edge(95, 94).
0.9::edge(96, 94).
optimizable edge(97, 95).
0.9::edge(97, 96).
optimizable edge(8, 97).
0.9::edge(94, 97).
optimizable edge(96, 97).
0.9::edge(4, 98).
optimizable edge(7, 101).
0.9::edge(82, 101).
optimizable edge(14, 102).
0.9::edge(77, 102).
optimizable edge(105, 102).
0.9::edge(135, 102).
optimizable edge(188, 104).
0.9::edge(102, 105).
optimizable edge(54, 106).
0.9::edge(107, 106).
optimizable edge(4, 107).
0.9::edge(54, 107).
optimizable edge(72, 107).
0.9::edge(106, 107).
optimizable edge(107, 107).
0.9::edge(217, 107).
optimizable edge(262, 107).
0.9::edge(4, 108).
optimizable edge(82, 108).
0.9::edge(191, 108).
optimizable edge(4, 109).
0.9::edge(140, 109).
optimizable edge(4, 110).
0.9::edge(64, 110).
optimizable edge(146, 110).
0.9::edge(4, 111).
optimizable edge(72, 111).
0.9::edge(54, 112).
optimizable edge(113, 112).
0.9::edge(4, 113).
optimizable edge(64, 113).
0.9::edge(72, 113).
optimizable edge(112, 113).
0.9::edge(2, 114).
optimizable edge(4, 114).
0.9::edge(4, 115).
optimizable edge(231, 115).
0.9::edge(4, 117).
optimizable edge(116, 117).
0.9::edge(119, 118).
optimizable edge(118, 119).
0.9::edge(4, 120).
optimizable edge(4, 121).
0.9::edge(50, 121).
optimizable edge(5, 122).
0.9::edge(123, 123).
optimizable edge(4, 125).
0.9::edge(10, 125).
optimizable edge(12, 125).
0.9::edge(4, 126).
optimizable edge(10, 126).
0.9::edge(4, 128).
optimizable edge(128, 128).
0.9::edge(191, 128).
optimizable edge(4, 130).
0.9::edge(64, 130).
optimizable edge(99, 130).
0.9::edge(129, 130).
optimizable edge(202, 130).
0.9::edge(4, 131).
optimizable edge(82, 132).
0.9::edge(189, 132).
optimizable edge(48, 133).
0.9::edge(4, 134).
optimizable edge(178, 134).
0.9::edge(83, 136).
optimizable edge(4, 137).
0.9::edge(139, 138).
optimizable edge(138, 139).
0.9::edge(4, 140).
optimizable edge(140, 140).
0.9::edge(256, 142).
optimizable edge(4, 144).
0.9::edge(50, 144).
optimizable edge(99, 144).
0.9::edge(199, 145).
optimizable edge(147, 146).
0.9::edge(262, 146).
optimizable edge(148, 147).
0.9::edge(4, 148).
optimizable edge(18, 148).
0.9::edge(19, 148).
optimizable edge(20, 148).
0.9::edge(21, 148).
optimizable edge(76, 148).
0.9::edge(140, 148).
optimizable edge(256, 148).
0.9::edge(47, 149).
optimizable edge(54, 149).
0.9::edge(54, 150).
optimizable edge(149, 151).
0.9::edge(150, 151).
optimizable edge(153, 153).
0.9::edge(54, 155).
optimizable edge(156, 155).
0.9::edge(155, 156).
optimizable edge(156, 156).
0.9::edge(4, 157).
optimizable edge(52, 157).
0.9::edge(54, 157).
optimizable edge(157, 157).
0.9::edge(4, 158).
optimizable edge(52, 158).
0.9::edge(71, 158).
optimizable edge(99, 158).
0.9::edge(100, 158).
optimizable edge(157, 158).
0.9::edge(4, 159).
optimizable edge(257, 159).
0.9::edge(4, 160).
optimizable edge(178, 160).
0.9::edge(4, 161).
optimizable edge(87, 161).
0.9::edge(4, 162).
optimizable edge(165, 162).
0.9::edge(225, 162).
optimizable edge(4, 163).
0.9::edge(54, 163).
optimizable edge(164, 163).
0.9::edge(4, 164).
optimizable edge(163, 164).
0.9::edge(164, 164).
optimizable edge(165, 165).
0.9::edge(54, 166).
optimizable edge(167, 166).
0.9::edge(4, 167).
optimizable edge(63, 167).
0.9::edge(85, 167).
optimizable edge(166, 167).
0.9::edge(169, 169).
optimizable edge(4, 171).
0.9::edge(26, 172).
optimizable edge(178, 172).
0.9::edge(12, 173).
optimizable edge(4, 174).
0.9::edge(180, 175).
optimizable edge(175, 176).
0.9::edge(180, 176).
optimizable edge(219, 176).
0.9::edge(4, 177).
optimizable edge(180, 177).
0.9::edge(4, 179).
optimizable edge(175, 180).
0.9::edge(180, 180).
optimizable edge(90, 181).
0.9::edge(4, 182).
optimizable edge(4, 183).
0.9::edge(4, 184).
optimizable edge(257, 184).
0.9::edge(4, 185).
optimizable edge(12, 185).
0.9::edge(191, 185).
optimizable edge(54, 186).
0.9::edge(187, 186).
optimizable edge(4, 187).
0.9::edge(186, 187).
optimizable edge(187, 187).
0.9::edge(4, 191).
optimizable edge(6, 191).
0.9::edge(7, 191).
optimizable edge(185, 191).
0.9::edge(4, 192).
optimizable edge(4, 193).
0.9::edge(66, 193).
optimizable edge(67, 193).
0.9::edge(70, 193).
optimizable edge(3, 194).
0.9::edge(4, 194).
optimizable edge(5, 194).
0.9::edge(41, 194).
optimizable edge(70, 194).
0.9::edge(194, 194).
optimizable edge(263, 194).
0.9::edge(83, 195).
optimizable edge(170, 195).
0.9::edge(48, 196).
optimizable edge(196, 197).
0.9::edge(4, 198).
optimizable edge(4, 199).
0.9::edge(4, 201).
optimizable edge(36, 202).
0.9::edge(48, 202).
optimizable edge(121, 202).
0.9::edge(127, 202).
optimizable edge(130, 202).
0.9::edge(141, 202).
optimizable edge(144, 202).
0.9::edge(168, 202).
optimizable edge(203, 202).
0.9::edge(237, 202).
optimizable edge(4, 204).
0.9::edge(170, 204).
optimizable edge(183, 204).
0.9::edge(195, 204).
optimizable edge(4, 205).
0.9::edge(4, 206).
optimizable edge(4, 207).
0.9::edge(11, 207).
optimizable edge(4, 208).
0.9::edge(140, 209).
optimizable edge(209, 209).
0.9::edge(256, 209).
optimizable edge(148, 210).
0.9::edge(87, 211).
optimizable edge(214, 211).
0.9::edge(215, 211).
optimizable edge(87, 212).
0.9::edge(213, 212).
optimizable edge(215, 212).
0.9::edge(87, 213).
optimizable edge(215, 213).
0.9::edge(87, 214).
optimizable edge(215, 214).
0.9::edge(4, 215).
optimizable edge(87, 215).
0.9::edge(212, 215).
optimizable edge(213, 215).
0.9::edge(214, 215).
optimizable edge(1, 216).
0.9::edge(4, 216).
optimizable edge(4, 217).
0.9::edge(4, 218).
optimizable edge(15, 218).
0.9::edge(91, 218).
optimizable edge(217, 218).
0.9::edge(219, 218).
optimizable edge(1, 219).
0.9::edge(4, 219).
optimizable edge(4, 220).
0.9::edge(148, 220).
optimizable edge(256, 220).
0.9::edge(4, 221).
optimizable edge(4, 222).
0.9::edge(223, 222).
optimizable edge(4, 223).
0.9::edge(4, 224).
optimizable edge(165, 224).
0.9::edge(4, 225).
optimizable edge(37, 225).
0.9::edge(162, 225).
optimizable edge(165, 225).
0.9::edge(249, 225).
optimizable edge(227, 226).
0.9::edge(4, 227).
optimizable edge(44, 227).
0.9::edge(152, 227).
optimizable edge(170, 227).
0.9::edge(173, 227).
optimizable edge(226, 227).
0.9::edge(227, 227).
optimizable edge(4, 228).
0.9::edge(4, 229).
optimizable edge(52, 229).
0.9::edge(76, 230).
optimizable edge(140, 230).
0.9::edge(148, 230).
optimizable edge(256, 230).
0.9::edge(4, 231).
optimizable edge(4, 232).
0.9::edge(231, 232).
optimizable edge(235, 234).
0.9::edge(4, 235).
optimizable edge(234, 235).
0.9::edge(60, 236).
optimizable edge(66, 236).
0.9::edge(67, 236).
optimizable edge(178, 238).
0.9::edge(4, 239).
optimizable edge(29, 239).
0.9::edge(47, 239).
optimizable edge(240, 239).
0.9::edge(52, 241).
optimizable edge(241, 242).
0.9::edge(76, 243).
optimizable edge(54, 245).
0.9::edge(246, 245).
optimizable edge(4, 246).
0.9::edge(245, 246).
optimizable edge(246, 246).
0.9::edge(4, 247).
optimizable edge(4, 248).
0.9::edge(4, 249).
optimizable edge(165, 249).
0.9::edge(225, 249).
optimizable edge(4, 250).
0.9::edge(4, 251).
optimizable edge(54, 251).
0.9::edge(252, 251).
optimizable edge(261, 251).
0.9::edge(4, 252).
optimizable edge(54, 252).
0.9::edge(67, 252).
optimizable edge(143, 252).
0.9::edge(251, 252).
optimizable edge(4, 253).
0.9::edge(70, 253).
optimizable edge(4, 255).
0.9::edge(82, 255).
optimizable edge(189, 255).
0.9::edge(191, 255).
optimizable edge(257, 255).
0.9::edge(4, 256).
optimizable edge(178, 256).
0.9::edge(4, 258).
optimizable edge(191, 258).
0.9::edge(4, 259).
optimizable edge(170, 259).
0.9::edge(26, 260).
optimizable edge(140, 260).
0.9::edge(178, 260).
optimizable edge(256, 260).
0.9::edge(4, 263).
optimizable edge(4, 265).

path(X, X).
path(X, Y):- path(X, Z), edge(Z, Y).



:- end_lpad.

run:- run_mma, run_ccsaq, run_slsqp.

run_mma(S,D):-
	statistics(runtime, [Start | _]), 
	prob_optimize(path(S,D),[edge(4,1) +edge(4,2) +edge(12,9) +edge(4,11) +edge(9,12) +edge(4,13) +edge(219,15) +edge(71,16) +edge(233,16) +edge(19,18) +edge(19,20) +edge(20,21) +edge(4,23) +edge(48,24) +edge(4,28) +edge(30,29) +edge(47,29) +edge(87,29) +edge(4,30) +edge(47,30) +edge(29,31) +edge(7,32) +edge(4,34) +edge(48,34) +edge(4,35) +edge(12,35) +edge(4,36) +edge(50,36) +edge(70,36) +edge(14,37) +edge(93,37) +edge(225,37) +edge(4,38) +edge(262,38) +edge(54,40) +edge(41,41) +edge(4,43) +edge(4,45) +edge(140,46) +edge(84,47) +edge(200,47) +edge(4,48) +edge(24,48) +edge(34,48) +edge(124,48) +edge(197,48) +edge(244,48) +edge(13,49) +edge(28,49) +edge(103,49) +edge(133,49) +edge(235,49) +edge(36,50) +edge(121,50) +edge(130,50) +edge(144,50) +edge(203,50) +edge(36,51) +edge(121,51) +edge(130,51) +edge(144,51) +edge(203,51) +edge(53,52) +edge(53,54) +edge(4,56) +edge(57,57) +edge(257,57) +edge(58,58) +edge(17,59) +edge(175,61) +edge(227,62) +edge(191,64) +edge(170,65) +edge(143,67) +edge(68,69) +edge(140,71) +edge(4,74) +edge(4,75) +edge(19,76) +edge(21,76) +edge(230,76) +edge(256,76) +edge(256,77) +edge(80,78) +edge(80,79) +edge(78,80) +edge(81,80) +edge(132,82) +edge(191,82) +edge(10,83) +edge(216,85) +edge(222,86) +edge(29,87) +edge(86,87) +edge(213,87) +edge(54,88) +edge(54,89) +edge(4,90) +edge(4,92) +edge(4,93) +edge(37,93) +edge(77,93) +edge(210,93) +edge(249,93) +edge(95,94) +edge(97,95) +edge(8,97) +edge(96,97) +edge(7,101) +edge(14,102) +edge(105,102) +edge(188,104) +edge(54,106) +edge(4,107) +edge(72,107) +edge(107,107) +edge(262,107) +edge(82,108) +edge(4,109) +edge(4,110) +edge(146,110) +edge(72,111) +edge(113,112) +edge(64,113) +edge(112,113) +edge(4,114) +edge(231,115) +edge(116,117) +edge(118,119) +edge(4,121) +edge(5,122) +edge(4,125) +edge(12,125) +edge(10,126) +edge(128,128) +edge(4,130) +edge(99,130) +edge(202,130) +edge(82,132) +edge(48,133) +edge(178,134) +edge(4,137) +edge(138,139) +edge(140,140) +edge(4,144) +edge(99,144) +edge(147,146) +edge(148,147) +edge(18,148) +edge(20,148) +edge(76,148) +edge(256,148) +edge(54,149) +edge(149,151) +edge(153,153) +edge(156,155) +edge(156,156) +edge(52,157) +edge(157,157) +edge(52,158) +edge(99,158) +edge(157,158) +edge(257,159) +edge(178,160) +edge(87,161) +edge(165,162) +edge(4,163) +edge(164,163) +edge(163,164) +edge(165,165) +edge(167,166) +edge(63,167) +edge(166,167) +edge(4,171) +edge(178,172) +edge(4,174) +edge(175,176) +edge(219,176) +edge(180,177) +edge(175,180) +edge(90,181) +edge(4,183) +edge(257,184) +edge(12,185) +edge(54,186) +edge(4,187) +edge(187,187) +edge(6,191) +edge(185,191) +edge(4,193) +edge(67,193) +edge(3,194) +edge(5,194) +edge(70,194) +edge(263,194) +edge(170,195) +edge(196,197) +edge(4,199) +edge(36,202) +edge(121,202) +edge(130,202) +edge(144,202) +edge(203,202) +edge(4,204) +edge(183,204) +edge(4,205) +edge(4,207) +edge(4,208) +edge(209,209) +edge(148,210) +edge(214,211) +edge(87,212) +edge(215,212) +edge(215,213) +edge(215,214) +edge(87,215) +edge(213,215) +edge(1,216) +edge(4,217) +edge(15,218) +edge(217,218) +edge(1,219) +edge(4,220) +edge(256,220) +edge(4,222) +edge(4,223) +edge(165,224) +edge(37,225) +edge(165,225) +edge(227,226) +edge(44,227) +edge(170,227) +edge(226,227) +edge(4,228) +edge(52,229) +edge(140,230) +edge(256,230) +edge(4,232) +edge(235,234) +edge(234,235) +edge(66,236) +edge(178,238) +edge(29,239) +edge(240,239) +edge(241,242) +edge(54,245) +edge(4,246) +edge(246,246) +edge(4,248) +edge(165,249) +edge(4,250) +edge(54,251) +edge(261,251) +edge(54,252) +edge(143,252) +edge(4,253) +edge(4,255) +edge(189,255) +edge(257,255) +edge(178,256) +edge(191,258) +edge(170,259) +edge(140,260) +edge(256,260) +edge(4,265) ],[path(S,D) > 0.8],mma,-1,A),
	statistics(runtime, [Stop | _]),
	Runtime is Stop - Start,
	writeln('mma'),
	writeln(Runtime),
	writeln(A).

run_ccsaq(S,D):-
	statistics(runtime, [Start | _]), 
	prob_optimize(path(S,D),[edge(4,1) +edge(4,2) +edge(12,9) +edge(4,11) +edge(9,12) +edge(4,13) +edge(219,15) +edge(71,16) +edge(233,16) +edge(19,18) +edge(19,20) +edge(20,21) +edge(4,23) +edge(48,24) +edge(4,28) +edge(30,29) +edge(47,29) +edge(87,29) +edge(4,30) +edge(47,30) +edge(29,31) +edge(7,32) +edge(4,34) +edge(48,34) +edge(4,35) +edge(12,35) +edge(4,36) +edge(50,36) +edge(70,36) +edge(14,37) +edge(93,37) +edge(225,37) +edge(4,38) +edge(262,38) +edge(54,40) +edge(41,41) +edge(4,43) +edge(4,45) +edge(140,46) +edge(84,47) +edge(200,47) +edge(4,48) +edge(24,48) +edge(34,48) +edge(124,48) +edge(197,48) +edge(244,48) +edge(13,49) +edge(28,49) +edge(103,49) +edge(133,49) +edge(235,49) +edge(36,50) +edge(121,50) +edge(130,50) +edge(144,50) +edge(203,50) +edge(36,51) +edge(121,51) +edge(130,51) +edge(144,51) +edge(203,51) +edge(53,52) +edge(53,54) +edge(4,56) +edge(57,57) +edge(257,57) +edge(58,58) +edge(17,59) +edge(175,61) +edge(227,62) +edge(191,64) +edge(170,65) +edge(143,67) +edge(68,69) +edge(140,71) +edge(4,74) +edge(4,75) +edge(19,76) +edge(21,76) +edge(230,76) +edge(256,76) +edge(256,77) +edge(80,78) +edge(80,79) +edge(78,80) +edge(81,80) +edge(132,82) +edge(191,82) +edge(10,83) +edge(216,85) +edge(222,86) +edge(29,87) +edge(86,87) +edge(213,87) +edge(54,88) +edge(54,89) +edge(4,90) +edge(4,92) +edge(4,93) +edge(37,93) +edge(77,93) +edge(210,93) +edge(249,93) +edge(95,94) +edge(97,95) +edge(8,97) +edge(96,97) +edge(7,101) +edge(14,102) +edge(105,102) +edge(188,104) +edge(54,106) +edge(4,107) +edge(72,107) +edge(107,107) +edge(262,107) +edge(82,108) +edge(4,109) +edge(4,110) +edge(146,110) +edge(72,111) +edge(113,112) +edge(64,113) +edge(112,113) +edge(4,114) +edge(231,115) +edge(116,117) +edge(118,119) +edge(4,121) +edge(5,122) +edge(4,125) +edge(12,125) +edge(10,126) +edge(128,128) +edge(4,130) +edge(99,130) +edge(202,130) +edge(82,132) +edge(48,133) +edge(178,134) +edge(4,137) +edge(138,139) +edge(140,140) +edge(4,144) +edge(99,144) +edge(147,146) +edge(148,147) +edge(18,148) +edge(20,148) +edge(76,148) +edge(256,148) +edge(54,149) +edge(149,151) +edge(153,153) +edge(156,155) +edge(156,156) +edge(52,157) +edge(157,157) +edge(52,158) +edge(99,158) +edge(157,158) +edge(257,159) +edge(178,160) +edge(87,161) +edge(165,162) +edge(4,163) +edge(164,163) +edge(163,164) +edge(165,165) +edge(167,166) +edge(63,167) +edge(166,167) +edge(4,171) +edge(178,172) +edge(4,174) +edge(175,176) +edge(219,176) +edge(180,177) +edge(175,180) +edge(90,181) +edge(4,183) +edge(257,184) +edge(12,185) +edge(54,186) +edge(4,187) +edge(187,187) +edge(6,191) +edge(185,191) +edge(4,193) +edge(67,193) +edge(3,194) +edge(5,194) +edge(70,194) +edge(263,194) +edge(170,195) +edge(196,197) +edge(4,199) +edge(36,202) +edge(121,202) +edge(130,202) +edge(144,202) +edge(203,202) +edge(4,204) +edge(183,204) +edge(4,205) +edge(4,207) +edge(4,208) +edge(209,209) +edge(148,210) +edge(214,211) +edge(87,212) +edge(215,212) +edge(215,213) +edge(215,214) +edge(87,215) +edge(213,215) +edge(1,216) +edge(4,217) +edge(15,218) +edge(217,218) +edge(1,219) +edge(4,220) +edge(256,220) +edge(4,222) +edge(4,223) +edge(165,224) +edge(37,225) +edge(165,225) +edge(227,226) +edge(44,227) +edge(170,227) +edge(226,227) +edge(4,228) +edge(52,229) +edge(140,230) +edge(256,230) +edge(4,232) +edge(235,234) +edge(234,235) +edge(66,236) +edge(178,238) +edge(29,239) +edge(240,239) +edge(241,242) +edge(54,245) +edge(4,246) +edge(246,246) +edge(4,248) +edge(165,249) +edge(4,250) +edge(54,251) +edge(261,251) +edge(54,252) +edge(143,252) +edge(4,253) +edge(4,255) +edge(189,255) +edge(257,255) +edge(178,256) +edge(191,258) +edge(170,259) +edge(140,260) +edge(256,260) +edge(4,265) ],[path(S,D) > 0.8],ccsaq,-1,A),
	statistics(runtime, [Stop | _]),
	Runtime is Stop - Start,
	writeln('ccsaq'),
	writeln(Runtime),
	writeln(A).

run_slsqp(S,D):-
	statistics(runtime, [Start | _]), 
	prob_optimize(path(S,D),[edge(4,1) +edge(4,2) +edge(12,9) +edge(4,11) +edge(9,12) +edge(4,13) +edge(219,15) +edge(71,16) +edge(233,16) +edge(19,18) +edge(19,20) +edge(20,21) +edge(4,23) +edge(48,24) +edge(4,28) +edge(30,29) +edge(47,29) +edge(87,29) +edge(4,30) +edge(47,30) +edge(29,31) +edge(7,32) +edge(4,34) +edge(48,34) +edge(4,35) +edge(12,35) +edge(4,36) +edge(50,36) +edge(70,36) +edge(14,37) +edge(93,37) +edge(225,37) +edge(4,38) +edge(262,38) +edge(54,40) +edge(41,41) +edge(4,43) +edge(4,45) +edge(140,46) +edge(84,47) +edge(200,47) +edge(4,48) +edge(24,48) +edge(34,48) +edge(124,48) +edge(197,48) +edge(244,48) +edge(13,49) +edge(28,49) +edge(103,49) +edge(133,49) +edge(235,49) +edge(36,50) +edge(121,50) +edge(130,50) +edge(144,50) +edge(203,50) +edge(36,51) +edge(121,51) +edge(130,51) +edge(144,51) +edge(203,51) +edge(53,52) +edge(53,54) +edge(4,56) +edge(57,57) +edge(257,57) +edge(58,58) +edge(17,59) +edge(175,61) +edge(227,62) +edge(191,64) +edge(170,65) +edge(143,67) +edge(68,69) +edge(140,71) +edge(4,74) +edge(4,75) +edge(19,76) +edge(21,76) +edge(230,76) +edge(256,76) +edge(256,77) +edge(80,78) +edge(80,79) +edge(78,80) +edge(81,80) +edge(132,82) +edge(191,82) +edge(10,83) +edge(216,85) +edge(222,86) +edge(29,87) +edge(86,87) +edge(213,87) +edge(54,88) +edge(54,89) +edge(4,90) +edge(4,92) +edge(4,93) +edge(37,93) +edge(77,93) +edge(210,93) +edge(249,93) +edge(95,94) +edge(97,95) +edge(8,97) +edge(96,97) +edge(7,101) +edge(14,102) +edge(105,102) +edge(188,104) +edge(54,106) +edge(4,107) +edge(72,107) +edge(107,107) +edge(262,107) +edge(82,108) +edge(4,109) +edge(4,110) +edge(146,110) +edge(72,111) +edge(113,112) +edge(64,113) +edge(112,113) +edge(4,114) +edge(231,115) +edge(116,117) +edge(118,119) +edge(4,121) +edge(5,122) +edge(4,125) +edge(12,125) +edge(10,126) +edge(128,128) +edge(4,130) +edge(99,130) +edge(202,130) +edge(82,132) +edge(48,133) +edge(178,134) +edge(4,137) +edge(138,139) +edge(140,140) +edge(4,144) +edge(99,144) +edge(147,146) +edge(148,147) +edge(18,148) +edge(20,148) +edge(76,148) +edge(256,148) +edge(54,149) +edge(149,151) +edge(153,153) +edge(156,155) +edge(156,156) +edge(52,157) +edge(157,157) +edge(52,158) +edge(99,158) +edge(157,158) +edge(257,159) +edge(178,160) +edge(87,161) +edge(165,162) +edge(4,163) +edge(164,163) +edge(163,164) +edge(165,165) +edge(167,166) +edge(63,167) +edge(166,167) +edge(4,171) +edge(178,172) +edge(4,174) +edge(175,176) +edge(219,176) +edge(180,177) +edge(175,180) +edge(90,181) +edge(4,183) +edge(257,184) +edge(12,185) +edge(54,186) +edge(4,187) +edge(187,187) +edge(6,191) +edge(185,191) +edge(4,193) +edge(67,193) +edge(3,194) +edge(5,194) +edge(70,194) +edge(263,194) +edge(170,195) +edge(196,197) +edge(4,199) +edge(36,202) +edge(121,202) +edge(130,202) +edge(144,202) +edge(203,202) +edge(4,204) +edge(183,204) +edge(4,205) +edge(4,207) +edge(4,208) +edge(209,209) +edge(148,210) +edge(214,211) +edge(87,212) +edge(215,212) +edge(215,213) +edge(215,214) +edge(87,215) +edge(213,215) +edge(1,216) +edge(4,217) +edge(15,218) +edge(217,218) +edge(1,219) +edge(4,220) +edge(256,220) +edge(4,222) +edge(4,223) +edge(165,224) +edge(37,225) +edge(165,225) +edge(227,226) +edge(44,227) +edge(170,227) +edge(226,227) +edge(4,228) +edge(52,229) +edge(140,230) +edge(256,230) +edge(4,232) +edge(235,234) +edge(234,235) +edge(66,236) +edge(178,238) +edge(29,239) +edge(240,239) +edge(241,242) +edge(54,245) +edge(4,246) +edge(246,246) +edge(4,248) +edge(165,249) +edge(4,250) +edge(54,251) +edge(261,251) +edge(54,252) +edge(143,252) +edge(4,253) +edge(4,255) +edge(189,255) +edge(257,255) +edge(178,256) +edge(191,258) +edge(170,259) +edge(140,260) +edge(256,260) +edge(4,265) ],[path(S,D) > 0.8],slsqp,-1,A),
	statistics(runtime, [Stop | _]),
	Runtime is Stop - Start,
	writeln('slsqp'),
	writeln(Runtime),
	writeln(A).
