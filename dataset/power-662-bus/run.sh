sbatch --job-name=p662_313_2_m --partition=longrun --mem=8G --output=power-662-bus_313_2_mma.log --wrap='srun swipl -s power-662-bus.pl -g "run_mma(313,2),halt." '
sbatch --job-name=p662_313_2_c --partition=longrun --mem=8G --output=power-662-bus_313_2_ccsaq.log --wrap='srun swipl -s power-662-bus.pl -g "run_ccsaq(313,2),halt." '
sbatch --job-name=p662_313_2_s --partition=longrun --mem=8G --output=power-662-bus_313_2_slsqp.log --wrap='srun swipl -s power-662-bus.pl -g "run_slsqp(313,2),halt." '

sbatch --job-name=p662_108_29_m --partition=longrun --mem=8G --output=power-662-bus_108_29_mma.log --wrap='srun swipl -s power-662-bus.pl -g "run_mma(108,29),halt." '
sbatch --job-name=p662_108_29_c --partition=longrun --mem=8G --output=power-662-bus_108_29_ccsaq.log --wrap='srun swipl -s power-662-bus.pl -g "run_ccsaq(108,29),halt." '
sbatch --job-name=p662_108_29_s --partition=longrun --mem=8G --output=power-662-bus_108_29_slsqp.log --wrap='srun swipl -s power-662-bus.pl -g "run_slsqp(108,29),halt." '

sbatch --job-name=p662_391_66_m --partition=longrun --mem=8G --output=power-662-bus_391_66_mma.log --wrap='srun swipl -s power-662-bus.pl -g "run_mma(391,66),halt." '
sbatch --job-name=p662_391_66_c --partition=longrun --mem=8G --output=power-662-bus_391_66_ccsaq.log --wrap='srun swipl -s power-662-bus.pl -g "run_ccsaq(391,66),halt." '
sbatch --job-name=p662_391_66_s --partition=longrun --mem=8G --output=power-662-bus_391_66_slsqp.log --wrap='srun swipl -s power-662-bus.pl -g "run_slsqp(391,66),halt." '

sbatch --job-name=p662_416_63_m --partition=longrun --mem=8G --output=power-662-bus_416_63_mma.log --wrap='srun swipl -s power-662-bus.pl -g "run_mma(416,63),halt." '
sbatch --job-name=p662_416_63_c --partition=longrun --mem=8G --output=power-662-bus_416_63_ccsaq.log --wrap='srun swipl -s power-662-bus.pl -g "run_ccsaq(416,63),halt." '
sbatch --job-name=p662_416_63_s --partition=longrun --mem=8G --output=power-662-bus_416_63_slsqp.log --wrap='srun swipl -s power-662-bus.pl -g "run_slsqp(416,63),halt." '

sbatch --job-name=p662_210_7_m --partition=longrun --mem=8G --output=power-662-bus_210_7_mma.log --wrap='srun swipl -s power-662-bus.pl -g "run_mma(210,7),halt." '
sbatch --job-name=p662_210_7_c --partition=longrun --mem=8G --output=power-662-bus_210_7_ccsaq.log --wrap='srun swipl -s power-662-bus.pl -g "run_ccsaq(210,7),halt." '
sbatch --job-name=p662_210_7_s --partition=longrun --mem=8G --output=power-662-bus_210_7_slsqp.log --wrap='srun swipl -s power-662-bus.pl -g "run_slsqp(210,7),halt." '

sbatch --job-name=p662_398_164_m --partition=longrun --mem=8G --output=power-662-bus_398_164_mma.log --wrap='srun swipl -s power-662-bus.pl -g "run_mma(398,164),halt." '
sbatch --job-name=p662_398_164_c --partition=longrun --mem=8G --output=power-662-bus_398_164_ccsaq.log --wrap='srun swipl -s power-662-bus.pl -g "run_ccsaq(398,164),halt." '
sbatch --job-name=p662_398_164_s --partition=longrun --mem=8G --output=power-662-bus_398_164_slsqp.log --wrap='srun swipl -s power-662-bus.pl -g "run_slsqp(398,164),halt." '

sbatch --job-name=p662_412_385_m --partition=longrun --mem=8G --output=power-662-bus_412_385_mma.log --wrap='srun swipl -s power-662-bus.pl -g "run_mma(412,385),halt." '
sbatch --job-name=p662_412_385_c --partition=longrun --mem=8G --output=power-662-bus_412_385_ccsaq.log --wrap='srun swipl -s power-662-bus.pl -g "run_ccsaq(412,385),halt." '
sbatch --job-name=p662_412_385_s --partition=longrun --mem=8G --output=power-662-bus_412_385_slsqp.log --wrap='srun swipl -s power-662-bus.pl -g "run_slsqp(412,385),halt." '

sbatch --job-name=p662_387_363_m --partition=longrun --mem=8G --output=power-662-bus_387_363_mma.log --wrap='srun swipl -s power-662-bus.pl -g "run_mma(387,363),halt." '
sbatch --job-name=p662_387_363_c --partition=longrun --mem=8G --output=power-662-bus_387_363_ccsaq.log --wrap='srun swipl -s power-662-bus.pl -g "run_ccsaq(387,363),halt." '
sbatch --job-name=p662_387_363_s --partition=longrun --mem=8G --output=power-662-bus_387_363_slsqp.log --wrap='srun swipl -s power-662-bus.pl -g "run_slsqp(387,363),halt." '

sbatch --job-name=p662_403_32_m --partition=longrun --mem=8G --output=power-662-bus_403_32_mma.log --wrap='srun swipl -s power-662-bus.pl -g "run_mma(403,32),halt." '
sbatch --job-name=p662_403_32_c --partition=longrun --mem=8G --output=power-662-bus_403_32_ccsaq.log --wrap='srun swipl -s power-662-bus.pl -g "run_ccsaq(403,32),halt." '
sbatch --job-name=p662_403_32_s --partition=longrun --mem=8G --output=power-662-bus_403_32_slsqp.log --wrap='srun swipl -s power-662-bus.pl -g "run_slsqp(403,32),halt." '

sbatch --job-name=p662_410_369_m --partition=longrun --mem=8G --output=power-662-bus_410_369_mma.log --wrap='srun swipl -s power-662-bus.pl -g "run_mma(410,369),halt." '
sbatch --job-name=p662_410_369_c --partition=longrun --mem=8G --output=power-662-bus_410_369_ccsaq.log --wrap='srun swipl -s power-662-bus.pl -g "run_ccsaq(410,369),halt." '
sbatch --job-name=p662_410_369_s --partition=longrun --mem=8G --output=power-662-bus_410_369_slsqp.log --wrap='srun swipl -s power-662-bus.pl -g "run_slsqp(410,369),halt." '