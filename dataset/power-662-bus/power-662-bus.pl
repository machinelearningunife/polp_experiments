:- use_module(library(pita)).
:- pita.
:- begin_lpad.

0.9::edge(1, 1).
optimizable edge(222, 1).
0.9::edge(286, 1).
optimizable edge(2, 2).
0.9::edge(9, 2).
optimizable edge(228, 2).
0.9::edge(298, 2).
optimizable edge(3, 3).
0.9::edge(95, 3).
optimizable edge(166, 3).
0.9::edge(4, 4).
optimizable edge(126, 4).
0.9::edge(259, 4).
optimizable edge(5, 5).
0.9::edge(83, 5).
optimizable edge(228, 5).
0.9::edge(6, 6).
optimizable edge(109, 6).
0.9::edge(213, 6).
optimizable edge(7, 7).
0.9::edge(45, 7).
optimizable edge(65, 7).
0.9::edge(8, 8).
optimizable edge(38, 8).
0.9::edge(117, 8).
optimizable edge(119, 8).
0.9::edge(199, 8).
optimizable edge(211, 8).
0.9::edge(267, 8).
optimizable edge(9, 9).
0.9::edge(222, 9).
optimizable edge(10, 10).
0.9::edge(49, 10).
optimizable edge(106, 10).
0.9::edge(216, 10).
optimizable edge(303, 10).
0.9::edge(11, 11).
optimizable edge(122, 11).
0.9::edge(124, 11).
optimizable edge(12, 12).
0.9::edge(25, 12).
optimizable edge(80, 12).
0.9::edge(13, 13).
optimizable edge(90, 13).
0.9::edge(294, 13).
optimizable edge(14, 14).
0.9::edge(80, 14).
optimizable edge(245, 14).
0.9::edge(15, 15).
optimizable edge(24, 15).
0.9::edge(86, 15).
optimizable edge(137, 15).
0.9::edge(165, 15).
optimizable edge(304, 15).
0.9::edge(16, 16).
optimizable edge(57, 16).
0.9::edge(277, 16).
optimizable edge(17, 17).
0.9::edge(223, 17).
optimizable edge(231, 17).
0.9::edge(18, 18).
optimizable edge(251, 18).
0.9::edge(289, 18).
optimizable edge(19, 19).
0.9::edge(201, 19).
optimizable edge(236, 19).
0.9::edge(20, 20).
optimizable edge(122, 20).
0.9::edge(141, 20).
optimizable edge(21, 21).
0.9::edge(75, 21).
optimizable edge(273, 21).
0.9::edge(22, 22).
optimizable edge(27, 22).
0.9::edge(227, 22).
optimizable edge(23, 23).
0.9::edge(34, 23).
optimizable edge(89, 23).
0.9::edge(24, 24).
optimizable edge(187, 24).
0.9::edge(25, 25).
optimizable edge(158, 25).
0.9::edge(193, 25).
optimizable edge(289, 25).
0.9::edge(305, 25).
optimizable edge(26, 26).
0.9::edge(202, 26).
optimizable edge(250, 26).
0.9::edge(27, 27).
optimizable edge(69, 27).
0.9::edge(28, 28).
optimizable edge(33, 28).
0.9::edge(140, 28).
optimizable edge(29, 29).
0.9::edge(54, 29).
optimizable edge(238, 29).
0.9::edge(30, 30).
optimizable edge(149, 30).
0.9::edge(154, 30).
optimizable edge(31, 31).
0.9::edge(96, 31).
optimizable edge(97, 31).
0.9::edge(308, 31).
optimizable edge(32, 32).
0.9::edge(157, 32).
optimizable edge(271, 32).
0.9::edge(33, 33).
optimizable edge(228, 33).
0.9::edge(34, 34).
optimizable edge(48, 34).
0.9::edge(136, 34).
optimizable edge(166, 34).
0.9::edge(167, 34).
optimizable edge(196, 34).
0.9::edge(310, 34).
optimizable edge(35, 35).
0.9::edge(370, 35).
optimizable edge(36, 36).
0.9::edge(265, 36).
optimizable edge(281, 36).
0.9::edge(37, 37).
optimizable edge(68, 37).
0.9::edge(277, 37).
optimizable edge(38, 38).
0.9::edge(105, 38).
optimizable edge(39, 39).
0.9::edge(104, 39).
optimizable edge(267, 39).
0.9::edge(40, 40).
optimizable edge(124, 40).
0.9::edge(293, 40).
optimizable edge(41, 41).
0.9::edge(98, 41).
optimizable edge(214, 41).
0.9::edge(42, 42).
optimizable edge(264, 42).
0.9::edge(43, 43).
optimizable edge(264, 43).
0.9::edge(287, 43).
optimizable edge(44, 44).
0.9::edge(67, 44).
optimizable edge(75, 44).
0.9::edge(146, 44).
optimizable edge(188, 44).
0.9::edge(45, 45).
optimizable edge(210, 45).
0.9::edge(234, 45).
optimizable edge(46, 46).
0.9::edge(81, 46).
optimizable edge(277, 46).
0.9::edge(47, 47).
optimizable edge(182, 47).
0.9::edge(201, 47).
optimizable edge(48, 48).
0.9::edge(286, 48).
optimizable edge(49, 49).
0.9::edge(212, 49).
optimizable edge(50, 50).
0.9::edge(171, 50).
optimizable edge(185, 50).
0.9::edge(51, 51).
optimizable edge(168, 51).
0.9::edge(272, 51).
optimizable edge(52, 52).
0.9::edge(237, 52).
optimizable edge(272, 52).
0.9::edge(53, 53).
optimizable edge(318, 53).
0.9::edge(54, 54).
optimizable edge(108, 54).
0.9::edge(55, 55).
optimizable edge(206, 55).
0.9::edge(290, 55).
optimizable edge(56, 56).
0.9::edge(132, 56).
optimizable edge(149, 56).
0.9::edge(151, 56).
optimizable edge(176, 56).
0.9::edge(57, 57).
optimizable edge(192, 57).
0.9::edge(58, 58).
optimizable edge(125, 58).
0.9::edge(232, 58).
optimizable edge(238, 58).
0.9::edge(284, 58).
optimizable edge(319, 58).
0.9::edge(59, 59).
optimizable edge(108, 59).
0.9::edge(282, 59).
optimizable edge(60, 60).
0.9::edge(277, 60).
optimizable edge(61, 61).
0.9::edge(146, 61).
optimizable edge(224, 61).
0.9::edge(62, 62).
optimizable edge(152, 62).
0.9::edge(190, 62).
optimizable edge(63, 63).
0.9::edge(113, 63).
optimizable edge(184, 63).
0.9::edge(64, 64).
optimizable edge(322, 64).
0.9::edge(65, 65).
optimizable edge(143, 65).
0.9::edge(66, 66).
optimizable edge(203, 66).
0.9::edge(253, 66).
optimizable edge(67, 67).
0.9::edge(296, 67).
optimizable edge(68, 68).
0.9::edge(281, 68).
optimizable edge(69, 69).
0.9::edge(214, 69).
optimizable edge(262, 69).
0.9::edge(326, 69).
optimizable edge(70, 70).
0.9::edge(75, 70).
optimizable edge(198, 70).
0.9::edge(71, 71).
optimizable edge(75, 71).
0.9::edge(197, 71).
optimizable edge(72, 72).
0.9::edge(117, 72).
optimizable edge(142, 72).
0.9::edge(73, 73).
optimizable edge(129, 73).
0.9::edge(142, 73).
optimizable edge(74, 74).
0.9::edge(100, 74).
optimizable edge(113, 74).
0.9::edge(133, 74).
optimizable edge(217, 74).
0.9::edge(75, 75).
optimizable edge(241, 75).
0.9::edge(328, 75).
optimizable edge(76, 76).
0.9::edge(234, 76).
optimizable edge(255, 76).
0.9::edge(77, 77).
optimizable edge(118, 77).
0.9::edge(78, 78).
optimizable edge(142, 78).
0.9::edge(144, 78).
optimizable edge(79, 79).
0.9::edge(215, 79).
optimizable edge(294, 79).
0.9::edge(80, 80).
optimizable edge(81, 81).
0.9::edge(226, 81).
optimizable edge(82, 82).
0.9::edge(217, 82).
optimizable edge(252, 82).
0.9::edge(83, 83).
optimizable edge(124, 83).
0.9::edge(84, 84).
optimizable edge(118, 84).
0.9::edge(195, 84).
optimizable edge(85, 85).
0.9::edge(139, 85).
optimizable edge(196, 85).
0.9::edge(86, 86).
optimizable edge(201, 86).
0.9::edge(87, 87).
optimizable edge(331, 87).
0.9::edge(88, 88).
optimizable edge(161, 88).
0.9::edge(197, 88).
optimizable edge(89, 89).
0.9::edge(293, 89).
optimizable edge(90, 90).
0.9::edge(136, 90).
optimizable edge(91, 91).
0.9::edge(333, 91).
optimizable edge(92, 92).
0.9::edge(337, 92).
optimizable edge(93, 93).
0.9::edge(216, 93).
optimizable edge(264, 93).
0.9::edge(94, 94).
optimizable edge(167, 94).
0.9::edge(247, 94).
optimizable edge(95, 95).
0.9::edge(246, 95).
optimizable edge(96, 96).
0.9::edge(227, 96).
optimizable edge(97, 97).
0.9::edge(288, 97).
optimizable edge(98, 98).
0.9::edge(170, 98).
optimizable edge(288, 98).
0.9::edge(99, 99).
optimizable edge(210, 99).
0.9::edge(100, 100).
optimizable edge(101, 101).
0.9::edge(221, 101).
optimizable edge(233, 101).
0.9::edge(102, 102).
optimizable edge(229, 102).
0.9::edge(273, 102).
optimizable edge(103, 103).
0.9::edge(343, 103).
optimizable edge(104, 104).
0.9::edge(253, 104).
optimizable edge(105, 105).
0.9::edge(291, 105).
optimizable edge(106, 106).
0.9::edge(191, 106).
optimizable edge(107, 107).
0.9::edge(135, 107).
optimizable edge(193, 107).
0.9::edge(251, 107).
optimizable edge(108, 108).
0.9::edge(121, 108).
optimizable edge(202, 108).
0.9::edge(344, 108).
optimizable edge(109, 109).
0.9::edge(271, 109).
optimizable edge(110, 110).
0.9::edge(162, 110).
optimizable edge(189, 110).
0.9::edge(111, 111).
optimizable edge(219, 111).
0.9::edge(264, 111).
optimizable edge(112, 112).
0.9::edge(202, 112).
optimizable edge(284, 112).
0.9::edge(113, 113).
optimizable edge(265, 113).
0.9::edge(275, 113).
optimizable edge(345, 113).
0.9::edge(114, 114).
optimizable edge(171, 114).
0.9::edge(229, 114).
optimizable edge(115, 115).
0.9::edge(134, 115).
optimizable edge(194, 115).
0.9::edge(116, 116).
optimizable edge(129, 116).
0.9::edge(200, 116).
optimizable edge(117, 117).
0.9::edge(118, 118).
optimizable edge(347, 118).
0.9::edge(119, 119).
optimizable edge(120, 119).
0.9::edge(199, 119).
optimizable edge(120, 120).
0.9::edge(142, 120).
optimizable edge(121, 121).
0.9::edge(134, 121).
optimizable edge(122, 122).
0.9::edge(152, 122).
optimizable edge(155, 122).
0.9::edge(348, 122).
optimizable edge(123, 123).
0.9::edge(224, 123).
optimizable edge(262, 123).
0.9::edge(124, 124).
optimizable edge(239, 124).
0.9::edge(125, 125).
optimizable edge(156, 125).
0.9::edge(126, 126).
optimizable edge(168, 126).
0.9::edge(127, 127).
optimizable edge(349, 127).
0.9::edge(128, 128).
optimizable edge(155, 128).
0.9::edge(192, 128).
optimizable edge(129, 129).
0.9::edge(130, 130).
optimizable edge(144, 130).
0.9::edge(221, 130).
optimizable edge(131, 131).
0.9::edge(277, 131).
optimizable edge(281, 131).
0.9::edge(132, 132).
optimizable edge(287, 132).
0.9::edge(133, 133).
optimizable edge(276, 133).
0.9::edge(134, 134).
optimizable edge(135, 135).
0.9::edge(251, 135).
optimizable edge(277, 135).
0.9::edge(136, 136).
optimizable edge(137, 137).
0.9::edge(208, 137).
optimizable edge(138, 138).
0.9::edge(142, 138).
optimizable edge(261, 138).
0.9::edge(139, 139).
optimizable edge(292, 139).
0.9::edge(140, 140).
optimizable edge(192, 140).
0.9::edge(141, 141).
optimizable edge(223, 141).
0.9::edge(142, 142).
optimizable edge(357, 142).
0.9::edge(143, 143).
optimizable edge(215, 143).
0.9::edge(292, 143).
optimizable edge(358, 143).
0.9::edge(144, 144).
optimizable edge(145, 145).
0.9::edge(359, 145).
optimizable edge(146, 146).
0.9::edge(188, 146).
optimizable edge(224, 146).
0.9::edge(367, 146).
optimizable edge(147, 147).
0.9::edge(237, 147).
optimizable edge(369, 147).
0.9::edge(148, 148).
optimizable edge(220, 148).
0.9::edge(246, 148).
optimizable edge(259, 148).
0.9::edge(260, 148).
optimizable edge(149, 149).
0.9::edge(150, 150).
optimizable edge(224, 150).
0.9::edge(241, 150).
optimizable edge(151, 151).
0.9::edge(213, 151).
optimizable edge(152, 152).
0.9::edge(153, 153).
optimizable edge(376, 153).
0.9::edge(154, 154).
optimizable edge(294, 154).
0.9::edge(155, 155).
optimizable edge(156, 156).
0.9::edge(194, 156).
optimizable edge(206, 156).
0.9::edge(275, 156).
optimizable edge(278, 156).
0.9::edge(285, 156).
optimizable edge(157, 157).
0.9::edge(230, 157).
optimizable edge(158, 158).
0.9::edge(276, 158).
optimizable edge(159, 159).
0.9::edge(203, 159).
optimizable edge(233, 159).
0.9::edge(160, 160).
optimizable edge(193, 160).
0.9::edge(277, 160).
optimizable edge(161, 161).
0.9::edge(253, 161).
optimizable edge(162, 162).
0.9::edge(254, 162).
optimizable edge(163, 163).
0.9::edge(245, 163).
optimizable edge(280, 163).
0.9::edge(164, 164).
optimizable edge(382, 164).
0.9::edge(165, 165).
optimizable edge(209, 165).
0.9::edge(255, 165).
optimizable edge(346, 165).
0.9::edge(166, 166).
optimizable edge(167, 167).
0.9::edge(168, 168).
optimizable edge(169, 169).
0.9::edge(173, 169).
optimizable edge(260, 169).
0.9::edge(170, 170).
optimizable edge(229, 170).
0.9::edge(171, 171).
optimizable edge(186, 171).
0.9::edge(240, 171).
optimizable edge(383, 171).
0.9::edge(172, 172).
optimizable edge(222, 172).
0.9::edge(272, 172).
optimizable edge(173, 173).
0.9::edge(272, 173).
optimizable edge(174, 174).
0.9::edge(281, 174).
optimizable edge(175, 175).
0.9::edge(233, 175).
optimizable edge(176, 176).
0.9::edge(239, 176).
optimizable edge(177, 177).
0.9::edge(385, 177).
optimizable edge(178, 178).
0.9::edge(245, 178).
optimizable edge(249, 178).
0.9::edge(179, 179).
optimizable edge(388, 179).
0.9::edge(180, 180).
optimizable edge(207, 180).
0.9::edge(181, 181).
optimizable edge(207, 181).
0.9::edge(182, 182).
optimizable edge(208, 182).
0.9::edge(183, 183).
optimizable edge(191, 183).
0.9::edge(201, 183).
optimizable edge(184, 184).
0.9::edge(258, 184).
optimizable edge(185, 185).
0.9::edge(207, 185).
optimizable edge(186, 186).
0.9::edge(207, 186).
optimizable edge(187, 187).
0.9::edge(242, 187).
optimizable edge(188, 188).
0.9::edge(189, 189).
optimizable edge(192, 189).
0.9::edge(190, 190).
optimizable edge(230, 190).
0.9::edge(191, 191).
optimizable edge(192, 192).
0.9::edge(290, 192).
optimizable edge(193, 193).
0.9::edge(194, 194).
optimizable edge(195, 195).
0.9::edge(207, 195).
optimizable edge(196, 196).
0.9::edge(197, 197).
optimizable edge(198, 198).
0.9::edge(270, 198).
optimizable edge(199, 199).
0.9::edge(211, 199).
optimizable edge(284, 199).
0.9::edge(389, 199).
optimizable edge(200, 200).
0.9::edge(233, 200).
optimizable edge(244, 200).
0.9::edge(201, 201).
optimizable edge(202, 202).
0.9::edge(261, 202).
optimizable edge(203, 203).
0.9::edge(205, 203).
optimizable edge(244, 203).
0.9::edge(391, 203).
optimizable edge(204, 204).
0.9::edge(206, 204).
optimizable edge(281, 204).
0.9::edge(205, 205).
optimizable edge(253, 205).
0.9::edge(206, 206).
optimizable edge(258, 206).
0.9::edge(283, 206).
optimizable edge(392, 206).
0.9::edge(207, 207).
optimizable edge(208, 208).
0.9::edge(209, 208).
optimizable edge(209, 209).
0.9::edge(210, 210).
optimizable edge(211, 211).
0.9::edge(212, 212).
optimizable edge(287, 212).
0.9::edge(213, 213).
optimizable edge(231, 213).
0.9::edge(214, 214).
optimizable edge(215, 215).
0.9::edge(216, 216).
optimizable edge(266, 216).
0.9::edge(217, 217).
optimizable edge(218, 218).
0.9::edge(395, 218).
optimizable edge(219, 219).
0.9::edge(271, 219).
optimizable edge(220, 220).
0.9::edge(269, 220).
optimizable edge(396, 220).
0.9::edge(221, 221).
optimizable edge(222, 222).
0.9::edge(223, 223).
optimizable edge(224, 224).
0.9::edge(225, 225).
optimizable edge(401, 225).
0.9::edge(226, 226).
optimizable edge(245, 226).
0.9::edge(227, 227).
optimizable edge(228, 228).
0.9::edge(280, 228).
optimizable edge(229, 229).
0.9::edge(270, 229).
optimizable edge(402, 229).
0.9::edge(230, 230).
optimizable edge(231, 230).
0.9::edge(231, 231).
optimizable edge(403, 231).
0.9::edge(232, 232).
optimizable edge(233, 233).
0.9::edge(296, 233).
optimizable edge(234, 234).
0.9::edge(235, 234).
optimizable edge(268, 234).
0.9::edge(404, 234).
optimizable edge(235, 235).
0.9::edge(295, 235).
optimizable edge(236, 236).
0.9::edge(266, 236).
optimizable edge(237, 237).
0.9::edge(238, 238).
optimizable edge(239, 239).
0.9::edge(240, 240).
optimizable edge(241, 241).
0.9::edge(242, 242).
optimizable edge(268, 242).
0.9::edge(287, 242).
optimizable edge(243, 243).
0.9::edge(406, 243).
optimizable edge(244, 244).
0.9::edge(245, 245).
optimizable edge(254, 245).
0.9::edge(407, 245).
optimizable edge(246, 246).
0.9::edge(247, 247).
optimizable edge(269, 247).
0.9::edge(248, 248).
optimizable edge(414, 248).
0.9::edge(249, 249).
optimizable edge(415, 249).
0.9::edge(250, 250).
optimizable edge(282, 250).
0.9::edge(251, 251).
optimizable edge(252, 252).
0.9::edge(416, 252).
optimizable edge(253, 253).
0.9::edge(291, 253).
optimizable edge(417, 253).
0.9::edge(254, 254).
optimizable edge(255, 255).
0.9::edge(256, 256).
optimizable edge(418, 256).
0.9::edge(257, 257).
optimizable edge(419, 257).
0.9::edge(258, 258).
optimizable edge(259, 259).
0.9::edge(260, 260).
optimizable edge(261, 261).
0.9::edge(262, 262).
optimizable edge(263, 263).
0.9::edge(423, 263).
optimizable edge(264, 264).
0.9::edge(424, 264).
optimizable edge(265, 265).
0.9::edge(266, 266).
optimizable edge(267, 267).
0.9::edge(268, 268).
optimizable edge(269, 269).
0.9::edge(270, 270).
optimizable edge(271, 271).
0.9::edge(272, 272).
optimizable edge(425, 272).
0.9::edge(273, 273).
optimizable edge(274, 274).
0.9::edge(427, 274).
optimizable edge(275, 275).
0.9::edge(276, 276).
optimizable edge(277, 277).
0.9::edge(428, 277).
optimizable edge(278, 278).
0.9::edge(283, 278).
optimizable edge(285, 278).
0.9::edge(279, 279).
optimizable edge(429, 279).
0.9::edge(280, 280).
optimizable edge(281, 281).
0.9::edge(282, 282).
optimizable edge(283, 283).
0.9::edge(284, 284).
optimizable edge(285, 285).
0.9::edge(286, 286).
optimizable edge(287, 287).
0.9::edge(288, 288).
optimizable edge(289, 289).
0.9::edge(290, 290).
optimizable edge(291, 291).
0.9::edge(292, 292).
optimizable edge(293, 293).
0.9::edge(294, 293).
optimizable edge(294, 294).
0.9::edge(295, 294).
optimizable edge(295, 295).
0.9::edge(296, 296).
optimizable edge(297, 297).
0.9::edge(448, 297).
optimizable edge(298, 298).
0.9::edge(312, 298).
optimizable edge(313, 298).
0.9::edge(407, 298).
optimizable edge(299, 299).
0.9::edge(300, 299).
optimizable edge(336, 299).
0.9::edge(339, 299).
optimizable edge(343, 299).
0.9::edge(300, 300).
optimizable edge(301, 301).
0.9::edge(371, 301).
optimizable edge(302, 302).
0.9::edge(372, 302).
optimizable edge(303, 303).
0.9::edge(304, 303).
optimizable edge(424, 303).
0.9::edge(304, 304).
optimizable edge(346, 304).
0.9::edge(305, 305).
optimizable edge(345, 305).
0.9::edge(366, 305).
optimizable edge(394, 305).
0.9::edge(407, 305).
optimizable edge(420, 305).
0.9::edge(428, 305).
optimizable edge(436, 305).
0.9::edge(306, 306).
optimizable edge(307, 306).
0.9::edge(308, 306).
optimizable edge(309, 306).
0.9::edge(378, 306).
optimizable edge(379, 306).
0.9::edge(434, 306).
optimizable edge(307, 307).
0.9::edge(381, 307).
optimizable edge(308, 308).
0.9::edge(419, 308).
optimizable edge(421, 308).
0.9::edge(309, 309).
optimizable edge(321, 309).
0.9::edge(326, 309).
optimizable edge(328, 309).
0.9::edge(332, 309).
optimizable edge(341, 309).
0.9::edge(342, 309).
optimizable edge(422, 309).
0.9::edge(310, 310).
optimizable edge(311, 310).
0.9::edge(312, 310).
optimizable edge(370, 310).
0.9::edge(410, 310).
optimizable edge(426, 310).
0.9::edge(311, 311).
optimizable edge(645, 311).
0.9::edge(312, 312).
optimizable edge(313, 313).
0.9::edge(314, 314).
optimizable edge(315, 314).
0.9::edge(413, 314).
optimizable edge(315, 315).
0.9::edge(322, 315).
optimizable edge(343, 315).
0.9::edge(386, 315).
optimizable edge(316, 316).
0.9::edge(380, 316).
optimizable edge(420, 316).
0.9::edge(317, 317).
optimizable edge(377, 317).
0.9::edge(432, 317).
optimizable edge(318, 318).
0.9::edge(359, 318).
optimizable edge(361, 318).
0.9::edge(395, 318).
optimizable edge(423, 318).
0.9::edge(319, 319).
optimizable edge(324, 319).
0.9::edge(442, 319).
optimizable edge(320, 320).
0.9::edge(336, 320).
optimizable edge(321, 321).
0.9::edge(418, 321).
optimizable edge(322, 322).
0.9::edge(377, 322).
optimizable edge(386, 322).
0.9::edge(430, 322).
optimizable edge(323, 323).
0.9::edge(393, 323).
optimizable edge(456, 323).
0.9::edge(324, 324).
optimizable edge(389, 324).
0.9::edge(417, 324).
optimizable edge(325, 325).
0.9::edge(331, 325).
optimizable edge(429, 325).
0.9::edge(326, 326).
optimizable edge(332, 326).
0.9::edge(458, 326).
optimizable edge(327, 327).
0.9::edge(357, 327).
optimizable edge(579, 327).
0.9::edge(328, 328).
optimizable edge(437, 328).
0.9::edge(329, 329).
optimizable edge(429, 329).
0.9::edge(330, 330).
optimizable edge(433, 330).
0.9::edge(331, 331).
optimizable edge(418, 331).
0.9::edge(435, 331).
optimizable edge(332, 332).
0.9::edge(367, 332).
optimizable edge(333, 333).
0.9::edge(334, 333).
optimizable edge(335, 333).
0.9::edge(376, 333).
optimizable edge(334, 334).
0.9::edge(385, 334).
optimizable edge(414, 334).
0.9::edge(335, 335).
optimizable edge(385, 335).
0.9::edge(414, 335).
optimizable edge(336, 336).
0.9::edge(386, 336).
optimizable edge(337, 337).
0.9::edge(388, 337).
optimizable edge(429, 337).
0.9::edge(448, 337).
optimizable edge(338, 338).
0.9::edge(340, 338).
optimizable edge(400, 338).
0.9::edge(432, 338).
optimizable edge(339, 339).
0.9::edge(340, 339).
optimizable edge(400, 339).
0.9::edge(340, 340).
optimizable edge(341, 341).
0.9::edge(342, 342).
optimizable edge(343, 343).
0.9::edge(377, 343).
optimizable edge(432, 343).
0.9::edge(344, 344).
optimizable edge(503, 344).
0.9::edge(345, 345).
optimizable edge(406, 345).
0.9::edge(416, 345).
optimizable edge(442, 345).
0.9::edge(346, 346).
optimizable edge(404, 346).
0.9::edge(347, 347).
optimizable edge(383, 347).
0.9::edge(445, 347).
optimizable edge(448, 347).
0.9::edge(348, 348).
optimizable edge(351, 348).
0.9::edge(356, 348).
optimizable edge(394, 348).
0.9::edge(492, 348).
optimizable edge(349, 349).
0.9::edge(350, 349).
optimizable edge(398, 349).
0.9::edge(350, 350).
optimizable edge(382, 350).
0.9::edge(401, 350).
optimizable edge(351, 351).
0.9::edge(426, 351).
optimizable edge(352, 352).
0.9::edge(375, 352).
optimizable edge(401, 352).
0.9::edge(421, 352).
optimizable edge(353, 353).
0.9::edge(424, 353).
optimizable edge(572, 353).
0.9::edge(354, 354).
optimizable edge(370, 354).
0.9::edge(355, 355).
optimizable edge(358, 355).
0.9::edge(427, 355).
optimizable edge(356, 356).
0.9::edge(403, 356).
optimizable edge(357, 357).
0.9::edge(384, 357).
optimizable edge(389, 357).
0.9::edge(358, 358).
optimizable edge(404, 358).
0.9::edge(359, 359).
optimizable edge(360, 359).
0.9::edge(385, 359).
optimizable edge(423, 359).
0.9::edge(360, 360).
optimizable edge(385, 360).
0.9::edge(388, 360).
optimizable edge(361, 361).
0.9::edge(362, 361).
optimizable edge(415, 361).
0.9::edge(362, 362).
optimizable edge(363, 363).
0.9::edge(364, 363).
optimizable edge(385, 363).
0.9::edge(415, 363).
optimizable edge(364, 364).
0.9::edge(365, 365).
optimizable edge(423, 365).
0.9::edge(436, 365).
optimizable edge(366, 366).
0.9::edge(423, 366).
optimizable edge(367, 367).
0.9::edge(368, 367).
optimizable edge(390, 367).
0.9::edge(368, 368).
optimizable edge(369, 369).
0.9::edge(409, 369).
optimizable edge(414, 369).
0.9::edge(370, 370).
optimizable edge(410, 370).
0.9::edge(427, 370).
optimizable edge(371, 371).
0.9::edge(382, 371).
optimizable edge(372, 372).
0.9::edge(399, 372).
optimizable edge(373, 373).
0.9::edge(375, 373).
optimizable edge(421, 373).
0.9::edge(374, 374).
optimizable edge(375, 374).
0.9::edge(421, 374).
optimizable edge(375, 375).
0.9::edge(376, 376).
optimizable edge(414, 376).
0.9::edge(377, 377).
optimizable edge(378, 378).
0.9::edge(434, 378).
optimizable edge(648, 378).
0.9::edge(379, 379).
optimizable edge(441, 379).
0.9::edge(650, 379).
optimizable edge(380, 380).
0.9::edge(428, 380).
optimizable edge(381, 381).
0.9::edge(382, 381).
optimizable edge(431, 381).
0.9::edge(382, 382).
optimizable edge(398, 382).
0.9::edge(419, 382).
optimizable edge(443, 382).
0.9::edge(383, 383).
optimizable edge(402, 383).
0.9::edge(435, 383).
optimizable edge(384, 384).
0.9::edge(417, 384).
optimizable edge(385, 385).
0.9::edge(386, 385).
optimizable edge(387, 385).
0.9::edge(395, 385).
optimizable edge(386, 386).
0.9::edge(387, 387).
optimizable edge(388, 388).
0.9::edge(406, 388).
optimizable edge(442, 388).
0.9::edge(389, 389).
optimizable edge(390, 390).
0.9::edge(391, 390).
optimizable edge(417, 390).
0.9::edge(391, 391).
optimizable edge(392, 392).
0.9::edge(442, 392).
optimizable edge(393, 393).
0.9::edge(394, 393).
optimizable edge(408, 393).
0.9::edge(413, 393).
optimizable edge(441, 393).
0.9::edge(566, 393).
optimizable edge(394, 394).
0.9::edge(428, 394).
optimizable edge(395, 395).
0.9::edge(409, 395).
optimizable edge(414, 395).
0.9::edge(396, 396).
optimizable edge(410, 396).
0.9::edge(397, 397).
optimizable edge(398, 397).
0.9::edge(399, 397).
optimizable edge(400, 397).
0.9::edge(398, 398).
optimizable edge(401, 398).
0.9::edge(432, 398).
optimizable edge(399, 399).
0.9::edge(429, 399).
optimizable edge(400, 400).
0.9::edge(430, 400).
optimizable edge(401, 401).
0.9::edge(421, 401).
optimizable edge(402, 402).
0.9::edge(422, 402).
optimizable edge(403, 403).
0.9::edge(424, 403).
optimizable edge(404, 404).
0.9::edge(405, 405).
optimizable edge(421, 405).
0.9::edge(406, 406).
optimizable edge(416, 406).
0.9::edge(407, 407).
optimizable edge(408, 408).
0.9::edge(411, 408).
optimizable edge(413, 408).
0.9::edge(566, 408).
optimizable edge(618, 408).
0.9::edge(409, 409).
optimizable edge(410, 409).
0.9::edge(410, 410).
optimizable edge(412, 410).
0.9::edge(414, 410).
optimizable edge(411, 411).
0.9::edge(412, 412).
optimizable edge(415, 412).
0.9::edge(413, 413).
optimizable edge(414, 413).
0.9::edge(414, 414).
optimizable edge(425, 414).
0.9::edge(415, 415).
optimizable edge(416, 416).
0.9::edge(442, 416).
optimizable edge(417, 417).
0.9::edge(435, 417).
optimizable edge(437, 417).
0.9::edge(438, 417).
optimizable edge(418, 418).
0.9::edge(419, 419).
optimizable edge(420, 420).
0.9::edge(421, 421).
optimizable edge(422, 421).
0.9::edge(443, 421).
optimizable edge(422, 422).
0.9::edge(445, 422).
optimizable edge(423, 423).
0.9::edge(424, 424).
optimizable edge(425, 425).
0.9::edge(426, 426).
optimizable edge(427, 426).
0.9::edge(427, 427).
optimizable edge(428, 428).
0.9::edge(429, 429).
optimizable edge(430, 429).
0.9::edge(430, 430).
optimizable edge(431, 431).
0.9::edge(432, 431).
optimizable edge(613, 431).
0.9::edge(432, 432).
optimizable edge(433, 432).
0.9::edge(433, 433).
optimizable edge(434, 434).
0.9::edge(435, 434).
optimizable edge(571, 434).
0.9::edge(435, 435).
optimizable edge(438, 435).
0.9::edge(436, 436).
optimizable edge(437, 437).
0.9::edge(439, 437).
optimizable edge(438, 438).
0.9::edge(439, 438).
optimizable edge(439, 439).
0.9::edge(440, 439).
optimizable edge(440, 440).
0.9::edge(441, 441).
optimizable edge(442, 441).
0.9::edge(442, 442).
optimizable edge(443, 443).
0.9::edge(444, 443).
optimizable edge(444, 444).
0.9::edge(445, 445).
optimizable edge(446, 445).
0.9::edge(446, 446).
optimizable edge(447, 446).
0.9::edge(447, 447).
optimizable edge(448, 448).
0.9::edge(449, 449).
optimizable edge(502, 449).
0.9::edge(562, 449).
optimizable edge(450, 450).
0.9::edge(525, 450).
optimizable edge(533, 450).
0.9::edge(451, 451).
optimizable edge(484, 451).
0.9::edge(500, 451).
optimizable edge(452, 452).
0.9::edge(466, 452).
optimizable edge(486, 452).
0.9::edge(453, 453).
optimizable edge(555, 453).
0.9::edge(573, 453).
optimizable edge(454, 454).
0.9::edge(533, 454).
optimizable edge(553, 454).
0.9::edge(559, 454).
optimizable edge(455, 455).
0.9::edge(561, 455).
optimizable edge(584, 455).
0.9::edge(456, 456).
optimizable edge(457, 456).
0.9::edge(518, 456).
optimizable edge(544, 456).
0.9::edge(564, 456).
optimizable edge(571, 456).
0.9::edge(457, 457).
optimizable edge(508, 457).
0.9::edge(523, 457).
optimizable edge(536, 457).
0.9::edge(552, 457).
optimizable edge(557, 457).
0.9::edge(575, 457).
optimizable edge(458, 458).
0.9::edge(585, 458).
optimizable edge(459, 459).
0.9::edge(534, 459).
optimizable edge(580, 459).
0.9::edge(460, 460).
optimizable edge(510, 460).
0.9::edge(578, 460).
optimizable edge(461, 461).
0.9::edge(536, 461).
optimizable edge(560, 461).
0.9::edge(462, 462).
optimizable edge(470, 462).
0.9::edge(479, 462).
optimizable edge(463, 463).
0.9::edge(575, 463).
optimizable edge(464, 464).
0.9::edge(475, 464).
optimizable edge(508, 464).
0.9::edge(558, 464).
optimizable edge(565, 464).
0.9::edge(582, 464).
optimizable edge(465, 465).
0.9::edge(476, 465).
optimizable edge(535, 465).
0.9::edge(556, 465).
optimizable edge(585, 465).
0.9::edge(466, 466).
optimizable edge(569, 466).
0.9::edge(467, 467).
optimizable edge(482, 467).
0.9::edge(545, 467).
optimizable edge(468, 468).
0.9::edge(546, 468).
optimizable edge(469, 469).
0.9::edge(482, 469).
optimizable edge(524, 469).
0.9::edge(528, 469).
optimizable edge(470, 470).
0.9::edge(475, 470).
optimizable edge(557, 470).
0.9::edge(471, 471).
optimizable edge(514, 471).
0.9::edge(543, 471).
optimizable edge(548, 471).
0.9::edge(549, 471).
optimizable edge(576, 471).
0.9::edge(579, 471).
optimizable edge(472, 472).
0.9::edge(511, 472).
optimizable edge(562, 472).
0.9::edge(583, 472).
optimizable edge(473, 473).
0.9::edge(489, 473).
optimizable edge(501, 473).
0.9::edge(474, 474).
optimizable edge(490, 474).
0.9::edge(493, 474).
optimizable edge(504, 474).
0.9::edge(563, 474).
optimizable edge(569, 474).
0.9::edge(475, 475).
optimizable edge(500, 475).
0.9::edge(558, 475).
optimizable edge(565, 475).
0.9::edge(476, 476).
optimizable edge(543, 476).
0.9::edge(477, 477).
optimizable edge(529, 477).
0.9::edge(570, 477).
optimizable edge(478, 478).
0.9::edge(489, 478).
optimizable edge(505, 478).
0.9::edge(517, 478).
optimizable edge(521, 478).
0.9::edge(527, 478).
optimizable edge(479, 479).
0.9::edge(480, 479).
optimizable edge(480, 480).
0.9::edge(491, 480).
optimizable edge(497, 480).
0.9::edge(500, 480).
optimizable edge(537, 480).
0.9::edge(565, 480).
optimizable edge(582, 480).
0.9::edge(584, 480).
optimizable edge(481, 481).
0.9::edge(536, 481).
optimizable edge(482, 482).
0.9::edge(488, 482).
optimizable edge(512, 482).
0.9::edge(521, 482).
optimizable edge(542, 482).
0.9::edge(483, 483).
optimizable edge(515, 483).
0.9::edge(517, 483).
optimizable edge(484, 484).
0.9::edge(497, 484).
optimizable edge(507, 484).
0.9::edge(529, 484).
optimizable edge(485, 485).
0.9::edge(519, 485).
optimizable edge(540, 485).
0.9::edge(486, 486).
optimizable edge(496, 486).
0.9::edge(487, 487).
optimizable edge(509, 487).
0.9::edge(567, 487).
optimizable edge(488, 488).
0.9::edge(516, 488).
optimizable edge(519, 488).
0.9::edge(576, 488).
optimizable edge(489, 489).
0.9::edge(545, 489).
optimizable edge(567, 489).
0.9::edge(580, 489).
optimizable edge(490, 490).
0.9::edge(491, 491).
optimizable edge(513, 491).
0.9::edge(492, 492).
optimizable edge(509, 492).
0.9::edge(493, 493).
optimizable edge(578, 493).
0.9::edge(494, 494).
optimizable edge(575, 494).
0.9::edge(495, 495).
optimizable edge(502, 495).
0.9::edge(546, 495).
optimizable edge(496, 496).
0.9::edge(539, 496).
optimizable edge(497, 497).
0.9::edge(498, 498).
optimizable edge(506, 498).
0.9::edge(509, 498).
optimizable edge(499, 499).
0.9::edge(549, 499).
optimizable edge(575, 499).
0.9::edge(500, 500).
optimizable edge(539, 500).
0.9::edge(582, 500).
optimizable edge(501, 501).
0.9::edge(509, 501).
optimizable edge(517, 501).
0.9::edge(502, 502).
optimizable edge(522, 502).
0.9::edge(554, 502).
optimizable edge(503, 503).
0.9::edge(519, 503).
optimizable edge(504, 504).
0.9::edge(555, 504).
optimizable edge(505, 505).
0.9::edge(538, 505).
optimizable edge(506, 506).
0.9::edge(540, 506).
optimizable edge(507, 507).
0.9::edge(508, 508).
optimizable edge(509, 509).
0.9::edge(567, 509).
optimizable edge(586, 509).
0.9::edge(510, 510).
optimizable edge(533, 510).
0.9::edge(511, 511).
optimizable edge(570, 511).
0.9::edge(512, 512).
optimizable edge(580, 512).
0.9::edge(513, 513).
optimizable edge(584, 513).
0.9::edge(514, 514).
optimizable edge(575, 514).
0.9::edge(515, 515).
optimizable edge(559, 515).
0.9::edge(516, 516).
optimizable edge(575, 516).
0.9::edge(517, 517).
optimizable edge(555, 517).
0.9::edge(572, 517).
optimizable edge(518, 518).
0.9::edge(526, 518).
optimizable edge(564, 518).
0.9::edge(566, 518).
optimizable edge(519, 519).
0.9::edge(520, 520).
optimizable edge(555, 520).
0.9::edge(569, 520).
optimizable edge(521, 521).
0.9::edge(522, 522).
optimizable edge(533, 522).
0.9::edge(523, 523).
optimizable edge(532, 523).
0.9::edge(524, 524).
optimizable edge(525, 525).
0.9::edge(573, 525).
optimizable edge(526, 526).
0.9::edge(564, 526).
optimizable edge(527, 527).
0.9::edge(528, 528).
optimizable edge(539, 528).
0.9::edge(558, 528).
optimizable edge(529, 529).
0.9::edge(530, 530).
optimizable edge(531, 530).
0.9::edge(562, 530).
optimizable edge(531, 531).
0.9::edge(532, 532).
optimizable edge(574, 532).
0.9::edge(533, 533).
optimizable edge(534, 534).
0.9::edge(555, 534).
optimizable edge(535, 535).
0.9::edge(536, 536).
optimizable edge(552, 536).
0.9::edge(574, 536).
optimizable edge(575, 536).
0.9::edge(537, 537).
optimizable edge(538, 538).
0.9::edge(555, 538).
optimizable edge(539, 539).
0.9::edge(540, 540).
optimizable edge(541, 541).
0.9::edge(550, 541).
optimizable edge(573, 541).
0.9::edge(542, 542).
optimizable edge(543, 543).
0.9::edge(544, 544).
optimizable edge(651, 544).
0.9::edge(659, 544).
optimizable edge(545, 545).
0.9::edge(546, 546).
optimizable edge(547, 547).
0.9::edge(551, 547).
optimizable edge(553, 547).
0.9::edge(548, 548).
optimizable edge(585, 548).
0.9::edge(549, 549).
optimizable edge(550, 550).
0.9::edge(551, 551).
optimizable edge(554, 551).
0.9::edge(552, 552).
optimizable edge(553, 553).
0.9::edge(554, 554).
optimizable edge(555, 555).
0.9::edge(581, 555).
optimizable edge(556, 556).
0.9::edge(585, 556).
optimizable edge(557, 557).
0.9::edge(558, 558).
optimizable edge(559, 559).
0.9::edge(560, 560).
optimizable edge(582, 560).
0.9::edge(561, 561).
optimizable edge(570, 561).
0.9::edge(562, 562).
optimizable edge(563, 563).
0.9::edge(564, 564).
optimizable edge(565, 564).
0.9::edge(565, 565).
optimizable edge(582, 565).
0.9::edge(566, 566).
optimizable edge(567, 566).
0.9::edge(568, 566).
optimizable edge(567, 567).
0.9::edge(572, 567).
optimizable edge(581, 567).
0.9::edge(568, 568).
optimizable edge(569, 568).
0.9::edge(569, 569).
optimizable edge(577, 569).
0.9::edge(570, 570).
optimizable edge(583, 570).
0.9::edge(571, 571).
optimizable edge(572, 572).
0.9::edge(573, 573).
optimizable edge(574, 574).
0.9::edge(575, 575).
optimizable edge(576, 576).
0.9::edge(577, 577).
optimizable edge(581, 577).
0.9::edge(578, 578).
optimizable edge(579, 579).
0.9::edge(580, 580).
optimizable edge(581, 581).
0.9::edge(586, 581).
optimizable edge(582, 582).
0.9::edge(583, 583).
optimizable edge(584, 584).
0.9::edge(585, 585).
optimizable edge(586, 586).
0.9::edge(587, 587).
optimizable edge(590, 587).
0.9::edge(628, 587).
optimizable edge(588, 588).
0.9::edge(595, 588).
optimizable edge(608, 588).
0.9::edge(589, 589).
optimizable edge(610, 589).
0.9::edge(640, 589).
optimizable edge(590, 590).
0.9::edge(591, 590).
optimizable edge(608, 590).
0.9::edge(632, 590).
optimizable edge(634, 590).
0.9::edge(591, 591).
optimizable edge(662, 591).
0.9::edge(592, 592).
optimizable edge(604, 592).
0.9::edge(593, 593).
optimizable edge(594, 593).
0.9::edge(596, 593).
optimizable edge(601, 593).
0.9::edge(619, 593).
optimizable edge(628, 593).
0.9::edge(647, 593).
optimizable edge(594, 594).
0.9::edge(642, 594).
optimizable edge(646, 594).
0.9::edge(595, 595).
optimizable edge(628, 595).
0.9::edge(632, 595).
optimizable edge(639, 595).
0.9::edge(596, 596).
optimizable edge(619, 596).
0.9::edge(643, 596).
optimizable edge(597, 597).
0.9::edge(598, 597).
optimizable edge(602, 597).
0.9::edge(610, 597).
optimizable edge(635, 597).
0.9::edge(598, 598).
optimizable edge(602, 598).
0.9::edge(610, 598).
optimizable edge(611, 598).
0.9::edge(623, 598).
optimizable edge(627, 598).
0.9::edge(635, 598).
optimizable edge(639, 598).
0.9::edge(599, 599).
optimizable edge(606, 599).
0.9::edge(614, 599).
optimizable edge(600, 600).
0.9::edge(612, 600).
optimizable edge(629, 600).
0.9::edge(601, 601).
optimizable edge(628, 601).
0.9::edge(633, 601).
optimizable edge(602, 602).
0.9::edge(603, 603).
optimizable edge(609, 603).
0.9::edge(604, 604).
optimizable edge(611, 604).
0.9::edge(640, 604).
optimizable edge(605, 605).
0.9::edge(626, 605).
optimizable edge(629, 605).
0.9::edge(633, 605).
optimizable edge(606, 606).
0.9::edge(622, 606).
optimizable edge(607, 607).
0.9::edge(616, 607).
optimizable edge(626, 607).
0.9::edge(638, 607).
optimizable edge(608, 608).
0.9::edge(628, 608).
optimizable edge(609, 609).
0.9::edge(633, 609).
optimizable edge(610, 610).
0.9::edge(611, 610).
optimizable edge(627, 610).
0.9::edge(640, 610).
optimizable edge(611, 611).
0.9::edge(640, 611).
optimizable edge(612, 612).
0.9::edge(631, 612).
optimizable edge(613, 613).
0.9::edge(614, 613).
optimizable edge(644, 613).
0.9::edge(614, 614).
optimizable edge(615, 615).
0.9::edge(622, 615).
optimizable edge(642, 615).
0.9::edge(646, 615).
optimizable edge(616, 616).
0.9::edge(617, 616).
optimizable edge(617, 617).
0.9::edge(639, 617).
optimizable edge(618, 618).
0.9::edge(619, 618).
optimizable edge(619, 619).
0.9::edge(620, 619).
optimizable edge(645, 619).
0.9::edge(620, 620).
optimizable edge(621, 621).
0.9::edge(622, 621).
optimizable edge(643, 621).
0.9::edge(622, 622).
optimizable edge(623, 623).
0.9::edge(624, 624).
optimizable edge(640, 624).
0.9::edge(661, 624).
optimizable edge(625, 625).
0.9::edge(639, 625).
optimizable edge(626, 626).
0.9::edge(631, 626).
optimizable edge(627, 627).
0.9::edge(628, 628).
optimizable edge(630, 628).
0.9::edge(632, 628).
optimizable edge(629, 629).
0.9::edge(639, 629).
optimizable edge(630, 630).
0.9::edge(631, 631).
optimizable edge(632, 632).
0.9::edge(633, 633).
optimizable edge(634, 634).
0.9::edge(660, 634).
optimizable edge(635, 635).
0.9::edge(636, 636).
optimizable edge(637, 636).
0.9::edge(638, 636).
optimizable edge(637, 637).
0.9::edge(638, 638).
optimizable edge(639, 639).
0.9::edge(640, 640).
optimizable edge(641, 640).
0.9::edge(641, 641).
optimizable edge(642, 642).
0.9::edge(643, 643).
optimizable edge(644, 643).
0.9::edge(644, 644).
optimizable edge(645, 645).
0.9::edge(646, 645).
optimizable edge(647, 645).
0.9::edge(646, 646).
optimizable edge(647, 647).
0.9::edge(648, 648).
optimizable edge(649, 648).
0.9::edge(655, 648).
optimizable edge(649, 649).
0.9::edge(650, 650).
optimizable edge(655, 650).
0.9::edge(651, 651).
optimizable edge(652, 651).
0.9::edge(652, 652).
optimizable edge(656, 652).
0.9::edge(657, 652).
optimizable edge(659, 652).
0.9::edge(653, 653).
optimizable edge(654, 653).
0.9::edge(656, 653).
optimizable edge(658, 653).
0.9::edge(659, 653).
optimizable edge(660, 653).
0.9::edge(662, 653).
optimizable edge(654, 654).
0.9::edge(655, 654).
optimizable edge(656, 654).
0.9::edge(657, 654).
optimizable edge(658, 654).
0.9::edge(655, 655).
optimizable edge(658, 655).
0.9::edge(660, 655).
optimizable edge(661, 655).
0.9::edge(662, 655).
optimizable edge(656, 656).
0.9::edge(658, 656).
optimizable edge(660, 656).
0.9::edge(661, 656).
optimizable edge(657, 657).
0.9::edge(658, 657).
optimizable edge(659, 657).
0.9::edge(658, 658).
optimizable edge(659, 658).
0.9::edge(662, 658).
optimizable edge(659, 659).
0.9::edge(660, 660).
optimizable edge(661, 660).
0.9::edge(662, 660).
optimizable edge(661, 661).
0.9::edge(662, 661).
optimizable edge(662, 662).

path(X, X).
path(X, Y):- path(X, Z), edge(Z, Y).



:- end_lpad.

run:- run_mma, run_ccsaq, run_slsqp.

run_mma(S,D):-
	statistics(runtime, [Start | _]), 
	prob_optimize(path(S,D),[edge(222,1) +edge(2,2) +edge(228,2) +edge(3,3) +edge(166,3) +edge(126,4) +edge(5,5) +edge(228,5) +edge(109,6) +edge(7,7) +edge(65,7) +edge(38,8) +edge(119,8) +edge(211,8) +edge(9,9) +edge(10,10) +edge(106,10) +edge(303,10) +edge(122,11) +edge(12,12) +edge(80,12) +edge(90,13) +edge(14,14) +edge(245,14) +edge(24,15) +edge(137,15) +edge(304,15) +edge(57,16) +edge(17,17) +edge(231,17) +edge(251,18) +edge(19,19) +edge(236,19) +edge(122,20) +edge(21,21) +edge(273,21) +edge(27,22) +edge(23,23) +edge(89,23) +edge(187,24) +edge(158,25) +edge(289,25) +edge(26,26) +edge(250,26) +edge(69,27) +edge(33,28) +edge(29,29) +edge(238,29) +edge(149,30) +edge(31,31) +edge(97,31) +edge(32,32) +edge(271,32) +edge(228,33) +edge(48,34) +edge(166,34) +edge(196,34) +edge(35,35) +edge(36,36) +edge(281,36) +edge(68,37) +edge(38,38) +edge(39,39) +edge(267,39) +edge(124,40) +edge(41,41) +edge(214,41) +edge(264,42) +edge(264,43) +edge(44,44) +edge(75,44) +edge(188,44) +edge(210,45) +edge(46,46) +edge(277,46) +edge(182,47) +edge(48,48) +edge(49,49) +edge(50,50) +edge(185,50) +edge(168,51) +edge(52,52) +edge(272,52) +edge(318,53) +edge(108,54) +edge(206,55) +edge(56,56) +edge(149,56) +edge(176,56) +edge(192,57) +edge(125,58) +edge(238,58) +edge(319,58) +edge(108,59) +edge(60,60) +edge(61,61) +edge(224,61) +edge(152,62) +edge(63,63) +edge(184,63) +edge(322,64) +edge(143,65) +edge(203,66) +edge(67,67) +edge(68,68) +edge(69,69) +edge(262,69) +edge(70,70) +edge(198,70) +edge(75,71) +edge(72,72) +edge(142,72) +edge(129,73) +edge(74,74) +edge(113,74) +edge(217,74) +edge(241,75) +edge(76,76) +edge(255,76) +edge(118,77) +edge(142,78) +edge(79,79) +edge(294,79) +edge(81,81) +edge(82,82) +edge(252,82) +edge(124,83) +edge(118,84) +edge(85,85) +edge(196,85) +edge(201,86) +edge(331,87) +edge(161,88) +edge(89,89) +edge(90,90) +edge(91,91) +edge(92,92) +edge(93,93) +edge(264,93) +edge(167,94) +edge(95,95) +edge(96,96) +edge(97,97) +edge(98,98) +edge(288,98) +edge(210,99) +edge(101,101) +edge(233,101) +edge(229,102) +edge(103,103) +edge(104,104) +edge(105,105) +edge(106,106) +edge(107,107) +edge(193,107) +edge(108,108) +edge(202,108) +edge(109,109) +edge(110,110) +edge(189,110) +edge(219,111) +edge(112,112) +edge(284,112) +edge(265,113) +edge(345,113) +edge(171,114) +edge(115,115) +edge(194,115) +edge(129,116) +edge(117,117) +edge(347,118) +edge(120,119) +edge(120,120) +edge(121,121) +edge(122,122) +edge(155,122) +edge(123,123) +edge(262,123) +edge(239,124) +edge(156,125) +edge(168,126) +edge(349,127) +edge(155,128) +edge(129,129) +edge(144,130) +edge(131,131) +edge(281,131) +edge(287,132) +edge(276,133) +edge(135,135) +edge(277,135) +edge(137,137) +edge(138,138) +edge(261,138) +edge(292,139) +edge(192,140) +edge(223,141) +edge(357,142) +edge(215,143) +edge(358,143) +edge(145,145) +edge(146,146) +edge(224,146) +edge(147,147) +edge(369,147) +edge(220,148) +edge(259,148) +edge(149,149) +edge(224,150) +edge(151,151) +edge(152,152) +edge(376,153) +edge(294,154) +edge(156,156) +edge(206,156) +edge(278,156) +edge(157,157) +edge(158,158) +edge(159,159) +edge(233,159) +edge(193,160) +edge(161,161) +edge(162,162) +edge(163,163) +edge(280,163) +edge(382,164) +edge(209,165) +edge(346,165) +edge(167,167) +edge(169,169) +edge(260,169) +edge(229,170) +edge(186,171) +edge(383,171) +edge(222,172) +edge(173,173) +edge(174,174) +edge(175,175) +edge(176,176) +edge(177,177) +edge(178,178) +edge(249,178) +edge(388,179) +edge(207,180) +edge(207,181) +edge(208,182) +edge(191,183) +edge(184,184) +edge(185,185) +edge(186,186) +edge(187,187) +edge(188,188) +edge(192,189) +edge(230,190) +edge(192,192) +edge(193,193) +edge(195,195) +edge(196,196) +edge(198,198) +edge(199,199) +edge(284,199) +edge(200,200) +edge(244,200) +edge(202,202) +edge(203,203) +edge(244,203) +edge(204,204) +edge(281,204) +edge(253,205) +edge(258,206) +edge(392,206) +edge(208,208) +edge(209,209) +edge(211,211) +edge(287,212) +edge(231,213) +edge(215,215) +edge(266,216) +edge(218,218) +edge(219,219) +edge(220,220) +edge(396,220) +edge(222,222) +edge(224,224) +edge(401,225) +edge(245,226) +edge(228,228) +edge(229,229) +edge(402,229) +edge(231,230) +edge(403,231) +edge(233,233) +edge(234,234) +edge(268,234) +edge(235,235) +edge(236,236) +edge(237,237) +edge(239,239) +edge(241,241) +edge(268,242) +edge(243,243) +edge(244,244) +edge(254,245) +edge(246,246) +edge(269,247) +edge(414,248) +edge(415,249) +edge(282,250) +edge(252,252) +edge(253,253) +edge(417,253) +edge(255,255) +edge(418,256) +edge(419,257) +edge(259,259) +edge(261,261) +edge(263,263) +edge(264,264) +edge(265,265) +edge(267,267) +edge(269,269) +edge(271,271) +edge(425,272) +edge(274,274) +edge(275,275) +edge(277,277) +edge(278,278) +edge(285,278) +edge(429,279) +edge(281,281) +edge(283,283) +edge(285,285) +edge(287,287) +edge(289,289) +edge(291,291) +edge(293,293) +edge(294,294) +edge(295,295) +edge(297,297) +edge(298,298) +edge(313,298) +edge(299,299) +edge(336,299) +edge(343,299) +edge(301,301) +edge(302,302) +edge(303,303) +edge(424,303) +edge(346,304) +edge(345,305) +edge(394,305) +edge(420,305) +edge(436,305) +edge(307,306) +edge(309,306) +edge(379,306) +edge(307,307) +edge(308,308) +edge(421,308) +edge(321,309) +edge(328,309) +edge(341,309) +edge(422,309) +edge(311,310) +edge(370,310) +edge(426,310) +edge(645,311) +edge(313,313) +edge(315,314) +edge(315,315) +edge(343,315) +edge(316,316) +edge(420,316) +edge(377,317) +edge(318,318) +edge(361,318) +edge(423,318) +edge(324,319) +edge(320,320) +edge(321,321) +edge(322,322) +edge(386,322) +edge(323,323) +edge(456,323) +edge(389,324) +edge(325,325) +edge(429,325) +edge(332,326) +edge(327,327) +edge(579,327) +edge(437,328) +edge(429,329) +edge(433,330) +edge(418,331) +edge(332,332) +edge(333,333) +edge(335,333) +edge(334,334) +edge(414,334) +edge(385,335) +edge(336,336) +edge(337,337) +edge(429,337) +edge(338,338) +edge(400,338) +edge(339,339) +edge(400,339) +edge(341,341) +edge(343,343) +edge(432,343) +edge(503,344) +edge(406,345) +edge(442,345) +edge(404,346) +edge(383,347) +edge(448,347) +edge(351,348) +edge(394,348) +edge(349,349) +edge(398,349) +edge(382,350) +edge(351,351) +edge(352,352) +edge(401,352) +edge(353,353) +edge(572,353) +edge(370,354) +edge(358,355) +edge(356,356) +edge(357,357) +edge(389,357) +edge(404,358) +edge(360,359) +edge(423,359) +edge(385,360) +edge(361,361) +edge(415,361) +edge(363,363) +edge(385,363) +edge(364,364) +edge(423,365) +edge(366,366) +edge(367,367) +edge(390,367) +edge(369,369) +edge(414,369) +edge(410,370) +edge(371,371) +edge(372,372) +edge(373,373) +edge(421,373) +edge(375,374) +edge(375,375) +edge(414,376) +edge(378,378) +edge(648,378) +edge(441,379) +edge(380,380) +edge(381,381) +edge(431,381) +edge(398,382) +edge(443,382) +edge(402,383) +edge(384,384) +edge(385,385) +edge(387,385) +edge(386,386) +edge(388,388) +edge(442,388) +edge(390,390) +edge(417,390) +edge(392,392) +edge(393,393) +edge(408,393) +edge(441,393) +edge(394,394) +edge(395,395) +edge(414,395) +edge(410,396) +edge(398,397) +edge(400,397) +edge(401,398) +edge(399,399) +edge(400,400) +edge(401,401) +edge(402,402) +edge(403,403) +edge(404,404) +edge(421,405) +edge(416,406) +edge(408,408) +edge(413,408) +edge(618,408) +edge(410,409) +edge(412,410) +edge(411,411) +edge(415,412) +edge(414,413) +edge(425,414) +edge(416,416) +edge(417,417) +edge(437,417) +edge(418,418) +edge(420,420) +edge(422,421) +edge(422,422) +edge(423,423) +edge(425,425) +edge(427,426) +edge(428,428) +edge(430,429) +edge(431,431) +edge(613,431) +edge(433,432) +edge(434,434) +edge(571,434) +edge(438,435) +edge(437,437) +edge(438,438) +edge(439,439) +edge(440,440) +edge(442,441) +edge(443,443) +edge(444,444) +edge(446,445) +edge(447,446) +edge(448,448) +edge(502,449) +edge(450,450) +edge(533,450) +edge(484,451) +edge(452,452) +edge(486,452) +edge(555,453) +edge(454,454) +edge(553,454) +edge(455,455) +edge(584,455) +edge(457,456) +edge(544,456) +edge(571,456) +edge(508,457) +edge(536,457) +edge(557,457) +edge(458,458) +edge(459,459) +edge(580,459) +edge(510,460) +edge(461,461) +edge(560,461) +edge(470,462) +edge(463,463) +edge(464,464) +edge(508,464) +edge(565,464) +edge(465,465) +edge(535,465) +edge(585,465) +edge(569,466) +edge(482,467) +edge(468,468) +edge(469,469) +edge(524,469) +edge(470,470) +edge(557,470) +edge(514,471) +edge(548,471) +edge(576,471) +edge(472,472) +edge(562,472) +edge(473,473) +edge(501,473) +edge(490,474) +edge(504,474) +edge(569,474) +edge(500,475) +edge(565,475) +edge(543,476) +edge(529,477) +edge(478,478) +edge(505,478) +edge(521,478) +edge(479,479) +edge(480,480) +edge(497,480) +edge(537,480) +edge(582,480) +edge(481,481) +edge(482,482) +edge(512,482) +edge(542,482) +edge(515,483) +edge(484,484) +edge(507,484) +edge(485,485) +edge(540,485) +edge(496,486) +edge(509,487) +edge(488,488) +edge(519,488) +edge(489,489) +edge(567,489) +edge(490,490) +edge(513,491) +edge(509,492) +edge(578,493) +edge(575,494) +edge(502,495) +edge(496,496) +edge(497,497) +edge(506,498) +edge(499,499) +edge(575,499) +edge(539,500) +edge(501,501) +edge(517,501) +edge(522,502) +edge(503,503) +edge(504,504) +edge(505,505) +edge(506,506) +edge(507,507) +edge(509,509) +edge(586,509) +edge(533,510) +edge(570,511) +edge(580,512) +edge(584,513) +edge(575,514) +edge(559,515) +edge(575,516) +edge(555,517) +edge(518,518) +edge(564,518) +edge(519,519) +edge(555,520) +edge(521,521) +edge(533,522) +edge(532,523) +edge(525,525) +edge(526,526) +edge(527,527) +edge(539,528) +edge(529,529) +edge(531,530) +edge(531,531) +edge(574,532) +edge(534,534) +edge(535,535) +edge(552,536) +edge(575,536) +edge(538,538) +edge(539,539) +edge(541,541) +edge(573,541) +edge(543,543) +edge(651,544) +edge(545,545) +edge(547,547) +edge(553,547) +edge(585,548) +edge(550,550) +edge(554,551) +edge(553,553) +edge(555,555) +edge(556,556) +edge(557,557) +edge(559,559) +edge(582,560) +edge(570,561) +edge(563,563) +edge(565,564) +edge(582,565) +edge(567,566) +edge(567,567) +edge(581,567) +edge(569,568) +edge(577,569) +edge(583,570) +edge(572,572) +edge(574,574) +edge(576,576) +edge(581,577) +edge(579,579) +edge(581,581) +edge(582,582) +edge(584,584) +edge(586,586) +edge(590,587) +edge(588,588) +edge(608,588) +edge(610,589) +edge(590,590) +edge(608,590) +edge(634,590) +edge(662,591) +edge(604,592) +edge(594,593) +edge(601,593) +edge(628,593) +edge(594,594) +edge(646,594) +edge(628,595) +edge(639,595) +edge(619,596) +edge(597,597) +edge(602,597) +edge(635,597) +edge(602,598) +edge(611,598) +edge(627,598) +edge(639,598) +edge(606,599) +edge(600,600) +edge(629,600) +edge(628,601) +edge(602,602) +edge(609,603) +edge(611,604) +edge(605,605) +edge(629,605) +edge(606,606) +edge(607,607) +edge(626,607) +edge(608,608) +edge(609,609) +edge(610,610) +edge(627,610) +edge(611,611) +edge(612,612) +edge(613,613) +edge(644,613) +edge(615,615) +edge(642,615) +edge(616,616) +edge(617,617) +edge(618,618) +edge(619,619) +edge(645,619) +edge(621,621) +edge(643,621) +edge(623,623) +edge(640,624) +edge(625,625) +edge(626,626) +edge(627,627) +edge(630,628) +edge(629,629) +edge(630,630) +edge(632,632) +edge(634,634) +edge(635,635) +edge(637,636) +edge(637,637) +edge(639,639) +edge(641,640) +edge(642,642) +edge(644,643) +edge(645,645) +edge(647,645) +edge(647,647) +edge(649,648) +edge(649,649) +edge(655,650) +edge(652,651) +edge(656,652) +edge(659,652) +edge(654,653) +edge(658,653) +edge(660,653) +edge(654,654) +edge(656,654) +edge(658,654) +edge(658,655) +edge(661,655) +edge(656,656) +edge(660,656) +edge(657,657) +edge(659,657) +edge(659,658) +edge(659,659) +edge(661,660) +edge(661,661) +edge(662,662) ],[path(S,D) > 0.8],mma,-1,A),
	statistics(runtime, [Stop | _]),
	Runtime is Stop - Start,
	writeln('mma'),
	writeln(Runtime),
	writeln(A).

run_ccsaq(S,D):-
	statistics(runtime, [Start | _]), 
	prob_optimize(path(S,D),[edge(222,1) +edge(2,2) +edge(228,2) +edge(3,3) +edge(166,3) +edge(126,4) +edge(5,5) +edge(228,5) +edge(109,6) +edge(7,7) +edge(65,7) +edge(38,8) +edge(119,8) +edge(211,8) +edge(9,9) +edge(10,10) +edge(106,10) +edge(303,10) +edge(122,11) +edge(12,12) +edge(80,12) +edge(90,13) +edge(14,14) +edge(245,14) +edge(24,15) +edge(137,15) +edge(304,15) +edge(57,16) +edge(17,17) +edge(231,17) +edge(251,18) +edge(19,19) +edge(236,19) +edge(122,20) +edge(21,21) +edge(273,21) +edge(27,22) +edge(23,23) +edge(89,23) +edge(187,24) +edge(158,25) +edge(289,25) +edge(26,26) +edge(250,26) +edge(69,27) +edge(33,28) +edge(29,29) +edge(238,29) +edge(149,30) +edge(31,31) +edge(97,31) +edge(32,32) +edge(271,32) +edge(228,33) +edge(48,34) +edge(166,34) +edge(196,34) +edge(35,35) +edge(36,36) +edge(281,36) +edge(68,37) +edge(38,38) +edge(39,39) +edge(267,39) +edge(124,40) +edge(41,41) +edge(214,41) +edge(264,42) +edge(264,43) +edge(44,44) +edge(75,44) +edge(188,44) +edge(210,45) +edge(46,46) +edge(277,46) +edge(182,47) +edge(48,48) +edge(49,49) +edge(50,50) +edge(185,50) +edge(168,51) +edge(52,52) +edge(272,52) +edge(318,53) +edge(108,54) +edge(206,55) +edge(56,56) +edge(149,56) +edge(176,56) +edge(192,57) +edge(125,58) +edge(238,58) +edge(319,58) +edge(108,59) +edge(60,60) +edge(61,61) +edge(224,61) +edge(152,62) +edge(63,63) +edge(184,63) +edge(322,64) +edge(143,65) +edge(203,66) +edge(67,67) +edge(68,68) +edge(69,69) +edge(262,69) +edge(70,70) +edge(198,70) +edge(75,71) +edge(72,72) +edge(142,72) +edge(129,73) +edge(74,74) +edge(113,74) +edge(217,74) +edge(241,75) +edge(76,76) +edge(255,76) +edge(118,77) +edge(142,78) +edge(79,79) +edge(294,79) +edge(81,81) +edge(82,82) +edge(252,82) +edge(124,83) +edge(118,84) +edge(85,85) +edge(196,85) +edge(201,86) +edge(331,87) +edge(161,88) +edge(89,89) +edge(90,90) +edge(91,91) +edge(92,92) +edge(93,93) +edge(264,93) +edge(167,94) +edge(95,95) +edge(96,96) +edge(97,97) +edge(98,98) +edge(288,98) +edge(210,99) +edge(101,101) +edge(233,101) +edge(229,102) +edge(103,103) +edge(104,104) +edge(105,105) +edge(106,106) +edge(107,107) +edge(193,107) +edge(108,108) +edge(202,108) +edge(109,109) +edge(110,110) +edge(189,110) +edge(219,111) +edge(112,112) +edge(284,112) +edge(265,113) +edge(345,113) +edge(171,114) +edge(115,115) +edge(194,115) +edge(129,116) +edge(117,117) +edge(347,118) +edge(120,119) +edge(120,120) +edge(121,121) +edge(122,122) +edge(155,122) +edge(123,123) +edge(262,123) +edge(239,124) +edge(156,125) +edge(168,126) +edge(349,127) +edge(155,128) +edge(129,129) +edge(144,130) +edge(131,131) +edge(281,131) +edge(287,132) +edge(276,133) +edge(135,135) +edge(277,135) +edge(137,137) +edge(138,138) +edge(261,138) +edge(292,139) +edge(192,140) +edge(223,141) +edge(357,142) +edge(215,143) +edge(358,143) +edge(145,145) +edge(146,146) +edge(224,146) +edge(147,147) +edge(369,147) +edge(220,148) +edge(259,148) +edge(149,149) +edge(224,150) +edge(151,151) +edge(152,152) +edge(376,153) +edge(294,154) +edge(156,156) +edge(206,156) +edge(278,156) +edge(157,157) +edge(158,158) +edge(159,159) +edge(233,159) +edge(193,160) +edge(161,161) +edge(162,162) +edge(163,163) +edge(280,163) +edge(382,164) +edge(209,165) +edge(346,165) +edge(167,167) +edge(169,169) +edge(260,169) +edge(229,170) +edge(186,171) +edge(383,171) +edge(222,172) +edge(173,173) +edge(174,174) +edge(175,175) +edge(176,176) +edge(177,177) +edge(178,178) +edge(249,178) +edge(388,179) +edge(207,180) +edge(207,181) +edge(208,182) +edge(191,183) +edge(184,184) +edge(185,185) +edge(186,186) +edge(187,187) +edge(188,188) +edge(192,189) +edge(230,190) +edge(192,192) +edge(193,193) +edge(195,195) +edge(196,196) +edge(198,198) +edge(199,199) +edge(284,199) +edge(200,200) +edge(244,200) +edge(202,202) +edge(203,203) +edge(244,203) +edge(204,204) +edge(281,204) +edge(253,205) +edge(258,206) +edge(392,206) +edge(208,208) +edge(209,209) +edge(211,211) +edge(287,212) +edge(231,213) +edge(215,215) +edge(266,216) +edge(218,218) +edge(219,219) +edge(220,220) +edge(396,220) +edge(222,222) +edge(224,224) +edge(401,225) +edge(245,226) +edge(228,228) +edge(229,229) +edge(402,229) +edge(231,230) +edge(403,231) +edge(233,233) +edge(234,234) +edge(268,234) +edge(235,235) +edge(236,236) +edge(237,237) +edge(239,239) +edge(241,241) +edge(268,242) +edge(243,243) +edge(244,244) +edge(254,245) +edge(246,246) +edge(269,247) +edge(414,248) +edge(415,249) +edge(282,250) +edge(252,252) +edge(253,253) +edge(417,253) +edge(255,255) +edge(418,256) +edge(419,257) +edge(259,259) +edge(261,261) +edge(263,263) +edge(264,264) +edge(265,265) +edge(267,267) +edge(269,269) +edge(271,271) +edge(425,272) +edge(274,274) +edge(275,275) +edge(277,277) +edge(278,278) +edge(285,278) +edge(429,279) +edge(281,281) +edge(283,283) +edge(285,285) +edge(287,287) +edge(289,289) +edge(291,291) +edge(293,293) +edge(294,294) +edge(295,295) +edge(297,297) +edge(298,298) +edge(313,298) +edge(299,299) +edge(336,299) +edge(343,299) +edge(301,301) +edge(302,302) +edge(303,303) +edge(424,303) +edge(346,304) +edge(345,305) +edge(394,305) +edge(420,305) +edge(436,305) +edge(307,306) +edge(309,306) +edge(379,306) +edge(307,307) +edge(308,308) +edge(421,308) +edge(321,309) +edge(328,309) +edge(341,309) +edge(422,309) +edge(311,310) +edge(370,310) +edge(426,310) +edge(645,311) +edge(313,313) +edge(315,314) +edge(315,315) +edge(343,315) +edge(316,316) +edge(420,316) +edge(377,317) +edge(318,318) +edge(361,318) +edge(423,318) +edge(324,319) +edge(320,320) +edge(321,321) +edge(322,322) +edge(386,322) +edge(323,323) +edge(456,323) +edge(389,324) +edge(325,325) +edge(429,325) +edge(332,326) +edge(327,327) +edge(579,327) +edge(437,328) +edge(429,329) +edge(433,330) +edge(418,331) +edge(332,332) +edge(333,333) +edge(335,333) +edge(334,334) +edge(414,334) +edge(385,335) +edge(336,336) +edge(337,337) +edge(429,337) +edge(338,338) +edge(400,338) +edge(339,339) +edge(400,339) +edge(341,341) +edge(343,343) +edge(432,343) +edge(503,344) +edge(406,345) +edge(442,345) +edge(404,346) +edge(383,347) +edge(448,347) +edge(351,348) +edge(394,348) +edge(349,349) +edge(398,349) +edge(382,350) +edge(351,351) +edge(352,352) +edge(401,352) +edge(353,353) +edge(572,353) +edge(370,354) +edge(358,355) +edge(356,356) +edge(357,357) +edge(389,357) +edge(404,358) +edge(360,359) +edge(423,359) +edge(385,360) +edge(361,361) +edge(415,361) +edge(363,363) +edge(385,363) +edge(364,364) +edge(423,365) +edge(366,366) +edge(367,367) +edge(390,367) +edge(369,369) +edge(414,369) +edge(410,370) +edge(371,371) +edge(372,372) +edge(373,373) +edge(421,373) +edge(375,374) +edge(375,375) +edge(414,376) +edge(378,378) +edge(648,378) +edge(441,379) +edge(380,380) +edge(381,381) +edge(431,381) +edge(398,382) +edge(443,382) +edge(402,383) +edge(384,384) +edge(385,385) +edge(387,385) +edge(386,386) +edge(388,388) +edge(442,388) +edge(390,390) +edge(417,390) +edge(392,392) +edge(393,393) +edge(408,393) +edge(441,393) +edge(394,394) +edge(395,395) +edge(414,395) +edge(410,396) +edge(398,397) +edge(400,397) +edge(401,398) +edge(399,399) +edge(400,400) +edge(401,401) +edge(402,402) +edge(403,403) +edge(404,404) +edge(421,405) +edge(416,406) +edge(408,408) +edge(413,408) +edge(618,408) +edge(410,409) +edge(412,410) +edge(411,411) +edge(415,412) +edge(414,413) +edge(425,414) +edge(416,416) +edge(417,417) +edge(437,417) +edge(418,418) +edge(420,420) +edge(422,421) +edge(422,422) +edge(423,423) +edge(425,425) +edge(427,426) +edge(428,428) +edge(430,429) +edge(431,431) +edge(613,431) +edge(433,432) +edge(434,434) +edge(571,434) +edge(438,435) +edge(437,437) +edge(438,438) +edge(439,439) +edge(440,440) +edge(442,441) +edge(443,443) +edge(444,444) +edge(446,445) +edge(447,446) +edge(448,448) +edge(502,449) +edge(450,450) +edge(533,450) +edge(484,451) +edge(452,452) +edge(486,452) +edge(555,453) +edge(454,454) +edge(553,454) +edge(455,455) +edge(584,455) +edge(457,456) +edge(544,456) +edge(571,456) +edge(508,457) +edge(536,457) +edge(557,457) +edge(458,458) +edge(459,459) +edge(580,459) +edge(510,460) +edge(461,461) +edge(560,461) +edge(470,462) +edge(463,463) +edge(464,464) +edge(508,464) +edge(565,464) +edge(465,465) +edge(535,465) +edge(585,465) +edge(569,466) +edge(482,467) +edge(468,468) +edge(469,469) +edge(524,469) +edge(470,470) +edge(557,470) +edge(514,471) +edge(548,471) +edge(576,471) +edge(472,472) +edge(562,472) +edge(473,473) +edge(501,473) +edge(490,474) +edge(504,474) +edge(569,474) +edge(500,475) +edge(565,475) +edge(543,476) +edge(529,477) +edge(478,478) +edge(505,478) +edge(521,478) +edge(479,479) +edge(480,480) +edge(497,480) +edge(537,480) +edge(582,480) +edge(481,481) +edge(482,482) +edge(512,482) +edge(542,482) +edge(515,483) +edge(484,484) +edge(507,484) +edge(485,485) +edge(540,485) +edge(496,486) +edge(509,487) +edge(488,488) +edge(519,488) +edge(489,489) +edge(567,489) +edge(490,490) +edge(513,491) +edge(509,492) +edge(578,493) +edge(575,494) +edge(502,495) +edge(496,496) +edge(497,497) +edge(506,498) +edge(499,499) +edge(575,499) +edge(539,500) +edge(501,501) +edge(517,501) +edge(522,502) +edge(503,503) +edge(504,504) +edge(505,505) +edge(506,506) +edge(507,507) +edge(509,509) +edge(586,509) +edge(533,510) +edge(570,511) +edge(580,512) +edge(584,513) +edge(575,514) +edge(559,515) +edge(575,516) +edge(555,517) +edge(518,518) +edge(564,518) +edge(519,519) +edge(555,520) +edge(521,521) +edge(533,522) +edge(532,523) +edge(525,525) +edge(526,526) +edge(527,527) +edge(539,528) +edge(529,529) +edge(531,530) +edge(531,531) +edge(574,532) +edge(534,534) +edge(535,535) +edge(552,536) +edge(575,536) +edge(538,538) +edge(539,539) +edge(541,541) +edge(573,541) +edge(543,543) +edge(651,544) +edge(545,545) +edge(547,547) +edge(553,547) +edge(585,548) +edge(550,550) +edge(554,551) +edge(553,553) +edge(555,555) +edge(556,556) +edge(557,557) +edge(559,559) +edge(582,560) +edge(570,561) +edge(563,563) +edge(565,564) +edge(582,565) +edge(567,566) +edge(567,567) +edge(581,567) +edge(569,568) +edge(577,569) +edge(583,570) +edge(572,572) +edge(574,574) +edge(576,576) +edge(581,577) +edge(579,579) +edge(581,581) +edge(582,582) +edge(584,584) +edge(586,586) +edge(590,587) +edge(588,588) +edge(608,588) +edge(610,589) +edge(590,590) +edge(608,590) +edge(634,590) +edge(662,591) +edge(604,592) +edge(594,593) +edge(601,593) +edge(628,593) +edge(594,594) +edge(646,594) +edge(628,595) +edge(639,595) +edge(619,596) +edge(597,597) +edge(602,597) +edge(635,597) +edge(602,598) +edge(611,598) +edge(627,598) +edge(639,598) +edge(606,599) +edge(600,600) +edge(629,600) +edge(628,601) +edge(602,602) +edge(609,603) +edge(611,604) +edge(605,605) +edge(629,605) +edge(606,606) +edge(607,607) +edge(626,607) +edge(608,608) +edge(609,609) +edge(610,610) +edge(627,610) +edge(611,611) +edge(612,612) +edge(613,613) +edge(644,613) +edge(615,615) +edge(642,615) +edge(616,616) +edge(617,617) +edge(618,618) +edge(619,619) +edge(645,619) +edge(621,621) +edge(643,621) +edge(623,623) +edge(640,624) +edge(625,625) +edge(626,626) +edge(627,627) +edge(630,628) +edge(629,629) +edge(630,630) +edge(632,632) +edge(634,634) +edge(635,635) +edge(637,636) +edge(637,637) +edge(639,639) +edge(641,640) +edge(642,642) +edge(644,643) +edge(645,645) +edge(647,645) +edge(647,647) +edge(649,648) +edge(649,649) +edge(655,650) +edge(652,651) +edge(656,652) +edge(659,652) +edge(654,653) +edge(658,653) +edge(660,653) +edge(654,654) +edge(656,654) +edge(658,654) +edge(658,655) +edge(661,655) +edge(656,656) +edge(660,656) +edge(657,657) +edge(659,657) +edge(659,658) +edge(659,659) +edge(661,660) +edge(661,661) +edge(662,662) ],[path(S,D) > 0.8],ccsaq,-1,A),
	statistics(runtime, [Stop | _]),
	Runtime is Stop - Start,
	writeln('ccsaq'),
	writeln(Runtime),
	writeln(A).

run_slsqp(S,D):-
	statistics(runtime, [Start | _]), 
	prob_optimize(path(S,D),[edge(222,1) +edge(2,2) +edge(228,2) +edge(3,3) +edge(166,3) +edge(126,4) +edge(5,5) +edge(228,5) +edge(109,6) +edge(7,7) +edge(65,7) +edge(38,8) +edge(119,8) +edge(211,8) +edge(9,9) +edge(10,10) +edge(106,10) +edge(303,10) +edge(122,11) +edge(12,12) +edge(80,12) +edge(90,13) +edge(14,14) +edge(245,14) +edge(24,15) +edge(137,15) +edge(304,15) +edge(57,16) +edge(17,17) +edge(231,17) +edge(251,18) +edge(19,19) +edge(236,19) +edge(122,20) +edge(21,21) +edge(273,21) +edge(27,22) +edge(23,23) +edge(89,23) +edge(187,24) +edge(158,25) +edge(289,25) +edge(26,26) +edge(250,26) +edge(69,27) +edge(33,28) +edge(29,29) +edge(238,29) +edge(149,30) +edge(31,31) +edge(97,31) +edge(32,32) +edge(271,32) +edge(228,33) +edge(48,34) +edge(166,34) +edge(196,34) +edge(35,35) +edge(36,36) +edge(281,36) +edge(68,37) +edge(38,38) +edge(39,39) +edge(267,39) +edge(124,40) +edge(41,41) +edge(214,41) +edge(264,42) +edge(264,43) +edge(44,44) +edge(75,44) +edge(188,44) +edge(210,45) +edge(46,46) +edge(277,46) +edge(182,47) +edge(48,48) +edge(49,49) +edge(50,50) +edge(185,50) +edge(168,51) +edge(52,52) +edge(272,52) +edge(318,53) +edge(108,54) +edge(206,55) +edge(56,56) +edge(149,56) +edge(176,56) +edge(192,57) +edge(125,58) +edge(238,58) +edge(319,58) +edge(108,59) +edge(60,60) +edge(61,61) +edge(224,61) +edge(152,62) +edge(63,63) +edge(184,63) +edge(322,64) +edge(143,65) +edge(203,66) +edge(67,67) +edge(68,68) +edge(69,69) +edge(262,69) +edge(70,70) +edge(198,70) +edge(75,71) +edge(72,72) +edge(142,72) +edge(129,73) +edge(74,74) +edge(113,74) +edge(217,74) +edge(241,75) +edge(76,76) +edge(255,76) +edge(118,77) +edge(142,78) +edge(79,79) +edge(294,79) +edge(81,81) +edge(82,82) +edge(252,82) +edge(124,83) +edge(118,84) +edge(85,85) +edge(196,85) +edge(201,86) +edge(331,87) +edge(161,88) +edge(89,89) +edge(90,90) +edge(91,91) +edge(92,92) +edge(93,93) +edge(264,93) +edge(167,94) +edge(95,95) +edge(96,96) +edge(97,97) +edge(98,98) +edge(288,98) +edge(210,99) +edge(101,101) +edge(233,101) +edge(229,102) +edge(103,103) +edge(104,104) +edge(105,105) +edge(106,106) +edge(107,107) +edge(193,107) +edge(108,108) +edge(202,108) +edge(109,109) +edge(110,110) +edge(189,110) +edge(219,111) +edge(112,112) +edge(284,112) +edge(265,113) +edge(345,113) +edge(171,114) +edge(115,115) +edge(194,115) +edge(129,116) +edge(117,117) +edge(347,118) +edge(120,119) +edge(120,120) +edge(121,121) +edge(122,122) +edge(155,122) +edge(123,123) +edge(262,123) +edge(239,124) +edge(156,125) +edge(168,126) +edge(349,127) +edge(155,128) +edge(129,129) +edge(144,130) +edge(131,131) +edge(281,131) +edge(287,132) +edge(276,133) +edge(135,135) +edge(277,135) +edge(137,137) +edge(138,138) +edge(261,138) +edge(292,139) +edge(192,140) +edge(223,141) +edge(357,142) +edge(215,143) +edge(358,143) +edge(145,145) +edge(146,146) +edge(224,146) +edge(147,147) +edge(369,147) +edge(220,148) +edge(259,148) +edge(149,149) +edge(224,150) +edge(151,151) +edge(152,152) +edge(376,153) +edge(294,154) +edge(156,156) +edge(206,156) +edge(278,156) +edge(157,157) +edge(158,158) +edge(159,159) +edge(233,159) +edge(193,160) +edge(161,161) +edge(162,162) +edge(163,163) +edge(280,163) +edge(382,164) +edge(209,165) +edge(346,165) +edge(167,167) +edge(169,169) +edge(260,169) +edge(229,170) +edge(186,171) +edge(383,171) +edge(222,172) +edge(173,173) +edge(174,174) +edge(175,175) +edge(176,176) +edge(177,177) +edge(178,178) +edge(249,178) +edge(388,179) +edge(207,180) +edge(207,181) +edge(208,182) +edge(191,183) +edge(184,184) +edge(185,185) +edge(186,186) +edge(187,187) +edge(188,188) +edge(192,189) +edge(230,190) +edge(192,192) +edge(193,193) +edge(195,195) +edge(196,196) +edge(198,198) +edge(199,199) +edge(284,199) +edge(200,200) +edge(244,200) +edge(202,202) +edge(203,203) +edge(244,203) +edge(204,204) +edge(281,204) +edge(253,205) +edge(258,206) +edge(392,206) +edge(208,208) +edge(209,209) +edge(211,211) +edge(287,212) +edge(231,213) +edge(215,215) +edge(266,216) +edge(218,218) +edge(219,219) +edge(220,220) +edge(396,220) +edge(222,222) +edge(224,224) +edge(401,225) +edge(245,226) +edge(228,228) +edge(229,229) +edge(402,229) +edge(231,230) +edge(403,231) +edge(233,233) +edge(234,234) +edge(268,234) +edge(235,235) +edge(236,236) +edge(237,237) +edge(239,239) +edge(241,241) +edge(268,242) +edge(243,243) +edge(244,244) +edge(254,245) +edge(246,246) +edge(269,247) +edge(414,248) +edge(415,249) +edge(282,250) +edge(252,252) +edge(253,253) +edge(417,253) +edge(255,255) +edge(418,256) +edge(419,257) +edge(259,259) +edge(261,261) +edge(263,263) +edge(264,264) +edge(265,265) +edge(267,267) +edge(269,269) +edge(271,271) +edge(425,272) +edge(274,274) +edge(275,275) +edge(277,277) +edge(278,278) +edge(285,278) +edge(429,279) +edge(281,281) +edge(283,283) +edge(285,285) +edge(287,287) +edge(289,289) +edge(291,291) +edge(293,293) +edge(294,294) +edge(295,295) +edge(297,297) +edge(298,298) +edge(313,298) +edge(299,299) +edge(336,299) +edge(343,299) +edge(301,301) +edge(302,302) +edge(303,303) +edge(424,303) +edge(346,304) +edge(345,305) +edge(394,305) +edge(420,305) +edge(436,305) +edge(307,306) +edge(309,306) +edge(379,306) +edge(307,307) +edge(308,308) +edge(421,308) +edge(321,309) +edge(328,309) +edge(341,309) +edge(422,309) +edge(311,310) +edge(370,310) +edge(426,310) +edge(645,311) +edge(313,313) +edge(315,314) +edge(315,315) +edge(343,315) +edge(316,316) +edge(420,316) +edge(377,317) +edge(318,318) +edge(361,318) +edge(423,318) +edge(324,319) +edge(320,320) +edge(321,321) +edge(322,322) +edge(386,322) +edge(323,323) +edge(456,323) +edge(389,324) +edge(325,325) +edge(429,325) +edge(332,326) +edge(327,327) +edge(579,327) +edge(437,328) +edge(429,329) +edge(433,330) +edge(418,331) +edge(332,332) +edge(333,333) +edge(335,333) +edge(334,334) +edge(414,334) +edge(385,335) +edge(336,336) +edge(337,337) +edge(429,337) +edge(338,338) +edge(400,338) +edge(339,339) +edge(400,339) +edge(341,341) +edge(343,343) +edge(432,343) +edge(503,344) +edge(406,345) +edge(442,345) +edge(404,346) +edge(383,347) +edge(448,347) +edge(351,348) +edge(394,348) +edge(349,349) +edge(398,349) +edge(382,350) +edge(351,351) +edge(352,352) +edge(401,352) +edge(353,353) +edge(572,353) +edge(370,354) +edge(358,355) +edge(356,356) +edge(357,357) +edge(389,357) +edge(404,358) +edge(360,359) +edge(423,359) +edge(385,360) +edge(361,361) +edge(415,361) +edge(363,363) +edge(385,363) +edge(364,364) +edge(423,365) +edge(366,366) +edge(367,367) +edge(390,367) +edge(369,369) +edge(414,369) +edge(410,370) +edge(371,371) +edge(372,372) +edge(373,373) +edge(421,373) +edge(375,374) +edge(375,375) +edge(414,376) +edge(378,378) +edge(648,378) +edge(441,379) +edge(380,380) +edge(381,381) +edge(431,381) +edge(398,382) +edge(443,382) +edge(402,383) +edge(384,384) +edge(385,385) +edge(387,385) +edge(386,386) +edge(388,388) +edge(442,388) +edge(390,390) +edge(417,390) +edge(392,392) +edge(393,393) +edge(408,393) +edge(441,393) +edge(394,394) +edge(395,395) +edge(414,395) +edge(410,396) +edge(398,397) +edge(400,397) +edge(401,398) +edge(399,399) +edge(400,400) +edge(401,401) +edge(402,402) +edge(403,403) +edge(404,404) +edge(421,405) +edge(416,406) +edge(408,408) +edge(413,408) +edge(618,408) +edge(410,409) +edge(412,410) +edge(411,411) +edge(415,412) +edge(414,413) +edge(425,414) +edge(416,416) +edge(417,417) +edge(437,417) +edge(418,418) +edge(420,420) +edge(422,421) +edge(422,422) +edge(423,423) +edge(425,425) +edge(427,426) +edge(428,428) +edge(430,429) +edge(431,431) +edge(613,431) +edge(433,432) +edge(434,434) +edge(571,434) +edge(438,435) +edge(437,437) +edge(438,438) +edge(439,439) +edge(440,440) +edge(442,441) +edge(443,443) +edge(444,444) +edge(446,445) +edge(447,446) +edge(448,448) +edge(502,449) +edge(450,450) +edge(533,450) +edge(484,451) +edge(452,452) +edge(486,452) +edge(555,453) +edge(454,454) +edge(553,454) +edge(455,455) +edge(584,455) +edge(457,456) +edge(544,456) +edge(571,456) +edge(508,457) +edge(536,457) +edge(557,457) +edge(458,458) +edge(459,459) +edge(580,459) +edge(510,460) +edge(461,461) +edge(560,461) +edge(470,462) +edge(463,463) +edge(464,464) +edge(508,464) +edge(565,464) +edge(465,465) +edge(535,465) +edge(585,465) +edge(569,466) +edge(482,467) +edge(468,468) +edge(469,469) +edge(524,469) +edge(470,470) +edge(557,470) +edge(514,471) +edge(548,471) +edge(576,471) +edge(472,472) +edge(562,472) +edge(473,473) +edge(501,473) +edge(490,474) +edge(504,474) +edge(569,474) +edge(500,475) +edge(565,475) +edge(543,476) +edge(529,477) +edge(478,478) +edge(505,478) +edge(521,478) +edge(479,479) +edge(480,480) +edge(497,480) +edge(537,480) +edge(582,480) +edge(481,481) +edge(482,482) +edge(512,482) +edge(542,482) +edge(515,483) +edge(484,484) +edge(507,484) +edge(485,485) +edge(540,485) +edge(496,486) +edge(509,487) +edge(488,488) +edge(519,488) +edge(489,489) +edge(567,489) +edge(490,490) +edge(513,491) +edge(509,492) +edge(578,493) +edge(575,494) +edge(502,495) +edge(496,496) +edge(497,497) +edge(506,498) +edge(499,499) +edge(575,499) +edge(539,500) +edge(501,501) +edge(517,501) +edge(522,502) +edge(503,503) +edge(504,504) +edge(505,505) +edge(506,506) +edge(507,507) +edge(509,509) +edge(586,509) +edge(533,510) +edge(570,511) +edge(580,512) +edge(584,513) +edge(575,514) +edge(559,515) +edge(575,516) +edge(555,517) +edge(518,518) +edge(564,518) +edge(519,519) +edge(555,520) +edge(521,521) +edge(533,522) +edge(532,523) +edge(525,525) +edge(526,526) +edge(527,527) +edge(539,528) +edge(529,529) +edge(531,530) +edge(531,531) +edge(574,532) +edge(534,534) +edge(535,535) +edge(552,536) +edge(575,536) +edge(538,538) +edge(539,539) +edge(541,541) +edge(573,541) +edge(543,543) +edge(651,544) +edge(545,545) +edge(547,547) +edge(553,547) +edge(585,548) +edge(550,550) +edge(554,551) +edge(553,553) +edge(555,555) +edge(556,556) +edge(557,557) +edge(559,559) +edge(582,560) +edge(570,561) +edge(563,563) +edge(565,564) +edge(582,565) +edge(567,566) +edge(567,567) +edge(581,567) +edge(569,568) +edge(577,569) +edge(583,570) +edge(572,572) +edge(574,574) +edge(576,576) +edge(581,577) +edge(579,579) +edge(581,581) +edge(582,582) +edge(584,584) +edge(586,586) +edge(590,587) +edge(588,588) +edge(608,588) +edge(610,589) +edge(590,590) +edge(608,590) +edge(634,590) +edge(662,591) +edge(604,592) +edge(594,593) +edge(601,593) +edge(628,593) +edge(594,594) +edge(646,594) +edge(628,595) +edge(639,595) +edge(619,596) +edge(597,597) +edge(602,597) +edge(635,597) +edge(602,598) +edge(611,598) +edge(627,598) +edge(639,598) +edge(606,599) +edge(600,600) +edge(629,600) +edge(628,601) +edge(602,602) +edge(609,603) +edge(611,604) +edge(605,605) +edge(629,605) +edge(606,606) +edge(607,607) +edge(626,607) +edge(608,608) +edge(609,609) +edge(610,610) +edge(627,610) +edge(611,611) +edge(612,612) +edge(613,613) +edge(644,613) +edge(615,615) +edge(642,615) +edge(616,616) +edge(617,617) +edge(618,618) +edge(619,619) +edge(645,619) +edge(621,621) +edge(643,621) +edge(623,623) +edge(640,624) +edge(625,625) +edge(626,626) +edge(627,627) +edge(630,628) +edge(629,629) +edge(630,630) +edge(632,632) +edge(634,634) +edge(635,635) +edge(637,636) +edge(637,637) +edge(639,639) +edge(641,640) +edge(642,642) +edge(644,643) +edge(645,645) +edge(647,645) +edge(647,647) +edge(649,648) +edge(649,649) +edge(655,650) +edge(652,651) +edge(656,652) +edge(659,652) +edge(654,653) +edge(658,653) +edge(660,653) +edge(654,654) +edge(656,654) +edge(658,654) +edge(658,655) +edge(661,655) +edge(656,656) +edge(660,656) +edge(657,657) +edge(659,657) +edge(659,658) +edge(659,659) +edge(661,660) +edge(661,661) +edge(662,662) ],[path(S,D) > 0.8],slsqp,-1,A),
	statistics(runtime, [Stop | _]),
	Runtime is Stop - Start,
	writeln('slsqp'),
	writeln(Runtime),
	writeln(A).
