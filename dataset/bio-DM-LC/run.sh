sbatch --job-name=bio-DM-LC_270_149 --partition=longrun --mem=8G --output=bio-DM-LC_270_149_ccsaq.log --wrap='srun swipl -s bio-DM-LC.pl -g "run_ccsaq(270, 149), halt." '
sbatch --job-name=bio-DM-LC_270_149 --partition=longrun --mem=8G --output=bio-DM-LC_270_149_mma.log --wrap='srun swipl -s bio-DM-LC.pl -g "run_mma(270, 149), halt." '
sbatch --job-name=bio-DM-LC_270_149 --partition=longrun --mem=8G --output=bio-DM-LC_270_149_slsqp.log --wrap='srun swipl -s bio-DM-LC.pl -g "run_slsqp(270, 149), halt." '

sbatch --job-name=bio-DM-LC_127_287 --partition=longrun --mem=8G --output=bio-DM-LC_127_287_ccsaq.log --wrap='srun swipl -s bio-DM-LC.pl -g "run_ccsaq(127, 287), halt." '
sbatch --job-name=bio-DM-LC_127_287 --partition=longrun --mem=8G --output=bio-DM-LC_127_287_mma.log --wrap='srun swipl -s bio-DM-LC.pl -g "run_mma(127, 287), halt." '
sbatch --job-name=bio-DM-LC_127_287 --partition=longrun --mem=8G --output=bio-DM-LC_127_287_slsqp.log --wrap='srun swipl -s bio-DM-LC.pl -g "run_slsqp(127, 287), halt." '

sbatch --job-name=bio-DM-LC_43_563 --partition=longrun --mem=8G --output=bio-DM-LC_43_563_ccsaq.log --wrap='srun swipl -s bio-DM-LC.pl -g "run_ccsaq(43, 563), halt." '
sbatch --job-name=bio-DM-LC_43_563 --partition=longrun --mem=8G --output=bio-DM-LC_43_563_mma.log --wrap='srun swipl -s bio-DM-LC.pl -g "run_mma(43, 563), halt." '
sbatch --job-name=bio-DM-LC_43_563 --partition=longrun --mem=8G --output=bio-DM-LC_43_563_slsqp.log --wrap='srun swipl -s bio-DM-LC.pl -g "run_slsqp(43, 563), halt." '

sbatch --job-name=bio-DM-LC_110_158 --partition=longrun --mem=8G --output=bio-DM-LC_110_158_ccsaq.log --wrap='srun swipl -s bio-DM-LC.pl -g "run_ccsaq(110, 158), halt." '
sbatch --job-name=bio-DM-LC_110_158 --partition=longrun --mem=8G --output=bio-DM-LC_110_158_mma.log --wrap='srun swipl -s bio-DM-LC.pl -g "run_mma(110, 158), halt." '
sbatch --job-name=bio-DM-LC_110_158 --partition=longrun --mem=8G --output=bio-DM-LC_110_158_slsqp.log --wrap='srun swipl -s bio-DM-LC.pl -g "run_slsqp(110, 158), halt." '

sbatch --job-name=bio-DM-LC_375_602 --partition=longrun --mem=8G --output=bio-DM-LC_375_602_ccsaq.log --wrap='srun swipl -s bio-DM-LC.pl -g "run_ccsaq(375, 602), halt." '
sbatch --job-name=bio-DM-LC_375_602 --partition=longrun --mem=8G --output=bio-DM-LC_375_602_mma.log --wrap='srun swipl -s bio-DM-LC.pl -g "run_mma(375, 602), halt." '
sbatch --job-name=bio-DM-LC_375_602 --partition=longrun --mem=8G --output=bio-DM-LC_375_602_slsqp.log --wrap='srun swipl -s bio-DM-LC.pl -g "run_slsqp(375, 602), halt." '

sbatch --job-name=bio-DM-LC_77_48 --partition=longrun --mem=8G --output=bio-DM-LC_77_288_ccsaq.log --wrap='srun swipl -s bio-DM-LC.pl -g "run_ccsaq(75, 48), halt." '
sbatch --job-name=bio-DM-LC_77_48 --partition=longrun --mem=8G --output=bio-DM-LC_77_288_mma.log --wrap='srun swipl -s bio-DM-LC.pl -g "run_mma(75, 48), halt." '
sbatch --job-name=bio-DM-LC_77_48 --partition=longrun --mem=8G --output=bio-DM-LC_77_288_slsqp.log --wrap='srun swipl -s bio-DM-LC.pl -g "run_slsqp(75, 48), halt." '

sbatch --job-name=bio-DM-LC_364_114 --partition=longrun --mem=8G --output=bio-DM-LC_364_114_ccsaq.log --wrap='srun swipl -s bio-DM-LC.pl -g "run_ccsaq(364, 114), halt." '
sbatch --job-name=bio-DM-LC_364_114 --partition=longrun --mem=8G --output=bio-DM-LC_364_114_mma.log --wrap='srun swipl -s bio-DM-LC.pl -g "run_mma(364, 114), halt." '
sbatch --job-name=bio-DM-LC_364_114 --partition=longrun --mem=8G --output=bio-DM-LC_364_114_slsqp.log --wrap='srun swipl -s bio-DM-LC.pl -g "run_slsqp(364, 114), halt." '

sbatch --job-name=bio-DM-LC_491_48 --partition=longrun --mem=8G --output=bio-DM-LC_491_48_ccsaq.log --wrap='srun swipl -s bio-DM-LC.pl -g "run_ccsaq(491, 48), halt." '
sbatch --job-name=bio-DM-LC_491_48 --partition=longrun --mem=8G --output=bio-DM-LC_491_48_mma.log --wrap='srun swipl -s bio-DM-LC.pl -g "run_mma(491, 48), halt." '
sbatch --job-name=bio-DM-LC_491_48 --partition=longrun --mem=8G --output=bio-DM-LC_491_48_slsqp.log --wrap='srun swipl -s bio-DM-LC.pl -g "run_slsqp(491, 48), halt." '

sbatch --job-name=bio-DM-LC_43_45 --partition=longrun --mem=8G --output=bio-DM-LC_43_45_ccsaq.log --wrap='srun swipl -s bio-DM-LC.pl -g "run_ccsaq(43, 45), halt." '
sbatch --job-name=bio-DM-LC_43_45 --partition=longrun --mem=8G --output=bio-DM-LC_43_45_mma.log --wrap='srun swipl -s bio-DM-LC.pl -g "run_mma(43, 45), halt." '
sbatch --job-name=bio-DM-LC_43_45 --partition=longrun --mem=8G --output=bio-DM-LC_43_45_slsqp.log --wrap='srun swipl -s bio-DM-LC.pl -g "run_slsqp(43, 45), halt." '

sbatch --job-name=bio-DM-LC_75_381 --partition=longrun --mem=8G --output=bio-DM-LC_75_381_ccsaq.log --wrap='srun swipl -s bio-DM-LC.pl -g "run_ccsaq(75, 381), halt." '
sbatch --job-name=bio-DM-LC_75_381 --partition=longrun --mem=8G --output=bio-DM-LC_75_381_mma.log --wrap='srun swipl -s bio-DM-LC.pl -g "run_mma(75, 381), halt." '
sbatch --job-name=bio-DM-LC_75_381 --partition=longrun --mem=8G --output=bio-DM-LC_75_381_slsqp.log --wrap='srun swipl -s bio-DM-LC.pl -g "run_slsqp(75, 381), halt." '