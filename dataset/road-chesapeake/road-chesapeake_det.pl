:- table path/2.

edge(7, 1).
edge(8, 1).
edge(11, 1).
edge(12, 1).
edge(13, 1).
edge(22, 1).
edge(23, 1).
edge(34, 1).
edge(35, 1).
edge(37, 1).
edge(39, 1).
edge(7, 2).
edge(8, 2).
edge(9, 2).
edge(11, 2).
edge(12, 2).
edge(13, 2).
edge(22, 2).
edge(23, 2).
edge(35, 2).
edge(36, 2).
edge(39, 2).
edge(14, 3).
edge(15, 3).
edge(16, 3).
edge(17, 3).
edge(18, 3).
edge(36, 3).
edge(39, 3).
edge(17, 4).
edge(36, 4).
edge(37, 4).
edge(39, 4).
edge(6, 5).
edge(34, 5).
edge(35, 5).
edge(39, 5).
edge(7, 6).
edge(35, 6).
edge(39, 6).
edge(8, 7).
edge(9, 7).
edge(11, 7).
edge(12, 7).
edge(13, 7).
edge(35, 7).
edge(39, 7).
edge(9, 8).
edge(10, 8).
edge(20, 8).
edge(21, 8).
edge(22, 8).
edge(23, 8).
edge(24, 8).
edge(35, 8).
edge(38, 8).
edge(39, 8).
edge(10, 9).
edge(35, 9).
edge(36, 9).
edge(39, 9).
edge(35, 10).
edge(36, 10).
edge(39, 10).
edge(19, 11).
edge(35, 11).
edge(36, 11).
edge(39, 11).
edge(19, 12).
edge(26, 12).
edge(35, 12).
edge(36, 12).
edge(38, 12).
edge(39, 12).
edge(35, 13).
edge(36, 13).
edge(38, 13).
edge(39, 13).
edge(25, 14).
edge(26, 14).
edge(27, 14).
edge(28, 14).
edge(29, 14).
edge(36, 14).
edge(39, 14).
edge(19, 15).
edge(25, 15).
edge(26, 15).
edge(27, 15).
edge(28, 15).
edge(29, 15).
edge(36, 15).
edge(39, 15).
edge(19, 16).
edge(27, 16).
edge(36, 16).
edge(39, 16).
edge(36, 17).
edge(39, 17).
edge(19, 18).
edge(25, 18).
edge(26, 18).
edge(27, 18).
edge(29, 18).
edge(32, 18).
edge(36, 18).
edge(39, 18).
edge(33, 19).
edge(36, 19).
edge(38, 19).
edge(39, 19).
edge(36, 20).
edge(38, 20).
edge(39, 20).
edge(33, 21).
edge(36, 21).
edge(38, 21).
edge(39, 21).
edge(27, 22).
edge(28, 22).
edge(30, 22).
edge(31, 22).
edge(32, 22).
edge(33, 22).
edge(35, 22).
edge(36, 22).
edge(38, 22).
edge(39, 22).
edge(30, 23).
edge(32, 23).
edge(33, 23).
edge(35, 23).
edge(36, 23).
edge(38, 23).
edge(39, 23).
edge(36, 24).
edge(38, 24).
edge(39, 24).
edge(36, 25).
edge(38, 25).
edge(39, 25).
edge(36, 26).
edge(38, 26).
edge(39, 26).
edge(30, 27).
edge(36, 27).
edge(38, 27).
edge(39, 27).
edge(36, 28).
edge(38, 28).
edge(39, 28).
edge(36, 29).
edge(38, 29).
edge(39, 29).
edge(36, 30).
edge(38, 30).
edge(39, 30).
edge(32, 31).
edge(36, 31).
edge(38, 31).
edge(39, 31).
edge(36, 32).
edge(38, 32).
edge(39, 32).
edge(36, 33).
edge(38, 33).
edge(39, 33).
edge(37, 34).
edge(36, 35).
edge(37, 35).


path(X, X).
path(X, Y):- path(X, Z), edge(Z, Y).

test_rand([S,D]):-
    random_between(1, 39, Source),
    random_between(1, 39, Dest),
    ( path(Source,Dest), (\+(edge(Source,Dest))), Source \= Dest -> 
        write('path between '), write(Source), write(' '), writeln(Dest), S = Source, D = Dest ;
        S = -1, D = -1 
    ).

find(N,L):-
    length(LT,N),
    maplist(test_rand,LT),
    include(\=([-1,-1]),LT,L),
    maplist(writeln,L).
