sbatch --job-name=road-chesapeake_37_11 --partition=longrun --mem=8G --output=road-chesapeake_37_11_ccsaq.log --wrap='srun swipl -s road-chesapeake.pl -g "run_ccsaq(37, 11), halt." '
sbatch --job-name=road-chesapeake_37_11 --partition=longrun --mem=8G --output=road-chesapeake_37_11_mma.log --wrap='srun swipl -s road-chesapeake.pl -g "run_mma(37, 11), halt." '
sbatch --job-name=road-chesapeake_37_11 --partition=longrun --mem=8G --output=road-chesapeake_37_11_slsqp.log --wrap='srun swipl -s road-chesapeake.pl -g "run_slsqp(37, 11), halt." '

sbatch --job-name=road-chesapeake_11_5 --partition=longrun --mem=8G --output=road-chesapeake_11_5_ccsaq.log --wrap='srun swipl -s road-chesapeake.pl -g "run_ccsaq(11, 5), halt." '
sbatch --job-name=road-chesapeake_11_5 --partition=longrun --mem=8G --output=road-chesapeake_11_5_mma.log --wrap='srun swipl -s road-chesapeake.pl -g "run_mma(11, 5), halt." '
sbatch --job-name=road-chesapeake_11_5 --partition=longrun --mem=8G --output=road-chesapeake_11_5_slsqp.log --wrap='srun swipl -s road-chesapeake.pl -g "run_slsqp(11, 5), halt." '

sbatch --job-name=road-chesapeake_37_12 --partition=longrun --mem=8G --output=road-chesapeake_37_12_ccsaq.log --wrap='srun swipl -s road-chesapeake.pl -g "run_ccsaq(37, 12), halt." '
sbatch --job-name=road-chesapeake_37_12 --partition=longrun --mem=8G --output=road-chesapeake_37_12_mma.log --wrap='srun swipl -s road-chesapeake.pl -g "run_mma(37, 12), halt." '
sbatch --job-name=road-chesapeake_37_12 --partition=longrun --mem=8G --output=road-chesapeake_37_12_slsqp.log --wrap='srun swipl -s road-chesapeake.pl -g "run_slsqp(37, 12), halt." '

sbatch --job-name=road-chesapeake_27_2 --partition=longrun --mem=8G --output=road-chesapeake_27_2_ccsaq.log --wrap='srun swipl -s road-chesapeake.pl -g "run_ccsaq(27, 2), halt." '
sbatch --job-name=road-chesapeake_27_2 --partition=longrun --mem=8G --output=road-chesapeake_27_2_mma.log --wrap='srun swipl -s road-chesapeake.pl -g "run_mma(27, 2), halt." '
sbatch --job-name=road-chesapeake_27_2 --partition=longrun --mem=8G --output=road-chesapeake_27_2_slsqp.log --wrap='srun swipl -s road-chesapeake.pl -g "run_slsqp(27, 2), halt." '

sbatch --job-name=road-chesapeake_24_1 --partition=longrun --mem=8G --output=road-chesapeake_24_1_ccsaq.log --wrap='srun swipl -s road-chesapeake.pl -g "run_ccsaq(24, 1), halt." '
sbatch --job-name=road-chesapeake_24_1 --partition=longrun --mem=8G --output=road-chesapeake_24_1_mma.log --wrap='srun swipl -s road-chesapeake.pl -g "run_mma(24, 1), halt." '
sbatch --job-name=road-chesapeake_24_1 --partition=longrun --mem=8G --output=road-chesapeake_24_1_slsqp.log --wrap='srun swipl -s road-chesapeake.pl -g "run_slsqp(24, 1), halt." '

sbatch --job-name=road-chesapeake_37_7 --partition=longrun --mem=8G --output=road-chesapeake_37_7_ccsaq.log --wrap='srun swipl -s road-chesapeake.pl -g "run_ccsaq(37, 7), halt." '
sbatch --job-name=road-chesapeake_37_7 --partition=longrun --mem=8G --output=road-chesapeake_37_7_mma.log --wrap='srun swipl -s road-chesapeake.pl -g "run_mma(37, 7), halt." '
sbatch --job-name=road-chesapeake_37_7 --partition=longrun --mem=8G --output=road-chesapeake_37_7_slsqp.log --wrap='srun swipl -s road-chesapeake.pl -g "run_slsqp(37, 7), halt." '

sbatch --job-name=road-chesapeake_33_6 --partition=longrun --mem=8G --output=road-chesapeake_33_6_ccsaq.log --wrap='srun swipl -s road-chesapeake.pl -g "run_ccsaq(33, 6), halt." '
sbatch --job-name=road-chesapeake_33_6 --partition=longrun --mem=8G --output=road-chesapeake_33_6_mma.log --wrap='srun swipl -s road-chesapeake.pl -g "run_mma(33, 6), halt." '
sbatch --job-name=road-chesapeake_33_6 --partition=longrun --mem=8G --output=road-chesapeake_33_6_slsqp.log --wrap='srun swipl -s road-chesapeake.pl -g "run_slsqp(33, 6), halt." '

sbatch --job-name=road-chesapeake_27_3 --partition=longrun --mem=8G --output=road-chesapeake_27_3_ccsaq.log --wrap='srun swipl -s road-chesapeake.pl -g "run_ccsaq(27, 3), halt." '
sbatch --job-name=road-chesapeake_27_3 --partition=longrun --mem=8G --output=road-chesapeake_27_3_mma.log --wrap='srun swipl -s road-chesapeake.pl -g "run_mma(27, 3), halt." '
sbatch --job-name=road-chesapeake_27_3 --partition=longrun --mem=8G --output=road-chesapeake_27_3_slsqp.log --wrap='srun swipl -s road-chesapeake.pl -g "run_slsqp(27, 3), halt." '

sbatch --job-name=road-chesapeake_9_1 --partition=longrun --mem=8G --output=road-chesapeake_9_1_ccsaq.log --wrap='srun swipl -s road-chesapeake.pl -g "run_ccsaq(9, 1), halt." '
sbatch --job-name=road-chesapeake_9_1 --partition=longrun --mem=8G --output=road-chesapeake_9_1_mma.log --wrap='srun swipl -s road-chesapeake.pl -g "run_mma(9, 1), halt." '
sbatch --job-name=road-chesapeake_9_1 --partition=longrun --mem=8G --output=road-chesapeake_9_1_slsqp.log --wrap='srun swipl -s road-chesapeake.pl -g "run_slsqp(9, 1), halt." '

sbatch --job-name=road-chesapeake_30_2 --partition=longrun --mem=8G --output=road-chesapeake_30_2_ccsaq.log --wrap='srun swipl -s road-chesapeake.pl -g "run_ccsaq(30, 2), halt." '
sbatch --job-name=road-chesapeake_30_2 --partition=longrun --mem=8G --output=road-chesapeake_30_2_mma.log --wrap='srun swipl -s road-chesapeake.pl -g "run_mma(30, 2), halt." '
sbatch --job-name=road-chesapeake_30_2 --partition=longrun --mem=8G --output=road-chesapeake_30_2_slsqp.log --wrap='srun swipl -s road-chesapeake.pl -g "run_slsqp(30, 2), halt." '