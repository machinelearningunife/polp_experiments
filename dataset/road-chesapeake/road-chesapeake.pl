:- use_module(library(pita)).
:- pita.
:- begin_lpad.

0.9::edge(7, 1).
optimizable edge(8, 1).
0.9::edge(11, 1).
optimizable edge(12, 1).
0.9::edge(13, 1).
optimizable edge(22, 1).
0.9::edge(23, 1).
optimizable edge(34, 1).
0.9::edge(35, 1).
optimizable edge(37, 1).
0.9::edge(39, 1).
optimizable edge(7, 2).
0.9::edge(8, 2).
optimizable edge(9, 2).
0.9::edge(11, 2).
optimizable edge(12, 2).
0.9::edge(13, 2).
optimizable edge(22, 2).
0.9::edge(23, 2).
optimizable edge(35, 2).
0.9::edge(36, 2).
optimizable edge(39, 2).
0.9::edge(14, 3).
optimizable edge(15, 3).
0.9::edge(16, 3).
optimizable edge(17, 3).
0.9::edge(18, 3).
optimizable edge(36, 3).
0.9::edge(39, 3).
optimizable edge(17, 4).
0.9::edge(36, 4).
optimizable edge(37, 4).
0.9::edge(39, 4).
optimizable edge(6, 5).
0.9::edge(34, 5).
optimizable edge(35, 5).
0.9::edge(39, 5).
optimizable edge(7, 6).
0.9::edge(35, 6).
optimizable edge(39, 6).
0.9::edge(8, 7).
optimizable edge(9, 7).
0.9::edge(11, 7).
optimizable edge(12, 7).
0.9::edge(13, 7).
optimizable edge(35, 7).
0.9::edge(39, 7).
optimizable edge(9, 8).
0.9::edge(10, 8).
optimizable edge(20, 8).
0.9::edge(21, 8).
optimizable edge(22, 8).
0.9::edge(23, 8).
optimizable edge(24, 8).
0.9::edge(35, 8).
optimizable edge(38, 8).
0.9::edge(39, 8).
optimizable edge(10, 9).
0.9::edge(35, 9).
optimizable edge(36, 9).
0.9::edge(39, 9).
optimizable edge(35, 10).
0.9::edge(36, 10).
optimizable edge(39, 10).
0.9::edge(19, 11).
optimizable edge(35, 11).
0.9::edge(36, 11).
optimizable edge(39, 11).
0.9::edge(19, 12).
optimizable edge(26, 12).
0.9::edge(35, 12).
optimizable edge(36, 12).
0.9::edge(38, 12).
optimizable edge(39, 12).
0.9::edge(35, 13).
optimizable edge(36, 13).
0.9::edge(38, 13).
optimizable edge(39, 13).
0.9::edge(25, 14).
optimizable edge(26, 14).
0.9::edge(27, 14).
optimizable edge(28, 14).
0.9::edge(29, 14).
optimizable edge(36, 14).
0.9::edge(39, 14).
optimizable edge(19, 15).
0.9::edge(25, 15).
optimizable edge(26, 15).
0.9::edge(27, 15).
optimizable edge(28, 15).
0.9::edge(29, 15).
optimizable edge(36, 15).
0.9::edge(39, 15).
optimizable edge(19, 16).
0.9::edge(27, 16).
optimizable edge(36, 16).
0.9::edge(39, 16).
optimizable edge(36, 17).
0.9::edge(39, 17).
optimizable edge(19, 18).
0.9::edge(25, 18).
optimizable edge(26, 18).
0.9::edge(27, 18).
optimizable edge(29, 18).
0.9::edge(32, 18).
optimizable edge(36, 18).
0.9::edge(39, 18).
optimizable edge(33, 19).
0.9::edge(36, 19).
optimizable edge(38, 19).
0.9::edge(39, 19).
optimizable edge(36, 20).
0.9::edge(38, 20).
optimizable edge(39, 20).
0.9::edge(33, 21).
optimizable edge(36, 21).
0.9::edge(38, 21).
optimizable edge(39, 21).
0.9::edge(27, 22).
optimizable edge(28, 22).
0.9::edge(30, 22).
optimizable edge(31, 22).
0.9::edge(32, 22).
optimizable edge(33, 22).
0.9::edge(35, 22).
optimizable edge(36, 22).
0.9::edge(38, 22).
optimizable edge(39, 22).
0.9::edge(30, 23).
optimizable edge(32, 23).
0.9::edge(33, 23).
optimizable edge(35, 23).
0.9::edge(36, 23).
optimizable edge(38, 23).
0.9::edge(39, 23).
optimizable edge(36, 24).
0.9::edge(38, 24).
optimizable edge(39, 24).
0.9::edge(36, 25).
optimizable edge(38, 25).
0.9::edge(39, 25).
optimizable edge(36, 26).
0.9::edge(38, 26).
optimizable edge(39, 26).
0.9::edge(30, 27).
optimizable edge(36, 27).
0.9::edge(38, 27).
optimizable edge(39, 27).
0.9::edge(36, 28).
optimizable edge(38, 28).
0.9::edge(39, 28).
optimizable edge(36, 29).
0.9::edge(38, 29).
optimizable edge(39, 29).
0.9::edge(36, 30).
optimizable edge(38, 30).
0.9::edge(39, 30).
optimizable edge(32, 31).
0.9::edge(36, 31).
optimizable edge(38, 31).
0.9::edge(39, 31).
optimizable edge(36, 32).
0.9::edge(38, 32).
optimizable edge(39, 32).
0.9::edge(36, 33).
optimizable edge(38, 33).
0.9::edge(39, 33).
optimizable edge(37, 34).
0.9::edge(36, 35).
optimizable edge(37, 35).

path(X, X).
path(X, Y):- path(X, Z), edge(Z, Y).



:- end_lpad.

run:- run_mma, run_ccsaq, run_slsqp.

run_mma(S,D):-
	statistics(runtime, [Start | _]), 
	prob_optimize(path(S,D),[edge(8,1) +edge(12,1) +edge(22,1) +edge(34,1) +edge(37,1) +edge(7,2) +edge(9,2) +edge(12,2) +edge(22,2) +edge(35,2) +edge(39,2) +edge(15,3) +edge(17,3) +edge(36,3) +edge(17,4) +edge(37,4) +edge(6,5) +edge(35,5) +edge(7,6) +edge(39,6) +edge(9,7) +edge(12,7) +edge(35,7) +edge(9,8) +edge(20,8) +edge(22,8) +edge(24,8) +edge(38,8) +edge(10,9) +edge(36,9) +edge(35,10) +edge(39,10) +edge(35,11) +edge(39,11) +edge(26,12) +edge(36,12) +edge(39,12) +edge(36,13) +edge(39,13) +edge(26,14) +edge(28,14) +edge(36,14) +edge(19,15) +edge(26,15) +edge(28,15) +edge(36,15) +edge(19,16) +edge(36,16) +edge(36,17) +edge(19,18) +edge(26,18) +edge(29,18) +edge(36,18) +edge(33,19) +edge(38,19) +edge(36,20) +edge(39,20) +edge(36,21) +edge(39,21) +edge(28,22) +edge(31,22) +edge(33,22) +edge(36,22) +edge(39,22) +edge(32,23) +edge(35,23) +edge(38,23) +edge(36,24) +edge(39,24) +edge(38,25) +edge(36,26) +edge(39,26) +edge(36,27) +edge(39,27) +edge(38,28) +edge(36,29) +edge(39,29) +edge(38,30) +edge(32,31) +edge(38,31) +edge(36,32) +edge(39,32) +edge(38,33) +edge(37,34) +edge(37,35) ],[path(S,D) > 0.8],mma,-1,A),
	statistics(runtime, [Stop | _]),
	Runtime is Stop - Start,
	writeln('mma'),
	writeln(Runtime),
	writeln(A).

run_ccsaq(S,D):-
	statistics(runtime, [Start | _]), 
	prob_optimize(path(S,D),[edge(8,1) +edge(12,1) +edge(22,1) +edge(34,1) +edge(37,1) +edge(7,2) +edge(9,2) +edge(12,2) +edge(22,2) +edge(35,2) +edge(39,2) +edge(15,3) +edge(17,3) +edge(36,3) +edge(17,4) +edge(37,4) +edge(6,5) +edge(35,5) +edge(7,6) +edge(39,6) +edge(9,7) +edge(12,7) +edge(35,7) +edge(9,8) +edge(20,8) +edge(22,8) +edge(24,8) +edge(38,8) +edge(10,9) +edge(36,9) +edge(35,10) +edge(39,10) +edge(35,11) +edge(39,11) +edge(26,12) +edge(36,12) +edge(39,12) +edge(36,13) +edge(39,13) +edge(26,14) +edge(28,14) +edge(36,14) +edge(19,15) +edge(26,15) +edge(28,15) +edge(36,15) +edge(19,16) +edge(36,16) +edge(36,17) +edge(19,18) +edge(26,18) +edge(29,18) +edge(36,18) +edge(33,19) +edge(38,19) +edge(36,20) +edge(39,20) +edge(36,21) +edge(39,21) +edge(28,22) +edge(31,22) +edge(33,22) +edge(36,22) +edge(39,22) +edge(32,23) +edge(35,23) +edge(38,23) +edge(36,24) +edge(39,24) +edge(38,25) +edge(36,26) +edge(39,26) +edge(36,27) +edge(39,27) +edge(38,28) +edge(36,29) +edge(39,29) +edge(38,30) +edge(32,31) +edge(38,31) +edge(36,32) +edge(39,32) +edge(38,33) +edge(37,34) +edge(37,35) ],[path(S,D) > 0.8],ccsaq,-1,A),
	statistics(runtime, [Stop | _]),
	Runtime is Stop - Start,
	writeln('ccsaq'),
	writeln(Runtime),
	writeln(A).

run_slsqp(S,D):-
	statistics(runtime, [Start | _]), 
	prob_optimize(path(S,D),[edge(8,1) +edge(12,1) +edge(22,1) +edge(34,1) +edge(37,1) +edge(7,2) +edge(9,2) +edge(12,2) +edge(22,2) +edge(35,2) +edge(39,2) +edge(15,3) +edge(17,3) +edge(36,3) +edge(17,4) +edge(37,4) +edge(6,5) +edge(35,5) +edge(7,6) +edge(39,6) +edge(9,7) +edge(12,7) +edge(35,7) +edge(9,8) +edge(20,8) +edge(22,8) +edge(24,8) +edge(38,8) +edge(10,9) +edge(36,9) +edge(35,10) +edge(39,10) +edge(35,11) +edge(39,11) +edge(26,12) +edge(36,12) +edge(39,12) +edge(36,13) +edge(39,13) +edge(26,14) +edge(28,14) +edge(36,14) +edge(19,15) +edge(26,15) +edge(28,15) +edge(36,15) +edge(19,16) +edge(36,16) +edge(36,17) +edge(19,18) +edge(26,18) +edge(29,18) +edge(36,18) +edge(33,19) +edge(38,19) +edge(36,20) +edge(39,20) +edge(36,21) +edge(39,21) +edge(28,22) +edge(31,22) +edge(33,22) +edge(36,22) +edge(39,22) +edge(32,23) +edge(35,23) +edge(38,23) +edge(36,24) +edge(39,24) +edge(38,25) +edge(36,26) +edge(39,26) +edge(36,27) +edge(39,27) +edge(38,28) +edge(36,29) +edge(39,29) +edge(38,30) +edge(32,31) +edge(38,31) +edge(36,32) +edge(39,32) +edge(38,33) +edge(37,34) +edge(37,35) ],[path(S,D) > 0.8],slsqp,-1,A),
	statistics(runtime, [Stop | _]),
	Runtime is Stop - Start,
	writeln('slsqp'),
	writeln(Runtime),
	writeln(A).
