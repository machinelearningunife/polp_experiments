:- use_module(library(pita)).
:- pita.
:- begin_lpad.

0.9::edge(2, 1).
optimizable edge(3, 1).
0.9::edge(148, 1).
optimizable edge(181, 1).
0.9::edge(62, 2).
optimizable edge(63, 2).
0.9::edge(65, 2).
optimizable edge(148, 2).
0.9::edge(181, 2).
optimizable edge(148, 3).
0.9::edge(168, 3).
optimizable edge(169, 3).
0.9::edge(180, 3).
optimizable edge(181, 3).
0.9::edge(5, 4).
optimizable edge(6, 4).
0.9::edge(7, 4).
optimizable edge(8, 4).
0.9::edge(182, 4).
optimizable edge(183, 4).
0.9::edge(184, 4).
optimizable edge(6, 5).
0.9::edge(7, 5).
optimizable edge(8, 5).
0.9::edge(9, 5).
optimizable edge(10, 5).
0.9::edge(11, 5).
optimizable edge(7, 6).
0.9::edge(8, 6).
optimizable edge(69, 6).
0.9::edge(182, 6).
optimizable edge(183, 6).
0.9::edge(234, 6).
optimizable edge(8, 7).
0.9::edge(9, 7).
optimizable edge(10, 7).
0.9::edge(11, 7).
optimizable edge(9, 8).
0.9::edge(183, 8).
optimizable edge(10, 9).
0.9::edge(11, 9).
optimizable edge(76, 9).
0.9::edge(211, 9).
optimizable edge(11, 10).
0.9::edge(195, 10).
optimizable edge(211, 10).
0.9::edge(193, 11).
optimizable edge(211, 11).
0.9::edge(13, 12).
optimizable edge(14, 12).
0.9::edge(17, 12).
optimizable edge(18, 12).
0.9::edge(20, 12).
optimizable edge(21, 12).
0.9::edge(162, 12).
optimizable edge(164, 12).
0.9::edge(14, 13).
optimizable edge(15, 13).
0.9::edge(16, 13).
optimizable edge(17, 13).
0.9::edge(18, 13).
optimizable edge(16, 14).
0.9::edge(17, 14).
optimizable edge(19, 14).
0.9::edge(20, 14).
optimizable edge(21, 14).
0.9::edge(16, 15).
optimizable edge(18, 15).
0.9::edge(93, 15).
optimizable edge(95, 15).
0.9::edge(97, 15).
optimizable edge(98, 15).
0.9::edge(131, 15).
optimizable edge(132, 15).
0.9::edge(98, 16).
optimizable edge(131, 16).
0.9::edge(132, 16).
optimizable edge(133, 16).
0.9::edge(138, 16).
optimizable edge(139, 16).
0.9::edge(141, 16).
optimizable edge(18, 17).
0.9::edge(21, 17).
optimizable edge(93, 18).
0.9::edge(163, 18).
optimizable edge(197, 18).
0.9::edge(20, 19).
optimizable edge(21, 19).
0.9::edge(22, 19).
optimizable edge(146, 19).
0.9::edge(21, 20).
optimizable edge(22, 20).
0.9::edge(22, 21).
optimizable edge(209, 22).
0.9::edge(24, 23).
optimizable edge(248, 24).
0.9::edge(26, 25).
optimizable edge(27, 25).
0.9::edge(28, 25).
optimizable edge(29, 25).
0.9::edge(30, 25).
optimizable edge(31, 25).
0.9::edge(32, 25).
optimizable edge(27, 26).
0.9::edge(30, 26).
optimizable edge(257, 26).
0.9::edge(28, 27).
optimizable edge(157, 27).
0.9::edge(158, 27).
optimizable edge(29, 28).
0.9::edge(31, 28).
optimizable edge(157, 28).
0.9::edge(31, 29).
optimizable edge(110, 29).
0.9::edge(160, 29).
optimizable edge(203, 29).
0.9::edge(32, 30).
optimizable edge(39, 30).
0.9::edge(257, 30).
optimizable edge(205, 31).
0.9::edge(243, 31).
optimizable edge(244, 31).
0.9::edge(38, 32).
optimizable edge(39, 32).
0.9::edge(122, 32).
optimizable edge(34, 33).
0.9::edge(35, 33).
optimizable edge(36, 33).
0.9::edge(37, 33).
optimizable edge(58, 33).
0.9::edge(79, 34).
optimizable edge(81, 34).
0.9::edge(82, 34).
optimizable edge(83, 34).
0.9::edge(172, 34).
optimizable edge(173, 34).
0.9::edge(36, 35).
optimizable edge(82, 35).
0.9::edge(83, 35).
optimizable edge(99, 35).
0.9::edge(100, 35).
optimizable edge(102, 35).
0.9::edge(173, 35).
optimizable edge(37, 36).
0.9::edge(57, 36).
optimizable edge(58, 36).
0.9::edge(61, 36).
optimizable edge(250, 36).
0.9::edge(57, 37).
optimizable edge(58, 37).
0.9::edge(100, 38).
optimizable edge(102, 38).
0.9::edge(103, 38).
optimizable edge(119, 38).
0.9::edge(243, 38).
optimizable edge(122, 39).
0.9::edge(41, 40).
optimizable edge(42, 40).
0.9::edge(43, 40).
optimizable edge(44, 40).
0.9::edge(56, 40).
optimizable edge(42, 41).
0.9::edge(43, 41).
optimizable edge(44, 41).
0.9::edge(54, 41).
optimizable edge(55, 41).
0.9::edge(56, 41).
optimizable edge(43, 42).
0.9::edge(54, 42).
optimizable edge(55, 42).
0.9::edge(56, 42).
optimizable edge(86, 42).
0.9::edge(44, 43).
optimizable edge(64, 43).
0.9::edge(129, 43).
optimizable edge(54, 44).
0.9::edge(124, 44).
optimizable edge(126, 44).
0.9::edge(129, 44).
optimizable edge(46, 45).
0.9::edge(47, 45).
optimizable edge(48, 45).
0.9::edge(212, 45).
optimizable edge(47, 46).
0.9::edge(48, 46).
optimizable edge(49, 46).
0.9::edge(174, 46).
optimizable edge(48, 47).
0.9::edge(225, 47).
optimizable edge(212, 48).
0.9::edge(174, 49).
optimizable edge(175, 49).
0.9::edge(176, 49).
optimizable edge(178, 49).
0.9::edge(179, 49).
optimizable edge(213, 49).
0.9::edge(290, 49).
optimizable edge(51, 50).
0.9::edge(52, 50).
optimizable edge(53, 50).
0.9::edge(55, 50).
optimizable edge(92, 50).
0.9::edge(135, 50).
optimizable edge(52, 51).
0.9::edge(53, 51).
optimizable edge(134, 51).
0.9::edge(53, 52).
optimizable edge(89, 52).
0.9::edge(92, 52).
optimizable edge(134, 53).
0.9::edge(127, 54).
optimizable edge(233, 54).
0.9::edge(56, 55).
optimizable edge(86, 55).
0.9::edge(87, 55).
optimizable edge(88, 55).
0.9::edge(135, 55).
optimizable edge(136, 55).
0.9::edge(86, 56).
optimizable edge(87, 56).
0.9::edge(88, 56).
optimizable edge(136, 56).
0.9::edge(58, 57).
optimizable edge(59, 57).
0.9::edge(60, 57).
optimizable edge(61, 57).
0.9::edge(250, 58).
optimizable edge(60, 59).
0.9::edge(186, 59).
optimizable edge(187, 59).
0.9::edge(125, 60).
optimizable edge(186, 60).
0.9::edge(187, 60).
optimizable edge(192, 60).
0.9::edge(288, 60).
optimizable edge(249, 61).
0.9::edge(250, 61).
optimizable edge(63, 62).
0.9::edge(64, 62).
optimizable edge(65, 62).
0.9::edge(66, 62).
optimizable edge(64, 63).
0.9::edge(66, 63).
optimizable edge(217, 63).
0.9::edge(65, 64).
optimizable edge(66, 64).
0.9::edge(215, 64).
optimizable edge(217, 64).
0.9::edge(148, 65).
optimizable edge(215, 66).
0.9::edge(216, 66).
optimizable edge(217, 66).
0.9::edge(68, 67).
optimizable edge(69, 67).
0.9::edge(70, 67).
optimizable edge(71, 67).
0.9::edge(72, 67).
optimizable edge(69, 68).
0.9::edge(70, 68).
optimizable edge(184, 68).
0.9::edge(261, 68).
optimizable edge(70, 69).
0.9::edge(71, 69).
optimizable edge(184, 69).
0.9::edge(234, 69).
optimizable edge(261, 69).
0.9::edge(71, 70).
optimizable edge(72, 70).
0.9::edge(74, 70).
optimizable edge(234, 70).
0.9::edge(234, 71).
optimizable edge(74, 72).
0.9::edge(75, 72).
optimizable edge(76, 72).
0.9::edge(234, 72).
optimizable edge(74, 73).
0.9::edge(75, 73).
optimizable edge(76, 73).
0.9::edge(77, 73).
optimizable edge(78, 73).
0.9::edge(241, 73).
optimizable edge(75, 74).
0.9::edge(76, 74).
optimizable edge(77, 74).
0.9::edge(234, 74).
optimizable edge(76, 75).
0.9::edge(77, 75).
optimizable edge(234, 75).
0.9::edge(77, 76).
optimizable edge(78, 76).
0.9::edge(234, 76).
optimizable edge(241, 76).
0.9::edge(78, 77).
optimizable edge(115, 77).
0.9::edge(241, 77).
optimizable edge(113, 78).
0.9::edge(114, 78).
optimizable edge(115, 78).
0.9::edge(241, 78).
optimizable edge(80, 79).
0.9::edge(81, 79).
optimizable edge(82, 79).
0.9::edge(83, 79).
optimizable edge(84, 79).
0.9::edge(85, 79).
optimizable edge(172, 79).
0.9::edge(81, 80).
optimizable edge(84, 80).
0.9::edge(85, 80).
optimizable edge(172, 80).
0.9::edge(273, 80).
optimizable edge(83, 81).
0.9::edge(85, 81).
optimizable edge(172, 81).
0.9::edge(194, 81).
optimizable edge(211, 81).
0.9::edge(273, 81).
optimizable edge(83, 82).
0.9::edge(100, 82).
optimizable edge(173, 82).
0.9::edge(173, 83).
optimizable edge(185, 84).
0.9::edge(188, 84).
optimizable edge(190, 84).
0.9::edge(252, 84).
optimizable edge(105, 85).
0.9::edge(159, 85).
optimizable edge(247, 85).
0.9::edge(273, 85).
optimizable edge(87, 86).
0.9::edge(88, 86).
optimizable edge(88, 87).
0.9::edge(134, 87).
optimizable edge(135, 87).
0.9::edge(136, 87).
optimizable edge(137, 87).
0.9::edge(165, 87).
optimizable edge(90, 89).
0.9::edge(91, 89).
optimizable edge(92, 89).
0.9::edge(92, 91).
optimizable edge(171, 91).
0.9::edge(135, 92).
optimizable edge(171, 92).
0.9::edge(264, 92).
optimizable edge(266, 92).
0.9::edge(94, 93).
optimizable edge(95, 93).
0.9::edge(96, 93).
optimizable edge(97, 93).
0.9::edge(98, 93).
optimizable edge(240, 93).
0.9::edge(95, 94).
optimizable edge(96, 94).
0.9::edge(202, 94).
optimizable edge(237, 94).
0.9::edge(240, 94).
optimizable edge(96, 95).
0.9::edge(97, 95).
optimizable edge(98, 95).
0.9::edge(202, 95).
optimizable edge(97, 96).
0.9::edge(240, 96).
optimizable edge(98, 97).
0.9::edge(131, 97).
optimizable edge(132, 97).
0.9::edge(131, 98).
optimizable edge(132, 98).
0.9::edge(100, 99).
optimizable edge(101, 99).
0.9::edge(102, 99).
optimizable edge(103, 99).
0.9::edge(101, 100).
optimizable edge(102, 100).
0.9::edge(103, 100).
optimizable edge(102, 101).
0.9::edge(103, 101).
optimizable edge(103, 102).
0.9::edge(173, 102).
optimizable edge(243, 102).
0.9::edge(244, 102).
optimizable edge(243, 103).
0.9::edge(105, 104).
optimizable edge(106, 104).
0.9::edge(107, 104).
optimizable edge(108, 104).
0.9::edge(109, 104).
optimizable edge(111, 104).
0.9::edge(107, 105).
optimizable edge(109, 105).
0.9::edge(155, 105).
optimizable edge(159, 105).
0.9::edge(247, 105).
optimizable edge(247, 106).
0.9::edge(248, 106).
optimizable edge(108, 107).
0.9::edge(109, 107).
optimizable edge(110, 107).
0.9::edge(155, 107).
optimizable edge(160, 107).
0.9::edge(109, 108).
optimizable edge(110, 108).
0.9::edge(111, 108).
optimizable edge(112, 108).
0.9::edge(200, 109).
optimizable edge(260, 109).
0.9::edge(160, 110).
optimizable edge(203, 110).
0.9::edge(204, 110).
optimizable edge(253, 110).
0.9::edge(112, 111).
optimizable edge(236, 111).
0.9::edge(238, 111).
optimizable edge(239, 111).
0.9::edge(240, 111).
optimizable edge(161, 112).
0.9::edge(162, 112).
optimizable edge(253, 112).
0.9::edge(114, 113).
optimizable edge(115, 113).
0.9::edge(116, 113).
optimizable edge(117, 113).
0.9::edge(118, 113).
optimizable edge(119, 113).
0.9::edge(115, 114).
optimizable edge(117, 114).
0.9::edge(119, 114).
optimizable edge(241, 114).
0.9::edge(119, 115).
optimizable edge(241, 115).
0.9::edge(117, 116).
optimizable edge(118, 116).
0.9::edge(119, 116).
optimizable edge(120, 116).
0.9::edge(121, 116).
optimizable edge(123, 116).
0.9::edge(118, 117).
optimizable edge(119, 117).
0.9::edge(123, 117).
optimizable edge(119, 118).
0.9::edge(120, 118).
optimizable edge(121, 118).
0.9::edge(123, 118).
optimizable edge(121, 120).
0.9::edge(122, 120).
optimizable edge(123, 120).
0.9::edge(122, 121).
optimizable edge(123, 121).
0.9::edge(123, 122).
optimizable edge(125, 124).
0.9::edge(126, 124).
optimizable edge(127, 124).
0.9::edge(128, 124).
optimizable edge(129, 124).
0.9::edge(130, 124).
optimizable edge(192, 124).
0.9::edge(126, 125).
optimizable edge(128, 125).
0.9::edge(130, 125).
optimizable edge(191, 125).
0.9::edge(192, 125).
optimizable edge(127, 126).
0.9::edge(128, 126).
optimizable edge(130, 126).
0.9::edge(191, 126).
optimizable edge(192, 126).
0.9::edge(254, 126).
optimizable edge(130, 127).
0.9::edge(191, 127).
optimizable edge(233, 127).
0.9::edge(192, 128).
optimizable edge(254, 128).
0.9::edge(255, 128).
optimizable edge(256, 128).
0.9::edge(254, 129).
optimizable edge(256, 129).
0.9::edge(262, 129).
optimizable edge(191, 130).
0.9::edge(192, 130).
optimizable edge(132, 131).
0.9::edge(133, 131).
optimizable edge(139, 131).
0.9::edge(139, 132).
optimizable edge(138, 133).
0.9::edge(139, 133).
optimizable edge(140, 133).
0.9::edge(141, 133).
optimizable edge(135, 134).
0.9::edge(136, 134).
optimizable edge(165, 134).
0.9::edge(264, 134).
optimizable edge(266, 134).
0.9::edge(269, 134).
optimizable edge(271, 134).
0.9::edge(264, 135).
optimizable edge(266, 135).
0.9::edge(137, 136).
optimizable edge(165, 136).
0.9::edge(166, 136).
optimizable edge(167, 136).
0.9::edge(165, 137).
optimizable edge(166, 137).
0.9::edge(167, 137).
optimizable edge(282, 137).
0.9::edge(139, 138).
optimizable edge(140, 138).
0.9::edge(141, 138).
optimizable edge(141, 139).
0.9::edge(141, 140).
optimizable edge(245, 140).
0.9::edge(267, 140).
optimizable edge(143, 142).
0.9::edge(144, 142).
optimizable edge(145, 142).
0.9::edge(189, 142).
optimizable edge(258, 142).
0.9::edge(288, 142).
optimizable edge(289, 142).
0.9::edge(144, 143).
optimizable edge(145, 143).
0.9::edge(146, 143).
optimizable edge(147, 143).
0.9::edge(146, 144).
optimizable edge(204, 144).
0.9::edge(206, 144).
optimizable edge(258, 144).
0.9::edge(259, 144).
optimizable edge(218, 145).
0.9::edge(289, 145).
optimizable edge(147, 146).
0.9::edge(204, 146).
optimizable edge(206, 146).
0.9::edge(287, 146).
optimizable edge(218, 147).
0.9::edge(220, 147).
optimizable edge(279, 147).
0.9::edge(280, 147).
optimizable edge(287, 147).
0.9::edge(168, 148).
optimizable edge(180, 148).
0.9::edge(181, 148).
optimizable edge(150, 149).
0.9::edge(151, 149).
optimizable edge(152, 149).
0.9::edge(153, 149).
optimizable edge(154, 149).
0.9::edge(151, 150).
optimizable edge(152, 150).
0.9::edge(196, 150).
optimizable edge(152, 151).
0.9::edge(161, 151).
optimizable edge(196, 151).
0.9::edge(251, 151).
optimizable edge(291, 151).
0.9::edge(196, 152).
optimizable edge(154, 153).
0.9::edge(161, 153).
optimizable edge(162, 153).
0.9::edge(163, 153).
optimizable edge(164, 153).
0.9::edge(284, 153).
optimizable edge(161, 154).
0.9::edge(162, 154).
optimizable edge(279, 154).
0.9::edge(283, 154).
optimizable edge(284, 154).
0.9::edge(285, 154).
optimizable edge(156, 155).
0.9::edge(157, 155).
optimizable edge(158, 155).
0.9::edge(159, 155).
optimizable edge(160, 155).
0.9::edge(157, 156).
optimizable edge(158, 156).
0.9::edge(193, 156).
optimizable edge(194, 156).
0.9::edge(195, 156).
optimizable edge(158, 157).
0.9::edge(160, 157).
optimizable edge(162, 161).
0.9::edge(163, 161).
optimizable edge(164, 161).
0.9::edge(164, 162).
optimizable edge(253, 162).
0.9::edge(287, 162).
optimizable edge(164, 163).
0.9::edge(197, 163).
optimizable edge(197, 164).
0.9::edge(284, 164).
optimizable edge(166, 165).
0.9::edge(167, 165).
optimizable edge(271, 165).
0.9::edge(167, 166).
optimizable edge(210, 166).
0.9::edge(282, 166).
optimizable edge(174, 167).
0.9::edge(176, 167).
optimizable edge(210, 167).
0.9::edge(282, 167).
optimizable edge(169, 168).
0.9::edge(170, 168).
optimizable edge(170, 169).
0.9::edge(180, 169).
optimizable edge(218, 169).
0.9::edge(281, 169).
optimizable edge(281, 170).
0.9::edge(264, 171).
optimizable edge(265, 171).
0.9::edge(211, 172).
optimizable edge(273, 172).
0.9::edge(175, 174).
optimizable edge(176, 174).
0.9::edge(177, 174).
optimizable edge(210, 174).
0.9::edge(282, 174).
optimizable edge(176, 175).
0.9::edge(177, 175).
optimizable edge(178, 175).
0.9::edge(179, 175).
optimizable edge(177, 176).
0.9::edge(179, 176).
optimizable edge(210, 176).
0.9::edge(282, 176).
optimizable edge(179, 177).
0.9::edge(210, 177).
optimizable edge(179, 178).
0.9::edge(290, 178).
optimizable edge(213, 179).
0.9::edge(290, 179).
optimizable edge(181, 180).
0.9::edge(183, 182).
optimizable edge(184, 182).
0.9::edge(261, 184).
optimizable edge(186, 185).
0.9::edge(188, 185).
optimizable edge(252, 185).
0.9::edge(187, 186).
optimizable edge(188, 186).
0.9::edge(190, 186).
optimizable edge(188, 187).
0.9::edge(189, 187).
optimizable edge(190, 188).
0.9::edge(252, 188).
optimizable edge(275, 188).
0.9::edge(276, 188).
optimizable edge(288, 189).
0.9::edge(289, 189).
optimizable edge(252, 190).
0.9::edge(274, 190).
optimizable edge(275, 190).
0.9::edge(276, 190).
optimizable edge(192, 191).
0.9::edge(254, 192).
optimizable edge(194, 193).
0.9::edge(195, 193).
optimizable edge(195, 194).
0.9::edge(211, 194).
optimizable edge(211, 195).
0.9::edge(251, 196).
optimizable edge(277, 196).
0.9::edge(291, 196).
optimizable edge(199, 198).
0.9::edge(200, 198).
optimizable edge(201, 198).
0.9::edge(202, 198).
optimizable edge(257, 198).
0.9::edge(200, 199).
optimizable edge(201, 199).
0.9::edge(202, 199).
optimizable edge(237, 199).
0.9::edge(202, 200).
optimizable edge(237, 200).
0.9::edge(240, 200).
optimizable edge(260, 200).
0.9::edge(202, 201).
optimizable edge(257, 201).
0.9::edge(257, 202).
optimizable edge(204, 203).
0.9::edge(205, 203).
optimizable edge(206, 203).
0.9::edge(206, 204).
optimizable edge(253, 204).
0.9::edge(287, 204).
optimizable edge(206, 205).
0.9::edge(244, 205).
optimizable edge(259, 205).
0.9::edge(259, 206).
optimizable edge(286, 206).
0.9::edge(208, 207).
optimizable edge(286, 207).
0.9::edge(209, 208).
optimizable edge(210, 208).
0.9::edge(286, 208).
optimizable edge(282, 210).
0.9::edge(213, 212).
optimizable edge(214, 212).
0.9::edge(214, 213).
optimizable edge(290, 213).
0.9::edge(290, 214).
optimizable edge(216, 215).
0.9::edge(217, 215).
optimizable edge(228, 215).
0.9::edge(229, 215).
optimizable edge(217, 216).
0.9::edge(227, 216).
optimizable edge(228, 216).
0.9::edge(229, 216).
optimizable edge(228, 217).
0.9::edge(219, 218).
optimizable edge(220, 218).
0.9::edge(221, 218).
optimizable edge(221, 219).
0.9::edge(227, 219).
optimizable edge(231, 219).
0.9::edge(278, 219).
optimizable edge(221, 220).
0.9::edge(279, 220).
optimizable edge(280, 220).
0.9::edge(281, 220).
optimizable edge(227, 221).
0.9::edge(229, 221).
optimizable edge(231, 221).
0.9::edge(232, 221).
optimizable edge(223, 222).
0.9::edge(224, 222).
optimizable edge(225, 222).
0.9::edge(226, 222).
optimizable edge(224, 223).
0.9::edge(226, 223).
optimizable edge(235, 223).
0.9::edge(246, 223).
optimizable edge(270, 223).
0.9::edge(226, 224).
optimizable edge(235, 224).
0.9::edge(246, 224).
optimizable edge(268, 224).
0.9::edge(271, 224).
optimizable edge(235, 226).
0.9::edge(246, 226).
optimizable edge(228, 227).
0.9::edge(229, 227).
optimizable edge(230, 227).
0.9::edge(231, 227).
optimizable edge(232, 227).
0.9::edge(229, 228).
optimizable edge(230, 228).
0.9::edge(232, 228).
optimizable edge(230, 229).
0.9::edge(232, 229).
optimizable edge(231, 230).
0.9::edge(232, 230).
optimizable edge(232, 231).
0.9::edge(246, 235).
optimizable edge(237, 236).
0.9::edge(238, 236).
optimizable edge(239, 236).
0.9::edge(240, 236).
optimizable edge(260, 236).
0.9::edge(240, 237).
optimizable edge(260, 237).
0.9::edge(239, 238).
optimizable edge(243, 242).
0.9::edge(244, 242).
optimizable edge(245, 242).
0.9::edge(244, 243).
optimizable edge(259, 244).
0.9::edge(267, 245).
optimizable edge(268, 246).
0.9::edge(270, 246).
optimizable edge(271, 246).
0.9::edge(258, 249).
optimizable edge(288, 249).
0.9::edge(274, 251).
optimizable edge(276, 251).
0.9::edge(277, 251).
optimizable edge(291, 251).
0.9::edge(275, 252).
optimizable edge(276, 252).
0.9::edge(255, 254).
optimizable edge(256, 254).
0.9::edge(256, 255).
optimizable edge(262, 255).
0.9::edge(263, 255).
optimizable edge(278, 255).
0.9::edge(289, 255).
optimizable edge(262, 256).
0.9::edge(263, 256).
optimizable edge(259, 258).
0.9::edge(286, 259).
optimizable edge(263, 262).
0.9::edge(278, 262).
optimizable edge(278, 263).
0.9::edge(265, 264).
optimizable edge(266, 264).
0.9::edge(269, 266).
optimizable edge(270, 266).
0.9::edge(271, 266).
optimizable edge(272, 266).
0.9::edge(269, 268).
optimizable edge(270, 268).
0.9::edge(271, 268).
optimizable edge(272, 268).
0.9::edge(271, 269).
optimizable edge(272, 269).
0.9::edge(271, 270).
optimizable edge(272, 270).
0.9::edge(272, 271).
optimizable edge(275, 274).
0.9::edge(276, 274).
optimizable edge(277, 274).
0.9::edge(291, 274).
optimizable edge(276, 275).
0.9::edge(277, 275).
optimizable edge(277, 276).
0.9::edge(291, 277).
optimizable edge(283, 279).
0.9::edge(287, 279).
optimizable edge(281, 280).
0.9::edge(283, 280).
optimizable edge(284, 283).
0.9::edge(285, 283).
optimizable edge(285, 284).

path(X, X).
path(X, Y):- path(X, Z), edge(Z, Y).



:- end_lpad.

run:- run_mma, run_ccsaq, run_slsqp.

run_mma(S,D):-
	statistics(runtime, [Start | _]), 
	prob_optimize(path(S,D),[edge(3,1) +edge(181,1) +edge(63,2) +edge(148,2) +edge(148,3) +edge(169,3) +edge(181,3) +edge(6,4) +edge(8,4) +edge(183,4) +edge(6,5) +edge(8,5) +edge(10,5) +edge(7,6) +edge(69,6) +edge(183,6) +edge(8,7) +edge(10,7) +edge(9,8) +edge(10,9) +edge(76,9) +edge(11,10) +edge(211,10) +edge(211,11) +edge(14,12) +edge(18,12) +edge(21,12) +edge(164,12) +edge(15,13) +edge(17,13) +edge(16,14) +edge(19,14) +edge(21,14) +edge(18,15) +edge(95,15) +edge(98,15) +edge(132,15) +edge(131,16) +edge(133,16) +edge(139,16) +edge(18,17) +edge(93,18) +edge(197,18) +edge(21,19) +edge(146,19) +edge(22,20) +edge(209,22) +edge(248,24) +edge(27,25) +edge(29,25) +edge(31,25) +edge(27,26) +edge(257,26) +edge(157,27) +edge(29,28) +edge(157,28) +edge(110,29) +edge(203,29) +edge(39,30) +edge(205,31) +edge(244,31) +edge(39,32) +edge(34,33) +edge(36,33) +edge(58,33) +edge(81,34) +edge(83,34) +edge(173,34) +edge(82,35) +edge(99,35) +edge(102,35) +edge(37,36) +edge(58,36) +edge(250,36) +edge(58,37) +edge(102,38) +edge(119,38) +edge(122,39) +edge(42,40) +edge(44,40) +edge(42,41) +edge(44,41) +edge(55,41) +edge(43,42) +edge(55,42) +edge(86,42) +edge(64,43) +edge(54,44) +edge(126,44) +edge(46,45) +edge(48,45) +edge(47,46) +edge(49,46) +edge(48,47) +edge(212,48) +edge(175,49) +edge(178,49) +edge(213,49) +edge(51,50) +edge(53,50) +edge(92,50) +edge(52,51) +edge(134,51) +edge(89,52) +edge(134,53) +edge(233,54) +edge(86,55) +edge(88,55) +edge(136,55) +edge(87,56) +edge(136,56) +edge(59,57) +edge(61,57) +edge(60,59) +edge(187,59) +edge(186,60) +edge(192,60) +edge(249,61) +edge(63,62) +edge(65,62) +edge(64,63) +edge(217,63) +edge(66,64) +edge(217,64) +edge(215,66) +edge(217,66) +edge(69,67) +edge(71,67) +edge(69,68) +edge(184,68) +edge(70,69) +edge(184,69) +edge(261,69) +edge(72,70) +edge(234,70) +edge(74,72) +edge(76,72) +edge(74,73) +edge(76,73) +edge(78,73) +edge(75,74) +edge(77,74) +edge(76,75) +edge(234,75) +edge(78,76) +edge(241,76) +edge(115,77) +edge(113,78) +edge(115,78) +edge(80,79) +edge(82,79) +edge(84,79) +edge(172,79) +edge(84,80) +edge(172,80) +edge(83,81) +edge(172,81) +edge(211,81) +edge(83,82) +edge(173,82) +edge(185,84) +edge(190,84) +edge(105,85) +edge(247,85) +edge(87,86) +edge(88,87) +edge(135,87) +edge(137,87) +edge(90,89) +edge(92,89) +edge(171,91) +edge(171,92) +edge(266,92) +edge(95,93) +edge(97,93) +edge(240,93) +edge(96,94) +edge(237,94) +edge(96,95) +edge(98,95) +edge(97,96) +edge(98,97) +edge(132,97) +edge(132,98) +edge(101,99) +edge(103,99) +edge(102,100) +edge(102,101) +edge(103,102) +edge(243,102) +edge(243,103) +edge(106,104) +edge(108,104) +edge(111,104) +edge(109,105) +edge(159,105) +edge(247,106) +edge(108,107) +edge(110,107) +edge(160,107) +edge(110,108) +edge(112,108) +edge(260,109) +edge(203,110) +edge(253,110) +edge(236,111) +edge(239,111) +edge(161,112) +edge(253,112) +edge(115,113) +edge(117,113) +edge(119,113) +edge(117,114) +edge(241,114) +edge(241,115) +edge(118,116) +edge(120,116) +edge(123,116) +edge(119,117) +edge(119,118) +edge(121,118) +edge(121,120) +edge(123,120) +edge(123,121) +edge(125,124) +edge(127,124) +edge(129,124) +edge(192,124) +edge(128,125) +edge(191,125) +edge(127,126) +edge(130,126) +edge(192,126) +edge(130,127) +edge(233,127) +edge(254,128) +edge(256,128) +edge(256,129) +edge(191,130) +edge(132,131) +edge(139,131) +edge(138,133) +edge(140,133) +edge(135,134) +edge(165,134) +edge(266,134) +edge(271,134) +edge(266,135) +edge(165,136) +edge(167,136) +edge(166,137) +edge(282,137) +edge(140,138) +edge(141,139) +edge(245,140) +edge(143,142) +edge(145,142) +edge(258,142) +edge(289,142) +edge(145,143) +edge(147,143) +edge(204,144) +edge(258,144) +edge(218,145) +edge(147,146) +edge(206,146) +edge(218,147) +edge(279,147) +edge(287,147) +edge(180,148) +edge(150,149) +edge(152,149) +edge(154,149) +edge(152,150) +edge(152,151) +edge(196,151) +edge(291,151) +edge(154,153) +edge(162,153) +edge(164,153) +edge(161,154) +edge(279,154) +edge(284,154) +edge(156,155) +edge(158,155) +edge(160,155) +edge(158,156) +edge(194,156) +edge(158,157) +edge(162,161) +edge(164,161) +edge(253,162) +edge(164,163) +edge(197,164) +edge(166,165) +edge(271,165) +edge(210,166) +edge(174,167) +edge(210,167) +edge(169,168) +edge(170,169) +edge(218,169) +edge(281,170) +edge(265,171) +edge(273,172) +edge(176,174) +edge(210,174) +edge(176,175) +edge(178,175) +edge(177,176) +edge(210,176) +edge(179,177) +edge(179,178) +edge(213,179) +edge(181,180) +edge(184,182) +edge(186,185) +edge(252,185) +edge(188,186) +edge(188,187) +edge(190,188) +edge(275,188) +edge(288,189) +edge(252,190) +edge(275,190) +edge(192,191) +edge(194,193) +edge(195,194) +edge(211,195) +edge(277,196) +edge(199,198) +edge(201,198) +edge(257,198) +edge(201,199) +edge(237,199) +edge(237,200) +edge(260,200) +edge(257,201) +edge(204,203) +edge(206,203) +edge(253,204) +edge(206,205) +edge(259,205) +edge(286,206) +edge(286,207) +edge(210,208) +edge(282,210) +edge(214,212) +edge(290,213) +edge(216,215) +edge(228,215) +edge(217,216) +edge(228,216) +edge(228,217) +edge(220,218) +edge(221,219) +edge(231,219) +edge(221,220) +edge(280,220) +edge(227,221) +edge(231,221) +edge(223,222) +edge(225,222) +edge(224,223) +edge(235,223) +edge(270,223) +edge(235,224) +edge(268,224) +edge(235,226) +edge(228,227) +edge(230,227) +edge(232,227) +edge(230,228) +edge(230,229) +edge(231,230) +edge(232,231) +edge(237,236) +edge(239,236) +edge(260,236) +edge(260,237) +edge(243,242) +edge(245,242) +edge(259,244) +edge(268,246) +edge(271,246) +edge(288,249) +edge(276,251) +edge(291,251) +edge(276,252) +edge(256,254) +edge(262,255) +edge(278,255) +edge(262,256) +edge(259,258) +edge(263,262) +edge(278,263) +edge(266,264) +edge(270,266) +edge(272,266) +edge(270,268) +edge(272,268) +edge(272,269) +edge(272,270) +edge(275,274) +edge(277,274) +edge(276,275) +edge(277,276) +edge(283,279) +edge(281,280) +edge(284,283) +edge(285,284) ],[path(S,D) > 0.8],mma,-1,A),
	statistics(runtime, [Stop | _]),
	Runtime is Stop - Start,
	writeln('mma'),
	writeln(Runtime),
	writeln(A).

run_ccsaq(S,D):-
	statistics(runtime, [Start | _]), 
	prob_optimize(path(S,D),[edge(3,1) +edge(181,1) +edge(63,2) +edge(148,2) +edge(148,3) +edge(169,3) +edge(181,3) +edge(6,4) +edge(8,4) +edge(183,4) +edge(6,5) +edge(8,5) +edge(10,5) +edge(7,6) +edge(69,6) +edge(183,6) +edge(8,7) +edge(10,7) +edge(9,8) +edge(10,9) +edge(76,9) +edge(11,10) +edge(211,10) +edge(211,11) +edge(14,12) +edge(18,12) +edge(21,12) +edge(164,12) +edge(15,13) +edge(17,13) +edge(16,14) +edge(19,14) +edge(21,14) +edge(18,15) +edge(95,15) +edge(98,15) +edge(132,15) +edge(131,16) +edge(133,16) +edge(139,16) +edge(18,17) +edge(93,18) +edge(197,18) +edge(21,19) +edge(146,19) +edge(22,20) +edge(209,22) +edge(248,24) +edge(27,25) +edge(29,25) +edge(31,25) +edge(27,26) +edge(257,26) +edge(157,27) +edge(29,28) +edge(157,28) +edge(110,29) +edge(203,29) +edge(39,30) +edge(205,31) +edge(244,31) +edge(39,32) +edge(34,33) +edge(36,33) +edge(58,33) +edge(81,34) +edge(83,34) +edge(173,34) +edge(82,35) +edge(99,35) +edge(102,35) +edge(37,36) +edge(58,36) +edge(250,36) +edge(58,37) +edge(102,38) +edge(119,38) +edge(122,39) +edge(42,40) +edge(44,40) +edge(42,41) +edge(44,41) +edge(55,41) +edge(43,42) +edge(55,42) +edge(86,42) +edge(64,43) +edge(54,44) +edge(126,44) +edge(46,45) +edge(48,45) +edge(47,46) +edge(49,46) +edge(48,47) +edge(212,48) +edge(175,49) +edge(178,49) +edge(213,49) +edge(51,50) +edge(53,50) +edge(92,50) +edge(52,51) +edge(134,51) +edge(89,52) +edge(134,53) +edge(233,54) +edge(86,55) +edge(88,55) +edge(136,55) +edge(87,56) +edge(136,56) +edge(59,57) +edge(61,57) +edge(60,59) +edge(187,59) +edge(186,60) +edge(192,60) +edge(249,61) +edge(63,62) +edge(65,62) +edge(64,63) +edge(217,63) +edge(66,64) +edge(217,64) +edge(215,66) +edge(217,66) +edge(69,67) +edge(71,67) +edge(69,68) +edge(184,68) +edge(70,69) +edge(184,69) +edge(261,69) +edge(72,70) +edge(234,70) +edge(74,72) +edge(76,72) +edge(74,73) +edge(76,73) +edge(78,73) +edge(75,74) +edge(77,74) +edge(76,75) +edge(234,75) +edge(78,76) +edge(241,76) +edge(115,77) +edge(113,78) +edge(115,78) +edge(80,79) +edge(82,79) +edge(84,79) +edge(172,79) +edge(84,80) +edge(172,80) +edge(83,81) +edge(172,81) +edge(211,81) +edge(83,82) +edge(173,82) +edge(185,84) +edge(190,84) +edge(105,85) +edge(247,85) +edge(87,86) +edge(88,87) +edge(135,87) +edge(137,87) +edge(90,89) +edge(92,89) +edge(171,91) +edge(171,92) +edge(266,92) +edge(95,93) +edge(97,93) +edge(240,93) +edge(96,94) +edge(237,94) +edge(96,95) +edge(98,95) +edge(97,96) +edge(98,97) +edge(132,97) +edge(132,98) +edge(101,99) +edge(103,99) +edge(102,100) +edge(102,101) +edge(103,102) +edge(243,102) +edge(243,103) +edge(106,104) +edge(108,104) +edge(111,104) +edge(109,105) +edge(159,105) +edge(247,106) +edge(108,107) +edge(110,107) +edge(160,107) +edge(110,108) +edge(112,108) +edge(260,109) +edge(203,110) +edge(253,110) +edge(236,111) +edge(239,111) +edge(161,112) +edge(253,112) +edge(115,113) +edge(117,113) +edge(119,113) +edge(117,114) +edge(241,114) +edge(241,115) +edge(118,116) +edge(120,116) +edge(123,116) +edge(119,117) +edge(119,118) +edge(121,118) +edge(121,120) +edge(123,120) +edge(123,121) +edge(125,124) +edge(127,124) +edge(129,124) +edge(192,124) +edge(128,125) +edge(191,125) +edge(127,126) +edge(130,126) +edge(192,126) +edge(130,127) +edge(233,127) +edge(254,128) +edge(256,128) +edge(256,129) +edge(191,130) +edge(132,131) +edge(139,131) +edge(138,133) +edge(140,133) +edge(135,134) +edge(165,134) +edge(266,134) +edge(271,134) +edge(266,135) +edge(165,136) +edge(167,136) +edge(166,137) +edge(282,137) +edge(140,138) +edge(141,139) +edge(245,140) +edge(143,142) +edge(145,142) +edge(258,142) +edge(289,142) +edge(145,143) +edge(147,143) +edge(204,144) +edge(258,144) +edge(218,145) +edge(147,146) +edge(206,146) +edge(218,147) +edge(279,147) +edge(287,147) +edge(180,148) +edge(150,149) +edge(152,149) +edge(154,149) +edge(152,150) +edge(152,151) +edge(196,151) +edge(291,151) +edge(154,153) +edge(162,153) +edge(164,153) +edge(161,154) +edge(279,154) +edge(284,154) +edge(156,155) +edge(158,155) +edge(160,155) +edge(158,156) +edge(194,156) +edge(158,157) +edge(162,161) +edge(164,161) +edge(253,162) +edge(164,163) +edge(197,164) +edge(166,165) +edge(271,165) +edge(210,166) +edge(174,167) +edge(210,167) +edge(169,168) +edge(170,169) +edge(218,169) +edge(281,170) +edge(265,171) +edge(273,172) +edge(176,174) +edge(210,174) +edge(176,175) +edge(178,175) +edge(177,176) +edge(210,176) +edge(179,177) +edge(179,178) +edge(213,179) +edge(181,180) +edge(184,182) +edge(186,185) +edge(252,185) +edge(188,186) +edge(188,187) +edge(190,188) +edge(275,188) +edge(288,189) +edge(252,190) +edge(275,190) +edge(192,191) +edge(194,193) +edge(195,194) +edge(211,195) +edge(277,196) +edge(199,198) +edge(201,198) +edge(257,198) +edge(201,199) +edge(237,199) +edge(237,200) +edge(260,200) +edge(257,201) +edge(204,203) +edge(206,203) +edge(253,204) +edge(206,205) +edge(259,205) +edge(286,206) +edge(286,207) +edge(210,208) +edge(282,210) +edge(214,212) +edge(290,213) +edge(216,215) +edge(228,215) +edge(217,216) +edge(228,216) +edge(228,217) +edge(220,218) +edge(221,219) +edge(231,219) +edge(221,220) +edge(280,220) +edge(227,221) +edge(231,221) +edge(223,222) +edge(225,222) +edge(224,223) +edge(235,223) +edge(270,223) +edge(235,224) +edge(268,224) +edge(235,226) +edge(228,227) +edge(230,227) +edge(232,227) +edge(230,228) +edge(230,229) +edge(231,230) +edge(232,231) +edge(237,236) +edge(239,236) +edge(260,236) +edge(260,237) +edge(243,242) +edge(245,242) +edge(259,244) +edge(268,246) +edge(271,246) +edge(288,249) +edge(276,251) +edge(291,251) +edge(276,252) +edge(256,254) +edge(262,255) +edge(278,255) +edge(262,256) +edge(259,258) +edge(263,262) +edge(278,263) +edge(266,264) +edge(270,266) +edge(272,266) +edge(270,268) +edge(272,268) +edge(272,269) +edge(272,270) +edge(275,274) +edge(277,274) +edge(276,275) +edge(277,276) +edge(283,279) +edge(281,280) +edge(284,283) +edge(285,284) ],[path(S,D) > 0.8],ccsaq,-1,A),
	statistics(runtime, [Stop | _]),
	Runtime is Stop - Start,
	writeln('ccsaq'),
	writeln(Runtime),
	writeln(A).

run_slsqp(S,D):-
	statistics(runtime, [Start | _]), 
	prob_optimize(path(S,D),[edge(3,1) +edge(181,1) +edge(63,2) +edge(148,2) +edge(148,3) +edge(169,3) +edge(181,3) +edge(6,4) +edge(8,4) +edge(183,4) +edge(6,5) +edge(8,5) +edge(10,5) +edge(7,6) +edge(69,6) +edge(183,6) +edge(8,7) +edge(10,7) +edge(9,8) +edge(10,9) +edge(76,9) +edge(11,10) +edge(211,10) +edge(211,11) +edge(14,12) +edge(18,12) +edge(21,12) +edge(164,12) +edge(15,13) +edge(17,13) +edge(16,14) +edge(19,14) +edge(21,14) +edge(18,15) +edge(95,15) +edge(98,15) +edge(132,15) +edge(131,16) +edge(133,16) +edge(139,16) +edge(18,17) +edge(93,18) +edge(197,18) +edge(21,19) +edge(146,19) +edge(22,20) +edge(209,22) +edge(248,24) +edge(27,25) +edge(29,25) +edge(31,25) +edge(27,26) +edge(257,26) +edge(157,27) +edge(29,28) +edge(157,28) +edge(110,29) +edge(203,29) +edge(39,30) +edge(205,31) +edge(244,31) +edge(39,32) +edge(34,33) +edge(36,33) +edge(58,33) +edge(81,34) +edge(83,34) +edge(173,34) +edge(82,35) +edge(99,35) +edge(102,35) +edge(37,36) +edge(58,36) +edge(250,36) +edge(58,37) +edge(102,38) +edge(119,38) +edge(122,39) +edge(42,40) +edge(44,40) +edge(42,41) +edge(44,41) +edge(55,41) +edge(43,42) +edge(55,42) +edge(86,42) +edge(64,43) +edge(54,44) +edge(126,44) +edge(46,45) +edge(48,45) +edge(47,46) +edge(49,46) +edge(48,47) +edge(212,48) +edge(175,49) +edge(178,49) +edge(213,49) +edge(51,50) +edge(53,50) +edge(92,50) +edge(52,51) +edge(134,51) +edge(89,52) +edge(134,53) +edge(233,54) +edge(86,55) +edge(88,55) +edge(136,55) +edge(87,56) +edge(136,56) +edge(59,57) +edge(61,57) +edge(60,59) +edge(187,59) +edge(186,60) +edge(192,60) +edge(249,61) +edge(63,62) +edge(65,62) +edge(64,63) +edge(217,63) +edge(66,64) +edge(217,64) +edge(215,66) +edge(217,66) +edge(69,67) +edge(71,67) +edge(69,68) +edge(184,68) +edge(70,69) +edge(184,69) +edge(261,69) +edge(72,70) +edge(234,70) +edge(74,72) +edge(76,72) +edge(74,73) +edge(76,73) +edge(78,73) +edge(75,74) +edge(77,74) +edge(76,75) +edge(234,75) +edge(78,76) +edge(241,76) +edge(115,77) +edge(113,78) +edge(115,78) +edge(80,79) +edge(82,79) +edge(84,79) +edge(172,79) +edge(84,80) +edge(172,80) +edge(83,81) +edge(172,81) +edge(211,81) +edge(83,82) +edge(173,82) +edge(185,84) +edge(190,84) +edge(105,85) +edge(247,85) +edge(87,86) +edge(88,87) +edge(135,87) +edge(137,87) +edge(90,89) +edge(92,89) +edge(171,91) +edge(171,92) +edge(266,92) +edge(95,93) +edge(97,93) +edge(240,93) +edge(96,94) +edge(237,94) +edge(96,95) +edge(98,95) +edge(97,96) +edge(98,97) +edge(132,97) +edge(132,98) +edge(101,99) +edge(103,99) +edge(102,100) +edge(102,101) +edge(103,102) +edge(243,102) +edge(243,103) +edge(106,104) +edge(108,104) +edge(111,104) +edge(109,105) +edge(159,105) +edge(247,106) +edge(108,107) +edge(110,107) +edge(160,107) +edge(110,108) +edge(112,108) +edge(260,109) +edge(203,110) +edge(253,110) +edge(236,111) +edge(239,111) +edge(161,112) +edge(253,112) +edge(115,113) +edge(117,113) +edge(119,113) +edge(117,114) +edge(241,114) +edge(241,115) +edge(118,116) +edge(120,116) +edge(123,116) +edge(119,117) +edge(119,118) +edge(121,118) +edge(121,120) +edge(123,120) +edge(123,121) +edge(125,124) +edge(127,124) +edge(129,124) +edge(192,124) +edge(128,125) +edge(191,125) +edge(127,126) +edge(130,126) +edge(192,126) +edge(130,127) +edge(233,127) +edge(254,128) +edge(256,128) +edge(256,129) +edge(191,130) +edge(132,131) +edge(139,131) +edge(138,133) +edge(140,133) +edge(135,134) +edge(165,134) +edge(266,134) +edge(271,134) +edge(266,135) +edge(165,136) +edge(167,136) +edge(166,137) +edge(282,137) +edge(140,138) +edge(141,139) +edge(245,140) +edge(143,142) +edge(145,142) +edge(258,142) +edge(289,142) +edge(145,143) +edge(147,143) +edge(204,144) +edge(258,144) +edge(218,145) +edge(147,146) +edge(206,146) +edge(218,147) +edge(279,147) +edge(287,147) +edge(180,148) +edge(150,149) +edge(152,149) +edge(154,149) +edge(152,150) +edge(152,151) +edge(196,151) +edge(291,151) +edge(154,153) +edge(162,153) +edge(164,153) +edge(161,154) +edge(279,154) +edge(284,154) +edge(156,155) +edge(158,155) +edge(160,155) +edge(158,156) +edge(194,156) +edge(158,157) +edge(162,161) +edge(164,161) +edge(253,162) +edge(164,163) +edge(197,164) +edge(166,165) +edge(271,165) +edge(210,166) +edge(174,167) +edge(210,167) +edge(169,168) +edge(170,169) +edge(218,169) +edge(281,170) +edge(265,171) +edge(273,172) +edge(176,174) +edge(210,174) +edge(176,175) +edge(178,175) +edge(177,176) +edge(210,176) +edge(179,177) +edge(179,178) +edge(213,179) +edge(181,180) +edge(184,182) +edge(186,185) +edge(252,185) +edge(188,186) +edge(188,187) +edge(190,188) +edge(275,188) +edge(288,189) +edge(252,190) +edge(275,190) +edge(192,191) +edge(194,193) +edge(195,194) +edge(211,195) +edge(277,196) +edge(199,198) +edge(201,198) +edge(257,198) +edge(201,199) +edge(237,199) +edge(237,200) +edge(260,200) +edge(257,201) +edge(204,203) +edge(206,203) +edge(253,204) +edge(206,205) +edge(259,205) +edge(286,206) +edge(286,207) +edge(210,208) +edge(282,210) +edge(214,212) +edge(290,213) +edge(216,215) +edge(228,215) +edge(217,216) +edge(228,216) +edge(228,217) +edge(220,218) +edge(221,219) +edge(231,219) +edge(221,220) +edge(280,220) +edge(227,221) +edge(231,221) +edge(223,222) +edge(225,222) +edge(224,223) +edge(235,223) +edge(270,223) +edge(235,224) +edge(268,224) +edge(235,226) +edge(228,227) +edge(230,227) +edge(232,227) +edge(230,228) +edge(230,229) +edge(231,230) +edge(232,231) +edge(237,236) +edge(239,236) +edge(260,236) +edge(260,237) +edge(243,242) +edge(245,242) +edge(259,244) +edge(268,246) +edge(271,246) +edge(288,249) +edge(276,251) +edge(291,251) +edge(276,252) +edge(256,254) +edge(262,255) +edge(278,255) +edge(262,256) +edge(259,258) +edge(263,262) +edge(278,263) +edge(266,264) +edge(270,266) +edge(272,266) +edge(270,268) +edge(272,268) +edge(272,269) +edge(272,270) +edge(275,274) +edge(277,274) +edge(276,275) +edge(277,276) +edge(283,279) +edge(281,280) +edge(284,283) +edge(285,284) ],[path(S,D) > 0.8],slsqp,-1,A),
	statistics(runtime, [Stop | _]),
	Runtime is Stop - Start,
	writeln('slsqp'),
	writeln(Runtime),
	writeln(A).
