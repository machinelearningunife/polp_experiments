sbatch --job-name=DD244_117_73 --partition=longrun --mem=8G --output=DD244_117_73_ccsaq.log --wrap='srun swipl -s DD244.pl -g "run_ccsaq(117, 73), halt." '
sbatch --job-name=DD244_117_73 --partition=longrun --mem=8G --output=DD244_117_73_mma.log --wrap='srun swipl -s DD244.pl -g "run_mma(117, 73), halt." '
sbatch --job-name=DD244_117_73 --partition=longrun --mem=8G --output=DD244_117_73_slsqp.log --wrap='srun swipl -s DD244.pl -g "run_slsqp(117, 73), halt." '

sbatch --job-name=DD244_111_79 --partition=longrun --mem=8G --output=DD244_111_79_ccsaq.log --wrap='srun swipl -s DD244.pl -g "run_ccsaq(111, 79), halt." '
sbatch --job-name=DD244_111_79 --partition=longrun --mem=8G --output=DD244_111_79_mma.log --wrap='srun swipl -s DD244.pl -g "run_mma(111, 79), halt." '
sbatch --job-name=DD244_111_79 --partition=longrun --mem=8G --output=DD244_111_79_slsqp.log --wrap='srun swipl -s DD244.pl -g "run_slsqp(111, 79), halt." '

sbatch --job-name=DD244_141_95 --partition=longrun --mem=8G --output=DD244_141_95_ccsaq.log --wrap='srun swipl -s DD244.pl -g "run_ccsaq(141, 95), halt." '
sbatch --job-name=DD244_141_95 --partition=longrun --mem=8G --output=DD244_141_95_mma.log --wrap='srun swipl -s DD244.pl -g "run_mma(141, 95), halt." '
sbatch --job-name=DD244_141_95 --partition=longrun --mem=8G --output=DD244_141_95_slsqp.log --wrap='srun swipl -s DD244.pl -g "run_slsqp(141, 95), halt." '

sbatch --job-name=DD244_277_60 --partition=longrun --mem=8G --output=DD244_277_60_ccsaq.log --wrap='srun swipl -s DD244.pl -g "run_ccsaq(277, 60), halt." '
sbatch --job-name=DD244_277_60 --partition=longrun --mem=8G --output=DD244_277_60_mma.log --wrap='srun swipl -s DD244.pl -g "run_mma(277, 60), halt." '
sbatch --job-name=DD244_277_60 --partition=longrun --mem=8G --output=DD244_277_60_slsqp.log --wrap='srun swipl -s DD244.pl -g "run_slsqp(277, 60), halt." '

sbatch --job-name=DD244_114_7 --partition=longrun --mem=8G --output=DD244_114_7_ccsaq.log --wrap='srun swipl -s DD244.pl -g "run_ccsaq(114, 7), halt." '
sbatch --job-name=DD244_114_7 --partition=longrun --mem=8G --output=DD244_114_7_mma.log --wrap='srun swipl -s DD244.pl -g "run_mma(114, 7), halt." '
sbatch --job-name=DD244_114_7 --partition=longrun --mem=8G --output=DD244_114_7_slsqp.log --wrap='srun swipl -s DD244.pl -g "run_slsqp(114, 7), halt." '

sbatch --job-name=DD244_169_148 --partition=longrun --mem=8G --output=DD244_169_148_ccsaq.log --wrap='srun swipl -s DD244.pl -g "run_ccsaq(169, 148), halt." '
sbatch --job-name=DD244_169_148 --partition=longrun --mem=8G --output=DD244_169_148_mma.log --wrap='srun swipl -s DD244.pl -g "run_mma(169, 148), halt." '
sbatch --job-name=DD244_169_148 --partition=longrun --mem=8G --output=DD244_169_148_slsqp.log --wrap='srun swipl -s DD244.pl -g "run_slsqp(169, 148), halt." '

sbatch --job-name=DD244_228_144 --partition=longrun --mem=8G --output=DD244_228_144_ccsaq.log --wrap='srun swipl -s DD244.pl -g "run_ccsaq(228, 144), halt." '
sbatch --job-name=DD244_228_144 --partition=longrun --mem=8G --output=DD244_228_144_mma.log --wrap='srun swipl -s DD244.pl -g "run_mma(228, 144), halt." '
sbatch --job-name=DD244_228_144 --partition=longrun --mem=8G --output=DD244_228_144_slsqp.log --wrap='srun swipl -s DD244.pl -g "run_slsqp(228, 144), halt." '

sbatch --job-name=DD244_123_8 --partition=longrun --mem=8G --output=DD244_123_8_ccsaq.log --wrap='srun swipl -s DD244.pl -g "run_ccsaq(123, 8), halt." '
sbatch --job-name=DD244_123_8 --partition=longrun --mem=8G --output=DD244_123_8_mma.log --wrap='srun swipl -s DD244.pl -g "run_mma(123, 8), halt." '
sbatch --job-name=DD244_123_8 --partition=longrun --mem=8G --output=DD244_123_8_slsqp.log --wrap='srun swipl -s DD244.pl -g "run_slsqp(123, 8), halt." '

sbatch --job-name=DD244_231_215 --partition=longrun --mem=8G --output=DD244_231_215_ccsaq.log --wrap='srun swipl -s DD244.pl -g "run_ccsaq(231, 215), halt." '
sbatch --job-name=DD244_231_215 --partition=longrun --mem=8G --output=DD244_231_215_mma.log --wrap='srun swipl -s DD244.pl -g "run_mma(231, 215), halt." '
sbatch --job-name=DD244_231_215 --partition=longrun --mem=8G --output=DD244_231_215_slsqp.log --wrap='srun swipl -s DD244.pl -g "run_slsqp(231, 215), halt." '

sbatch --job-name=DD244_186_35 --partition=longrun --mem=8G --output=DD244_186_35_ccsaq.log --wrap='srun swipl -s DD244.pl -g "run_ccsaq(186, 35), halt." '
sbatch --job-name=DD244_186_35 --partition=longrun --mem=8G --output=DD244_186_35_mma.log --wrap='srun swipl -s DD244.pl -g "run_mma(186, 35), halt." '
sbatch --job-name=DD244_186_35 --partition=longrun --mem=8G --output=DD244_186_35_slsqp.log --wrap='srun swipl -s DD244.pl -g "run_slsqp(186, 35), halt." '