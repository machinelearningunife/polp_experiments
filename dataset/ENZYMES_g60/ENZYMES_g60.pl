:- use_module(library(pita)).
:- pita.
:- begin_lpad.

0.9::edge(7, 1).
optimizable edge(8, 1).
0.9::edge(9, 1).
optimizable edge(3, 2).
0.9::edge(9, 2).
optimizable edge(10, 2).
0.9::edge(2, 3).
optimizable edge(4, 3).
0.9::edge(5, 3).
optimizable edge(6, 3).
0.9::edge(10, 3).
optimizable edge(3, 4).
0.9::edge(5, 4).
optimizable edge(6, 4).
0.9::edge(3, 5).
optimizable edge(4, 5).
0.9::edge(6, 5).
optimizable edge(3, 6).
0.9::edge(4, 6).
optimizable edge(5, 6).
0.9::edge(1, 7).
optimizable edge(8, 7).
0.9::edge(9, 7).
optimizable edge(1, 8).
0.9::edge(7, 8).
optimizable edge(9, 8).
0.9::edge(10, 8).
optimizable edge(1, 9).
0.9::edge(2, 9).
optimizable edge(7, 9).
0.9::edge(8, 9).
optimizable edge(10, 9).
0.9::edge(2, 10).
optimizable edge(3, 10).
0.9::edge(8, 10).
optimizable edge(9, 10).

path(X, X).
path(X, Y):- path(X, Z), edge(Z, Y).



:- end_lpad.

run:- run_mma, run_ccsaq, run_slsqp.

run_mma(S,D):-
	statistics(runtime, [Start | _]), 
	prob_optimize(path(S,D),[edge(8,1) +edge(3,2) +edge(10,2) +edge(4,3) +edge(6,3) +edge(3,4) +edge(6,4) +edge(4,5) +edge(3,6) +edge(5,6) +edge(8,7) +edge(1,8) +edge(9,8) +edge(1,9) +edge(7,9) +edge(10,9) +edge(3,10) +edge(9,10) ],[path(S,D) > 0.8],mma,-1,A),
	statistics(runtime, [Stop | _]),
	Runtime is Stop - Start,
	writeln('mma'),
	writeln(Runtime),
	writeln(A).

run_ccsaq(S,D):-
	statistics(runtime, [Start | _]), 
	prob_optimize(path(S,D),[edge(8,1) +edge(3,2) +edge(10,2) +edge(4,3) +edge(6,3) +edge(3,4) +edge(6,4) +edge(4,5) +edge(3,6) +edge(5,6) +edge(8,7) +edge(1,8) +edge(9,8) +edge(1,9) +edge(7,9) +edge(10,9) +edge(3,10) +edge(9,10) ],[path(S,D) > 0.8],ccsaq,-1,A),
	statistics(runtime, [Stop | _]),
	Runtime is Stop - Start,
	writeln('ccsaq'),
	writeln(Runtime),
	writeln(A).

run_slsqp(S,D):-
	statistics(runtime, [Start | _]), 
	prob_optimize(path(S,D),[edge(8,1) +edge(3,2) +edge(10,2) +edge(4,3) +edge(6,3) +edge(3,4) +edge(6,4) +edge(4,5) +edge(3,6) +edge(5,6) +edge(8,7) +edge(1,8) +edge(9,8) +edge(1,9) +edge(7,9) +edge(10,9) +edge(3,10) +edge(9,10) ],[path(S,D) > 0.8],slsqp,-1,A),
	statistics(runtime, [Stop | _]),
	Runtime is Stop - Start,
	writeln('slsqp'),
	writeln(Runtime),
	writeln(A).
