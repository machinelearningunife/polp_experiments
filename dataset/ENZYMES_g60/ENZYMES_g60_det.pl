:- table path/2.

edge(7, 1).
edge(8, 1).
edge(9, 1).
edge(3, 2).
edge(9, 2).
edge(10, 2).
edge(2, 3).
edge(4, 3).
edge(5, 3).
edge(6, 3).
edge(10, 3).
edge(3, 4).
edge(5, 4).
edge(6, 4).
edge(3, 5).
edge(4, 5).
edge(6, 5).
edge(3, 6).
edge(4, 6).
edge(5, 6).
edge(1, 7).
edge(8, 7).
edge(9, 7).
edge(1, 8).
edge(7, 8).
edge(9, 8).
edge(10, 8).
edge(1, 9).
edge(2, 9).
edge(7, 9).
edge(8, 9).
edge(10, 9).
edge(2, 10).
edge(3, 10).
edge(8, 10).
edge(9, 10).


path(X, X).
path(X, Y):- path(X, Z), edge(Z, Y).

test_rand([S,D]):-
    random_between(1, 10, Source),
    random_between(1, 10, Dest),
    ( path(Source,Dest), (\+(edge(Source,Dest))), Source \= Dest -> 
        write('path between '), write(Source), write(' '), writeln(Dest), S = Source, D = Dest ;
        S = -1, D = -1 
    ).

find(N,L):-
    length(LT,N),
    maplist(test_rand,LT),
    include(\=([-1,-1]),LT,L),
    maplist(writeln,L).
