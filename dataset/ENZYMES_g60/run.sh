sbatch --job-name=ENZYMES_g60_2_8 --partition=longrun --mem=8G --output=ENZYMES_g60_2_8_ccsaq.log --wrap='srun swipl -s ENZYMES_g60.pl -g "run_ccsaq(2, 8), halt." '
sbatch --job-name=ENZYMES_g60_2_8 --partition=longrun --mem=8G --output=ENZYMES_g60_2_8_mma.log --wrap='srun swipl -s ENZYMES_g60.pl -g "run_mma(2, 8), halt." '
sbatch --job-name=ENZYMES_g60_2_8 --partition=longrun --mem=8G --output=ENZYMES_g60_2_8_slsqp.log --wrap='srun swipl -s ENZYMES_g60.pl -g "run_slsqp(2, 8), halt." '

sbatch --job-name=ENZYMES_g60_8_2 --partition=longrun --mem=8G --output=ENZYMES_g60_8_2_ccsaq.log --wrap='srun swipl -s ENZYMES_g60.pl -g "run_ccsaq(8, 2), halt." '
sbatch --job-name=ENZYMES_g60_8_2 --partition=longrun --mem=8G --output=ENZYMES_g60_8_2_mma.log --wrap='srun swipl -s ENZYMES_g60.pl -g "run_mma(8, 2), halt." '
sbatch --job-name=ENZYMES_g60_8_2 --partition=longrun --mem=8G --output=ENZYMES_g60_8_2_slsqp.log --wrap='srun swipl -s ENZYMES_g60.pl -g "run_slsqp(8, 2), halt." '

sbatch --job-name=ENZYMES_g60_7_10 --partition=longrun --mem=8G --output=ENZYMES_g60_7_10_ccsaq.log --wrap='srun swipl -s ENZYMES_g60.pl -g "run_ccsaq(7, 10), halt." '
sbatch --job-name=ENZYMES_g60_7_10 --partition=longrun --mem=8G --output=ENZYMES_g60_7_10_mma.log --wrap='srun swipl -s ENZYMES_g60.pl -g "run_mma(7, 10), halt." '
sbatch --job-name=ENZYMES_g60_7_10 --partition=longrun --mem=8G --output=ENZYMES_g60_7_10_slsqp.log --wrap='srun swipl -s ENZYMES_g60.pl -g "run_slsqp(7, 10), halt." '

sbatch --job-name=ENZYMES_g60_2_5 --partition=longrun --mem=8G --output=ENZYMES_g60_2_5_ccsaq.log --wrap='srun swipl -s ENZYMES_g60.pl -g "run_ccsaq(2, 5), halt." '
sbatch --job-name=ENZYMES_g60_2_5 --partition=longrun --mem=8G --output=ENZYMES_g60_2_5_mma.log --wrap='srun swipl -s ENZYMES_g60.pl -g "run_mma(2, 5), halt." '
sbatch --job-name=ENZYMES_g60_2_5 --partition=longrun --mem=8G --output=ENZYMES_g60_2_5_slsqp.log --wrap='srun swipl -s ENZYMES_g60.pl -g "run_slsqp(2, 5), halt." '

sbatch --job-name=ENZYMES_g60_10_6 --partition=longrun --mem=8G --output=ENZYMES_g60_10_6_ccsaq.log --wrap='srun swipl -s ENZYMES_g60.pl -g "run_ccsaq(10, 6), halt." '
sbatch --job-name=ENZYMES_g60_10_6 --partition=longrun --mem=8G --output=ENZYMES_g60_10_6_mma.log --wrap='srun swipl -s ENZYMES_g60.pl -g "run_mma(10, 6), halt." '
sbatch --job-name=ENZYMES_g60_10_6 --partition=longrun --mem=8G --output=ENZYMES_g60_10_6_slsqp.log --wrap='srun swipl -s ENZYMES_g60.pl -g "run_slsqp(10, 6), halt." '

sbatch --job-name=ENZYMES_g60_10_7 --partition=longrun --mem=8G --output=ENZYMES_g60_10_7_ccsaq.log --wrap='srun swipl -s ENZYMES_g60.pl -g "run_ccsaq(10, 7), halt." '
sbatch --job-name=ENZYMES_g60_10_7 --partition=longrun --mem=8G --output=ENZYMES_g60_10_7_mma.log --wrap='srun swipl -s ENZYMES_g60.pl -g "run_mma(10, 7), halt." '
sbatch --job-name=ENZYMES_g60_10_7 --partition=longrun --mem=8G --output=ENZYMES_g60_10_7_slsqp.log --wrap='srun swipl -s ENZYMES_g60.pl -g "run_slsqp(10, 7), halt." '

sbatch --job-name=ENZYMES_g60_2_1 --partition=longrun --mem=8G --output=ENZYMES_g60_2_1_ccsaq.log --wrap='srun swipl -s ENZYMES_g60.pl -g "run_ccsaq(2, 1), halt." '
sbatch --job-name=ENZYMES_g60_2_1 --partition=longrun --mem=8G --output=ENZYMES_g60_2_1_mma.log --wrap='srun swipl -s ENZYMES_g60.pl -g "run_mma(2, 1), halt." '
sbatch --job-name=ENZYMES_g60_2_1 --partition=longrun --mem=8G --output=ENZYMES_g60_2_1_slsqp.log --wrap='srun swipl -s ENZYMES_g60.pl -g "run_slsqp(2, 1), halt." '

sbatch --job-name=ENZYMES_g60_3_9 --partition=longrun --mem=8G --output=ENZYMES_g60_3_9_ccsaq.log --wrap='srun swipl -s ENZYMES_g60.pl -g "run_ccsaq(3, 9), halt." '
sbatch --job-name=ENZYMES_g60_3_9 --partition=longrun --mem=8G --output=ENZYMES_g60_3_9_mma.log --wrap='srun swipl -s ENZYMES_g60.pl -g "run_mma(3, 9), halt." '
sbatch --job-name=ENZYMES_g60_3_9 --partition=longrun --mem=8G --output=ENZYMES_g60_3_9_slsqp.log --wrap='srun swipl -s ENZYMES_g60.pl -g "run_slsqp(3, 9), halt." '

sbatch --job-name=ENZYMES_g60_9_3 --partition=longrun --mem=8G --output=ENZYMES_g60_9_3_ccsaq.log --wrap='srun swipl -s ENZYMES_g60.pl -g "run_ccsaq(9, 3), halt." '
sbatch --job-name=ENZYMES_g60_9_3 --partition=longrun --mem=8G --output=ENZYMES_g60_9_3_mma.log --wrap='srun swipl -s ENZYMES_g60.pl -g "run_mma(9, 3), halt." '
sbatch --job-name=ENZYMES_g60_9_3 --partition=longrun --mem=8G --output=ENZYMES_g60_9_3_slsqp.log --wrap='srun swipl -s ENZYMES_g60.pl -g "run_slsqp(9, 3), halt." '

sbatch --job-name=ENZYMES_g60_2_7 --partition=longrun --mem=8G --output=ENZYMES_g60_2_7_ccsaq.log --wrap='srun swipl -s ENZYMES_g60.pl -g "run_ccsaq(2, 7), halt." '
sbatch --job-name=ENZYMES_g60_2_7 --partition=longrun --mem=8G --output=ENZYMES_g60_2_7_mma.log --wrap='srun swipl -s ENZYMES_g60.pl -g "run_mma(2, 7), halt." '
sbatch --job-name=ENZYMES_g60_2_7 --partition=longrun --mem=8G --output=ENZYMES_g60_2_7_slsqp.log --wrap='srun swipl -s ENZYMES_g60.pl -g "run_slsqp(2, 7), halt." '