:- table path/2.

edge(1, 2).
edge(1, 3).
edge(2, 3).
edge(1, 4).
edge(3, 4).
edge(1, 5).
edge(2, 5).
edge(1, 6).
edge(2, 6).
edge(3, 6).
edge(3, 7).
edge(5, 7).
edge(6, 7).
edge(3, 8).
edge(4, 8).
edge(6, 8).
edge(7, 8).
edge(2, 9).
edge(5, 9).
edge(6, 9).
edge(2, 10).
edge(9, 10).
edge(6, 11).
edge(7, 11).
edge(8, 11).
edge(9, 11).
edge(10, 11).
edge(1, 12).
edge(6, 12).
edge(7, 12).
edge(8, 12).
edge(11, 12).
edge(6, 13).
edge(7, 13).
edge(9, 13).
edge(10, 13).
edge(11, 13).
edge(5, 14).
edge(8, 14).
edge(12, 14).
edge(13, 14).
edge(1, 15).
edge(2, 15).
edge(5, 15).
edge(9, 15).
edge(10, 15).
edge(11, 15).
edge(12, 15).
edge(13, 15).
edge(1, 16).
edge(2, 16).
edge(5, 16).
edge(6, 16).
edge(11, 16).
edge(12, 16).
edge(13, 16).
edge(14, 16).
edge(15, 16).



path(X, X).
path(X, Y):- path(X, Z), edge(Z, Y).

test_rand([S,D]):-
    random_between(1, 662, Source),
    random_between(1, 662, Dest),
    ( path(Source,Dest), (\+(edge(Source,Dest))), Source \= Dest -> 
        write('path between '), write(Source), write(' '), writeln(Dest), S = Source, D = Dest ;
        S = -1, D = -1 
    ).

find(N,L):-
    length(LT,N),
    maplist(test_rand,LT),
    include(\=([-1,-1]),LT,L),
    maplist(writeln,L).
