sbatch --job-name=soc-tribes_3_12 --partition=longrun --mem=8G --output=soc-tribes_3_12_ccsaq.log --wrap='srun swipl -s soc-tribes.pl -g "run_ccsaq(3, 12), halt." '
sbatch --job-name=soc-tribes_3_12 --partition=longrun --mem=8G --output=soc-tribes_3_12_mma.log --wrap='srun swipl -s soc-tribes.pl -g "run_mma(3, 12), halt." '
sbatch --job-name=soc-tribes_3_12 --partition=longrun --mem=8G --output=soc-tribes_3_12_slsqp.log --wrap='srun swipl -s soc-tribes.pl -g "run_slsqp(3, 12), halt." '

sbatch --job-name=soc-tribes_4_12 --partition=longrun --mem=8G --output=soc-tribes_4_12_ccsaq.log --wrap='srun swipl -s soc-tribes.pl -g "run_ccsaq(4, 12), halt." '
sbatch --job-name=soc-tribes_4_12 --partition=longrun --mem=8G --output=soc-tribes_4_12_mma.log --wrap='srun swipl -s soc-tribes.pl -g "run_mma(4, 12), halt." '
sbatch --job-name=soc-tribes_4_12 --partition=longrun --mem=8G --output=soc-tribes_4_12_slsqp.log --wrap='srun swipl -s soc-tribes.pl -g "run_slsqp(4, 12), halt." '

sbatch --job-name=soc-tribes_3_13 --partition=longrun --mem=8G --output=soc-tribes_3_13_ccsaq.log --wrap='srun swipl -s soc-tribes.pl -g "run_ccsaq(3, 13), halt." '
sbatch --job-name=soc-tribes_3_13 --partition=longrun --mem=8G --output=soc-tribes_3_13_mma.log --wrap='srun swipl -s soc-tribes.pl -g "run_mma(3, 13), halt." '
sbatch --job-name=soc-tribes_3_13 --partition=longrun --mem=8G --output=soc-tribes_3_13_slsqp.log --wrap='srun swipl -s soc-tribes.pl -g "run_slsqp(3, 13), halt." '

sbatch --job-name=soc-tribes_6_15 --partition=longrun --mem=8G --output=soc-tribes_6_15_ccsaq.log --wrap='srun swipl -s soc-tribes.pl -g "run_ccsaq(6, 15), halt." '
sbatch --job-name=soc-tribes_6_15 --partition=longrun --mem=8G --output=soc-tribes_6_15_mma.log --wrap='srun swipl -s soc-tribes.pl -g "run_mma(6, 15), halt." '
sbatch --job-name=soc-tribes_6_15 --partition=longrun --mem=8G --output=soc-tribes_6_15_slsqp.log --wrap='srun swipl -s soc-tribes.pl -g "run_slsqp(6, 15), halt." '

sbatch --job-name=soc-tribes_3_15 --partition=longrun --mem=8G --output=soc-tribes_3_15_ccsaq.log --wrap='srun swipl -s soc-tribes.pl -g "run_ccsaq(3, 15), halt." '
sbatch --job-name=soc-tribes_3_15 --partition=longrun --mem=8G --output=soc-tribes_3_15_mma.log --wrap='srun swipl -s soc-tribes.pl -g "run_mma(3, 15), halt." '
sbatch --job-name=soc-tribes_3_15 --partition=longrun --mem=8G --output=soc-tribes_3_15_slsqp.log --wrap='srun swipl -s soc-tribes.pl -g "run_slsqp(3, 15), halt." '

sbatch --job-name=soc-tribes_10_14 --partition=longrun --mem=8G --output=soc-tribes_10_14_ccsaq.log --wrap='srun swipl -s soc-tribes.pl -g "run_ccsaq(10, 14), halt." '
sbatch --job-name=soc-tribes_10_14 --partition=longrun --mem=8G --output=soc-tribes_10_14_mma.log --wrap='srun swipl -s soc-tribes.pl -g "run_mma(10, 14), halt." '
sbatch --job-name=soc-tribes_10_14 --partition=longrun --mem=8G --output=soc-tribes_10_14_slsqp.log --wrap='srun swipl -s soc-tribes.pl -g "run_slsqp(10, 14), halt." '

sbatch --job-name=soc-tribes_1_11 --partition=longrun --mem=8G --output=soc-tribes_1_11_ccsaq.log --wrap='srun swipl -s soc-tribes.pl -g "run_ccsaq(1, 11), halt." '
sbatch --job-name=soc-tribes_1_11 --partition=longrun --mem=8G --output=soc-tribes_1_11_mma.log --wrap='srun swipl -s soc-tribes.pl -g "run_mma(1, 11), halt." '
sbatch --job-name=soc-tribes_1_11 --partition=longrun --mem=8G --output=soc-tribes_1_11_slsqp.log --wrap='srun swipl -s soc-tribes.pl -g "run_slsqp(1, 11), halt." '

sbatch --job-name=soc-tribes_7_15 --partition=longrun --mem=8G --output=soc-tribes_7_15_ccsaq.log --wrap='srun swipl -s soc-tribes.pl -g "run_ccsaq(7, 15), halt." '
sbatch --job-name=soc-tribes_7_15 --partition=longrun --mem=8G --output=soc-tribes_7_15_mma.log --wrap='srun swipl -s soc-tribes.pl -g "run_mma(7, 15), halt." '
sbatch --job-name=soc-tribes_7_15 --partition=longrun --mem=8G --output=soc-tribes_7_15_slsqp.log --wrap='srun swipl -s soc-tribes.pl -g "run_slsqp(7, 15), halt." '

sbatch --job-name=soc-tribes_1_10 --partition=longrun --mem=8G --output=soc-tribes_1_10_ccsaq.log --wrap='srun swipl -s soc-tribes.pl -g "run_ccsaq(1, 10), halt." '
sbatch --job-name=soc-tribes_1_10 --partition=longrun --mem=8G --output=soc-tribes_1_10_mma.log --wrap='srun swipl -s soc-tribes.pl -g "run_mma(1, 10), halt." '
sbatch --job-name=soc-tribes_1_10 --partition=longrun --mem=8G --output=soc-tribes_1_10_slsqp.log --wrap='srun swipl -s soc-tribes.pl -g "run_slsqp(1, 10), halt." '

sbatch --job-name=soc-tribes_1_9 --partition=longrun --mem=8G --output=soc-tribes_1_9_ccsaq.log --wrap='srun swipl -s soc-tribes.pl -g "run_ccsaq(1, 9), halt." '
sbatch --job-name=soc-tribes_1_9 --partition=longrun --mem=8G --output=soc-tribes_1_9_mma.log --wrap='srun swipl -s soc-tribes.pl -g "run_mma(1, 9), halt." '
sbatch --job-name=soc-tribes_1_9 --partition=longrun --mem=8G --output=soc-tribes_1_9_slsqp.log --wrap='srun swipl -s soc-tribes.pl -g "run_slsqp(1, 9), halt." '