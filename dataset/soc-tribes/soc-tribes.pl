:- use_module(library(pita)).
:- pita.
:- begin_lpad.

0.9::edge(1, 2).
optimizable edge(1, 3).
0.9::edge(2, 3).
optimizable edge(1, 4).
0.9::edge(3, 4).
optimizable edge(1, 5).
0.9::edge(2, 5).
optimizable edge(1, 6).
0.9::edge(2, 6).
optimizable edge(3, 6).
0.9::edge(3, 7).
optimizable edge(5, 7).
0.9::edge(6, 7).
optimizable edge(3, 8).
0.9::edge(4, 8).
optimizable edge(6, 8).
0.9::edge(7, 8).
optimizable edge(2, 9).
0.9::edge(5, 9).
optimizable edge(6, 9).
0.9::edge(2, 10).
optimizable edge(9, 10).
0.9::edge(6, 11).
optimizable edge(7, 11).
0.9::edge(8, 11).
optimizable edge(9, 11).
0.9::edge(10, 11).
optimizable edge(1, 12).
0.9::edge(6, 12).
optimizable edge(7, 12).
0.9::edge(8, 12).
optimizable edge(11, 12).
0.9::edge(6, 13).
optimizable edge(7, 13).
0.9::edge(9, 13).
optimizable edge(10, 13).
0.9::edge(11, 13).
optimizable edge(5, 14).
0.9::edge(8, 14).
optimizable edge(12, 14).
0.9::edge(13, 14).
optimizable edge(1, 15).
0.9::edge(2, 15).
optimizable edge(5, 15).
0.9::edge(9, 15).
optimizable edge(10, 15).
0.9::edge(11, 15).
optimizable edge(12, 15).
0.9::edge(13, 15).
optimizable edge(1, 16).
0.9::edge(2, 16).
optimizable edge(5, 16).
0.9::edge(6, 16).
optimizable edge(11, 16).
0.9::edge(12, 16).
optimizable edge(13, 16).
0.9::edge(14, 16).
optimizable edge(15, 16).

path(X, X).
path(X, Y):- path(X, Z), edge(Z, Y).



:- end_lpad.

run:- run_mma, run_ccsaq, run_slsqp.

run_mma(S,D):-
	statistics(runtime, [Start | _]), 
	prob_optimize(path(S,D),[edge(1,3) +edge(1,4) +edge(1,5) +edge(1,6) +edge(3,6) +edge(5,7) +edge(3,8) +edge(6,8) +edge(2,9) +edge(6,9) +edge(9,10) +edge(7,11) +edge(9,11) +edge(1,12) +edge(7,12) +edge(11,12) +edge(7,13) +edge(10,13) +edge(5,14) +edge(12,14) +edge(1,15) +edge(5,15) +edge(10,15) +edge(12,15) +edge(1,16) +edge(5,16) +edge(11,16) +edge(13,16) +edge(15,16) ],[path(S,D) > 0.8],mma,-1,A),
	statistics(runtime, [Stop | _]),
	Runtime is Stop - Start,
	writeln('mma'),
	writeln(Runtime),
	writeln(A).

run_ccsaq(S,D):-
	statistics(runtime, [Start | _]), 
	prob_optimize(path(S,D),[edge(1,3) +edge(1,4) +edge(1,5) +edge(1,6) +edge(3,6) +edge(5,7) +edge(3,8) +edge(6,8) +edge(2,9) +edge(6,9) +edge(9,10) +edge(7,11) +edge(9,11) +edge(1,12) +edge(7,12) +edge(11,12) +edge(7,13) +edge(10,13) +edge(5,14) +edge(12,14) +edge(1,15) +edge(5,15) +edge(10,15) +edge(12,15) +edge(1,16) +edge(5,16) +edge(11,16) +edge(13,16) +edge(15,16) ],[path(S,D) > 0.8],ccsaq,-1,A),
	statistics(runtime, [Stop | _]),
	Runtime is Stop - Start,
	writeln('ccsaq'),
	writeln(Runtime),
	writeln(A).

run_slsqp(S,D):-
	statistics(runtime, [Start | _]), 
	prob_optimize(path(S,D),[edge(1,3) +edge(1,4) +edge(1,5) +edge(1,6) +edge(3,6) +edge(5,7) +edge(3,8) +edge(6,8) +edge(2,9) +edge(6,9) +edge(9,10) +edge(7,11) +edge(9,11) +edge(1,12) +edge(7,12) +edge(11,12) +edge(7,13) +edge(10,13) +edge(5,14) +edge(12,14) +edge(1,15) +edge(5,15) +edge(10,15) +edge(12,15) +edge(1,16) +edge(5,16) +edge(11,16) +edge(13,16) +edge(15,16) ],[path(S,D) > 0.8],slsqp,-1,A),
	statistics(runtime, [Stop | _]),
	Runtime is Stop - Start,
	writeln('slsqp'),
	writeln(Runtime),
	writeln(A).
