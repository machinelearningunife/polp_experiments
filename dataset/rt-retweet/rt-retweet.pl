:- use_module(library(pita)).
:- pita.
:- begin_lpad.

0.9::edge(6, 1).
optimizable edge(8, 1).
0.9::edge(54, 1).
optimizable edge(76, 1).
0.9::edge(85, 1).
optimizable edge(56, 2).
0.9::edge(75, 3).
optimizable edge(19, 4).
0.9::edge(16, 5).
optimizable edge(17, 5).
0.9::edge(20, 5).
optimizable edge(40, 5).
0.9::edge(93, 5).
optimizable edge(8, 6).
0.9::edge(45, 6).
optimizable edge(54, 6).
0.9::edge(89, 6).
optimizable edge(61, 7).
0.9::edge(28, 8).
optimizable edge(38, 8).
0.9::edge(39, 8).
optimizable edge(42, 8).
0.9::edge(54, 8).
optimizable edge(72, 8).
0.9::edge(54, 9).
optimizable edge(89, 9).
0.9::edge(95, 9).
optimizable edge(54, 10).
0.9::edge(45, 11).
optimizable edge(56, 11).
0.9::edge(94, 11).
optimizable edge(15, 12).
0.9::edge(24, 13).
optimizable edge(75, 13).
0.9::edge(41, 14).
optimizable edge(50, 14).
0.9::edge(21, 15).
optimizable edge(33, 15).
0.9::edge(93, 15).
optimizable edge(84, 18).
0.9::edge(54, 19).
optimizable edge(96, 21).
0.9::edge(53, 22).
optimizable edge(65, 22).
0.9::edge(68, 22).
optimizable edge(51, 23).
0.9::edge(28, 25).
optimizable edge(89, 26).
0.9::edge(53, 27).
optimizable edge(32, 28).
0.9::edge(78, 28).
optimizable edge(93, 28).
0.9::edge(54, 29).
optimizable edge(68, 29).
0.9::edge(72, 29).
optimizable edge(64, 30).
0.9::edge(45, 31).
optimizable edge(55, 31).
0.9::edge(58, 31).
optimizable edge(93, 34).
0.9::edge(45, 35).
optimizable edge(54, 36).
0.9::edge(54, 37).
optimizable edge(54, 38).
0.9::edge(50, 39).
optimizable edge(59, 40).
0.9::edge(93, 40).
optimizable edge(66, 41).
0.9::edge(81, 41).
optimizable edge(88, 41).
0.9::edge(45, 43).
optimizable edge(74, 43).
0.9::edge(72, 44).
optimizable edge(56, 45).
0.9::edge(58, 45).
optimizable edge(74, 45).
0.9::edge(75, 45).
optimizable edge(79, 45).
0.9::edge(83, 45).
optimizable edge(89, 46).
0.9::edge(54, 47).
optimizable edge(73, 48).
0.9::edge(87, 48).
optimizable edge(93, 48).
0.9::edge(70, 49).
optimizable edge(77, 50).
0.9::edge(54, 51).
optimizable edge(56, 51).
0.9::edge(72, 51).
optimizable edge(74, 51).
0.9::edge(56, 52).
optimizable edge(57, 53).
0.9::edge(64, 53).
optimizable edge(70, 53).
0.9::edge(61, 54).
optimizable edge(67, 54).
0.9::edge(89, 54).
optimizable edge(92, 54).
0.9::edge(93, 54).
optimizable edge(72, 56).
0.9::edge(80, 56).
optimizable edge(90, 56).
0.9::edge(74, 60).
optimizable edge(72, 61).
0.9::edge(95, 61).
optimizable edge(72, 62).
0.9::edge(95, 63).
optimizable edge(81, 64).
0.9::edge(89, 64).
optimizable edge(91, 65).
0.9::edge(75, 66).
optimizable edge(72, 69).
0.9::edge(79, 71).
optimizable edge(93, 72).
0.9::edge(89, 82).
optimizable edge(89, 84).
0.9::edge(89, 86).

path(X, X).
path(X, Y):- path(X, Z), edge(Z, Y).



:- end_lpad.

run:- run_mma, run_ccsaq, run_slsqp.

run_mma(S,D):-
	statistics(runtime, [Start | _]), 
	prob_optimize(path(S,D),[edge(8,1) +edge(76,1) +edge(56,2) +edge(19,4) +edge(17,5) +edge(40,5) +edge(8,6) +edge(54,6) +edge(61,7) +edge(38,8) +edge(42,8) +edge(72,8) +edge(89,9) +edge(54,10) +edge(56,11) +edge(15,12) +edge(75,13) +edge(50,14) +edge(33,15) +edge(84,18) +edge(96,21) +edge(65,22) +edge(51,23) +edge(89,26) +edge(32,28) +edge(93,28) +edge(68,29) +edge(64,30) +edge(55,31) +edge(93,34) +edge(54,36) +edge(54,38) +edge(59,40) +edge(66,41) +edge(88,41) +edge(74,43) +edge(56,45) +edge(74,45) +edge(79,45) +edge(89,46) +edge(73,48) +edge(93,48) +edge(77,50) +edge(56,51) +edge(74,51) +edge(57,53) +edge(70,53) +edge(67,54) +edge(92,54) +edge(72,56) +edge(90,56) +edge(72,61) +edge(72,62) +edge(81,64) +edge(91,65) +edge(72,69) +edge(93,72) +edge(89,84) ],[path(S,D) > 0.8],mma,-1,A),
	statistics(runtime, [Stop | _]),
	Runtime is Stop - Start,
	writeln('mma'),
	writeln(Runtime),
	writeln(A).

run_ccsaq(S,D):-
	statistics(runtime, [Start | _]), 
	prob_optimize(path(S,D),[edge(8,1) +edge(76,1) +edge(56,2) +edge(19,4) +edge(17,5) +edge(40,5) +edge(8,6) +edge(54,6) +edge(61,7) +edge(38,8) +edge(42,8) +edge(72,8) +edge(89,9) +edge(54,10) +edge(56,11) +edge(15,12) +edge(75,13) +edge(50,14) +edge(33,15) +edge(84,18) +edge(96,21) +edge(65,22) +edge(51,23) +edge(89,26) +edge(32,28) +edge(93,28) +edge(68,29) +edge(64,30) +edge(55,31) +edge(93,34) +edge(54,36) +edge(54,38) +edge(59,40) +edge(66,41) +edge(88,41) +edge(74,43) +edge(56,45) +edge(74,45) +edge(79,45) +edge(89,46) +edge(73,48) +edge(93,48) +edge(77,50) +edge(56,51) +edge(74,51) +edge(57,53) +edge(70,53) +edge(67,54) +edge(92,54) +edge(72,56) +edge(90,56) +edge(72,61) +edge(72,62) +edge(81,64) +edge(91,65) +edge(72,69) +edge(93,72) +edge(89,84) ],[path(S,D) > 0.8],ccsaq,-1,A),
	statistics(runtime, [Stop | _]),
	Runtime is Stop - Start,
	writeln('ccsaq'),
	writeln(Runtime),
	writeln(A).

run_slsqp(S,D):-
	statistics(runtime, [Start | _]), 
	prob_optimize(path(S,D),[edge(8,1) +edge(76,1) +edge(56,2) +edge(19,4) +edge(17,5) +edge(40,5) +edge(8,6) +edge(54,6) +edge(61,7) +edge(38,8) +edge(42,8) +edge(72,8) +edge(89,9) +edge(54,10) +edge(56,11) +edge(15,12) +edge(75,13) +edge(50,14) +edge(33,15) +edge(84,18) +edge(96,21) +edge(65,22) +edge(51,23) +edge(89,26) +edge(32,28) +edge(93,28) +edge(68,29) +edge(64,30) +edge(55,31) +edge(93,34) +edge(54,36) +edge(54,38) +edge(59,40) +edge(66,41) +edge(88,41) +edge(74,43) +edge(56,45) +edge(74,45) +edge(79,45) +edge(89,46) +edge(73,48) +edge(93,48) +edge(77,50) +edge(56,51) +edge(74,51) +edge(57,53) +edge(70,53) +edge(67,54) +edge(92,54) +edge(72,56) +edge(90,56) +edge(72,61) +edge(72,62) +edge(81,64) +edge(91,65) +edge(72,69) +edge(93,72) +edge(89,84) ],[path(S,D) > 0.8],slsqp,-1,A),
	statistics(runtime, [Stop | _]),
	Runtime is Stop - Start,
	writeln('slsqp'),
	writeln(Runtime),
	writeln(A).
