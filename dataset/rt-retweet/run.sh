sbatch --job-name=rt-retweet_89_30 --partition=longrun --mem=8G --output=rt-retweet_89_30_ccsaq.log --wrap='srun swipl -s rt-retweet.pl -g "run_ccsaq(89, 30), halt." '
sbatch --job-name=rt-retweet_89_30 --partition=longrun --mem=8G --output=rt-retweet_89_30_mma.log --wrap='srun swipl -s rt-retweet.pl -g "run_mma(89, 30), halt." '
sbatch --job-name=rt-retweet_89_30 --partition=longrun --mem=8G --output=rt-retweet_89_30_slsqp.log --wrap='srun swipl -s rt-retweet.pl -g "run_slsqp(89, 30), halt." '

sbatch --job-name=rt-retweet_93_29 --partition=longrun --mem=8G --output=rt-retweet_93_29_ccsaq.log --wrap='srun swipl -s rt-retweet.pl -g "run_ccsaq(93, 29), halt." '
sbatch --job-name=rt-retweet_93_29 --partition=longrun --mem=8G --output=rt-retweet_93_29_mma.log --wrap='srun swipl -s rt-retweet.pl -g "run_mma(93, 29), halt." '
sbatch --job-name=rt-retweet_93_29 --partition=longrun --mem=8G --output=rt-retweet_93_29_slsqp.log --wrap='srun swipl -s rt-retweet.pl -g "run_slsqp(93, 29), halt." '

sbatch --job-name=rt-retweet_93_2 --partition=longrun --mem=8G --output=rt-retweet_93_2_ccsaq.log --wrap='srun swipl -s rt-retweet.pl -g "run_ccsaq(93, 2), halt." '
sbatch --job-name=rt-retweet_93_2 --partition=longrun --mem=8G --output=rt-retweet_93_2_mma.log --wrap='srun swipl -s rt-retweet.pl -g "run_mma(93, 2), halt." '
sbatch --job-name=rt-retweet_93_2 --partition=longrun --mem=8G --output=rt-retweet_93_2_slsqp.log --wrap='srun swipl -s rt-retweet.pl -g "run_slsqp(93, 2), halt." '

sbatch --job-name=rt-retweet_72_1 --partition=longrun --mem=8G --output=rt-retweet_72_1_ccsaq.log --wrap='srun swipl -s rt-retweet.pl -g "run_ccsaq(72, 1), halt." '
sbatch --job-name=rt-retweet_72_1 --partition=longrun --mem=8G --output=rt-retweet_72_1_mma.log --wrap='srun swipl -s rt-retweet.pl -g "run_mma(72, 1), halt." '
sbatch --job-name=rt-retweet_72_1 --partition=longrun --mem=8G --output=rt-retweet_72_1_slsqp.log --wrap='srun swipl -s rt-retweet.pl -g "run_slsqp(72, 1), halt." '

sbatch --job-name=rt-retweet_95_36 --partition=longrun --mem=8G --output=rt-retweet_95_36_ccsaq.log --wrap='srun swipl -s rt-retweet.pl -g "run_ccsaq(95, 36), halt." '
sbatch --job-name=rt-retweet_95_36 --partition=longrun --mem=8G --output=rt-retweet_95_36_mma.log --wrap='srun swipl -s rt-retweet.pl -g "run_mma(95, 36), halt." '
sbatch --job-name=rt-retweet_95_36 --partition=longrun --mem=8G --output=rt-retweet_95_36_slsqp.log --wrap='srun swipl -s rt-retweet.pl -g "run_slsqp(95, 36), halt." '

sbatch --job-name=rt-retweet_61_38 --partition=longrun --mem=8G --output=rt-retweet_61_38_ccsaq.log --wrap='srun swipl -s rt-retweet.pl -g "run_ccsaq(61, 38), halt." '
sbatch --job-name=rt-retweet_61_38 --partition=longrun --mem=8G --output=rt-retweet_61_38_mma.log --wrap='srun swipl -s rt-retweet.pl -g "run_mma(61, 38), halt." '
sbatch --job-name=rt-retweet_61_38 --partition=longrun --mem=8G --output=rt-retweet_61_38_slsqp.log --wrap='srun swipl -s rt-retweet.pl -g "run_slsqp(61, 38), halt." '

sbatch --job-name=rt-retweet_75_41 --partition=longrun --mem=8G --output=rt-retweet_75_41_ccsaq.log --wrap='srun swipl -s rt-retweet.pl -g "run_ccsaq(75, 41), halt." '
sbatch --job-name=rt-retweet_75_41 --partition=longrun --mem=8G --output=rt-retweet_75_41_mma.log --wrap='srun swipl -s rt-retweet.pl -g "run_mma(75, 41), halt." '
sbatch --job-name=rt-retweet_75_41 --partition=longrun --mem=8G --output=rt-retweet_75_41_slsqp.log --wrap='srun swipl -s rt-retweet.pl -g "run_slsqp(75, 41), halt." '

sbatch --job-name=rt-retweet_78_1 --partition=longrun --mem=8G --output=rt-retweet_78_1_ccsaq.log --wrap='srun swipl -s rt-retweet.pl -g "run_ccsaq(78, 1), halt." '
sbatch --job-name=rt-retweet_78_1 --partition=longrun --mem=8G --output=rt-retweet_78_1_mma.log --wrap='srun swipl -s rt-retweet.pl -g "run_mma(78, 1), halt." '
sbatch --job-name=rt-retweet_78_1 --partition=longrun --mem=8G --output=rt-retweet_78_1_slsqp.log --wrap='srun swipl -s rt-retweet.pl -g "run_slsqp(78, 1), halt." '

sbatch --job-name=rt-retweet_77_39 --partition=longrun --mem=8G --output=rt-retweet_77_39_ccsaq.log --wrap='srun swipl -s rt-retweet.pl -g "run_ccsaq(77, 39), halt." '
sbatch --job-name=rt-retweet_77_39 --partition=longrun --mem=8G --output=rt-retweet_77_39_mma.log --wrap='srun swipl -s rt-retweet.pl -g "run_mma(77, 39), halt." '
sbatch --job-name=rt-retweet_77_39 --partition=longrun --mem=8G --output=rt-retweet_77_39_slsqp.log --wrap='srun swipl -s rt-retweet.pl -g "run_slsqp(77, 39), halt." '

sbatch --job-name=rt-retweet_93_29 --partition=longrun --mem=8G --output=rt-retweet_93_29_ccsaq.log --wrap='srun swipl -s rt-retweet.pl -g "run_ccsaq(93, 29), halt." '
sbatch --job-name=rt-retweet_93_29 --partition=longrun --mem=8G --output=rt-retweet_93_29_mma.log --wrap='srun swipl -s rt-retweet.pl -g "run_mma(93, 29), halt." '
sbatch --job-name=rt-retweet_93_29 --partition=longrun --mem=8G --output=rt-retweet_93_29_slsqp.log --wrap='srun swipl -s rt-retweet.pl -g "run_slsqp(93, 29), halt." '