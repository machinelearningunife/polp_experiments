:- table path/2.

edge(6, 1).
edge(8, 1).
edge(54, 1).
edge(76, 1).
edge(85, 1).
edge(56, 2).
edge(75, 3).
edge(19, 4).
edge(16, 5).
edge(17, 5).
edge(20, 5).
edge(40, 5).
edge(93, 5).
edge(8, 6).
edge(45, 6).
edge(54, 6).
edge(89, 6).
edge(61, 7).
edge(28, 8).
edge(38, 8).
edge(39, 8).
edge(42, 8).
edge(54, 8).
edge(72, 8).
edge(54, 9).
edge(89, 9).
edge(95, 9).
edge(54, 10).
edge(45, 11).
edge(56, 11).
edge(94, 11).
edge(15, 12).
edge(24, 13).
edge(75, 13).
edge(41, 14).
edge(50, 14).
edge(21, 15).
edge(33, 15).
edge(93, 15).
edge(84, 18).
edge(54, 19).
edge(96, 21).
edge(53, 22).
edge(65, 22).
edge(68, 22).
edge(51, 23).
edge(28, 25).
edge(89, 26).
edge(53, 27).
edge(32, 28).
edge(78, 28).
edge(93, 28).
edge(54, 29).
edge(68, 29).
edge(72, 29).
edge(64, 30).
edge(45, 31).
edge(55, 31).
edge(58, 31).
edge(93, 34).
edge(45, 35).
edge(54, 36).
edge(54, 37).
edge(54, 38).
edge(50, 39).
edge(59, 40).
edge(93, 40).
edge(66, 41).
edge(81, 41).
edge(88, 41).
edge(45, 43).
edge(74, 43).
edge(72, 44).
edge(56, 45).
edge(58, 45).
edge(74, 45).
edge(75, 45).
edge(79, 45).
edge(83, 45).
edge(89, 46).
edge(54, 47).
edge(73, 48).
edge(87, 48).
edge(93, 48).
edge(70, 49).
edge(77, 50).
edge(54, 51).
edge(56, 51).
edge(72, 51).
edge(74, 51).
edge(56, 52).
edge(57, 53).
edge(64, 53).
edge(70, 53).
edge(61, 54).
edge(67, 54).
edge(89, 54).
edge(92, 54).
edge(93, 54).
edge(72, 56).
edge(80, 56).
edge(90, 56).
edge(74, 60).
edge(72, 61).
edge(95, 61).
edge(72, 62).
edge(95, 63).
edge(81, 64).
edge(89, 64).
edge(91, 65).
edge(75, 66).
edge(72, 69).
edge(79, 71).
edge(93, 72).
edge(89, 82).
edge(89, 84).
edge(89, 86).


path(X, X).
path(X, Y):- path(X, Z), edge(Z, Y).

test_rand([S,D]):-
    random_between(1, 662, Source),
    random_between(1, 662, Dest),
    ( path(Source,Dest), (\+(edge(Source,Dest))), Source \= Dest -> 
        write('path between '), write(Source), write(' '), writeln(Dest), S = Source, D = Dest ;
        S = -1, D = -1 
    ).

find(N,L):-
    length(LT,N),
    maplist(test_rand,LT),
    include(\=([-1,-1]),LT,L),
    maplist(writeln,L).
