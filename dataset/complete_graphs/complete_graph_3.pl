:- use_module(library(pita)).
:- pita.

:- begin_lpad.

optimizable edge(0,1).
0.5::edge(0,2).
optimizable edge(1,2).


path(X, X).
path(X, Y):- 
	path(X, Z), 
	edge(Z, Y).

:- end_lpad.
run:-
	statistics(runtime, [Start | _]), 
	prob_optimize(path(0,2),[edge(0,1) +edge(1,2) ],[path(0,2) > 0.8],A),
	statistics(runtime, [Stop | _]),
	Runtime is Stop - Start,
	writeln(A),
	writeln(Runtime).
