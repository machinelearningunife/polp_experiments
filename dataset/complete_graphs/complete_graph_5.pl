:- use_module(library(pita)).
:- pita.

:- begin_lpad.

optimizable edge(0,1).
0.5::edge(0,2).
optimizable edge(0,3).
0.5::edge(0,4).
optimizable edge(1,2).
0.5::edge(1,3).
optimizable edge(1,4).
0.5::edge(2,3).
optimizable edge(2,4).
0.5::edge(3,4).


path(X, X).
path(X, Y):- 
	path(X, Z), 
	edge(Z, Y).

:- end_lpad.
run:-
	statistics(runtime, [Start | _]), 
	prob_optimize(path(0,4),[edge(0,1) +edge(0,3) +edge(1,2) +edge(1,4) +edge(2,4) ],[path(0,4) > 0.8],A),
	statistics(runtime, [Stop | _]),
	Runtime is Stop - Start,
	writeln(A),
	writeln(Runtime).
