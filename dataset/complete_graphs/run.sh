for i in {3..10}
do
    echo complete_graph_$i.pl
    swipl -s complete_graph_$i.pl -g "run_ccsaq,halt."
    swipl -s complete_graph_$i.pl -g "run_mma,halt."
    swipl -s complete_graph_$i.pl -g "run_slsqp,halt."
done
