:- use_module(library(pita)).
:- pita.

:- begin_lpad.

optimizable edge(0,1).
0.5::edge(0,2).
optimizable edge(0,3).
0.5::edge(0,4).
optimizable edge(0,5).
0.5::edge(0,6).
optimizable edge(1,2).
0.5::edge(1,3).
optimizable edge(1,4).
0.5::edge(1,5).
optimizable edge(1,6).
0.5::edge(2,3).
optimizable edge(2,4).
0.5::edge(2,5).
optimizable edge(2,6).
0.5::edge(3,4).
optimizable edge(3,5).
0.5::edge(3,6).
optimizable edge(4,5).
0.5::edge(4,6).
optimizable edge(5,6).


path(X, X).
path(X, Y):- 
	path(X, Z), 
	edge(Z, Y).

:- end_lpad.
run:-
	statistics(runtime, [Start | _]), 
	prob_optimize(path(0,6),[edge(0,1) +edge(0,3) +edge(0,5) +edge(1,2) +edge(1,4) +edge(1,6) +edge(2,4) +edge(2,6) +edge(3,5) +edge(4,5) +edge(5,6) ],[path(0,6) > 0.8],A),
	statistics(runtime, [Stop | _]),
	Runtime is Stop - Start,
	writeln(A),
	writeln(Runtime).
