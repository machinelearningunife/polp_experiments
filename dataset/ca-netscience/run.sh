sbatch --job-name=ca-netscience_187_104 --partition=longrun --mem=8G --output=ca-netscience_187_104_ccsaq.log --wrap='srun swipl -s ca-netscience.pl -g "run_ccsaq(187, 104), halt." '
sbatch --job-name=ca-netscience_187_104 --partition=longrun --mem=8G --output=ca-netscience_187_104_mma.log --wrap='srun swipl -s ca-netscience.pl -g "run_mma(187, 104), halt." '
sbatch --job-name=ca-netscience_187_104 --partition=longrun --mem=8G --output=ca-netscience_187_104_slsqp.log --wrap='srun swipl -s ca-netscience.pl -g "run_slsqp(187, 104), halt." '

sbatch --job-name=ca-netscience_294_71 --partition=longrun --mem=8G --output=ca-netscience_294_71_ccsaq.log --wrap='srun swipl -s ca-netscience.pl -g "run_ccsaq(294, 71), halt." '
sbatch --job-name=ca-netscience_294_71 --partition=longrun --mem=8G --output=ca-netscience_294_71_mma.log --wrap='srun swipl -s ca-netscience.pl -g "run_mma(294, 71), halt." '
sbatch --job-name=ca-netscience_294_71 --partition=longrun --mem=8G --output=ca-netscience_294_71_slsqp.log --wrap='srun swipl -s ca-netscience.pl -g "run_slsqp(294, 71), halt." '

sbatch --job-name=ca-netscience_259_91 --partition=longrun --mem=8G --output=ca-netscience_259_91_ccsaq.log --wrap='srun swipl -s ca-netscience.pl -g "run_ccsaq(259, 91), halt." '
sbatch --job-name=ca-netscience_259_91 --partition=longrun --mem=8G --output=ca-netscience_259_91_mma.log --wrap='srun swipl -s ca-netscience.pl -g "run_mma(259, 91), halt." '
sbatch --job-name=ca-netscience_259_91 --partition=longrun --mem=8G --output=ca-netscience_259_91_slsqp.log --wrap='srun swipl -s ca-netscience.pl -g "run_slsqp(259, 91), halt." '

sbatch --job-name=ca-netscience_291_48 --partition=longrun --mem=8G --output=ca-netscience_291_48_ccsaq.log --wrap='srun swipl -s ca-netscience.pl -g "run_ccsaq(291, 48), halt." '
sbatch --job-name=ca-netscience_291_48 --partition=longrun --mem=8G --output=ca-netscience_291_48_mma.log --wrap='srun swipl -s ca-netscience.pl -g "run_mma(291, 48), halt." '
sbatch --job-name=ca-netscience_291_48 --partition=longrun --mem=8G --output=ca-netscience_291_48_slsqp.log --wrap='srun swipl -s ca-netscience.pl -g "run_slsqp(291, 48), halt." '

sbatch --job-name=ca-netscience_245_199 --partition=longrun --mem=8G --output=ca-netscience_245_199_ccsaq.log --wrap='srun swipl -s ca-netscience.pl -g "run_ccsaq(245, 199), halt." '
sbatch --job-name=ca-netscience_245_199 --partition=longrun --mem=8G --output=ca-netscience_245_199_mma.log --wrap='srun swipl -s ca-netscience.pl -g "run_mma(245, 199), halt." '
sbatch --job-name=ca-netscience_245_199 --partition=longrun --mem=8G --output=ca-netscience_245_199_slsqp.log --wrap='srun swipl -s ca-netscience.pl -g "run_slsqp(245, 199), halt." '

sbatch --job-name=ca-netscience_368_1 --partition=longrun --mem=8G --output=ca-netscience_368_1_ccsaq.log --wrap='srun swipl -s ca-netscience.pl -g "run_ccsaq(368, 1), halt." '
sbatch --job-name=ca-netscience_368_1 --partition=longrun --mem=8G --output=ca-netscience_368_1_mma.log --wrap='srun swipl -s ca-netscience.pl -g "run_mma(368, 1), halt." '
sbatch --job-name=ca-netscience_368_1 --partition=longrun --mem=8G --output=ca-netscience_368_1_slsqp.log --wrap='srun swipl -s ca-netscience.pl -g "run_slsqp(368, 1), halt." '

sbatch --job-name=ca-netscience_178_21 --partition=longrun --mem=8G --output=ca-netscience_178_21_ccsaq.log --wrap='srun swipl -s ca-netscience.pl -g "run_ccsaq(178, 21), halt." '
sbatch --job-name=ca-netscience_178_21 --partition=longrun --mem=8G --output=ca-netscience_178_21_mma.log --wrap='srun swipl -s ca-netscience.pl -g "run_mma(178, 21), halt." '
sbatch --job-name=ca-netscience_178_21 --partition=longrun --mem=8G --output=ca-netscience_178_21_slsqp.log --wrap='srun swipl -s ca-netscience.pl -g "run_slsqp(178, 21), halt." '

sbatch --job-name=ca-netscience_154_13 --partition=longrun --mem=8G --output=ca-netscience_154_13_ccsaq.log --wrap='srun swipl -s ca-netscience.pl -g "run_ccsaq(154, 13), halt." '
sbatch --job-name=ca-netscience_154_13 --partition=longrun --mem=8G --output=ca-netscience_154_13_mma.log --wrap='srun swipl -s ca-netscience.pl -g "run_mma(154, 13), halt." '
sbatch --job-name=ca-netscience_154_13 --partition=longrun --mem=8G --output=ca-netscience_154_13_slsqp.log --wrap='srun swipl -s ca-netscience.pl -g "run_slsqp(154, 13), halt." '

sbatch --job-name=ca-netscience_189_131 --partition=longrun --mem=8G --output=ca-netscience_189_131_ccsaq.log --wrap='srun swipl -s ca-netscience.pl -g "run_ccsaq(189, 131), halt." '
sbatch --job-name=ca-netscience_189_131 --partition=longrun --mem=8G --output=ca-netscience_189_131_mma.log --wrap='srun swipl -s ca-netscience.pl -g "run_mma(189, 131), halt." '
sbatch --job-name=ca-netscience_189_131 --partition=longrun --mem=8G --output=ca-netscience_189_131_slsqp.log --wrap='srun swipl -s ca-netscience.pl -g "run_slsqp(189, 131), halt." '

sbatch --job-name=ca-netscience_156_85 --partition=longrun --mem=8G --output=ca-netscience_156_85_ccsaq.log --wrap='srun swipl -s ca-netscience.pl -g "run_ccsaq(156, 85), halt." '
sbatch --job-name=ca-netscience_156_85 --partition=longrun --mem=8G --output=ca-netscience_156_85_mma.log --wrap='srun swipl -s ca-netscience.pl -g "run_mma(156, 85), halt." '
sbatch --job-name=ca-netscience_156_85 --partition=longrun --mem=8G --output=ca-netscience_156_85_slsqp.log --wrap='srun swipl -s ca-netscience.pl -g "run_slsqp(156, 85), halt." '