:- use_module(library(pita)).
:- pita.
:- begin_lpad.

0.9::edge(2, 1).
optimizable edge(3, 1).
0.9::edge(4, 1).
optimizable edge(5, 1).
0.9::edge(16, 1).
optimizable edge(44, 1).
0.9::edge(113, 1).
optimizable edge(131, 1).
0.9::edge(250, 1).
optimizable edge(259, 1).
0.9::edge(3, 2).
optimizable edge(5, 4).
0.9::edge(13, 4).
optimizable edge(14, 4).
0.9::edge(15, 4).
optimizable edge(16, 4).
0.9::edge(44, 4).
optimizable edge(45, 4).
0.9::edge(46, 4).
optimizable edge(47, 4).
0.9::edge(61, 4).
optimizable edge(126, 4).
0.9::edge(127, 4).
optimizable edge(128, 4).
0.9::edge(146, 4).
optimizable edge(152, 4).
0.9::edge(153, 4).
optimizable edge(154, 4).
0.9::edge(164, 4).
optimizable edge(165, 4).
0.9::edge(166, 4).
optimizable edge(176, 4).
0.9::edge(177, 4).
optimizable edge(249, 4).
0.9::edge(250, 4).
optimizable edge(274, 4).
0.9::edge(313, 4).
optimizable edge(314, 4).
0.9::edge(323, 4).
optimizable edge(324, 4).
0.9::edge(330, 4).
optimizable edge(371, 4).
0.9::edge(373, 4).
optimizable edge(374, 4).
0.9::edge(15, 5).
optimizable edge(16, 5).
0.9::edge(44, 5).
optimizable edge(45, 5).
0.9::edge(46, 5).
optimizable edge(47, 5).
0.9::edge(176, 5).
optimizable edge(177, 5).
0.9::edge(199, 5).
optimizable edge(201, 5).
0.9::edge(202, 5).
optimizable edge(204, 5).
0.9::edge(231, 5).
optimizable edge(235, 5).
0.9::edge(236, 5).
optimizable edge(237, 5).
0.9::edge(238, 5).
optimizable edge(249, 5).
0.9::edge(250, 5).
optimizable edge(254, 5).
0.9::edge(298, 5).
optimizable edge(313, 5).
0.9::edge(314, 5).
optimizable edge(373, 5).
0.9::edge(374, 5).
optimizable edge(7, 6).
0.9::edge(8, 6).
optimizable edge(8, 7).
0.9::edge(190, 7).
optimizable edge(191, 7).
0.9::edge(192, 7).
optimizable edge(193, 7).
0.9::edge(26, 8).
optimizable edge(62, 8).
0.9::edge(63, 8).
optimizable edge(64, 8).
0.9::edge(65, 8).
optimizable edge(137, 8).
0.9::edge(189, 8).
optimizable edge(342, 8).
0.9::edge(343, 8).
optimizable edge(344, 8).
0.9::edge(10, 9).
optimizable edge(11, 9).
0.9::edge(12, 9).
optimizable edge(11, 10).
0.9::edge(12, 10).
optimizable edge(67, 10).
0.9::edge(68, 10).
optimizable edge(69, 10).
0.9::edge(12, 11).
optimizable edge(14, 13).
0.9::edge(15, 13).
optimizable edge(16, 13).
0.9::edge(17, 13).
optimizable edge(18, 13).
0.9::edge(19, 13).
optimizable edge(20, 13).
0.9::edge(274, 13).
optimizable edge(15, 14).
0.9::edge(16, 14).
optimizable edge(16, 15).
0.9::edge(45, 15).
optimizable edge(46, 15).
0.9::edge(47, 15).
optimizable edge(176, 15).
0.9::edge(177, 15).
optimizable edge(278, 15).
0.9::edge(279, 15).
optimizable edge(334, 15).
0.9::edge(366, 15).
optimizable edge(367, 15).
0.9::edge(368, 15).
optimizable edge(45, 16).
0.9::edge(46, 16).
optimizable edge(47, 16).
0.9::edge(153, 16).
optimizable edge(154, 16).
0.9::edge(176, 16).
optimizable edge(177, 16).
0.9::edge(249, 16).
optimizable edge(250, 16).
0.9::edge(313, 16).
optimizable edge(314, 16).
0.9::edge(323, 16).
optimizable edge(324, 16).
0.9::edge(371, 16).
optimizable edge(373, 16).
0.9::edge(18, 17).
optimizable edge(29, 17).
0.9::edge(58, 17).
optimizable edge(172, 17).
0.9::edge(201, 17).
optimizable edge(258, 17).
0.9::edge(261, 17).
optimizable edge(365, 17).
0.9::edge(58, 18).
optimizable edge(172, 18).
0.9::edge(201, 18).
optimizable edge(258, 18).
0.9::edge(261, 18).
optimizable edge(365, 18).
0.9::edge(20, 19).
optimizable edge(208, 19).
0.9::edge(22, 21).
optimizable edge(23, 21).
0.9::edge(24, 21).
optimizable edge(33, 21).
0.9::edge(109, 21).
optimizable edge(220, 21).
0.9::edge(221, 21).
optimizable edge(232, 21).
0.9::edge(233, 21).
optimizable edge(268, 21).
0.9::edge(287, 21).
optimizable edge(288, 21).
0.9::edge(23, 22).
optimizable edge(24, 22).
0.9::edge(24, 23).
optimizable edge(50, 23).
0.9::edge(51, 23).
optimizable edge(52, 23).
0.9::edge(54, 23).
optimizable edge(55, 23).
0.9::edge(220, 23).
optimizable edge(227, 23).
0.9::edge(228, 23).
optimizable edge(79, 24).
0.9::edge(140, 24).
optimizable edge(220, 24).
0.9::edge(229, 24).
optimizable edge(232, 24).
0.9::edge(233, 24).
optimizable edge(268, 24).
0.9::edge(26, 25).
optimizable edge(27, 25).
0.9::edge(28, 25).
optimizable edge(27, 26).
0.9::edge(28, 26).
optimizable edge(40, 26).
0.9::edge(95, 26).
optimizable edge(104, 26).
0.9::edge(105, 26).
optimizable edge(106, 26).
0.9::edge(107, 26).
optimizable edge(108, 26).
0.9::edge(124, 26).
optimizable edge(125, 26).
0.9::edge(155, 26).
optimizable edge(197, 26).
0.9::edge(198, 26).
optimizable edge(231, 26).
0.9::edge(234, 26).
optimizable edge(251, 26).
0.9::edge(273, 26).
optimizable edge(295, 26).
0.9::edge(296, 26).
optimizable edge(297, 26).
0.9::edge(306, 26).
optimizable edge(315, 26).
0.9::edge(316, 26).
optimizable edge(317, 26).
0.9::edge(28, 27).
optimizable edge(31, 30).
0.9::edge(32, 30).
optimizable edge(33, 30).
0.9::edge(34, 30).
optimizable edge(35, 30).
0.9::edge(36, 30).
optimizable edge(51, 30).
0.9::edge(76, 30).
optimizable edge(219, 30).
0.9::edge(32, 31).
optimizable edge(33, 31).
0.9::edge(34, 31).
optimizable edge(33, 32).
0.9::edge(34, 32).
optimizable edge(35, 32).
0.9::edge(36, 32).
optimizable edge(51, 32).
0.9::edge(76, 32).
optimizable edge(216, 32).
0.9::edge(217, 32).
optimizable edge(218, 32).
0.9::edge(219, 32).
optimizable edge(307, 32).
0.9::edge(369, 32).
optimizable edge(370, 32).
0.9::edge(34, 33).
optimizable edge(35, 33).
0.9::edge(36, 33).
optimizable edge(109, 33).
0.9::edge(219, 33).
optimizable edge(220, 33).
0.9::edge(221, 33).
optimizable edge(36, 35).
0.9::edge(219, 35).
optimizable edge(38, 37).
0.9::edge(304, 38).
optimizable edge(305, 38).
0.9::edge(40, 39).
optimizable edge(173, 40).
0.9::edge(174, 40).
optimizable edge(175, 40).
0.9::edge(239, 40).
optimizable edge(240, 40).
0.9::edge(282, 40).
optimizable edge(326, 40).
0.9::edge(42, 41).
optimizable edge(43, 41).
0.9::edge(241, 41).
optimizable edge(242, 41).
0.9::edge(243, 41).
optimizable edge(244, 41).
0.9::edge(43, 42).
optimizable edge(52, 42).
0.9::edge(170, 42).
optimizable edge(241, 42).
0.9::edge(242, 42).
optimizable edge(243, 42).
0.9::edge(244, 42).
optimizable edge(275, 42).
0.9::edge(276, 42).
optimizable edge(277, 42).
0.9::edge(364, 42).
optimizable edge(275, 43).
0.9::edge(276, 43).
optimizable edge(277, 43).
0.9::edge(66, 44).
optimizable edge(46, 45).
0.9::edge(47, 45).
optimizable edge(176, 45).
0.9::edge(177, 45).
optimizable edge(323, 45).
0.9::edge(324, 45).
optimizable edge(47, 46).
0.9::edge(176, 46).
optimizable edge(177, 46).
0.9::edge(176, 47).
optimizable edge(177, 47).
0.9::edge(49, 48).
optimizable edge(67, 49).
0.9::edge(74, 49).
optimizable edge(181, 49).
0.9::edge(182, 49).
optimizable edge(183, 49).
0.9::edge(226, 49).
optimizable edge(51, 50).
0.9::edge(52, 50).
optimizable edge(53, 50).
0.9::edge(52, 51).
optimizable edge(76, 51).
0.9::edge(95, 51).
optimizable edge(100, 51).
0.9::edge(160, 51).
optimizable edge(169, 51).
0.9::edge(170, 51).
optimizable edge(307, 51).
0.9::edge(308, 51).
optimizable edge(322, 51).
0.9::edge(337, 51).
optimizable edge(76, 52).
0.9::edge(100, 52).
optimizable edge(116, 52).
0.9::edge(117, 52).
optimizable edge(169, 52).
0.9::edge(170, 52).
optimizable edge(266, 52).
0.9::edge(267, 52).
optimizable edge(291, 52).
0.9::edge(364, 52).
optimizable edge(170, 53).
0.9::edge(55, 54).
optimizable edge(57, 56).
0.9::edge(100, 56).
optimizable edge(110, 56).
0.9::edge(194, 56).
optimizable edge(195, 56).
0.9::edge(59, 58).
optimizable edge(60, 58).
0.9::edge(60, 59).
optimizable edge(304, 60).
0.9::edge(357, 60).
optimizable edge(358, 60).
0.9::edge(359, 60).
optimizable edge(164, 61).
0.9::edge(165, 61).
optimizable edge(166, 61).
0.9::edge(63, 62).
optimizable edge(64, 62).
0.9::edge(65, 62).
optimizable edge(64, 63).
0.9::edge(264, 65).
optimizable edge(265, 65).
0.9::edge(299, 65).
optimizable edge(300, 65).
0.9::edge(301, 65).
optimizable edge(302, 65).
0.9::edge(345, 65).
optimizable edge(346, 65).
0.9::edge(100, 66).
optimizable edge(101, 66).
0.9::edge(102, 66).
optimizable edge(110, 66).
0.9::edge(111, 66).
optimizable edge(68, 67).
0.9::edge(69, 67).
optimizable edge(70, 67).
0.9::edge(71, 67).
optimizable edge(72, 67).
0.9::edge(73, 67).
optimizable edge(74, 67).
0.9::edge(75, 67).
optimizable edge(80, 67).
0.9::edge(81, 67).
optimizable edge(121, 67).
0.9::edge(122, 67).
optimizable edge(123, 67).
0.9::edge(169, 67).
optimizable edge(248, 67).
0.9::edge(285, 67).
optimizable edge(363, 67).
0.9::edge(69, 68).
optimizable edge(80, 68).
0.9::edge(81, 68).
optimizable edge(70, 69).
0.9::edge(71, 69).
optimizable edge(75, 69).
0.9::edge(80, 69).
optimizable edge(81, 69).
0.9::edge(285, 69).
optimizable edge(71, 70).
0.9::edge(72, 70).
optimizable edge(73, 70).
0.9::edge(75, 70).
optimizable edge(119, 70).
0.9::edge(150, 70).
optimizable edge(214, 70).
0.9::edge(303, 70).
optimizable edge(328, 70).
0.9::edge(329, 70).
optimizable edge(348, 70).
0.9::edge(349, 70).
optimizable edge(350, 70).
0.9::edge(351, 70).
optimizable edge(378, 70).
0.9::edge(379, 70).
optimizable edge(72, 71).
0.9::edge(73, 71).
optimizable edge(75, 71).
0.9::edge(73, 72).
optimizable edge(119, 72).
0.9::edge(303, 72).
optimizable edge(150, 73).
0.9::edge(285, 75).
optimizable edge(169, 76).
0.9::edge(170, 76).
optimizable edge(78, 77).
0.9::edge(79, 77).
optimizable edge(79, 78).
0.9::edge(81, 80).
optimizable edge(121, 81).
0.9::edge(122, 81).
optimizable edge(123, 81).
0.9::edge(83, 82).
optimizable edge(84, 82).
0.9::edge(85, 82).
optimizable edge(86, 82).
0.9::edge(87, 82).
optimizable edge(88, 82).
0.9::edge(89, 82).
optimizable edge(84, 83).
0.9::edge(85, 83).
optimizable edge(86, 83).
0.9::edge(87, 83).
optimizable edge(88, 83).
0.9::edge(89, 83).
optimizable edge(262, 83).
0.9::edge(263, 83).
optimizable edge(85, 84).
0.9::edge(86, 84).
optimizable edge(87, 84).
0.9::edge(88, 84).
optimizable edge(89, 84).
0.9::edge(86, 85).
optimizable edge(87, 85).
0.9::edge(88, 85).
optimizable edge(89, 85).
0.9::edge(106, 85).
optimizable edge(260, 85).
0.9::edge(262, 85).
optimizable edge(263, 85).
0.9::edge(87, 86).
optimizable edge(88, 86).
0.9::edge(89, 86).
optimizable edge(106, 86).
0.9::edge(260, 86).
optimizable edge(262, 86).
0.9::edge(263, 86).
optimizable edge(88, 87).
0.9::edge(89, 87).
optimizable edge(89, 88).
0.9::edge(106, 88).
optimizable edge(260, 88).
0.9::edge(262, 88).
optimizable edge(263, 88).
0.9::edge(91, 90).
optimizable edge(92, 90).
0.9::edge(92, 91).
optimizable edge(130, 91).
0.9::edge(131, 91).
optimizable edge(132, 91).
0.9::edge(133, 91).
optimizable edge(134, 91).
0.9::edge(188, 91).
optimizable edge(130, 92).
0.9::edge(131, 92).
optimizable edge(132, 92).
0.9::edge(133, 92).
optimizable edge(134, 92).
0.9::edge(188, 92).
optimizable edge(94, 93).
0.9::edge(95, 93).
optimizable edge(96, 93).
0.9::edge(97, 93).
optimizable edge(98, 93).
0.9::edge(99, 93).
optimizable edge(95, 94).
0.9::edge(96, 94).
optimizable edge(97, 94).
0.9::edge(98, 94).
optimizable edge(99, 94).
0.9::edge(96, 95).
optimizable edge(97, 95).
0.9::edge(98, 95).
optimizable edge(99, 95).
0.9::edge(178, 95).
optimizable edge(179, 95).
0.9::edge(180, 95).
optimizable edge(286, 95).
0.9::edge(308, 95).
optimizable edge(337, 95).
0.9::edge(338, 95).
optimizable edge(339, 95).
0.9::edge(362, 95).
optimizable edge(97, 96).
0.9::edge(98, 96).
optimizable edge(99, 96).
0.9::edge(141, 96).
optimizable edge(98, 97).
0.9::edge(99, 97).
optimizable edge(178, 97).
0.9::edge(362, 97).
optimizable edge(99, 98).
0.9::edge(101, 100).
optimizable edge(102, 100).
0.9::edge(103, 100).
optimizable edge(110, 100).
0.9::edge(111, 100).
optimizable edge(145, 100).
0.9::edge(194, 100).
optimizable edge(195, 100).
0.9::edge(102, 101).
optimizable edge(103, 101).
0.9::edge(311, 101).
optimizable edge(159, 102).
0.9::edge(280, 102).
optimizable edge(360, 102).
0.9::edge(361, 102).
optimizable edge(105, 104).
0.9::edge(106, 104).
optimizable edge(107, 104).
0.9::edge(108, 104).
optimizable edge(106, 105).
0.9::edge(107, 105).
optimizable edge(107, 106).
0.9::edge(185, 106).
optimizable edge(260, 106).
0.9::edge(108, 107).
optimizable edge(283, 107).
0.9::edge(284, 107).
optimizable edge(372, 107).
0.9::edge(125, 108).
optimizable edge(155, 108).
0.9::edge(156, 108).
optimizable edge(158, 108).
0.9::edge(111, 110).
optimizable edge(194, 110).
0.9::edge(195, 110).
optimizable edge(113, 112).
0.9::edge(114, 112).
optimizable edge(115, 112).
0.9::edge(114, 113).
optimizable edge(115, 113).
0.9::edge(131, 113).
optimizable edge(135, 113).
0.9::edge(136, 113).
optimizable edge(189, 113).
0.9::edge(259, 113).
optimizable edge(312, 113).
0.9::edge(352, 113).
optimizable edge(353, 113).
0.9::edge(354, 113).
optimizable edge(355, 113).
0.9::edge(356, 113).
optimizable edge(115, 114).
0.9::edge(131, 114).
optimizable edge(135, 114).
0.9::edge(312, 114).
optimizable edge(172, 115).
0.9::edge(347, 115).
optimizable edge(117, 116).
0.9::edge(318, 116).
optimizable edge(319, 116).
0.9::edge(320, 116).
optimizable edge(321, 116).
0.9::edge(119, 118).
optimizable edge(120, 118).
0.9::edge(209, 118).
optimizable edge(120, 119).
0.9::edge(214, 119).
optimizable edge(303, 119).
0.9::edge(348, 119).
optimizable edge(349, 119).
0.9::edge(350, 119).
optimizable edge(351, 119).
0.9::edge(122, 121).
optimizable edge(123, 121).
0.9::edge(123, 122).
optimizable edge(169, 122).
0.9::edge(248, 122).
optimizable edge(125, 124).
0.9::edge(234, 125).
optimizable edge(255, 125).
0.9::edge(256, 125).
optimizable edge(127, 126).
0.9::edge(128, 126).
optimizable edge(129, 126).
0.9::edge(327, 126).
optimizable edge(330, 126).
0.9::edge(128, 127).
optimizable edge(129, 127).
0.9::edge(327, 127).
optimizable edge(330, 127).
0.9::edge(129, 128).
optimizable edge(327, 128).
0.9::edge(330, 128).
optimizable edge(340, 128).
0.9::edge(341, 128).
optimizable edge(131, 130).
0.9::edge(132, 130).
optimizable edge(133, 130).
0.9::edge(134, 130).
optimizable edge(132, 131).
0.9::edge(133, 131).
optimizable edge(134, 131).
0.9::edge(135, 131).
optimizable edge(136, 131).
0.9::edge(259, 131).
optimizable edge(133, 132).
0.9::edge(134, 132).
optimizable edge(188, 132).
0.9::edge(134, 133).
optimizable edge(189, 135).
0.9::edge(352, 135).
optimizable edge(353, 135).
0.9::edge(354, 135).
optimizable edge(355, 135).
0.9::edge(356, 135).
optimizable edge(138, 137).
0.9::edge(342, 137).
optimizable edge(343, 137).
0.9::edge(140, 139).
optimizable edge(206, 140).
0.9::edge(207, 140).
optimizable edge(229, 140).
0.9::edge(230, 140).
optimizable edge(143, 142).
0.9::edge(144, 142).
optimizable edge(145, 142).
0.9::edge(144, 143).
optimizable edge(145, 143).
0.9::edge(145, 144).
optimizable edge(194, 145).
0.9::edge(147, 146).
optimizable edge(148, 146).
0.9::edge(148, 147).
optimizable edge(150, 149).
0.9::edge(151, 149).
optimizable edge(270, 149).
0.9::edge(293, 149).
optimizable edge(151, 150).
0.9::edge(270, 150).
optimizable edge(271, 150).
0.9::edge(293, 150).
optimizable edge(294, 150).
0.9::edge(154, 153).
optimizable edge(156, 155).
0.9::edge(157, 155).
optimizable edge(158, 155).
0.9::edge(161, 160).
optimizable edge(162, 160).
0.9::edge(163, 160).
optimizable edge(322, 160).
0.9::edge(162, 161).
optimizable edge(163, 162).
0.9::edge(165, 164).
optimizable edge(166, 164).
0.9::edge(166, 165).
optimizable edge(168, 167).
0.9::edge(169, 167).
optimizable edge(170, 167).
0.9::edge(169, 168).
optimizable edge(170, 168).
0.9::edge(205, 168).
optimizable edge(170, 169).
0.9::edge(205, 169).
optimizable edge(248, 169).
0.9::edge(289, 169).
optimizable edge(290, 169).
0.9::edge(291, 169).
optimizable edge(292, 169).
0.9::edge(266, 170).
optimizable edge(267, 170).
0.9::edge(336, 170).
optimizable edge(364, 170).
0.9::edge(172, 171).
optimizable edge(325, 172).
0.9::edge(347, 172).
optimizable edge(174, 173).
0.9::edge(175, 173).
optimizable edge(175, 174).
0.9::edge(282, 175).
optimizable edge(177, 176).
0.9::edge(179, 178).
optimizable edge(180, 178).
0.9::edge(182, 181).
optimizable edge(183, 181).
0.9::edge(183, 182).
optimizable edge(226, 183).
0.9::edge(185, 184).
optimizable edge(186, 184).
0.9::edge(187, 184).
optimizable edge(309, 184).
0.9::edge(310, 184).
optimizable edge(186, 185).
0.9::edge(187, 185).
optimizable edge(309, 185).
0.9::edge(310, 185).
optimizable edge(187, 186).
0.9::edge(309, 186).
optimizable edge(310, 186).
0.9::edge(191, 190).
optimizable edge(192, 190).
0.9::edge(193, 190).
optimizable edge(193, 192).
0.9::edge(195, 194).
optimizable edge(196, 194).
0.9::edge(251, 198).
optimizable edge(200, 199).
0.9::edge(201, 199).
optimizable edge(202, 199).
0.9::edge(203, 199).
optimizable edge(204, 199).
0.9::edge(258, 199).
optimizable edge(201, 200).
0.9::edge(202, 200).
optimizable edge(202, 201).
0.9::edge(203, 201).
optimizable edge(204, 201).
0.9::edge(245, 201).
optimizable edge(252, 201).
0.9::edge(253, 201).
optimizable edge(254, 201).
0.9::edge(258, 201).
optimizable edge(298, 201).
0.9::edge(203, 202).
optimizable edge(204, 202).
0.9::edge(258, 202).
optimizable edge(245, 204).
0.9::edge(298, 204).
optimizable edge(207, 206).
0.9::edge(375, 207).
optimizable edge(376, 207).
0.9::edge(377, 207).
optimizable edge(211, 210).
0.9::edge(212, 210).
optimizable edge(213, 210).
0.9::edge(214, 210).
optimizable edge(215, 210).
0.9::edge(212, 211).
optimizable edge(213, 211).
0.9::edge(214, 211).
optimizable edge(215, 211).
0.9::edge(213, 212).
optimizable edge(214, 212).
0.9::edge(215, 212).
optimizable edge(222, 212).
0.9::edge(223, 212).
optimizable edge(224, 212).
0.9::edge(225, 212).
optimizable edge(214, 213).
0.9::edge(215, 213).
optimizable edge(215, 214).
0.9::edge(303, 214).
optimizable edge(348, 214).
0.9::edge(349, 214).
optimizable edge(350, 214).
0.9::edge(351, 214).
optimizable edge(217, 216).
0.9::edge(218, 216).
optimizable edge(218, 217).
0.9::edge(221, 220).
optimizable edge(223, 222).
0.9::edge(224, 222).
optimizable edge(225, 222).
0.9::edge(224, 223).
optimizable edge(225, 223).
0.9::edge(225, 224).
optimizable edge(228, 227).
0.9::edge(232, 231).
optimizable edge(233, 231).
0.9::edge(234, 231).
optimizable edge(235, 231).
0.9::edge(236, 231).
optimizable edge(237, 231).
0.9::edge(238, 231).
optimizable edge(239, 231).
0.9::edge(240, 231).
optimizable edge(246, 231).
0.9::edge(257, 231).
optimizable edge(297, 231).
0.9::edge(233, 232).
optimizable edge(268, 232).
0.9::edge(268, 233).
optimizable edge(237, 236).
0.9::edge(238, 236).
optimizable edge(239, 236).
0.9::edge(240, 236).
optimizable edge(245, 236).
0.9::edge(246, 236).
optimizable edge(247, 236).
0.9::edge(257, 236).
optimizable edge(238, 237).
0.9::edge(240, 239).
optimizable edge(246, 239).
0.9::edge(257, 239).
optimizable edge(326, 239).
0.9::edge(246, 240).
optimizable edge(257, 240).
0.9::edge(326, 240).
optimizable edge(242, 241).
0.9::edge(243, 241).
optimizable edge(244, 241).
0.9::edge(243, 242).
optimizable edge(244, 242).
0.9::edge(244, 243).
optimizable edge(246, 245).
0.9::edge(247, 245).
optimizable edge(298, 245).
0.9::edge(247, 246).
optimizable edge(257, 246).
0.9::edge(313, 250).
optimizable edge(314, 250).
0.9::edge(253, 252).
optimizable edge(256, 255).
0.9::edge(263, 262).
optimizable edge(265, 264).
0.9::edge(299, 265).
optimizable edge(300, 265).
0.9::edge(301, 265).
optimizable edge(302, 265).
0.9::edge(267, 266).
optimizable edge(270, 269).
0.9::edge(271, 269).
optimizable edge(272, 269).
0.9::edge(271, 270).
optimizable edge(272, 270).
0.9::edge(293, 270).
optimizable edge(294, 270).
0.9::edge(294, 271).
optimizable edge(276, 275).
0.9::edge(277, 275).
optimizable edge(277, 276).
0.9::edge(279, 278).
optimizable edge(281, 280).
0.9::edge(360, 280).
optimizable edge(361, 280).
0.9::edge(284, 283).
optimizable edge(288, 287).
0.9::edge(290, 289).
optimizable edge(291, 290).
0.9::edge(292, 290).
optimizable edge(296, 295).
0.9::edge(300, 299).
optimizable edge(301, 299).
0.9::edge(302, 299).
optimizable edge(301, 300).
0.9::edge(328, 303).
optimizable edge(329, 303).
0.9::edge(348, 303).
optimizable edge(349, 303).
0.9::edge(350, 303).
optimizable edge(351, 303).
0.9::edge(378, 303).
optimizable edge(379, 303).
0.9::edge(305, 304).
optimizable edge(357, 304).
0.9::edge(358, 304).
optimizable edge(359, 304).
0.9::edge(337, 308).
optimizable edge(310, 309).
0.9::edge(314, 313).
optimizable edge(316, 315).
0.9::edge(317, 315).
optimizable edge(317, 316).
0.9::edge(319, 318).
optimizable edge(320, 318).
0.9::edge(321, 318).
optimizable edge(320, 319).
0.9::edge(321, 319).
optimizable edge(321, 320).
0.9::edge(324, 323).
optimizable edge(329, 328).
0.9::edge(332, 331).
optimizable edge(333, 331).
0.9::edge(334, 331).
optimizable edge(335, 331).
0.9::edge(333, 332).
optimizable edge(334, 332).
0.9::edge(335, 332).
optimizable edge(334, 333).
0.9::edge(335, 333).
optimizable edge(335, 334).
0.9::edge(366, 334).
optimizable edge(367, 334).
0.9::edge(368, 334).
optimizable edge(339, 338).
0.9::edge(341, 340).
optimizable edge(343, 342).
0.9::edge(346, 345).
optimizable edge(349, 348).
0.9::edge(350, 348).
optimizable edge(351, 348).
0.9::edge(350, 349).
optimizable edge(351, 349).
0.9::edge(351, 350).
optimizable edge(353, 352).
0.9::edge(354, 352).
optimizable edge(355, 352).
0.9::edge(356, 352).
optimizable edge(354, 353).
0.9::edge(355, 353).
optimizable edge(356, 353).
0.9::edge(355, 354).
optimizable edge(356, 354).
0.9::edge(356, 355).
optimizable edge(358, 357).
0.9::edge(359, 357).
optimizable edge(359, 358).
0.9::edge(361, 360).
optimizable edge(367, 366).
0.9::edge(368, 366).
optimizable edge(368, 367).
0.9::edge(370, 369).
optimizable edge(374, 373).
0.9::edge(376, 375).
optimizable edge(377, 375).
0.9::edge(377, 376).
optimizable edge(379, 378).

path(X, X).
path(X, Y):- path(X, Z), edge(Z, Y).



:- end_lpad.

run:- run_mma, run_ccsaq, run_slsqp.

run_mma(S,D):-
	statistics(runtime, [Start | _]), 
	prob_optimize(path(S,D),[edge(3,1) +edge(5,1) +edge(44,1) +edge(131,1) +edge(259,1) +edge(5,4) +edge(14,4) +edge(16,4) +edge(45,4) +edge(47,4) +edge(126,4) +edge(128,4) +edge(152,4) +edge(154,4) +edge(165,4) +edge(176,4) +edge(249,4) +edge(274,4) +edge(314,4) +edge(324,4) +edge(371,4) +edge(374,4) +edge(16,5) +edge(45,5) +edge(47,5) +edge(177,5) +edge(201,5) +edge(204,5) +edge(235,5) +edge(237,5) +edge(249,5) +edge(254,5) +edge(313,5) +edge(373,5) +edge(7,6) +edge(8,7) +edge(191,7) +edge(193,7) +edge(62,8) +edge(64,8) +edge(137,8) +edge(342,8) +edge(344,8) +edge(11,9) +edge(11,10) +edge(67,10) +edge(69,10) +edge(14,13) +edge(16,13) +edge(18,13) +edge(20,13) +edge(15,14) +edge(16,15) +edge(46,15) +edge(176,15) +edge(278,15) +edge(334,15) +edge(367,15) +edge(45,16) +edge(47,16) +edge(154,16) +edge(177,16) +edge(250,16) +edge(314,16) +edge(324,16) +edge(373,16) +edge(29,17) +edge(172,17) +edge(258,17) +edge(365,17) +edge(172,18) +edge(258,18) +edge(365,18) +edge(208,19) +edge(23,21) +edge(33,21) +edge(220,21) +edge(232,21) +edge(268,21) +edge(288,21) +edge(24,22) +edge(50,23) +edge(52,23) +edge(55,23) +edge(227,23) +edge(79,24) +edge(220,24) +edge(232,24) +edge(268,24) +edge(27,25) +edge(27,26) +edge(40,26) +edge(104,26) +edge(106,26) +edge(108,26) +edge(125,26) +edge(197,26) +edge(231,26) +edge(251,26) +edge(295,26) +edge(297,26) +edge(315,26) +edge(317,26) +edge(31,30) +edge(33,30) +edge(35,30) +edge(51,30) +edge(219,30) +edge(33,31) +edge(33,32) +edge(35,32) +edge(51,32) +edge(216,32) +edge(218,32) +edge(307,32) +edge(370,32) +edge(35,33) +edge(109,33) +edge(220,33) +edge(36,35) +edge(38,37) +edge(305,38) +edge(173,40) +edge(175,40) +edge(240,40) +edge(326,40) +edge(43,41) +edge(242,41) +edge(244,41) +edge(52,42) +edge(241,42) +edge(243,42) +edge(275,42) +edge(277,42) +edge(275,43) +edge(277,43) +edge(46,45) +edge(176,45) +edge(323,45) +edge(47,46) +edge(177,46) +edge(177,47) +edge(67,49) +edge(181,49) +edge(183,49) +edge(51,50) +edge(53,50) +edge(76,51) +edge(100,51) +edge(169,51) +edge(307,51) +edge(322,51) +edge(76,52) +edge(116,52) +edge(169,52) +edge(266,52) +edge(291,52) +edge(170,53) +edge(57,56) +edge(110,56) +edge(195,56) +edge(60,58) +edge(304,60) +edge(358,60) +edge(164,61) +edge(166,61) +edge(64,62) +edge(64,63) +edge(265,65) +edge(300,65) +edge(302,65) +edge(346,65) +edge(101,66) +edge(110,66) +edge(68,67) +edge(70,67) +edge(72,67) +edge(74,67) +edge(80,67) +edge(121,67) +edge(123,67) +edge(248,67) +edge(363,67) +edge(80,68) +edge(70,69) +edge(75,69) +edge(81,69) +edge(71,70) +edge(73,70) +edge(119,70) +edge(214,70) +edge(328,70) +edge(348,70) +edge(350,70) +edge(378,70) +edge(72,71) +edge(75,71) +edge(119,72) +edge(150,73) +edge(169,76) +edge(78,77) +edge(79,78) +edge(121,81) +edge(123,81) +edge(84,82) +edge(86,82) +edge(88,82) +edge(84,83) +edge(86,83) +edge(88,83) +edge(262,83) +edge(85,84) +edge(87,84) +edge(89,84) +edge(87,85) +edge(89,85) +edge(260,85) +edge(263,85) +edge(88,86) +edge(106,86) +edge(262,86) +edge(88,87) +edge(89,88) +edge(260,88) +edge(263,88) +edge(92,90) +edge(130,91) +edge(132,91) +edge(134,91) +edge(130,92) +edge(132,92) +edge(134,92) +edge(94,93) +edge(96,93) +edge(98,93) +edge(95,94) +edge(97,94) +edge(99,94) +edge(97,95) +edge(99,95) +edge(179,95) +edge(286,95) +edge(337,95) +edge(339,95) +edge(97,96) +edge(99,96) +edge(98,97) +edge(178,97) +edge(99,98) +edge(102,100) +edge(110,100) +edge(145,100) +edge(195,100) +edge(103,101) +edge(159,102) +edge(360,102) +edge(105,104) +edge(107,104) +edge(106,105) +edge(107,106) +edge(260,106) +edge(283,107) +edge(372,107) +edge(155,108) +edge(158,108) +edge(194,110) +edge(113,112) +edge(115,112) +edge(115,113) +edge(135,113) +edge(189,113) +edge(312,113) +edge(353,113) +edge(355,113) +edge(115,114) +edge(135,114) +edge(172,115) +edge(117,116) +edge(319,116) +edge(321,116) +edge(120,118) +edge(120,119) +edge(303,119) +edge(349,119) +edge(351,119) +edge(123,121) +edge(169,122) +edge(125,124) +edge(255,125) +edge(127,126) +edge(129,126) +edge(330,126) +edge(129,127) +edge(330,127) +edge(327,128) +edge(340,128) +edge(131,130) +edge(133,130) +edge(132,131) +edge(134,131) +edge(136,131) +edge(133,132) +edge(188,132) +edge(189,135) +edge(353,135) +edge(355,135) +edge(138,137) +edge(343,137) +edge(206,140) +edge(229,140) +edge(143,142) +edge(145,142) +edge(145,143) +edge(194,145) +edge(148,146) +edge(150,149) +edge(270,149) +edge(151,150) +edge(271,150) +edge(294,150) +edge(156,155) +edge(158,155) +edge(162,160) +edge(322,160) +edge(163,162) +edge(166,164) +edge(168,167) +edge(170,167) +edge(170,168) +edge(170,169) +edge(248,169) +edge(290,169) +edge(292,169) +edge(267,170) +edge(364,170) +edge(325,172) +edge(174,173) +edge(175,174) +edge(177,176) +edge(180,178) +edge(183,181) +edge(226,183) +edge(186,184) +edge(309,184) +edge(186,185) +edge(309,185) +edge(187,186) +edge(310,186) +edge(192,190) +edge(193,192) +edge(196,194) +edge(200,199) +edge(202,199) +edge(204,199) +edge(201,200) +edge(202,201) +edge(204,201) +edge(252,201) +edge(254,201) +edge(298,201) +edge(204,202) +edge(245,204) +edge(207,206) +edge(376,207) +edge(211,210) +edge(213,210) +edge(215,210) +edge(213,211) +edge(215,211) +edge(214,212) +edge(222,212) +edge(224,212) +edge(214,213) +edge(215,214) +edge(348,214) +edge(350,214) +edge(217,216) +edge(218,217) +edge(223,222) +edge(225,222) +edge(225,223) +edge(228,227) +edge(233,231) +edge(235,231) +edge(237,231) +edge(239,231) +edge(246,231) +edge(297,231) +edge(268,232) +edge(237,236) +edge(239,236) +edge(245,236) +edge(247,236) +edge(238,237) +edge(246,239) +edge(326,239) +edge(257,240) +edge(242,241) +edge(244,241) +edge(244,242) +edge(246,245) +edge(298,245) +edge(257,246) +edge(314,250) +edge(256,255) +edge(265,264) +edge(300,265) +edge(302,265) +edge(270,269) +edge(272,269) +edge(272,270) +edge(294,270) +edge(276,275) +edge(277,276) +edge(281,280) +edge(361,280) +edge(288,287) +edge(291,290) +edge(296,295) +edge(301,299) +edge(301,300) +edge(329,303) +edge(349,303) +edge(351,303) +edge(379,303) +edge(357,304) +edge(359,304) +edge(310,309) +edge(316,315) +edge(317,316) +edge(320,318) +edge(320,319) +edge(321,320) +edge(329,328) +edge(333,331) +edge(335,331) +edge(334,332) +edge(334,333) +edge(335,334) +edge(367,334) +edge(339,338) +edge(343,342) +edge(349,348) +edge(351,348) +edge(351,349) +edge(353,352) +edge(355,352) +edge(354,353) +edge(356,353) +edge(356,354) +edge(358,357) +edge(359,358) +edge(367,366) +edge(368,367) +edge(374,373) +edge(377,375) +edge(379,378) ],[path(S,D) > 0.8],mma,-1,A),
	statistics(runtime, [Stop | _]),
	Runtime is Stop - Start,
	writeln('mma'),
	writeln(Runtime),
	writeln(A).

run_ccsaq(S,D):-
	statistics(runtime, [Start | _]), 
	prob_optimize(path(S,D),[edge(3,1) +edge(5,1) +edge(44,1) +edge(131,1) +edge(259,1) +edge(5,4) +edge(14,4) +edge(16,4) +edge(45,4) +edge(47,4) +edge(126,4) +edge(128,4) +edge(152,4) +edge(154,4) +edge(165,4) +edge(176,4) +edge(249,4) +edge(274,4) +edge(314,4) +edge(324,4) +edge(371,4) +edge(374,4) +edge(16,5) +edge(45,5) +edge(47,5) +edge(177,5) +edge(201,5) +edge(204,5) +edge(235,5) +edge(237,5) +edge(249,5) +edge(254,5) +edge(313,5) +edge(373,5) +edge(7,6) +edge(8,7) +edge(191,7) +edge(193,7) +edge(62,8) +edge(64,8) +edge(137,8) +edge(342,8) +edge(344,8) +edge(11,9) +edge(11,10) +edge(67,10) +edge(69,10) +edge(14,13) +edge(16,13) +edge(18,13) +edge(20,13) +edge(15,14) +edge(16,15) +edge(46,15) +edge(176,15) +edge(278,15) +edge(334,15) +edge(367,15) +edge(45,16) +edge(47,16) +edge(154,16) +edge(177,16) +edge(250,16) +edge(314,16) +edge(324,16) +edge(373,16) +edge(29,17) +edge(172,17) +edge(258,17) +edge(365,17) +edge(172,18) +edge(258,18) +edge(365,18) +edge(208,19) +edge(23,21) +edge(33,21) +edge(220,21) +edge(232,21) +edge(268,21) +edge(288,21) +edge(24,22) +edge(50,23) +edge(52,23) +edge(55,23) +edge(227,23) +edge(79,24) +edge(220,24) +edge(232,24) +edge(268,24) +edge(27,25) +edge(27,26) +edge(40,26) +edge(104,26) +edge(106,26) +edge(108,26) +edge(125,26) +edge(197,26) +edge(231,26) +edge(251,26) +edge(295,26) +edge(297,26) +edge(315,26) +edge(317,26) +edge(31,30) +edge(33,30) +edge(35,30) +edge(51,30) +edge(219,30) +edge(33,31) +edge(33,32) +edge(35,32) +edge(51,32) +edge(216,32) +edge(218,32) +edge(307,32) +edge(370,32) +edge(35,33) +edge(109,33) +edge(220,33) +edge(36,35) +edge(38,37) +edge(305,38) +edge(173,40) +edge(175,40) +edge(240,40) +edge(326,40) +edge(43,41) +edge(242,41) +edge(244,41) +edge(52,42) +edge(241,42) +edge(243,42) +edge(275,42) +edge(277,42) +edge(275,43) +edge(277,43) +edge(46,45) +edge(176,45) +edge(323,45) +edge(47,46) +edge(177,46) +edge(177,47) +edge(67,49) +edge(181,49) +edge(183,49) +edge(51,50) +edge(53,50) +edge(76,51) +edge(100,51) +edge(169,51) +edge(307,51) +edge(322,51) +edge(76,52) +edge(116,52) +edge(169,52) +edge(266,52) +edge(291,52) +edge(170,53) +edge(57,56) +edge(110,56) +edge(195,56) +edge(60,58) +edge(304,60) +edge(358,60) +edge(164,61) +edge(166,61) +edge(64,62) +edge(64,63) +edge(265,65) +edge(300,65) +edge(302,65) +edge(346,65) +edge(101,66) +edge(110,66) +edge(68,67) +edge(70,67) +edge(72,67) +edge(74,67) +edge(80,67) +edge(121,67) +edge(123,67) +edge(248,67) +edge(363,67) +edge(80,68) +edge(70,69) +edge(75,69) +edge(81,69) +edge(71,70) +edge(73,70) +edge(119,70) +edge(214,70) +edge(328,70) +edge(348,70) +edge(350,70) +edge(378,70) +edge(72,71) +edge(75,71) +edge(119,72) +edge(150,73) +edge(169,76) +edge(78,77) +edge(79,78) +edge(121,81) +edge(123,81) +edge(84,82) +edge(86,82) +edge(88,82) +edge(84,83) +edge(86,83) +edge(88,83) +edge(262,83) +edge(85,84) +edge(87,84) +edge(89,84) +edge(87,85) +edge(89,85) +edge(260,85) +edge(263,85) +edge(88,86) +edge(106,86) +edge(262,86) +edge(88,87) +edge(89,88) +edge(260,88) +edge(263,88) +edge(92,90) +edge(130,91) +edge(132,91) +edge(134,91) +edge(130,92) +edge(132,92) +edge(134,92) +edge(94,93) +edge(96,93) +edge(98,93) +edge(95,94) +edge(97,94) +edge(99,94) +edge(97,95) +edge(99,95) +edge(179,95) +edge(286,95) +edge(337,95) +edge(339,95) +edge(97,96) +edge(99,96) +edge(98,97) +edge(178,97) +edge(99,98) +edge(102,100) +edge(110,100) +edge(145,100) +edge(195,100) +edge(103,101) +edge(159,102) +edge(360,102) +edge(105,104) +edge(107,104) +edge(106,105) +edge(107,106) +edge(260,106) +edge(283,107) +edge(372,107) +edge(155,108) +edge(158,108) +edge(194,110) +edge(113,112) +edge(115,112) +edge(115,113) +edge(135,113) +edge(189,113) +edge(312,113) +edge(353,113) +edge(355,113) +edge(115,114) +edge(135,114) +edge(172,115) +edge(117,116) +edge(319,116) +edge(321,116) +edge(120,118) +edge(120,119) +edge(303,119) +edge(349,119) +edge(351,119) +edge(123,121) +edge(169,122) +edge(125,124) +edge(255,125) +edge(127,126) +edge(129,126) +edge(330,126) +edge(129,127) +edge(330,127) +edge(327,128) +edge(340,128) +edge(131,130) +edge(133,130) +edge(132,131) +edge(134,131) +edge(136,131) +edge(133,132) +edge(188,132) +edge(189,135) +edge(353,135) +edge(355,135) +edge(138,137) +edge(343,137) +edge(206,140) +edge(229,140) +edge(143,142) +edge(145,142) +edge(145,143) +edge(194,145) +edge(148,146) +edge(150,149) +edge(270,149) +edge(151,150) +edge(271,150) +edge(294,150) +edge(156,155) +edge(158,155) +edge(162,160) +edge(322,160) +edge(163,162) +edge(166,164) +edge(168,167) +edge(170,167) +edge(170,168) +edge(170,169) +edge(248,169) +edge(290,169) +edge(292,169) +edge(267,170) +edge(364,170) +edge(325,172) +edge(174,173) +edge(175,174) +edge(177,176) +edge(180,178) +edge(183,181) +edge(226,183) +edge(186,184) +edge(309,184) +edge(186,185) +edge(309,185) +edge(187,186) +edge(310,186) +edge(192,190) +edge(193,192) +edge(196,194) +edge(200,199) +edge(202,199) +edge(204,199) +edge(201,200) +edge(202,201) +edge(204,201) +edge(252,201) +edge(254,201) +edge(298,201) +edge(204,202) +edge(245,204) +edge(207,206) +edge(376,207) +edge(211,210) +edge(213,210) +edge(215,210) +edge(213,211) +edge(215,211) +edge(214,212) +edge(222,212) +edge(224,212) +edge(214,213) +edge(215,214) +edge(348,214) +edge(350,214) +edge(217,216) +edge(218,217) +edge(223,222) +edge(225,222) +edge(225,223) +edge(228,227) +edge(233,231) +edge(235,231) +edge(237,231) +edge(239,231) +edge(246,231) +edge(297,231) +edge(268,232) +edge(237,236) +edge(239,236) +edge(245,236) +edge(247,236) +edge(238,237) +edge(246,239) +edge(326,239) +edge(257,240) +edge(242,241) +edge(244,241) +edge(244,242) +edge(246,245) +edge(298,245) +edge(257,246) +edge(314,250) +edge(256,255) +edge(265,264) +edge(300,265) +edge(302,265) +edge(270,269) +edge(272,269) +edge(272,270) +edge(294,270) +edge(276,275) +edge(277,276) +edge(281,280) +edge(361,280) +edge(288,287) +edge(291,290) +edge(296,295) +edge(301,299) +edge(301,300) +edge(329,303) +edge(349,303) +edge(351,303) +edge(379,303) +edge(357,304) +edge(359,304) +edge(310,309) +edge(316,315) +edge(317,316) +edge(320,318) +edge(320,319) +edge(321,320) +edge(329,328) +edge(333,331) +edge(335,331) +edge(334,332) +edge(334,333) +edge(335,334) +edge(367,334) +edge(339,338) +edge(343,342) +edge(349,348) +edge(351,348) +edge(351,349) +edge(353,352) +edge(355,352) +edge(354,353) +edge(356,353) +edge(356,354) +edge(358,357) +edge(359,358) +edge(367,366) +edge(368,367) +edge(374,373) +edge(377,375) +edge(379,378) ],[path(S,D) > 0.8],ccsaq,-1,A),
	statistics(runtime, [Stop | _]),
	Runtime is Stop - Start,
	writeln('ccsaq'),
	writeln(Runtime),
	writeln(A).

run_slsqp(S,D):-
	statistics(runtime, [Start | _]), 
	prob_optimize(path(S,D),[edge(3,1) +edge(5,1) +edge(44,1) +edge(131,1) +edge(259,1) +edge(5,4) +edge(14,4) +edge(16,4) +edge(45,4) +edge(47,4) +edge(126,4) +edge(128,4) +edge(152,4) +edge(154,4) +edge(165,4) +edge(176,4) +edge(249,4) +edge(274,4) +edge(314,4) +edge(324,4) +edge(371,4) +edge(374,4) +edge(16,5) +edge(45,5) +edge(47,5) +edge(177,5) +edge(201,5) +edge(204,5) +edge(235,5) +edge(237,5) +edge(249,5) +edge(254,5) +edge(313,5) +edge(373,5) +edge(7,6) +edge(8,7) +edge(191,7) +edge(193,7) +edge(62,8) +edge(64,8) +edge(137,8) +edge(342,8) +edge(344,8) +edge(11,9) +edge(11,10) +edge(67,10) +edge(69,10) +edge(14,13) +edge(16,13) +edge(18,13) +edge(20,13) +edge(15,14) +edge(16,15) +edge(46,15) +edge(176,15) +edge(278,15) +edge(334,15) +edge(367,15) +edge(45,16) +edge(47,16) +edge(154,16) +edge(177,16) +edge(250,16) +edge(314,16) +edge(324,16) +edge(373,16) +edge(29,17) +edge(172,17) +edge(258,17) +edge(365,17) +edge(172,18) +edge(258,18) +edge(365,18) +edge(208,19) +edge(23,21) +edge(33,21) +edge(220,21) +edge(232,21) +edge(268,21) +edge(288,21) +edge(24,22) +edge(50,23) +edge(52,23) +edge(55,23) +edge(227,23) +edge(79,24) +edge(220,24) +edge(232,24) +edge(268,24) +edge(27,25) +edge(27,26) +edge(40,26) +edge(104,26) +edge(106,26) +edge(108,26) +edge(125,26) +edge(197,26) +edge(231,26) +edge(251,26) +edge(295,26) +edge(297,26) +edge(315,26) +edge(317,26) +edge(31,30) +edge(33,30) +edge(35,30) +edge(51,30) +edge(219,30) +edge(33,31) +edge(33,32) +edge(35,32) +edge(51,32) +edge(216,32) +edge(218,32) +edge(307,32) +edge(370,32) +edge(35,33) +edge(109,33) +edge(220,33) +edge(36,35) +edge(38,37) +edge(305,38) +edge(173,40) +edge(175,40) +edge(240,40) +edge(326,40) +edge(43,41) +edge(242,41) +edge(244,41) +edge(52,42) +edge(241,42) +edge(243,42) +edge(275,42) +edge(277,42) +edge(275,43) +edge(277,43) +edge(46,45) +edge(176,45) +edge(323,45) +edge(47,46) +edge(177,46) +edge(177,47) +edge(67,49) +edge(181,49) +edge(183,49) +edge(51,50) +edge(53,50) +edge(76,51) +edge(100,51) +edge(169,51) +edge(307,51) +edge(322,51) +edge(76,52) +edge(116,52) +edge(169,52) +edge(266,52) +edge(291,52) +edge(170,53) +edge(57,56) +edge(110,56) +edge(195,56) +edge(60,58) +edge(304,60) +edge(358,60) +edge(164,61) +edge(166,61) +edge(64,62) +edge(64,63) +edge(265,65) +edge(300,65) +edge(302,65) +edge(346,65) +edge(101,66) +edge(110,66) +edge(68,67) +edge(70,67) +edge(72,67) +edge(74,67) +edge(80,67) +edge(121,67) +edge(123,67) +edge(248,67) +edge(363,67) +edge(80,68) +edge(70,69) +edge(75,69) +edge(81,69) +edge(71,70) +edge(73,70) +edge(119,70) +edge(214,70) +edge(328,70) +edge(348,70) +edge(350,70) +edge(378,70) +edge(72,71) +edge(75,71) +edge(119,72) +edge(150,73) +edge(169,76) +edge(78,77) +edge(79,78) +edge(121,81) +edge(123,81) +edge(84,82) +edge(86,82) +edge(88,82) +edge(84,83) +edge(86,83) +edge(88,83) +edge(262,83) +edge(85,84) +edge(87,84) +edge(89,84) +edge(87,85) +edge(89,85) +edge(260,85) +edge(263,85) +edge(88,86) +edge(106,86) +edge(262,86) +edge(88,87) +edge(89,88) +edge(260,88) +edge(263,88) +edge(92,90) +edge(130,91) +edge(132,91) +edge(134,91) +edge(130,92) +edge(132,92) +edge(134,92) +edge(94,93) +edge(96,93) +edge(98,93) +edge(95,94) +edge(97,94) +edge(99,94) +edge(97,95) +edge(99,95) +edge(179,95) +edge(286,95) +edge(337,95) +edge(339,95) +edge(97,96) +edge(99,96) +edge(98,97) +edge(178,97) +edge(99,98) +edge(102,100) +edge(110,100) +edge(145,100) +edge(195,100) +edge(103,101) +edge(159,102) +edge(360,102) +edge(105,104) +edge(107,104) +edge(106,105) +edge(107,106) +edge(260,106) +edge(283,107) +edge(372,107) +edge(155,108) +edge(158,108) +edge(194,110) +edge(113,112) +edge(115,112) +edge(115,113) +edge(135,113) +edge(189,113) +edge(312,113) +edge(353,113) +edge(355,113) +edge(115,114) +edge(135,114) +edge(172,115) +edge(117,116) +edge(319,116) +edge(321,116) +edge(120,118) +edge(120,119) +edge(303,119) +edge(349,119) +edge(351,119) +edge(123,121) +edge(169,122) +edge(125,124) +edge(255,125) +edge(127,126) +edge(129,126) +edge(330,126) +edge(129,127) +edge(330,127) +edge(327,128) +edge(340,128) +edge(131,130) +edge(133,130) +edge(132,131) +edge(134,131) +edge(136,131) +edge(133,132) +edge(188,132) +edge(189,135) +edge(353,135) +edge(355,135) +edge(138,137) +edge(343,137) +edge(206,140) +edge(229,140) +edge(143,142) +edge(145,142) +edge(145,143) +edge(194,145) +edge(148,146) +edge(150,149) +edge(270,149) +edge(151,150) +edge(271,150) +edge(294,150) +edge(156,155) +edge(158,155) +edge(162,160) +edge(322,160) +edge(163,162) +edge(166,164) +edge(168,167) +edge(170,167) +edge(170,168) +edge(170,169) +edge(248,169) +edge(290,169) +edge(292,169) +edge(267,170) +edge(364,170) +edge(325,172) +edge(174,173) +edge(175,174) +edge(177,176) +edge(180,178) +edge(183,181) +edge(226,183) +edge(186,184) +edge(309,184) +edge(186,185) +edge(309,185) +edge(187,186) +edge(310,186) +edge(192,190) +edge(193,192) +edge(196,194) +edge(200,199) +edge(202,199) +edge(204,199) +edge(201,200) +edge(202,201) +edge(204,201) +edge(252,201) +edge(254,201) +edge(298,201) +edge(204,202) +edge(245,204) +edge(207,206) +edge(376,207) +edge(211,210) +edge(213,210) +edge(215,210) +edge(213,211) +edge(215,211) +edge(214,212) +edge(222,212) +edge(224,212) +edge(214,213) +edge(215,214) +edge(348,214) +edge(350,214) +edge(217,216) +edge(218,217) +edge(223,222) +edge(225,222) +edge(225,223) +edge(228,227) +edge(233,231) +edge(235,231) +edge(237,231) +edge(239,231) +edge(246,231) +edge(297,231) +edge(268,232) +edge(237,236) +edge(239,236) +edge(245,236) +edge(247,236) +edge(238,237) +edge(246,239) +edge(326,239) +edge(257,240) +edge(242,241) +edge(244,241) +edge(244,242) +edge(246,245) +edge(298,245) +edge(257,246) +edge(314,250) +edge(256,255) +edge(265,264) +edge(300,265) +edge(302,265) +edge(270,269) +edge(272,269) +edge(272,270) +edge(294,270) +edge(276,275) +edge(277,276) +edge(281,280) +edge(361,280) +edge(288,287) +edge(291,290) +edge(296,295) +edge(301,299) +edge(301,300) +edge(329,303) +edge(349,303) +edge(351,303) +edge(379,303) +edge(357,304) +edge(359,304) +edge(310,309) +edge(316,315) +edge(317,316) +edge(320,318) +edge(320,319) +edge(321,320) +edge(329,328) +edge(333,331) +edge(335,331) +edge(334,332) +edge(334,333) +edge(335,334) +edge(367,334) +edge(339,338) +edge(343,342) +edge(349,348) +edge(351,348) +edge(351,349) +edge(353,352) +edge(355,352) +edge(354,353) +edge(356,353) +edge(356,354) +edge(358,357) +edge(359,358) +edge(367,366) +edge(368,367) +edge(374,373) +edge(377,375) +edge(379,378) ],[path(S,D) > 0.8],slsqp,-1,A),
	statistics(runtime, [Stop | _]),
	Runtime is Stop - Start,
	writeln('slsqp'),
	writeln(Runtime),
	writeln(A).
