:- use_module(library(pita)).
:- pita.
:- begin_lpad.

0.9::edge(494, 494).
optimizable edge(1, 1).
0.9::edge(16, 1).
optimizable edge(46, 1).
0.9::edge(267, 1).
optimizable edge(2, 2).
0.9::edge(4, 2).
optimizable edge(3, 3).
0.9::edge(52, 3).
optimizable edge(186, 3).
0.9::edge(4, 4).
optimizable edge(8, 4).
0.9::edge(120, 4).
optimizable edge(158, 4).
0.9::edge(429, 4).
optimizable edge(432, 4).
0.9::edge(5, 5).
optimizable edge(115, 5).
0.9::edge(6, 6).
optimizable edge(150, 6).
0.9::edge(433, 6).
optimizable edge(7, 7).
0.9::edge(18, 7).
optimizable edge(165, 7).
0.9::edge(367, 7).
optimizable edge(426, 7).
0.9::edge(8, 8).
optimizable edge(9, 9).
0.9::edge(422, 9).
optimizable edge(10, 10).
0.9::edge(205, 10).
optimizable edge(277, 10).
0.9::edge(11, 11).
optimizable edge(412, 11).
0.9::edge(436, 11).
optimizable edge(12, 12).
0.9::edge(13, 12).
optimizable edge(14, 12).
0.9::edge(16, 12).
optimizable edge(13, 13).
0.9::edge(14, 14).
optimizable edge(15, 15).
0.9::edge(16, 15).
optimizable edge(16, 16).
0.9::edge(17, 16).
optimizable edge(19, 16).
0.9::edge(57, 16).
optimizable edge(17, 17).
0.9::edge(20, 17).
optimizable edge(21, 17).
0.9::edge(281, 17).
optimizable edge(334, 17).
0.9::edge(18, 18).
optimizable edge(19, 19).
0.9::edge(20, 20).
optimizable edge(21, 21).
0.9::edge(22, 22).
optimizable edge(23, 22).
0.9::edge(140, 22).
optimizable edge(23, 23).
0.9::edge(140, 23).
optimizable edge(155, 23).
0.9::edge(197, 23).
optimizable edge(24, 24).
0.9::edge(25, 24).
optimizable edge(25, 25).
0.9::edge(157, 25).
optimizable edge(170, 25).
0.9::edge(26, 26).
optimizable edge(99, 26).
0.9::edge(425, 26).
optimizable edge(27, 27).
0.9::edge(28, 27).
optimizable edge(28, 28).
0.9::edge(85, 28).
optimizable edge(173, 28).
0.9::edge(29, 29).
optimizable edge(307, 29).
0.9::edge(316, 29).
optimizable edge(30, 30).
0.9::edge(31, 30).
optimizable edge(31, 31).
0.9::edge(182, 31).
optimizable edge(186, 31).
0.9::edge(32, 32).
optimizable edge(376, 32).
0.9::edge(444, 32).
optimizable edge(33, 33).
0.9::edge(34, 33).
optimizable edge(239, 33).
0.9::edge(34, 34).
optimizable edge(35, 35).
0.9::edge(222, 35).
optimizable edge(36, 36).
0.9::edge(325, 36).
optimizable edge(419, 36).
0.9::edge(37, 37).
optimizable edge(147, 37).
0.9::edge(262, 37).
optimizable edge(38, 38).
0.9::edge(39, 38).
optimizable edge(398, 38).
0.9::edge(39, 39).
optimizable edge(40, 39).
0.9::edge(281, 39).
optimizable edge(40, 40).
0.9::edge(417, 40).
optimizable edge(41, 41).
0.9::edge(47, 41).
optimizable edge(328, 41).
0.9::edge(413, 41).
optimizable edge(42, 42).
0.9::edge(172, 42).
optimizable edge(328, 42).
0.9::edge(43, 43).
optimizable edge(175, 43).
0.9::edge(44, 44).
optimizable edge(102, 44).
0.9::edge(286, 44).
optimizable edge(45, 45).
0.9::edge(174, 45).
optimizable edge(203, 45).
0.9::edge(264, 45).
optimizable edge(285, 45).
0.9::edge(46, 46).
optimizable edge(253, 46).
0.9::edge(47, 47).
optimizable edge(153, 47).
0.9::edge(285, 47).
optimizable edge(48, 48).
0.9::edge(205, 48).
optimizable edge(271, 48).
0.9::edge(283, 48).
optimizable edge(306, 48).
0.9::edge(49, 49).
optimizable edge(50, 49).
0.9::edge(114, 49).
optimizable edge(455, 49).
0.9::edge(50, 50).
optimizable edge(238, 50).
0.9::edge(331, 50).
optimizable edge(442, 50).
0.9::edge(51, 51).
optimizable edge(82, 51).
0.9::edge(409, 51).
optimizable edge(52, 52).
0.9::edge(397, 52).
optimizable edge(53, 53).
0.9::edge(271, 53).
optimizable edge(310, 53).
0.9::edge(54, 54).
optimizable edge(162, 54).
0.9::edge(406, 54).
optimizable edge(55, 55).
0.9::edge(269, 55).
optimizable edge(363, 55).
0.9::edge(56, 56).
optimizable edge(124, 56).
0.9::edge(224, 56).
optimizable edge(57, 57).
0.9::edge(84, 57).
optimizable edge(58, 58).
0.9::edge(59, 58).
optimizable edge(60, 58).
0.9::edge(59, 59).
optimizable edge(347, 59).
0.9::edge(377, 59).
optimizable edge(60, 60).
0.9::edge(61, 61).
optimizable edge(332, 61).
0.9::edge(418, 61).
optimizable edge(62, 62).
0.9::edge(438, 62).
optimizable edge(63, 63).
0.9::edge(64, 63).
optimizable edge(89, 63).
0.9::edge(190, 63).
optimizable edge(394, 63).
0.9::edge(64, 64).
optimizable edge(65, 65).
0.9::edge(66, 65).
optimizable edge(428, 65).
0.9::edge(436, 65).
optimizable edge(66, 66).
0.9::edge(67, 67).
optimizable edge(69, 67).
0.9::edge(70, 67).
optimizable edge(71, 67).
0.9::edge(68, 68).
optimizable edge(70, 68).
0.9::edge(72, 68).
optimizable edge(73, 68).
0.9::edge(214, 68).
optimizable edge(69, 69).
0.9::edge(70, 70).
optimizable edge(71, 70).
0.9::edge(72, 70).
optimizable edge(142, 70).
0.9::edge(311, 70).
optimizable edge(320, 70).
0.9::edge(71, 71).
optimizable edge(74, 71).
0.9::edge(75, 71).
optimizable edge(76, 71).
0.9::edge(91, 71).
optimizable edge(353, 71).
0.9::edge(72, 72).
optimizable edge(212, 72).
0.9::edge(213, 72).
optimizable edge(440, 72).
0.9::edge(464, 72).
optimizable edge(73, 73).
0.9::edge(74, 74).
optimizable edge(75, 75).
0.9::edge(76, 76).
optimizable edge(77, 77).
0.9::edge(78, 77).
optimizable edge(187, 77).
0.9::edge(264, 77).
optimizable edge(78, 78).
0.9::edge(123, 78).
optimizable edge(265, 78).
0.9::edge(79, 79).
optimizable edge(80, 79).
0.9::edge(214, 79).
optimizable edge(421, 79).
0.9::edge(80, 80).
optimizable edge(147, 80).
0.9::edge(252, 80).
optimizable edge(355, 80).
0.9::edge(393, 80).
optimizable edge(437, 80).
0.9::edge(81, 81).
optimizable edge(396, 81).
0.9::edge(457, 81).
optimizable edge(82, 82).
0.9::edge(287, 82).
optimizable edge(83, 83).
0.9::edge(387, 83).
optimizable edge(423, 83).
0.9::edge(84, 84).
optimizable edge(173, 84).
0.9::edge(85, 85).
optimizable edge(88, 85).
0.9::edge(116, 85).
optimizable edge(275, 85).
0.9::edge(454, 85).
optimizable edge(86, 86).
0.9::edge(88, 86).
optimizable edge(87, 87).
0.9::edge(88, 87).
optimizable edge(236, 87).
0.9::edge(88, 88).
optimizable edge(89, 89).
0.9::edge(326, 89).
optimizable edge(90, 90).
0.9::edge(91, 90).
optimizable edge(386, 90).
0.9::edge(91, 91).
optimizable edge(93, 91).
0.9::edge(94, 91).
optimizable edge(95, 91).
0.9::edge(371, 91).
optimizable edge(92, 92).
0.9::edge(95, 92).
optimizable edge(387, 92).
0.9::edge(93, 93).
optimizable edge(94, 94).
0.9::edge(95, 95).
optimizable edge(96, 95).
0.9::edge(96, 96).
optimizable edge(97, 97).
0.9::edge(326, 97).
optimizable edge(422, 97).
0.9::edge(98, 98).
optimizable edge(150, 98).
0.9::edge(156, 98).
optimizable edge(99, 99).
0.9::edge(224, 99).
optimizable edge(100, 100).
0.9::edge(158, 100).
optimizable edge(220, 100).
0.9::edge(391, 100).
optimizable edge(101, 101).
0.9::edge(102, 101).
optimizable edge(102, 102).
0.9::edge(178, 102).
optimizable edge(233, 102).
0.9::edge(286, 102).
optimizable edge(103, 103).
0.9::edge(306, 103).
optimizable edge(429, 103).
0.9::edge(104, 104).
optimizable edge(105, 104).
0.9::edge(191, 104).
optimizable edge(257, 104).
0.9::edge(263, 104).
optimizable edge(105, 105).
0.9::edge(106, 106).
optimizable edge(255, 106).
0.9::edge(107, 107).
optimizable edge(246, 107).
0.9::edge(334, 107).
optimizable edge(108, 108).
0.9::edge(191, 108).
optimizable edge(380, 108).
0.9::edge(109, 109).
optimizable edge(112, 109).
0.9::edge(110, 110).
optimizable edge(112, 110).
0.9::edge(111, 111).
optimizable edge(112, 111).
0.9::edge(112, 112).
optimizable edge(123, 112).
0.9::edge(179, 112).
optimizable edge(298, 112).
0.9::edge(113, 113).
optimizable edge(263, 113).
0.9::edge(337, 113).
optimizable edge(378, 113).
0.9::edge(416, 113).
optimizable edge(114, 114).
0.9::edge(115, 114).
optimizable edge(285, 114).
0.9::edge(115, 115).
optimizable edge(354, 115).
0.9::edge(116, 116).
optimizable edge(117, 116).
0.9::edge(117, 117).
optimizable edge(119, 117).
0.9::edge(118, 118).
optimizable edge(124, 118).
0.9::edge(346, 118).
optimizable edge(119, 119).
0.9::edge(346, 119).
optimizable edge(120, 120).
0.9::edge(429, 120).
optimizable edge(121, 121).
0.9::edge(123, 121).
optimizable edge(122, 122).
0.9::edge(123, 122).
optimizable edge(187, 122).
0.9::edge(264, 122).
optimizable edge(297, 122).
0.9::edge(123, 123).
optimizable edge(179, 123).
0.9::edge(265, 123).
optimizable edge(124, 124).
0.9::edge(125, 125).
optimizable edge(126, 125).
0.9::edge(126, 126).
optimizable edge(385, 126).
0.9::edge(127, 127).
optimizable edge(129, 127).
0.9::edge(226, 127).
optimizable edge(128, 128).
0.9::edge(225, 128).
optimizable edge(438, 128).
0.9::edge(129, 129).
optimizable edge(130, 130).
0.9::edge(141, 130).
optimizable edge(131, 131).
0.9::edge(141, 131).
optimizable edge(132, 132).
0.9::edge(141, 132).
optimizable edge(133, 133).
0.9::edge(142, 133).
optimizable edge(134, 134).
0.9::edge(143, 134).
optimizable edge(135, 135).
0.9::edge(140, 135).
optimizable edge(136, 136).
0.9::edge(141, 136).
optimizable edge(142, 136).
0.9::edge(144, 136).
optimizable edge(137, 137).
0.9::edge(141, 137).
optimizable edge(142, 137).
0.9::edge(145, 137).
optimizable edge(138, 138).
0.9::edge(199, 138).
optimizable edge(340, 138).
0.9::edge(139, 139).
optimizable edge(147, 139).
0.9::edge(140, 140).
optimizable edge(141, 141).
0.9::edge(142, 142).
optimizable edge(143, 142).
0.9::edge(473, 142).
optimizable edge(478, 142).
0.9::edge(487, 142).
optimizable edge(143, 143).
0.9::edge(242, 143).
optimizable edge(144, 144).
0.9::edge(145, 145).
optimizable edge(146, 146).
0.9::edge(183, 146).
optimizable edge(245, 146).
0.9::edge(339, 146).
optimizable edge(147, 147).
0.9::edge(148, 148).
optimizable edge(434, 148).
0.9::edge(443, 148).
optimizable edge(149, 149).
0.9::edge(150, 149).
optimizable edge(217, 149).
0.9::edge(150, 150).
optimizable edge(397, 150).
0.9::edge(151, 151).
optimizable edge(153, 151).
0.9::edge(154, 151).
optimizable edge(155, 151).
0.9::edge(152, 152).
optimizable edge(153, 152).
0.9::edge(154, 152).
optimizable edge(155, 152).
0.9::edge(153, 153).
optimizable edge(203, 153).
0.9::edge(154, 154).
optimizable edge(155, 155).
0.9::edge(241, 155).
optimizable edge(156, 156).
0.9::edge(157, 156).
optimizable edge(157, 157).
0.9::edge(158, 158).
optimizable edge(159, 159).
0.9::edge(160, 159).
optimizable edge(183, 159).
0.9::edge(248, 159).
optimizable edge(388, 159).
0.9::edge(392, 159).
optimizable edge(160, 160).
0.9::edge(369, 160).
optimizable edge(399, 160).
0.9::edge(161, 161).
optimizable edge(410, 161).
0.9::edge(162, 162).
optimizable edge(355, 162).
0.9::edge(163, 163).
optimizable edge(167, 163).
0.9::edge(246, 163).
optimizable edge(164, 164).
0.9::edge(167, 164).
optimizable edge(231, 164).
0.9::edge(417, 164).
optimizable edge(470, 164).
0.9::edge(165, 165).
optimizable edge(166, 166).
0.9::edge(341, 166).
optimizable edge(423, 166).
0.9::edge(167, 167).
optimizable edge(168, 168).
0.9::edge(169, 168).
optimizable edge(169, 169).
0.9::edge(344, 169).
optimizable edge(400, 169).
0.9::edge(170, 170).
optimizable edge(325, 170).
0.9::edge(171, 171).
optimizable edge(172, 171).
0.9::edge(172, 172).
optimizable edge(173, 173).
0.9::edge(202, 173).
optimizable edge(174, 174).
0.9::edge(175, 174).
optimizable edge(285, 174).
0.9::edge(175, 175).
optimizable edge(176, 176).
0.9::edge(464, 176).
optimizable edge(177, 177).
0.9::edge(226, 177).
optimizable edge(178, 178).
0.9::edge(340, 178).
optimizable edge(179, 179).
0.9::edge(180, 180).
optimizable edge(181, 180).
0.9::edge(181, 181).
optimizable edge(233, 181).
0.9::edge(182, 182).
optimizable edge(183, 183).
0.9::edge(184, 183).
optimizable edge(185, 183).
0.9::edge(248, 183).
optimizable edge(184, 184).
0.9::edge(185, 185).
optimizable edge(186, 186).
0.9::edge(187, 187).
optimizable edge(188, 188).
0.9::edge(198, 188).
optimizable edge(212, 188).
0.9::edge(189, 189).
optimizable edge(190, 189).
0.9::edge(190, 190).
optimizable edge(401, 190).
0.9::edge(191, 191).
optimizable edge(192, 191).
0.9::edge(195, 191).
optimizable edge(196, 191).
0.9::edge(374, 191).
optimizable edge(380, 191).
0.9::edge(416, 191).
optimizable edge(192, 192).
0.9::edge(193, 192).
optimizable edge(194, 192).
0.9::edge(193, 193).
optimizable edge(194, 194).
0.9::edge(195, 195).
optimizable edge(196, 196).
0.9::edge(197, 197).
optimizable edge(465, 197).
0.9::edge(198, 198).
optimizable edge(201, 198).
0.9::edge(199, 199).
optimizable edge(364, 199).
0.9::edge(200, 200).
optimizable edge(280, 200).
0.9::edge(201, 201).
optimizable edge(202, 202).
0.9::edge(247, 202).
optimizable edge(203, 203).
0.9::edge(297, 203).
optimizable edge(204, 204).
0.9::edge(205, 204).
optimizable edge(206, 204).
0.9::edge(207, 204).
optimizable edge(205, 205).
0.9::edge(208, 205).
optimizable edge(283, 205).
0.9::edge(357, 205).
optimizable edge(369, 205).
0.9::edge(206, 206).
optimizable edge(207, 207).
0.9::edge(278, 207).
optimizable edge(208, 208).
0.9::edge(209, 208).
optimizable edge(209, 209).
0.9::edge(210, 210).
optimizable edge(211, 210).
0.9::edge(211, 211).
optimizable edge(457, 211).
0.9::edge(465, 211).
optimizable edge(212, 212).
0.9::edge(213, 213).
optimizable edge(444, 213).
0.9::edge(214, 214).
optimizable edge(215, 215).
0.9::edge(390, 215).
optimizable edge(440, 215).
0.9::edge(216, 216).
optimizable edge(217, 216).
0.9::edge(218, 216).
optimizable edge(219, 216).
0.9::edge(217, 217).
optimizable edge(277, 217).
0.9::edge(286, 217).
optimizable edge(429, 217).
0.9::edge(218, 218).
optimizable edge(219, 219).
0.9::edge(235, 219).
optimizable edge(220, 220).
0.9::edge(271, 220).
optimizable edge(449, 220).
0.9::edge(221, 221).
optimizable edge(367, 221).
0.9::edge(464, 221).
optimizable edge(222, 222).
0.9::edge(240, 222).
optimizable edge(436, 222).
0.9::edge(223, 223).
optimizable edge(345, 223).
0.9::edge(398, 223).
optimizable edge(407, 223).
0.9::edge(224, 224).
optimizable edge(377, 224).
0.9::edge(454, 224).
optimizable edge(225, 225).
0.9::edge(226, 225).
optimizable edge(282, 225).
0.9::edge(377, 225).
optimizable edge(226, 226).
0.9::edge(227, 227).
optimizable edge(228, 227).
0.9::edge(228, 228).
optimizable edge(420, 228).
0.9::edge(229, 229).
optimizable edge(230, 229).
0.9::edge(230, 230).
optimizable edge(315, 230).
0.9::edge(436, 230).
optimizable edge(231, 231).
0.9::edge(386, 231).
optimizable edge(473, 231).
0.9::edge(232, 232).
optimizable edge(233, 232).
0.9::edge(234, 232).
optimizable edge(235, 232).
0.9::edge(233, 233).
optimizable edge(234, 234).
0.9::edge(235, 235).
optimizable edge(236, 236).
0.9::edge(301, 236).
optimizable edge(399, 236).
0.9::edge(237, 237).
optimizable edge(300, 237).
0.9::edge(308, 237).
optimizable edge(238, 238).
0.9::edge(294, 238).
optimizable edge(239, 239).
0.9::edge(322, 239).
optimizable edge(240, 240).
0.9::edge(422, 240).
optimizable edge(241, 241).
0.9::edge(329, 241).
optimizable edge(242, 242).
0.9::edge(258, 242).
optimizable edge(431, 242).
0.9::edge(480, 242).
optimizable edge(243, 243).
0.9::edge(244, 243).
optimizable edge(309, 243).
0.9::edge(244, 244).
optimizable edge(245, 245).
0.9::edge(300, 245).
optimizable edge(246, 246).
0.9::edge(247, 247).
optimizable edge(333, 247).
0.9::edge(248, 248).
optimizable edge(249, 248).
0.9::edge(249, 249).
optimizable edge(250, 249).
0.9::edge(251, 249).
optimizable edge(250, 250).
0.9::edge(251, 251).
optimizable edge(252, 252).
0.9::edge(363, 252).
optimizable edge(253, 253).
0.9::edge(256, 253).
optimizable edge(254, 254).
0.9::edge(256, 254).
optimizable edge(255, 255).
0.9::edge(256, 255).
optimizable edge(385, 255).
0.9::edge(256, 256).
optimizable edge(257, 257).
0.9::edge(266, 257).
optimizable edge(336, 257).
0.9::edge(402, 257).
optimizable edge(427, 257).
0.9::edge(258, 258).
optimizable edge(259, 258).
0.9::edge(260, 258).
optimizable edge(261, 258).
0.9::edge(431, 258).
optimizable edge(466, 258).
0.9::edge(259, 259).
optimizable edge(260, 260).
0.9::edge(261, 261).
optimizable edge(262, 262).
0.9::edge(443, 262).
optimizable edge(263, 263).
0.9::edge(336, 263).
optimizable edge(378, 263).
0.9::edge(264, 264).
optimizable edge(265, 264).
0.9::edge(265, 265).
optimizable edge(266, 266).
0.9::edge(267, 267).
optimizable edge(407, 267).
0.9::edge(268, 268).
optimizable edge(269, 268).
0.9::edge(269, 269).
optimizable edge(422, 269).
0.9::edge(270, 270).
optimizable edge(372, 270).
0.9::edge(404, 270).
optimizable edge(271, 271).
0.9::edge(272, 271).
optimizable edge(310, 271).
0.9::edge(377, 271).
optimizable edge(272, 272).
0.9::edge(273, 272).
optimizable edge(274, 272).
0.9::edge(273, 273).
optimizable edge(274, 274).
0.9::edge(275, 275).
optimizable edge(345, 275).
0.9::edge(375, 275).
optimizable edge(407, 275).
0.9::edge(276, 276).
optimizable edge(315, 276).
0.9::edge(394, 276).
optimizable edge(277, 277).
0.9::edge(432, 277).
optimizable edge(278, 278).
0.9::edge(279, 278).
optimizable edge(431, 278).
0.9::edge(479, 278).
optimizable edge(279, 279).
0.9::edge(280, 280).
optimizable edge(281, 280).
0.9::edge(281, 281).
optimizable edge(282, 282).
0.9::edge(283, 283).
optimizable edge(284, 284).
0.9::edge(285, 284).
optimizable edge(287, 284).
0.9::edge(288, 284).
optimizable edge(285, 285).
0.9::edge(289, 285).
optimizable edge(290, 285).
0.9::edge(286, 286).
optimizable edge(289, 286).
0.9::edge(290, 286).
optimizable edge(287, 287).
0.9::edge(288, 288).
optimizable edge(289, 289).
0.9::edge(291, 289).
optimizable edge(290, 290).
0.9::edge(292, 290).
optimizable edge(291, 291).
0.9::edge(292, 292).
optimizable edge(293, 293).
0.9::edge(294, 293).
optimizable edge(294, 294).
0.9::edge(419, 294).
optimizable edge(295, 295).
0.9::edge(447, 295).
optimizable edge(296, 296).
0.9::edge(297, 296).
optimizable edge(298, 296).
0.9::edge(299, 296).
optimizable edge(297, 297).
0.9::edge(456, 297).
optimizable edge(298, 298).
0.9::edge(299, 299).
optimizable edge(300, 300).
0.9::edge(372, 300).
optimizable edge(301, 301).
0.9::edge(302, 301).
optimizable edge(304, 301).
0.9::edge(306, 301).
optimizable edge(310, 301).
0.9::edge(323, 301).
optimizable edge(429, 301).
0.9::edge(302, 302).
optimizable edge(303, 302).
0.9::edge(493, 302).
optimizable edge(303, 303).
0.9::edge(304, 304).
optimizable edge(305, 304).
0.9::edge(494, 304).
optimizable edge(305, 305).
0.9::edge(306, 306).
optimizable edge(310, 306).
0.9::edge(307, 307).
optimizable edge(434, 307).
0.9::edge(308, 308).
optimizable edge(373, 308).
0.9::edge(309, 309).
optimizable edge(409, 309).
0.9::edge(310, 310).
optimizable edge(317, 310).
0.9::edge(318, 310).
optimizable edge(319, 310).
0.9::edge(391, 310).
optimizable edge(311, 311).
0.9::edge(317, 311).
optimizable edge(318, 311).
0.9::edge(319, 311).
optimizable edge(320, 311).
0.9::edge(312, 312).
optimizable edge(317, 312).
0.9::edge(313, 313).
optimizable edge(318, 313).
0.9::edge(314, 314).
optimizable edge(319, 314).
0.9::edge(315, 315).
optimizable edge(316, 316).
0.9::edge(428, 316).
optimizable edge(317, 317).
0.9::edge(318, 318).
optimizable edge(319, 319).
0.9::edge(320, 320).
optimizable edge(321, 320).
0.9::edge(321, 321).
optimizable edge(401, 321).
0.9::edge(322, 322).
optimizable edge(323, 322).
0.9::edge(323, 323).
optimizable edge(340, 323).
0.9::edge(435, 323).
optimizable edge(324, 324).
0.9::edge(325, 324).
optimizable edge(325, 325).
0.9::edge(326, 326).
optimizable edge(327, 327).
0.9::edge(328, 327).
optimizable edge(328, 328).
0.9::edge(329, 329).
optimizable edge(396, 329).
0.9::edge(330, 330).
optimizable edge(331, 330).
0.9::edge(331, 331).
optimizable edge(332, 332).
0.9::edge(335, 332).
optimizable edge(333, 333).
0.9::edge(392, 333).
optimizable edge(334, 334).
0.9::edge(335, 335).
optimizable edge(434, 335).
0.9::edge(336, 336).
optimizable edge(337, 337).
0.9::edge(343, 337).
optimizable edge(338, 338).
0.9::edge(394, 338).
optimizable edge(339, 339).
0.9::edge(342, 339).
optimizable edge(340, 340).
0.9::edge(341, 341).
optimizable edge(423, 341).
0.9::edge(342, 342).
optimizable edge(343, 343).
0.9::edge(416, 343).
optimizable edge(344, 344).
0.9::edge(345, 345).
optimizable edge(348, 345).
0.9::edge(350, 345).
optimizable edge(352, 345).
0.9::edge(353, 345).
optimizable edge(346, 346).
0.9::edge(347, 346).
optimizable edge(349, 346).
0.9::edge(351, 346).
optimizable edge(353, 346).
0.9::edge(375, 346).
optimizable edge(347, 347).
0.9::edge(348, 348).
optimizable edge(349, 349).
0.9::edge(350, 350).
optimizable edge(351, 351).
0.9::edge(352, 352).
optimizable edge(395, 352).
0.9::edge(353, 353).
optimizable edge(403, 353).
0.9::edge(354, 354).
optimizable edge(420, 354).
0.9::edge(355, 355).
optimizable edge(356, 356).
0.9::edge(385, 356).
optimizable edge(357, 357).
0.9::edge(358, 357).
optimizable edge(359, 357).
0.9::edge(361, 357).
optimizable edge(399, 357).
0.9::edge(358, 358).
optimizable edge(360, 358).
0.9::edge(362, 358).
optimizable edge(359, 359).
0.9::edge(360, 360).
optimizable edge(361, 361).
0.9::edge(362, 362).
optimizable edge(363, 363).
0.9::edge(364, 364).
optimizable edge(365, 365).
0.9::edge(438, 365).
optimizable edge(366, 366).
0.9::edge(367, 366).
optimizable edge(367, 367).
0.9::edge(376, 367).
optimizable edge(426, 367).
0.9::edge(368, 368).
optimizable edge(369, 368).
0.9::edge(370, 368).
optimizable edge(371, 368).
0.9::edge(369, 369).
optimizable edge(372, 369).
0.9::edge(373, 369).
optimizable edge(370, 370).
0.9::edge(371, 371).
optimizable edge(372, 372).
0.9::edge(373, 373).
optimizable edge(392, 373).
0.9::edge(374, 374).
optimizable edge(408, 374).
0.9::edge(375, 375).
optimizable edge(376, 376).
0.9::edge(377, 377).
optimizable edge(395, 377).
0.9::edge(378, 378).
optimizable edge(379, 379).
0.9::edge(380, 379).
optimizable edge(381, 379).
0.9::edge(383, 379).
optimizable edge(380, 380).
0.9::edge(384, 380).
optimizable edge(381, 381).
0.9::edge(417, 381).
optimizable edge(430, 381).
0.9::edge(382, 382).
optimizable edge(384, 382).
0.9::edge(383, 383).
optimizable edge(384, 384).
0.9::edge(385, 385).
optimizable edge(386, 386).
0.9::edge(387, 387).
optimizable edge(388, 388).
0.9::edge(389, 388).
optimizable edge(389, 389).
0.9::edge(390, 390).
optimizable edge(439, 390).
0.9::edge(391, 391).
optimizable edge(392, 392).
0.9::edge(404, 392).
optimizable edge(393, 393).
0.9::edge(439, 393).
optimizable edge(394, 394).
0.9::edge(395, 395).
optimizable edge(396, 396).
0.9::edge(457, 396).
optimizable edge(465, 396).
0.9::edge(397, 397).
optimizable edge(398, 398).
0.9::edge(399, 399).
optimizable edge(400, 400).
0.9::edge(442, 400).
optimizable edge(401, 401).
0.9::edge(402, 402).
optimizable edge(403, 402).
0.9::edge(403, 403).
optimizable edge(404, 404).
0.9::edge(405, 405).
optimizable edge(406, 405).
0.9::edge(406, 406).
optimizable edge(407, 407).
0.9::edge(408, 408).
optimizable edge(427, 408).
0.9::edge(409, 409).
optimizable edge(410, 410).
0.9::edge(418, 410).
optimizable edge(411, 411).
0.9::edge(412, 411).
optimizable edge(412, 412).
0.9::edge(437, 412).
optimizable edge(413, 413).
0.9::edge(414, 414).
optimizable edge(415, 414).
0.9::edge(416, 414).
optimizable edge(417, 414).
0.9::edge(415, 415).
optimizable edge(416, 416).
0.9::edge(417, 417).
optimizable edge(418, 418).
0.9::edge(419, 419).
optimizable edge(420, 420).
0.9::edge(424, 420).
optimizable edge(421, 421).
0.9::edge(422, 421).
optimizable edge(435, 421).
0.9::edge(422, 422).
optimizable edge(423, 423).
0.9::edge(424, 424).
optimizable edge(425, 425).
0.9::edge(426, 426).
optimizable edge(427, 427).
0.9::edge(428, 428).
optimizable edge(429, 429).
0.9::edge(431, 429).
optimizable edge(430, 430).
0.9::edge(431, 430).
optimizable edge(431, 431).
0.9::edge(432, 432).
optimizable edge(433, 433).
0.9::edge(434, 434).
optimizable edge(437, 434).
0.9::edge(435, 435).
optimizable edge(436, 435).
0.9::edge(436, 436).
optimizable edge(437, 437).
0.9::edge(438, 438).
optimizable edge(439, 439).
0.9::edge(440, 440).
optimizable edge(441, 441).
0.9::edge(442, 441).
optimizable edge(442, 442).
0.9::edge(443, 443).
optimizable edge(444, 444).
0.9::edge(445, 445).
optimizable edge(446, 445).
0.9::edge(447, 445).
optimizable edge(449, 445).
0.9::edge(446, 446).
optimizable edge(450, 446).
0.9::edge(455, 446).
optimizable edge(447, 447).
0.9::edge(455, 447).
optimizable edge(448, 448).
0.9::edge(450, 448).
optimizable edge(449, 449).
0.9::edge(451, 449).
optimizable edge(452, 449).
0.9::edge(453, 449).
optimizable edge(450, 450).
0.9::edge(451, 451).
optimizable edge(452, 452).
0.9::edge(453, 453).
optimizable edge(454, 454).
0.9::edge(455, 454).
optimizable edge(455, 455).
0.9::edge(456, 456).
optimizable edge(457, 456).
0.9::edge(458, 456).
optimizable edge(457, 457).
0.9::edge(459, 457).
optimizable edge(460, 457).
0.9::edge(461, 457).
optimizable edge(462, 457).
0.9::edge(463, 457).
optimizable edge(458, 458).
0.9::edge(459, 459).
optimizable edge(460, 460).
0.9::edge(461, 461).
optimizable edge(462, 462).
0.9::edge(463, 463).
optimizable edge(464, 464).
0.9::edge(465, 465).
optimizable edge(466, 466).
0.9::edge(467, 466).
optimizable edge(480, 466).
0.9::edge(467, 467).
optimizable edge(481, 467).
0.9::edge(468, 468).
optimizable edge(472, 468).
0.9::edge(478, 468).
optimizable edge(469, 469).
0.9::edge(473, 469).
optimizable edge(478, 469).
0.9::edge(470, 470).
optimizable edge(471, 470).
0.9::edge(471, 471).
optimizable edge(472, 471).
0.9::edge(472, 472).
optimizable edge(473, 473).
0.9::edge(474, 473).
optimizable edge(475, 473).
0.9::edge(476, 473).
optimizable edge(477, 473).
0.9::edge(474, 474).
optimizable edge(475, 475).
0.9::edge(476, 476).
optimizable edge(477, 477).
0.9::edge(478, 478).
optimizable edge(479, 479).
0.9::edge(480, 480).
optimizable edge(481, 480).
0.9::edge(483, 480).
optimizable edge(481, 481).
0.9::edge(482, 481).
optimizable edge(483, 481).
0.9::edge(486, 481).
optimizable edge(482, 482).
0.9::edge(486, 482).
optimizable edge(483, 483).
0.9::edge(484, 483).
optimizable edge(485, 483).
0.9::edge(484, 484).
optimizable edge(485, 485).
0.9::edge(486, 486).
optimizable edge(487, 487).
0.9::edge(488, 488).
optimizable edge(490, 488).
0.9::edge(493, 488).
optimizable edge(494, 488).
0.9::edge(489, 489).
optimizable edge(490, 489).
0.9::edge(491, 489).
optimizable edge(490, 490).
0.9::edge(492, 490).
optimizable edge(491, 491).
0.9::edge(492, 492).
optimizable edge(493, 493).
0.9::edge(494, 494).

path(X, X).
path(X, Y):- path(X, Z), edge(Z, Y).



:- end_lpad.

run:- run_mma, run_ccsaq, run_slsqp.

run_mma:-
	statistics(runtime, [Start | _]), 
	prob_optimize(path(95,68),[edge(1,1) +edge(46,1) +edge(2,2) +edge(3,3) +edge(186,3) +edge(8,4) +edge(158,4) +edge(432,4) +edge(115,5) +edge(150,6) +edge(7,7) +edge(165,7) +edge(426,7) +edge(9,9) +edge(10,10) +edge(277,10) +edge(412,11) +edge(12,12) +edge(14,12) +edge(13,13) +edge(15,15) +edge(16,16) +edge(19,16) +edge(17,17) +edge(21,17) +edge(334,17) +edge(19,19) +edge(21,21) +edge(23,22) +edge(23,23) +edge(155,23) +edge(24,24) +edge(25,25) +edge(170,25) +edge(99,26) +edge(27,27) +edge(28,28) +edge(173,28) +edge(307,29) +edge(30,30) +edge(31,31) +edge(186,31) +edge(376,32) +edge(33,33) +edge(239,33) +edge(35,35) +edge(36,36) +edge(419,36) +edge(147,37) +edge(38,38) +edge(398,38) +edge(40,39) +edge(40,40) +edge(41,41) +edge(328,41) +edge(42,42) +edge(328,42) +edge(175,43) +edge(102,44) +edge(45,45) +edge(203,45) +edge(285,45) +edge(253,46) +edge(153,47) +edge(48,48) +edge(271,48) +edge(306,48) +edge(50,49) +edge(455,49) +edge(238,50) +edge(442,50) +edge(82,51) +edge(52,52) +edge(53,53) +edge(310,53) +edge(162,54) +edge(55,55) +edge(363,55) +edge(124,56) +edge(57,57) +edge(58,58) +edge(60,58) +edge(347,59) +edge(60,60) +edge(332,61) +edge(62,62) +edge(63,63) +edge(89,63) +edge(394,63) +edge(65,65) +edge(428,65) +edge(66,66) +edge(69,67) +edge(71,67) +edge(70,68) +edge(73,68) +edge(69,69) +edge(71,70) +edge(142,70) +edge(320,70) +edge(74,71) +edge(76,71) +edge(353,71) +edge(212,72) +edge(440,72) +edge(73,73) +edge(75,75) +edge(77,77) +edge(187,77) +edge(78,78) +edge(265,78) +edge(80,79) +edge(421,79) +edge(147,80) +edge(355,80) +edge(437,80) +edge(396,81) +edge(82,82) +edge(83,83) +edge(423,83) +edge(173,84) +edge(88,85) +edge(275,85) +edge(86,86) +edge(87,87) +edge(236,87) +edge(89,89) +edge(90,90) +edge(386,90) +edge(93,91) +edge(95,91) +edge(92,92) +edge(387,92) +edge(94,94) +edge(96,95) +edge(97,97) +edge(422,97) +edge(150,98) +edge(99,99) +edge(100,100) +edge(220,100) +edge(101,101) +edge(102,102) +edge(233,102) +edge(103,103) +edge(429,103) +edge(105,104) +edge(257,104) +edge(105,105) +edge(255,106) +edge(246,107) +edge(108,108) +edge(380,108) +edge(112,109) +edge(112,110) +edge(112,111) +edge(123,112) +edge(298,112) +edge(263,113) +edge(378,113) +edge(114,114) +edge(285,114) +edge(354,115) +edge(117,116) +edge(119,117) +edge(124,118) +edge(119,119) +edge(120,120) +edge(121,121) +edge(122,122) +edge(187,122) +edge(297,122) +edge(179,123) +edge(124,124) +edge(126,125) +edge(385,126) +edge(129,127) +edge(128,128) +edge(438,128) +edge(130,130) +edge(131,131) +edge(132,132) +edge(133,133) +edge(134,134) +edge(135,135) +edge(136,136) +edge(142,136) +edge(137,137) +edge(142,137) +edge(138,138) +edge(340,138) +edge(147,139) +edge(141,141) +edge(143,142) +edge(478,142) +edge(143,143) +edge(144,144) +edge(146,146) +edge(245,146) +edge(147,147) +edge(434,148) +edge(149,149) +edge(217,149) +edge(397,150) +edge(153,151) +edge(155,151) +edge(153,152) +edge(155,152) +edge(203,153) +edge(155,155) +edge(156,156) +edge(157,157) +edge(159,159) +edge(183,159) +edge(388,159) +edge(160,160) +edge(399,160) +edge(410,161) +edge(355,162) +edge(167,163) +edge(164,164) +edge(231,164) +edge(470,164) +edge(166,166) +edge(423,166) +edge(168,168) +edge(169,169) +edge(400,169) +edge(325,170) +edge(172,171) +edge(173,173) +edge(174,174) +edge(285,174) +edge(176,176) +edge(177,177) +edge(178,178) +edge(179,179) +edge(181,180) +edge(233,181) +edge(183,183) +edge(185,183) +edge(184,184) +edge(186,186) +edge(188,188) +edge(212,188) +edge(190,189) +edge(401,190) +edge(192,191) +edge(196,191) +edge(380,191) +edge(192,192) +edge(194,192) +edge(194,194) +edge(196,196) +edge(465,197) +edge(201,198) +edge(364,199) +edge(280,200) +edge(202,202) +edge(203,203) +edge(204,204) +edge(206,204) +edge(205,205) +edge(283,205) +edge(369,205) +edge(207,207) +edge(208,208) +edge(209,209) +edge(211,210) +edge(457,211) +edge(212,212) +edge(444,213) +edge(215,215) +edge(440,215) +edge(217,216) +edge(219,216) +edge(277,217) +edge(429,217) +edge(219,219) +edge(220,220) +edge(449,220) +edge(367,221) +edge(222,222) +edge(436,222) +edge(345,223) +edge(407,223) +edge(377,224) +edge(225,225) +edge(282,225) +edge(226,226) +edge(228,227) +edge(420,228) +edge(230,229) +edge(315,230) +edge(231,231) +edge(473,231) +edge(233,232) +edge(235,232) +edge(234,234) +edge(236,236) +edge(399,236) +edge(300,237) +edge(238,238) +edge(239,239) +edge(240,240) +edge(241,241) +edge(242,242) +edge(431,242) +edge(243,243) +edge(309,243) +edge(245,245) +edge(246,246) +edge(333,247) +edge(249,248) +edge(250,249) +edge(250,250) +edge(252,252) +edge(253,253) +edge(254,254) +edge(255,255) +edge(385,255) +edge(257,257) +edge(336,257) +edge(427,257) +edge(259,258) +edge(261,258) +edge(466,258) +edge(260,260) +edge(262,262) +edge(263,263) +edge(378,263) +edge(265,264) +edge(266,266) +edge(407,267) +edge(269,268) +edge(422,269) +edge(372,270) +edge(271,271) +edge(310,271) +edge(272,272) +edge(274,272) +edge(274,274) +edge(345,275) +edge(407,275) +edge(315,276) +edge(277,277) +edge(278,278) +edge(431,278) +edge(279,279) +edge(281,280) +edge(282,282) +edge(284,284) +edge(287,284) +edge(285,285) +edge(290,285) +edge(289,286) +edge(287,287) +edge(289,289) +edge(290,290) +edge(291,291) +edge(293,293) +edge(294,294) +edge(295,295) +edge(296,296) +edge(298,296) +edge(297,297) +edge(298,298) +edge(300,300) +edge(301,301) +edge(304,301) +edge(310,301) +edge(429,301) +edge(303,302) +edge(303,303) +edge(305,304) +edge(305,305) +edge(310,306) +edge(434,307) +edge(373,308) +edge(409,309) +edge(317,310) +edge(319,310) +edge(311,311) +edge(318,311) +edge(320,311) +edge(317,312) +edge(318,313) +edge(319,314) +edge(316,316) +edge(317,317) +edge(319,319) +edge(321,320) +edge(401,321) +edge(323,322) +edge(340,323) +edge(324,324) +edge(325,325) +edge(327,327) +edge(328,328) +edge(396,329) +edge(331,330) +edge(332,332) +edge(333,333) +edge(334,334) +edge(434,335) +edge(337,337) +edge(338,338) +edge(339,339) +edge(340,340) +edge(423,341) +edge(343,343) +edge(344,344) +edge(348,345) +edge(352,345) +edge(346,346) +edge(349,346) +edge(353,346) +edge(347,347) +edge(349,349) +edge(351,351) +edge(395,352) +edge(403,353) +edge(420,354) +edge(356,356) +edge(357,357) +edge(359,357) +edge(399,357) +edge(360,358) +edge(359,359) +edge(361,361) +edge(363,363) +edge(365,365) +edge(366,366) +edge(367,367) +edge(426,367) +edge(369,368) +edge(371,368) +edge(372,369) +edge(370,370) +edge(372,372) +edge(392,373) +edge(408,374) +edge(376,376) +edge(395,377) +edge(379,379) +edge(381,379) +edge(380,380) +edge(381,381) +edge(430,381) +edge(384,382) +edge(384,384) +edge(386,386) +edge(388,388) +edge(389,389) +edge(439,390) +edge(392,392) +edge(393,393) +edge(394,394) +edge(396,396) +edge(465,396) +edge(398,398) +edge(400,400) +edge(401,401) +edge(403,402) +edge(404,404) +edge(406,405) +edge(407,407) +edge(427,408) +edge(410,410) +edge(411,411) +edge(412,412) +edge(413,413) +edge(415,414) +edge(417,414) +edge(416,416) +edge(418,418) +edge(420,420) +edge(421,421) +edge(435,421) +edge(423,423) +edge(425,425) +edge(427,427) +edge(429,429) +edge(430,430) +edge(431,431) +edge(433,433) +edge(437,434) +edge(436,435) +edge(437,437) +edge(439,439) +edge(441,441) +edge(442,442) +edge(444,444) +edge(446,445) +edge(449,445) +edge(450,446) +edge(447,447) +edge(448,448) +edge(449,449) +edge(452,449) +edge(450,450) +edge(452,452) +edge(454,454) +edge(455,455) +edge(457,456) +edge(457,457) +edge(460,457) +edge(462,457) +edge(458,458) +edge(460,460) +edge(462,462) +edge(464,464) +edge(466,466) +edge(480,466) +edge(481,467) +edge(472,468) +edge(469,469) +edge(478,469) +edge(471,470) +edge(472,471) +edge(473,473) +edge(475,473) +edge(477,473) +edge(475,475) +edge(477,477) +edge(479,479) +edge(481,480) +edge(481,481) +edge(483,481) +edge(482,482) +edge(483,483) +edge(485,483) +edge(485,485) +edge(487,487) +edge(490,488) +edge(494,488) +edge(490,489) +edge(490,490) +edge(491,491) +edge(493,493) ],[path(95,68) > 0.8],mma,-1,A),
	statistics(runtime, [Stop | _]),
	Runtime is Stop - Start,
	writeln('mma'),
	writeln(Runtime),
	writeln(A).

run_ccsaq:-
	statistics(runtime, [Start | _]), 
	prob_optimize(path(95,68),[edge(1,1) +edge(46,1) +edge(2,2) +edge(3,3) +edge(186,3) +edge(8,4) +edge(158,4) +edge(432,4) +edge(115,5) +edge(150,6) +edge(7,7) +edge(165,7) +edge(426,7) +edge(9,9) +edge(10,10) +edge(277,10) +edge(412,11) +edge(12,12) +edge(14,12) +edge(13,13) +edge(15,15) +edge(16,16) +edge(19,16) +edge(17,17) +edge(21,17) +edge(334,17) +edge(19,19) +edge(21,21) +edge(23,22) +edge(23,23) +edge(155,23) +edge(24,24) +edge(25,25) +edge(170,25) +edge(99,26) +edge(27,27) +edge(28,28) +edge(173,28) +edge(307,29) +edge(30,30) +edge(31,31) +edge(186,31) +edge(376,32) +edge(33,33) +edge(239,33) +edge(35,35) +edge(36,36) +edge(419,36) +edge(147,37) +edge(38,38) +edge(398,38) +edge(40,39) +edge(40,40) +edge(41,41) +edge(328,41) +edge(42,42) +edge(328,42) +edge(175,43) +edge(102,44) +edge(45,45) +edge(203,45) +edge(285,45) +edge(253,46) +edge(153,47) +edge(48,48) +edge(271,48) +edge(306,48) +edge(50,49) +edge(455,49) +edge(238,50) +edge(442,50) +edge(82,51) +edge(52,52) +edge(53,53) +edge(310,53) +edge(162,54) +edge(55,55) +edge(363,55) +edge(124,56) +edge(57,57) +edge(58,58) +edge(60,58) +edge(347,59) +edge(60,60) +edge(332,61) +edge(62,62) +edge(63,63) +edge(89,63) +edge(394,63) +edge(65,65) +edge(428,65) +edge(66,66) +edge(69,67) +edge(71,67) +edge(70,68) +edge(73,68) +edge(69,69) +edge(71,70) +edge(142,70) +edge(320,70) +edge(74,71) +edge(76,71) +edge(353,71) +edge(212,72) +edge(440,72) +edge(73,73) +edge(75,75) +edge(77,77) +edge(187,77) +edge(78,78) +edge(265,78) +edge(80,79) +edge(421,79) +edge(147,80) +edge(355,80) +edge(437,80) +edge(396,81) +edge(82,82) +edge(83,83) +edge(423,83) +edge(173,84) +edge(88,85) +edge(275,85) +edge(86,86) +edge(87,87) +edge(236,87) +edge(89,89) +edge(90,90) +edge(386,90) +edge(93,91) +edge(95,91) +edge(92,92) +edge(387,92) +edge(94,94) +edge(96,95) +edge(97,97) +edge(422,97) +edge(150,98) +edge(99,99) +edge(100,100) +edge(220,100) +edge(101,101) +edge(102,102) +edge(233,102) +edge(103,103) +edge(429,103) +edge(105,104) +edge(257,104) +edge(105,105) +edge(255,106) +edge(246,107) +edge(108,108) +edge(380,108) +edge(112,109) +edge(112,110) +edge(112,111) +edge(123,112) +edge(298,112) +edge(263,113) +edge(378,113) +edge(114,114) +edge(285,114) +edge(354,115) +edge(117,116) +edge(119,117) +edge(124,118) +edge(119,119) +edge(120,120) +edge(121,121) +edge(122,122) +edge(187,122) +edge(297,122) +edge(179,123) +edge(124,124) +edge(126,125) +edge(385,126) +edge(129,127) +edge(128,128) +edge(438,128) +edge(130,130) +edge(131,131) +edge(132,132) +edge(133,133) +edge(134,134) +edge(135,135) +edge(136,136) +edge(142,136) +edge(137,137) +edge(142,137) +edge(138,138) +edge(340,138) +edge(147,139) +edge(141,141) +edge(143,142) +edge(478,142) +edge(143,143) +edge(144,144) +edge(146,146) +edge(245,146) +edge(147,147) +edge(434,148) +edge(149,149) +edge(217,149) +edge(397,150) +edge(153,151) +edge(155,151) +edge(153,152) +edge(155,152) +edge(203,153) +edge(155,155) +edge(156,156) +edge(157,157) +edge(159,159) +edge(183,159) +edge(388,159) +edge(160,160) +edge(399,160) +edge(410,161) +edge(355,162) +edge(167,163) +edge(164,164) +edge(231,164) +edge(470,164) +edge(166,166) +edge(423,166) +edge(168,168) +edge(169,169) +edge(400,169) +edge(325,170) +edge(172,171) +edge(173,173) +edge(174,174) +edge(285,174) +edge(176,176) +edge(177,177) +edge(178,178) +edge(179,179) +edge(181,180) +edge(233,181) +edge(183,183) +edge(185,183) +edge(184,184) +edge(186,186) +edge(188,188) +edge(212,188) +edge(190,189) +edge(401,190) +edge(192,191) +edge(196,191) +edge(380,191) +edge(192,192) +edge(194,192) +edge(194,194) +edge(196,196) +edge(465,197) +edge(201,198) +edge(364,199) +edge(280,200) +edge(202,202) +edge(203,203) +edge(204,204) +edge(206,204) +edge(205,205) +edge(283,205) +edge(369,205) +edge(207,207) +edge(208,208) +edge(209,209) +edge(211,210) +edge(457,211) +edge(212,212) +edge(444,213) +edge(215,215) +edge(440,215) +edge(217,216) +edge(219,216) +edge(277,217) +edge(429,217) +edge(219,219) +edge(220,220) +edge(449,220) +edge(367,221) +edge(222,222) +edge(436,222) +edge(345,223) +edge(407,223) +edge(377,224) +edge(225,225) +edge(282,225) +edge(226,226) +edge(228,227) +edge(420,228) +edge(230,229) +edge(315,230) +edge(231,231) +edge(473,231) +edge(233,232) +edge(235,232) +edge(234,234) +edge(236,236) +edge(399,236) +edge(300,237) +edge(238,238) +edge(239,239) +edge(240,240) +edge(241,241) +edge(242,242) +edge(431,242) +edge(243,243) +edge(309,243) +edge(245,245) +edge(246,246) +edge(333,247) +edge(249,248) +edge(250,249) +edge(250,250) +edge(252,252) +edge(253,253) +edge(254,254) +edge(255,255) +edge(385,255) +edge(257,257) +edge(336,257) +edge(427,257) +edge(259,258) +edge(261,258) +edge(466,258) +edge(260,260) +edge(262,262) +edge(263,263) +edge(378,263) +edge(265,264) +edge(266,266) +edge(407,267) +edge(269,268) +edge(422,269) +edge(372,270) +edge(271,271) +edge(310,271) +edge(272,272) +edge(274,272) +edge(274,274) +edge(345,275) +edge(407,275) +edge(315,276) +edge(277,277) +edge(278,278) +edge(431,278) +edge(279,279) +edge(281,280) +edge(282,282) +edge(284,284) +edge(287,284) +edge(285,285) +edge(290,285) +edge(289,286) +edge(287,287) +edge(289,289) +edge(290,290) +edge(291,291) +edge(293,293) +edge(294,294) +edge(295,295) +edge(296,296) +edge(298,296) +edge(297,297) +edge(298,298) +edge(300,300) +edge(301,301) +edge(304,301) +edge(310,301) +edge(429,301) +edge(303,302) +edge(303,303) +edge(305,304) +edge(305,305) +edge(310,306) +edge(434,307) +edge(373,308) +edge(409,309) +edge(317,310) +edge(319,310) +edge(311,311) +edge(318,311) +edge(320,311) +edge(317,312) +edge(318,313) +edge(319,314) +edge(316,316) +edge(317,317) +edge(319,319) +edge(321,320) +edge(401,321) +edge(323,322) +edge(340,323) +edge(324,324) +edge(325,325) +edge(327,327) +edge(328,328) +edge(396,329) +edge(331,330) +edge(332,332) +edge(333,333) +edge(334,334) +edge(434,335) +edge(337,337) +edge(338,338) +edge(339,339) +edge(340,340) +edge(423,341) +edge(343,343) +edge(344,344) +edge(348,345) +edge(352,345) +edge(346,346) +edge(349,346) +edge(353,346) +edge(347,347) +edge(349,349) +edge(351,351) +edge(395,352) +edge(403,353) +edge(420,354) +edge(356,356) +edge(357,357) +edge(359,357) +edge(399,357) +edge(360,358) +edge(359,359) +edge(361,361) +edge(363,363) +edge(365,365) +edge(366,366) +edge(367,367) +edge(426,367) +edge(369,368) +edge(371,368) +edge(372,369) +edge(370,370) +edge(372,372) +edge(392,373) +edge(408,374) +edge(376,376) +edge(395,377) +edge(379,379) +edge(381,379) +edge(380,380) +edge(381,381) +edge(430,381) +edge(384,382) +edge(384,384) +edge(386,386) +edge(388,388) +edge(389,389) +edge(439,390) +edge(392,392) +edge(393,393) +edge(394,394) +edge(396,396) +edge(465,396) +edge(398,398) +edge(400,400) +edge(401,401) +edge(403,402) +edge(404,404) +edge(406,405) +edge(407,407) +edge(427,408) +edge(410,410) +edge(411,411) +edge(412,412) +edge(413,413) +edge(415,414) +edge(417,414) +edge(416,416) +edge(418,418) +edge(420,420) +edge(421,421) +edge(435,421) +edge(423,423) +edge(425,425) +edge(427,427) +edge(429,429) +edge(430,430) +edge(431,431) +edge(433,433) +edge(437,434) +edge(436,435) +edge(437,437) +edge(439,439) +edge(441,441) +edge(442,442) +edge(444,444) +edge(446,445) +edge(449,445) +edge(450,446) +edge(447,447) +edge(448,448) +edge(449,449) +edge(452,449) +edge(450,450) +edge(452,452) +edge(454,454) +edge(455,455) +edge(457,456) +edge(457,457) +edge(460,457) +edge(462,457) +edge(458,458) +edge(460,460) +edge(462,462) +edge(464,464) +edge(466,466) +edge(480,466) +edge(481,467) +edge(472,468) +edge(469,469) +edge(478,469) +edge(471,470) +edge(472,471) +edge(473,473) +edge(475,473) +edge(477,473) +edge(475,475) +edge(477,477) +edge(479,479) +edge(481,480) +edge(481,481) +edge(483,481) +edge(482,482) +edge(483,483) +edge(485,483) +edge(485,485) +edge(487,487) +edge(490,488) +edge(494,488) +edge(490,489) +edge(490,490) +edge(491,491) +edge(493,493) ],[path(95,68) > 0.8],ccsaq,-1,A),
	statistics(runtime, [Stop | _]),
	Runtime is Stop - Start,
	writeln('ccsaq'),
	writeln(Runtime),
	writeln(A).

run_slsqp:-
	statistics(runtime, [Start | _]), 
	prob_optimize(path(95,68),[edge(1,1) +edge(46,1) +edge(2,2) +edge(3,3) +edge(186,3) +edge(8,4) +edge(158,4) +edge(432,4) +edge(115,5) +edge(150,6) +edge(7,7) +edge(165,7) +edge(426,7) +edge(9,9) +edge(10,10) +edge(277,10) +edge(412,11) +edge(12,12) +edge(14,12) +edge(13,13) +edge(15,15) +edge(16,16) +edge(19,16) +edge(17,17) +edge(21,17) +edge(334,17) +edge(19,19) +edge(21,21) +edge(23,22) +edge(23,23) +edge(155,23) +edge(24,24) +edge(25,25) +edge(170,25) +edge(99,26) +edge(27,27) +edge(28,28) +edge(173,28) +edge(307,29) +edge(30,30) +edge(31,31) +edge(186,31) +edge(376,32) +edge(33,33) +edge(239,33) +edge(35,35) +edge(36,36) +edge(419,36) +edge(147,37) +edge(38,38) +edge(398,38) +edge(40,39) +edge(40,40) +edge(41,41) +edge(328,41) +edge(42,42) +edge(328,42) +edge(175,43) +edge(102,44) +edge(45,45) +edge(203,45) +edge(285,45) +edge(253,46) +edge(153,47) +edge(48,48) +edge(271,48) +edge(306,48) +edge(50,49) +edge(455,49) +edge(238,50) +edge(442,50) +edge(82,51) +edge(52,52) +edge(53,53) +edge(310,53) +edge(162,54) +edge(55,55) +edge(363,55) +edge(124,56) +edge(57,57) +edge(58,58) +edge(60,58) +edge(347,59) +edge(60,60) +edge(332,61) +edge(62,62) +edge(63,63) +edge(89,63) +edge(394,63) +edge(65,65) +edge(428,65) +edge(66,66) +edge(69,67) +edge(71,67) +edge(70,68) +edge(73,68) +edge(69,69) +edge(71,70) +edge(142,70) +edge(320,70) +edge(74,71) +edge(76,71) +edge(353,71) +edge(212,72) +edge(440,72) +edge(73,73) +edge(75,75) +edge(77,77) +edge(187,77) +edge(78,78) +edge(265,78) +edge(80,79) +edge(421,79) +edge(147,80) +edge(355,80) +edge(437,80) +edge(396,81) +edge(82,82) +edge(83,83) +edge(423,83) +edge(173,84) +edge(88,85) +edge(275,85) +edge(86,86) +edge(87,87) +edge(236,87) +edge(89,89) +edge(90,90) +edge(386,90) +edge(93,91) +edge(95,91) +edge(92,92) +edge(387,92) +edge(94,94) +edge(96,95) +edge(97,97) +edge(422,97) +edge(150,98) +edge(99,99) +edge(100,100) +edge(220,100) +edge(101,101) +edge(102,102) +edge(233,102) +edge(103,103) +edge(429,103) +edge(105,104) +edge(257,104) +edge(105,105) +edge(255,106) +edge(246,107) +edge(108,108) +edge(380,108) +edge(112,109) +edge(112,110) +edge(112,111) +edge(123,112) +edge(298,112) +edge(263,113) +edge(378,113) +edge(114,114) +edge(285,114) +edge(354,115) +edge(117,116) +edge(119,117) +edge(124,118) +edge(119,119) +edge(120,120) +edge(121,121) +edge(122,122) +edge(187,122) +edge(297,122) +edge(179,123) +edge(124,124) +edge(126,125) +edge(385,126) +edge(129,127) +edge(128,128) +edge(438,128) +edge(130,130) +edge(131,131) +edge(132,132) +edge(133,133) +edge(134,134) +edge(135,135) +edge(136,136) +edge(142,136) +edge(137,137) +edge(142,137) +edge(138,138) +edge(340,138) +edge(147,139) +edge(141,141) +edge(143,142) +edge(478,142) +edge(143,143) +edge(144,144) +edge(146,146) +edge(245,146) +edge(147,147) +edge(434,148) +edge(149,149) +edge(217,149) +edge(397,150) +edge(153,151) +edge(155,151) +edge(153,152) +edge(155,152) +edge(203,153) +edge(155,155) +edge(156,156) +edge(157,157) +edge(159,159) +edge(183,159) +edge(388,159) +edge(160,160) +edge(399,160) +edge(410,161) +edge(355,162) +edge(167,163) +edge(164,164) +edge(231,164) +edge(470,164) +edge(166,166) +edge(423,166) +edge(168,168) +edge(169,169) +edge(400,169) +edge(325,170) +edge(172,171) +edge(173,173) +edge(174,174) +edge(285,174) +edge(176,176) +edge(177,177) +edge(178,178) +edge(179,179) +edge(181,180) +edge(233,181) +edge(183,183) +edge(185,183) +edge(184,184) +edge(186,186) +edge(188,188) +edge(212,188) +edge(190,189) +edge(401,190) +edge(192,191) +edge(196,191) +edge(380,191) +edge(192,192) +edge(194,192) +edge(194,194) +edge(196,196) +edge(465,197) +edge(201,198) +edge(364,199) +edge(280,200) +edge(202,202) +edge(203,203) +edge(204,204) +edge(206,204) +edge(205,205) +edge(283,205) +edge(369,205) +edge(207,207) +edge(208,208) +edge(209,209) +edge(211,210) +edge(457,211) +edge(212,212) +edge(444,213) +edge(215,215) +edge(440,215) +edge(217,216) +edge(219,216) +edge(277,217) +edge(429,217) +edge(219,219) +edge(220,220) +edge(449,220) +edge(367,221) +edge(222,222) +edge(436,222) +edge(345,223) +edge(407,223) +edge(377,224) +edge(225,225) +edge(282,225) +edge(226,226) +edge(228,227) +edge(420,228) +edge(230,229) +edge(315,230) +edge(231,231) +edge(473,231) +edge(233,232) +edge(235,232) +edge(234,234) +edge(236,236) +edge(399,236) +edge(300,237) +edge(238,238) +edge(239,239) +edge(240,240) +edge(241,241) +edge(242,242) +edge(431,242) +edge(243,243) +edge(309,243) +edge(245,245) +edge(246,246) +edge(333,247) +edge(249,248) +edge(250,249) +edge(250,250) +edge(252,252) +edge(253,253) +edge(254,254) +edge(255,255) +edge(385,255) +edge(257,257) +edge(336,257) +edge(427,257) +edge(259,258) +edge(261,258) +edge(466,258) +edge(260,260) +edge(262,262) +edge(263,263) +edge(378,263) +edge(265,264) +edge(266,266) +edge(407,267) +edge(269,268) +edge(422,269) +edge(372,270) +edge(271,271) +edge(310,271) +edge(272,272) +edge(274,272) +edge(274,274) +edge(345,275) +edge(407,275) +edge(315,276) +edge(277,277) +edge(278,278) +edge(431,278) +edge(279,279) +edge(281,280) +edge(282,282) +edge(284,284) +edge(287,284) +edge(285,285) +edge(290,285) +edge(289,286) +edge(287,287) +edge(289,289) +edge(290,290) +edge(291,291) +edge(293,293) +edge(294,294) +edge(295,295) +edge(296,296) +edge(298,296) +edge(297,297) +edge(298,298) +edge(300,300) +edge(301,301) +edge(304,301) +edge(310,301) +edge(429,301) +edge(303,302) +edge(303,303) +edge(305,304) +edge(305,305) +edge(310,306) +edge(434,307) +edge(373,308) +edge(409,309) +edge(317,310) +edge(319,310) +edge(311,311) +edge(318,311) +edge(320,311) +edge(317,312) +edge(318,313) +edge(319,314) +edge(316,316) +edge(317,317) +edge(319,319) +edge(321,320) +edge(401,321) +edge(323,322) +edge(340,323) +edge(324,324) +edge(325,325) +edge(327,327) +edge(328,328) +edge(396,329) +edge(331,330) +edge(332,332) +edge(333,333) +edge(334,334) +edge(434,335) +edge(337,337) +edge(338,338) +edge(339,339) +edge(340,340) +edge(423,341) +edge(343,343) +edge(344,344) +edge(348,345) +edge(352,345) +edge(346,346) +edge(349,346) +edge(353,346) +edge(347,347) +edge(349,349) +edge(351,351) +edge(395,352) +edge(403,353) +edge(420,354) +edge(356,356) +edge(357,357) +edge(359,357) +edge(399,357) +edge(360,358) +edge(359,359) +edge(361,361) +edge(363,363) +edge(365,365) +edge(366,366) +edge(367,367) +edge(426,367) +edge(369,368) +edge(371,368) +edge(372,369) +edge(370,370) +edge(372,372) +edge(392,373) +edge(408,374) +edge(376,376) +edge(395,377) +edge(379,379) +edge(381,379) +edge(380,380) +edge(381,381) +edge(430,381) +edge(384,382) +edge(384,384) +edge(386,386) +edge(388,388) +edge(389,389) +edge(439,390) +edge(392,392) +edge(393,393) +edge(394,394) +edge(396,396) +edge(465,396) +edge(398,398) +edge(400,400) +edge(401,401) +edge(403,402) +edge(404,404) +edge(406,405) +edge(407,407) +edge(427,408) +edge(410,410) +edge(411,411) +edge(412,412) +edge(413,413) +edge(415,414) +edge(417,414) +edge(416,416) +edge(418,418) +edge(420,420) +edge(421,421) +edge(435,421) +edge(423,423) +edge(425,425) +edge(427,427) +edge(429,429) +edge(430,430) +edge(431,431) +edge(433,433) +edge(437,434) +edge(436,435) +edge(437,437) +edge(439,439) +edge(441,441) +edge(442,442) +edge(444,444) +edge(446,445) +edge(449,445) +edge(450,446) +edge(447,447) +edge(448,448) +edge(449,449) +edge(452,449) +edge(450,450) +edge(452,452) +edge(454,454) +edge(455,455) +edge(457,456) +edge(457,457) +edge(460,457) +edge(462,457) +edge(458,458) +edge(460,460) +edge(462,462) +edge(464,464) +edge(466,466) +edge(480,466) +edge(481,467) +edge(472,468) +edge(469,469) +edge(478,469) +edge(471,470) +edge(472,471) +edge(473,473) +edge(475,473) +edge(477,473) +edge(475,475) +edge(477,477) +edge(479,479) +edge(481,480) +edge(481,481) +edge(483,481) +edge(482,482) +edge(483,483) +edge(485,483) +edge(485,485) +edge(487,487) +edge(490,488) +edge(494,488) +edge(490,489) +edge(490,490) +edge(491,491) +edge(493,493) ],[path(95,68) > 0.8],slsqp,-1,A),
	statistics(runtime, [Stop | _]),
	Runtime is Stop - Start,
	writeln('slsqp'),
	writeln(Runtime),
	writeln(A).
