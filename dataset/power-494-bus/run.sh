# NOTE: for this you need to create a new file foreach experiment
sbatch --job-name=p494_297_153_m --partition=longrun --mem=16G --output=power-494-bus_297_153_mma.log --wrap="srun swipl -s power-494-bus_297_153.pl -g "run_mma,halt." "
sbatch --job-name=p494_297_153_c --partition=longrun --mem=16G --output=power-494-bus_297_153_ccsaq.log --wrap="srun swipl -s power-494-bus_297_153.pl -g "run_ccsaq,halt." "
sbatch --job-name=p494_297_153_s --partition=longrun --mem=16G --output=power-494-bus_297_153_slsqp.log --wrap="srun swipl -s power-494-bus_297_153.pl -g "run_slsqp,halt." "

sbatch --job-name=p494_348_275_m --partition=longrun --mem=16G --output=power-494-bus_348_275_mma.log --wrap="srun swipl -s power-494-bus_348_275.pl -g "run_mma,halt." "
sbatch --job-name=p494_348_275_c --partition=longrun --mem=16G --output=power-494-bus_348_275_ccsaq.log --wrap="srun swipl -s power-494-bus_348_275.pl -g "run_ccsaq,halt." "
sbatch --job-name=p494_348_275_s --partition=longrun --mem=16G --output=power-494-bus_348_275_slsqp.log --wrap="srun swipl -s power-494-bus_348_275.pl -g "run_slsqp,halt." "

sbatch --job-name=p494_350_85_m --partition=longrun --mem=16G --output=power-494-bus_350_85_mma.log --wrap="srun swipl -s power-494-bus_350_85.pl -g "run_mma,halt." "
sbatch --job-name=p494_350_85_c --partition=longrun --mem=16G --output=power-494-bus_350_85_ccsaq.log --wrap="srun swipl -s power-494-bus_350_85.pl -g "run_ccsaq,halt." "
sbatch --job-name=p494_350_85_s --partition=longrun --mem=16G --output=power-494-bus_350_85_slsqp.log --wrap="srun swipl -s power-494-bus_350_85.pl -g "run_slsqp,halt." "

sbatch --job-name=p494_399_48_m --partition=longrun --mem=16G --output=power-494-bus_399_48_mma.log --wrap="srun swipl -s power-494-bus_399_48.pl -g "run_mma,halt." "
sbatch --job-name=p494_399_48_c --partition=longrun --mem=16G --output=power-494-bus_399_48_ccsaq.log --wrap="srun swipl -s power-494-bus_399_48.pl -g "run_ccsaq,halt." "
sbatch --job-name=p494_399_48_s --partition=longrun --mem=16G --output=power-494-bus_399_48_slsqp.log --wrap="srun swipl -s power-494-bus_399_48.pl -g "run_slsqp,halt." "

sbatch --job-name=p494_473_136_m --partition=longrun --mem=16G --output=power-494-bus_473_136_mma.log --wrap="srun swipl -s power-494-bus_473_136.pl -g "run_mma,halt." "
sbatch --job-name=p494_473_136_c --partition=longrun --mem=16G --output=power-494-bus_473_136_ccsaq.log --wrap="srun swipl -s power-494-bus_473_136.pl -g "run_ccsaq,halt." "
sbatch --job-name=p494_473_136_s --partition=longrun --mem=16G --output=power-494-bus_473_136_slsqp.log --wrap="srun swipl -s power-494-bus_473_136.pl -g "run_slsqp,halt." "

sbatch --job-name=p494_477_137_m --partition=longrun --mem=16G --output=power-494-bus_477_137_mma.log --wrap="srun swipl -s power-494-bus_477_137.pl -g "run_mma,halt." "
sbatch --job-name=p494_477_137_c --partition=longrun --mem=16G --output=power-494-bus_477_137_ccsaq.log --wrap="srun swipl -s power-494-bus_477_137.pl -g "run_ccsaq,halt." "
sbatch --job-name=p494_477_137_s --partition=longrun --mem=16G --output=power-494-bus_477_137_slsqp.log --wrap="srun swipl -s power-494-bus_477_137.pl -g "run_slsqp,halt." "

sbatch --job-name=p494_95_68_m --mem=16G --output=p494_95_68_mma.log --wrap="srun swipl -s power-494-bus_95_68.pl -g "run_mma,halt." "
sbatch --job-name=p494_95_68_c --mem=16G --output=p494_95_68_ccsaq.log --wrap="srun swipl -s power-494-bus_95_68.pl -g "run_ccsaq,halt." "
sbatch --job-name=p494_95_68_s --mem=16G --output=p494_95_68_slsqp.log --wrap="srun swipl -s power-494-bus_95_68.pl -g "run_slsqp,halt." "

sbatch --job-name=p494_261_136_m --partition=longrun --mem=16G --output=power-494-bus_261_136_mma.log --wrap="srun swipl -s power-494-bus_261_136.pl -g "run_mma,halt." "
sbatch --job-name=p494_261_136_c --partition=longrun --mem=16G --output=power-494-bus_261_136_ccsaq.log --wrap="srun swipl -s power-494-bus_261_136.pl -g "run_ccsaq,halt." "
sbatch --job-name=p494_261_136_s --partition=longrun --mem=16G --output=power-494-bus_261_136_slsqp.log --wrap="srun swipl -s power-494-bus_261_136.pl -g "run_slsqp,halt." "

sbatch --job-name=p494_395_224_m --partition=longrun --mem=16G --output=power-494-bus_395_224_mma.log --wrap="srun swipl -s power-494-bus_395_224.pl -g "run_mma,halt." "
sbatch --job-name=p494_395_224_c --partition=longrun --mem=16G --output=power-494-bus_395_224_ccsaq.log --wrap="srun swipl -s power-494-bus_395_224.pl -g "run_ccsaq,halt." "
sbatch --job-name=p494_395_224_s --partition=longrun --mem=16G --output=power-494-bus_395_224_slsqp.log --wrap="srun swipl -s power-494-bus_395_224.pl -g "run_slsqp,halt." "

sbatch --job-name=p494_404_205_m --mem=16G --output=power-494-bus_404_205_mma.log --wrap="srun swipl -s power-494-bus_404_205.pl -g "run_mma,halt." "
sbatch --job-name=p494_404_205_c --mem=16G --output=power-494-bus_404_205_ccsaq.log --wrap="srun swipl -s power-494-bus_404_205.pl -g "run_ccsaq,halt." "
sbatch --job-name=p494_404_205_s --mem=16G --output=power-494-bus_404_205_slsqp.log --wrap="srun swipl -s power-494-bus_404_205.pl -g "run_slsqp,halt." "

