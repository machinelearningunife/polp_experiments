:- use_module(library(pita)).

:- pita.

:- begin_lpad.

edge(a,b):0.9.
optimizable edge(b,c):[0.3,0.8].
optimizable edge(b,d):[0.3,0.8].
edge(c,e):0.3.
edge(d,e):0.8.

path(X,X).
path(X,Y):-path(X,Z), edge(Z,Y).

:- end_lpad.

test_constraint:-
    statistics(runtime,[Start|_]),
    prob_optimize(path(a,e),[edge(b,c)+edge(b,d)],[path(a,e) > 0.6,edge(b,c) - edge(b,d) < 0.1, edge(b,d) - edge(b,c) < 0.1],LAss),
    statistics(runtime,[Stop|_]),
    Runtime is Stop - Start,
    write('Total: '), writeln(Runtime),
    writeln(LAss).
